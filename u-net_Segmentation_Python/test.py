import cv2
import cv
import numpy as np
from scipy.interpolate import griddata
import scipy.ndimage as ndimage
import libtiff
import myTools
import csv
import time
import caffe
import matplotlib.pyplot as plt

csvFilename = '/mnt/medical/microscopy/drosophila/egg_segmentation/ovary_image_info_for_prague.csv';
cropImDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_2d/stage2/';
cropTruthDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_2d_binary/stage2/';
fullImDir = '/mnt/medical/microscopy/drosophila/egg_segmentation/slice/stage2/';

imSize = 572
maskSize = 388

context = (imSize - maskSize)/2

class BBOX:
	def __init__(self, x1, x2, y1, y2):
		self.x_1, self.x_2, self.y_1, self.y_2 = x1, x2, y1, y2

caffe.set_device(1)
caffe.set_mode_gpu()

# ?? Load the final model ??
net = caffe.Net('test-v5.prototxt', 'train/u-net_train_v5_iter_5000.caffemodel', caffe.TEST)

for layer_name, param in net.params.iteritems():
	assert np.isnan(param[0].data).any() == False
	assert np.isnan(param[1].data).any() == False

with open(csvFilename) as csvfile:
	r = csv.DictReader(csvfile, delimiter='\t')

	for row in r:
		if row['stage'] != "2":
			continue

		cB = BBOX(int(row['bb_begin_x']), int(row['bb_end_x']), int(row['bb_begin_y']), int(row['bb_end_y']))
		tif = libtiff.TIFF.open(fullImDir+'slice_'+row['image_id']+'.tif', mode='r')
		im = tif.read_image()
		im /= np.amax(im)

		assert np.isnan(im).any() == False

		mask = cv2.imread(cropTruthDir+"mask"+row['image_id']+".png", 0)
		wtmap = myTools.constructWeightMap(mask)

		R = int(np.ceil(float(im.shape[0])/maskSize));
		C = int(np.ceil(float(im.shape[1])/maskSize));

		print mask.shape, R, C

		mask_tiled = np.lib.pad(mask, ((cB.y_1, R * maskSize - mask.shape[0] - cB.y_1), (cB.x_1, C * maskSize - mask.shape[1] - cB.x_1)), mode = 'constant', constant_values = 2)
		wtmap_tiled = np.lib.pad(wtmap, ((cB.y_1, R * maskSize - mask.shape[0] - cB.y_1), (cB.x_1, C * maskSize - mask.shape[1] - cB.x_1)), mode = 'constant', constant_values = 0)

		softmax = np.zeros((2, im.shape[0], im.shape[1]))

		for r in range(R):
			for c in range(C):
				x1, x2 = c * maskSize, (c + 1) * maskSize
				y1, y2 = r * maskSize, (r + 1) * maskSize
				cBsmall = BBOX(x1,x2,y1,y2)

				im_cut = myTools.cutWithContext(im, cBsmall, context)
				mask_cut = mask_tiled[y1:y2, x1:x2]
				wtmap_cut = wtmap_tiled[y1:y2, x1:x2]

				net.blobs['data'].data[0, 0, :, :] = im_cut.astype(np.float32)
				
				net.forward()

				# softmax = np.zeros((2,388,388))
				# softmax = np.zeros((2,196,196))
				softmax[:, y1:min(y2, im.shape[0]), x1:min(x2, im.shape[1])] \
					= net.blobs['softmax'].data[0,:,:min(maskSize, im.shape[0] - y1), :min(maskSize, im.shape[1] - x1)]
				# plt.figure(); plt.imshow(myTools.cutWithContext(im, cBsmall, 0)); plt.colorbar()
				# plt.figure(); plt.imshow(mask_cut); plt.colorbar()
				# plt.figure(); plt.imshow(net.blobs['softmax'].data[0,1,:min(maskSize, im.shape[0] - y1), :min(maskSize, im.shape[1] - x1)]); plt.colorbar()
				# plt.show()

		prediction = np.argmax(softmax,axis=0)

		print softmax.shape, prediction.shape, mask_tiled.shape, wtmap_tiled.shape

		mask_tiled = mask_tiled[:im.shape[0], :im.shape[1]]
		wtmap_tiled = wtmap_tiled[:im.shape[0], :im.shape[1]]

		print softmax.shape, prediction.shape, mask_tiled.shape, wtmap_tiled.shape

		# cv2.imwrite('augmentation_results/augmented'+row['image_id']+'.png', np.dstack( ( (im*(255/np.amax(im))), mask*127, mask*127)))
		# plt.matshow(wtmap); plt.colorbar()
		
		s0, s1 = softmax[0,:,:], softmax[1,:,:]
		p = np.ones(im.shape)
		p[mask_tiled == 1] = s1[mask_tiled == 1]
		p[mask_tiled == 0] = s0[mask_tiled == 0]
		count = np.sum(mask != 2)
		err = np.nansum(wtmap_tiled * - np.log(np.maximum(p, np.finfo(np.float32).eps)))
		print err, count, err/count
		# plt.matshow(mask, cmap='hot'); plt.colorbar()
		# plt.matshow(im[94:482, 94:482], cmap='hot'); plt.colorbar()
		# plt.matshow(prediction, cmap='hot'); plt.colorbar()
		# plt.matshow(s1, cmap='hot'); plt.colorbar()
		# plt.matshow(p, cmap='hot'); plt.colorbar()

		XBG = np.expand_dims(np.float32(np.logical_and(mask_tiled == 2, prediction == 0)), 2)		* (0.75, 0, 0.75)	#light purple
		XFG = np.expand_dims(np.float32(np.logical_and(mask_tiled == 2, prediction == 1)), 2)		* (0, 0.75, 0)		#light green
		BG = np.expand_dims(np.float32(np.logical_and(mask_tiled == 0, prediction == 0)), 2)			* (1, 0, 1)			#background = purple
		FG = np.expand_dims(np.float32(np.logical_and(mask_tiled == 1, prediction == 1)), 2)			* (0, 1, 0)			#foreground = green
		FalseBG = np.expand_dims(np.float32(np.logical_and(mask_tiled == 1, prediction == 0)), 2)	* (0.75, 0.75, 0)	#false bg = yellow
		FalseFG = np.expand_dims(np.float32(np.logical_and(mask_tiled == 0, prediction == 1)), 2)	* (1, 0, 0)			#false fg = red

		colorMask = XBG + XFG + BG + FG + FalseBG + FalseFG

		# plt.imshow(colorMask * np.expand_dims(0.2+im[94:482, 94:482] * 0.8, 2) )
		# plt.title('Purple=BG, Green=FG, Yellow=FalseBG, Red=FalseFG, Lighter color=No Ground Truth')

		trueBoundary = np.expand_dims(ndimage.morphology.morphological_gradient(mask_tiled, size=(3,3), mode='reflect'), 2) * (1,1,1)
		predictedBoundary = np.expand_dims(ndimage.morphology.morphological_gradient(prediction, size=(3,3), mode='reflect'), 2) * (1,1,1)

		# plt.figure(); plt.imshow(np.expand_dims(im[94:482, 94:482], 2) * (1-trueBoundary) * (1-predictedBoundary) + trueBoundary * (0,1,0) + predictedBoundary * (1,0,0))
		# plt.title('Green=true boundary, Red=Computed, Box=known ground truth')

		# print colorMask.shape, im.shape

		font = cv2.FONT_HERSHEY_SIMPLEX
		img = colorMask * np.expand_dims(0.2+im * 0.8, 2)
		cv2.putText(img, "loss = "+str(err/count),(5,15), font, 0.5,(np.amax(img),np.amax(img),np.amax(img)))
		plt.imsave("results/out-"+row['image_id']+"-1.jpg", img)

		img = np.expand_dims(im, 2) * (1-trueBoundary) * (1-predictedBoundary) + trueBoundary * (0,1,0) + predictedBoundary * (1,0,0)
		cv2.putText(img, "loss = "+str(err/count),(5,15), font, 0.5,(np.amax(img),np.amax(img),np.amax(img)))
		plt.imsave("results/out-"+row['image_id']+"-2.jpg", img)

		plt.imsave("results/out-"+row['image_id']+"-3.jpg", s1, cmap = 'hot')

		img = wtmap_tiled * - np.log(np.maximum(p, np.finfo(np.float32).eps))
		cv2.putText(img, "max = "+str(np.amax(img)),(5,15), font, 0.5,(np.amax(img)))
		cv2.putText(img, "avg = "+str(err/count),(5,30), font, 0.5,(err/count))
		plt.imsave("results/out-"+row['image_id']+"-4.jpg", img, cmap = 'hot')
		# plt.matshow(p); plt.colorbar()
		# plt.matshow(wtmap, cmap='hot'); plt.colorbar()
		# plt.show()
		# cv2.waitKey(0)
