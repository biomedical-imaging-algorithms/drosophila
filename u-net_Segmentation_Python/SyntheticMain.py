import cv2
import cv
import numpy as np
from scipy.interpolate import griddata
import libtiff
import myTools
import csv
import time
import caffe
import matplotlib.pyplot as plt

caffe.set_device(0)
caffe.set_mode_gpu()

# net = caffe.Net('phseg_v5-train.prototxt', caffe.TRAIN)
solver = caffe.SGDSolver('syntheticSolver.prototxt')

net = solver.net

while True:
	solver.step(60)
	l1a = np.squeeze(net.params['conv_d0a-b'][0].data)

	for layer_name, param in net.params.iteritems():
		assert np.isnan(param[0].data).any() == False
		assert np.isnan(param[1].data).any() == False

	f = libtiff.TIFF.open("Synthetic_l1a.tif", mode='w'); f.write_image(l1a); del f
	