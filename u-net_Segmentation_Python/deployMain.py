#!/usr/bin/env python

'''
@author Siddhant Ranade

Script to perform segmentation, compute the accuracy from given ground truth, save results, do a comparison, etc.

Calls the tileAndSegment function from deploy.py
'''

# import cv2
# import cv
import numpy as np
# from scipy.interpolate import griddata
# import scipy.ndimage as ndimage
# import libtiff
# import myTools
# import csv
# import time
import caffe
# import matplotlib.pyplot as plt
import argparse
# import os
import deploy

parser = argparse.ArgumentParser(description='Tile and Segment the given (2D) slice.')
parser.add_argument('-m', '--model', metavar='path/to/model.caffemodel', type=str, action='store', required=True,
	help='Path to the .caffemodel file to load')
parser.add_argument('-i', '--input', metavar='path/to/input', type=str, action='store', required=True,
	help='Path to the image file to load')
parser.add_argument('-n', '--net', metavar='path/to/net.prototxt', type=str, action='store', default='test-v5.prototxt', required=True,
	help='Path to the net .prototxt to load')
parser.add_argument('-o', '--output', metavar='path/to/output', type=str, action='store', required=True,
	help='Path to the output location to save the segmentation map')
parser.add_argument('-g', '--gpu_id', metavar='N', type=int, action='store', default=3,   # 1
	help='ID of the GPU to use (0,1,2,...)')
parser.add_argument('-v', '--verbose', action='store_true',
	help='Saves the activations along with the prediction, prints additional info.')
parser.add_argument('-t', '--truth', metavar='path/to/groundTruth', type=str, action='store',
	help='Path to the ground truth (for comparison)')
parser.add_argument('-b', '--bbox', metavar=('x1', 'y1'), type=int, nargs=2, default=(0, 0),
	help='Coordinates of the top left corner of the bounding box for which the ground truth exists. (default: 0 0)')
# parser.add_argument
args = parser.parse_args()

imSize = 572
maskSize = 388

caffe.set_device(args.gpu_id)
caffe.set_mode_gpu()

net = caffe.Net(args.net, args.model, caffe.TEST)

for layer_name, param in net.params.iteritems():
	assert np.isnan(param[0].data).any() == False
	assert np.isnan(param[1].data).any() == False

deploy.tileAndSegment(net, args, imSize, maskSize)