#!/usr/bin/env python

'''
@author Siddhant Ranade

Perform segmentation on the 3D stacks
'''

import numpy as np
import myTools
import time
import caffe
import argparse
import deploy
import libtiff
import glob
import os
import matplotlib.pyplot as plt
import csv

imSize = 572
maskSize = 388

csvFilename = '/mnt/datagrid/Medical/microscopy/drosophila/all_ovary_image_info_for_prague.txt'

inputStackDir = '/mnt/datagrid/Medical/microscopy/drosophila/ovary_raw_stacks/'
outputSliceDir = '/mnt/datagrid/Medical/microscopy/drosophila/ovary_selected_slices/'
outputSegmentationDir = '/mnt/datagrid/Medical/microscopy/drosophila/ovary_selected_slices_segmentation/'

parser = argparse.ArgumentParser(description='Perform 2D segmentation on all (15000?) 2D slices using given model.')
parser.add_argument('-m', '--model', metavar='path/to/model.caffemodel', type=str, action='store', required=True,
	help='Path to the .caffemodel file to load')
parser.add_argument('-n', '--net', metavar='path/to/net.prototxt', type=str, action='store', default='test-v5.prototxt',
	help='Path to the net .prototxt to load')
parser.add_argument('-g', '--gpu_id', metavar='N', type=int, action='store', default=1,
	help='ID of the GPU to use (0,1,2,...)')
args = parser.parse_args()

caffe.set_device(args.gpu_id)
caffe.set_mode_gpu()
net = caffe.Net(args.net, args.model, caffe.TEST)

args.verbose = False

counter = 0

with open(csvFilename) as csvfile:
	r = csv.DictReader(csvfile, delimiter='\t')
	for row in r:
		if os.path.splitext(row['stack_path'])[1] != '.tif':
			continue
		inputStackFilename = inputStackDir + row['stack_path']#'insitu' + row['image_id'] + '_mag.tif'
		# print inputStackFilename
		img3D = myTools.load_image_tiff_3d(inputStackFilename)
		img = img3D[int(row['slice_index'])-1, :, :]
		args.inputImg = img.astype(np.float)/np.amax(img)
		output = deploy.tileAndSegment(net, args, imSize, maskSize)
		
		outputSliceFilename = outputSliceDir+'insitu'+row['image_id']+'.tif'
		outputSegmentationFilename = outputSegmentationDir+'insitu'+row['image_id']+'-label.tif'
		
		tif = libtiff.TIFF.open(outputSliceFilename, mode='w')
		tif.write_image(img)
		tif.close()
		tif = libtiff.TIFF.open(outputSegmentationFilename, mode='w')
		tif.write_image(output.astype(np.uint8))
		tif.close()
		print row['image_id']
		counter = counter+1

print "Segmented ", counter, " images"