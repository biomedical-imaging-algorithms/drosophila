import caffe
import myTools
import numpy as np
import csv
import cv2
import libtiff
import matplotlib.pyplot as plt

batchSize = 1
imSize = 572
maskSize = 388

class inputLayer(caffe.Layer):
	def setup(self, bottom, top):
		if len(top) != 3:
			raise Exception('must have exactly three outputs')
		# if len(top) != 2:
		# 	raise Exception('must have exactly two outputs')
		self.X, self.Y = np.meshgrid(range(1,imSize+1), range(1,imSize+1));
		self.A = np.square(np.sin((self.X+self.Y)*np.pi/30));
		self.B = np.square(np.sin((self.Y)*np.pi/30));

	def reshape(self, bottom, top):
		top[0].reshape(batchSize, 1, imSize, imSize)
		top[1].reshape(batchSize, 1, maskSize, maskSize)
		top[2].reshape(batchSize, 1, maskSize, maskSize)
		
	def forward(self, bottom, top):
		Xc = imSize/4 + imSize/2*np.random.rand(1); Yc = imSize/4 + imSize/2*np.random.rand(1); R = imSize/2*np.random.rand(1);
		mask = (np.square(self.X-Xc) + np.square(self.Y-Yc) > R**2 )
		im = self.A*mask + self.B*(np.logical_not(mask))
		top[0].data[0, 0, :, :] = im.astype(np.float32)
		top[1].data[0, 0, :, :] = mask[94:482, 94:482].astype(np.float32)
		top[2].data[0, 0, :, :] = 1


	def backward(self, top, propagate_down, bottom):
		# no back propagation
		pass
