import cv2
import cv
import numpy as np
from scipy.interpolate import griddata
import libtiff
import myTools
import csv
import time
import caffe
import matplotlib.pyplot as plt
import datetime

# import signal
import sys

caffe.set_device(0)
caffe.set_mode_gpu()

solver = caffe.SGDSolver('solver-v6.prototxt')
# solver.restore('train/u-net_train_iter_38000.solverstate')
net = solver.net

errs = []
f = open('errsList'+datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+'.txt', 'w', 1)

while True:
	try:
		solver.step(1)

		errs.append(net.blobs['loss'].data[()])
		f.write(str(net.blobs['loss'].data[()])+"\n")
		# l1a = np.squeeze(net.params['conv_d0a-b'][0].data)
		# l1b = np.squeeze(net.params['conv_d0b-c'][0].data)
		# l2a = np.squeeze(net.params['conv_d1a-b'][0].data)
		# l2b = np.squeeze(net.params['conv_d1b-c'][0].data)
		# l3a = np.squeeze(net.params['conv_d2a-b'][0].data)
		# l3b = np.squeeze(net.params['conv_d2b-c'][0].data)
		# l4a = np.squeeze(net.params['conv_d3a-b'][0].data)
		# l4b = np.squeeze(net.params['conv_d3b-c'][0].data)

		# assert np.isnan(l1a).any() == False
		# assert np.isnan(l1b).any() == False
		# assert np.isnan(l2a).any() == False
		# assert np.isnan(l2b).any() == False
		# assert np.isnan(l3a).any() == False
		# assert np.isnan(l3b).any() == False
		# assert np.isnan(l4a).any() == False
		# assert np.isnan(l4b).any() == False

		for layer_name, param in net.params.iteritems():
			assert np.isnan(param[0].data).any() == False
			assert np.isnan(param[1].data).any() == False

		# plt.matshow(l1a); plt.colorbar()
		# plt.matshow(l1b); plt.colorbar()
		# plt.matshow(l2a); plt.colorbar()
		# plt.matshow(l2b); plt.colorbar()
		# plt.matshow(l3a); plt.colorbar()
		# plt.matshow(l3b); plt.colorbar()
		# plt.matshow(l4a); plt.colorbar()
		# plt.matshow(l4b); plt.colorbar()
		# plt.show()

		# scores = np.zeros((2,388,388))
		# scores[...] = net.blobs['score'].data[0,:,:,:]
		# s0, s1 = np.exp(scores[0,:,:]), np.exp(scores[1,:,:])
		# denom = s0 + s1
		# s0, s1 = s0/denom, s1/denom
		# plt.matshow(s1); plt.colorbar()
		# plt.matshow(np.squeeze(net.blobs['label'].data[0,:,:,:])); plt.colorbar()
		# plt.show()

		# f = libtiff.TIFF.open("l1a.tif", mode='w'); f.write_image(l1a); del f
		# f = libtiff.TIFF.open("l1b.tif", mode='w'); f.write_image(l1b); del f
		# f = libtiff.TIFF.open("l2a.tif", mode='w'); f.write_image(l2a); del f
		# f = libtiff.TIFF.open("l2b.tif", mode='w'); f.write_image(l2b); del f
		# f = libtiff.TIFF.open("l3a.tif", mode='w'); f.write_image(l3a); del f
		# f = libtiff.TIFF.open("l3b.tif", mode='w'); f.write_image(l3b); del f
		# f = libtiff.TIFF.open("l4a.tif", mode='w'); f.write_image(l4a); del f
		# f = libtiff.TIFF.open("l4b.tif", mode='w'); f.write_image(l4b); del f
	except KeyboardInterrupt:
		print('Exiting, you pressed Ctrl+C!')
		f.close()
		sys.exit(0)
