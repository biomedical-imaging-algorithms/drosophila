import caffe
import myTools
import numpy as np
import csv
import matplotlib.pyplot as plt

csvFilename = '/mnt/medical/microscopy/drosophila/egg_segmentation/ovary_image_info_for_prague.csv';
cropImDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_2d/stage';
cropTruthDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_2d_binary/stage';
fullImDir = '/mnt/medical/microscopy/drosophila/egg_segmentation/slice/stage';

class BBOX:
	def __init__(self, x1, x2, y1, y2):
		self.x_1, self.x_2, self.y_1, self.y_2 = x1, x2, y1, y2
class INPUT_DATA:
	def __init__(self, fullImg, cB, mask, wtmap):
		self.fullImg, self.cB, self.mask, self.wtmap = fullImg, cB, mask, wtmap

from settings import batchSize, imSize_pad as imSize, maskSize_pad as maskSize

class augmentLayer(caffe.Layer):
	def setup(self, bottom, top):
		if len(top) != 3:
			raise Exception('must have exactly three outputs')
		# if len(top) != 2:
		# 	raise Exception('must have exactly two outputs')
		self.inputList = []
		with open(csvFilename) as csvfile:
			r = csv.DictReader(csvfile, delimiter='\t')
			for row in r:
				cB = BBOX(int(row['bb_begin_x']), int(row['bb_end_x']), int(row['bb_begin_y']), int(row['bb_end_y']))
				im = myTools.readImage(fullImDir+row['stage']+'/slice_'+row['image_id']+'.tif', normalize = True)

				assert np.isnan(im).any() == False
				
				truth = myTools.readImage(cropTruthDir+row['stage']+"/mask"+row['image_id']+".png")
				wtmap = myTools.constructWeightMap(truth)
				# plt.matshow(wtmap); plt.colorbar()
				# plt.matshow(truth); plt.colorbar()
				# plt.matshow(im); plt.colorbar()
				# print im
				# print im.dtype
				# plt.show()
				self.inputList.append(INPUT_DATA(im, cB, truth, wtmap))
		self.counter = 0;

	def reshape(self, bottom, top):
		top[0].reshape(batchSize, 1, imSize, imSize)
		top[1].reshape(batchSize, 1, maskSize, maskSize)
		top[2].reshape(batchSize, 1, maskSize, maskSize)
		
	def forward(self, bottom, top):
		for ii in range(top[0].shape[0]):
			im = self.inputList[self.counter%len(self.inputList)].fullImg
			cB = self.inputList[self.counter%len(self.inputList)].cB
			mask = self.inputList[self.counter%len(self.inputList)].mask
			wtmap = self.inputList[self.counter%len(self.inputList)].wtmap

			im_aug, mask_aug, wtmap_aug = myTools.augment(im, cB, mask, wtmap, req_size = imSize, mask_size = maskSize, full_size = 1020)

			assert np.isnan(im_aug).any() == False
			assert np.isnan(mask_aug).any() == False
			assert np.isnan(wtmap_aug).any() == False

			# plt.matshow(wtmap_aug); plt.colorbar()
			# plt.matshow(mask_aug); plt.colorbar()
			# plt.matshow(im_aug); plt.colorbar()
			# plt.show()

			top[0].data[ii, 0, :, :] = im_aug.astype(np.float32)
			top[1].data[ii, 0, :, :] = mask_aug.astype(np.float32)
			top[2].data[ii, 0, :, :] = wtmap_aug.astype(np.float32)

			self.counter = self.counter + 1

	def backward(self, top, propagate_down, bottom):
		# no back propagation
		pass
