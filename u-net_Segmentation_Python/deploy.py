#!/usr/bin/env python

'''
@author Siddhant Ranade

Function to perform segmentation, compute the accuracy from given ground truth, save results, do a comparison, etc.

Requires the net instance, input image, mask, and the output destination.
'''

import cv2
import cv
import numpy as np
from scipy.interpolate import griddata
import scipy.ndimage as ndimage
import libtiff
import myTools
import csv
import time
import caffe
import matplotlib.pyplot as plt
import argparse
class BBOX:
	def __init__(self, x1, x2, y1, y2):
		pass

import os

class OUTPUT:
	def __init__(self, c, fp, fn, t):
		self.accurate, self.falsePositives, self.falseNegatives, self.total = c, fp, fn, t

def tileAndSegment(net, args, imSize, maskSize):

	# maskSize = tile size?
	context = (imSize - maskSize)/2

	# Read Image
	if hasattr(args, 'input'):
		im = myTools.readImage(args.input, normalize=True)
	elif hasattr(args, 'inputImg'):
		im = args.inputImg
	else:
		print "No input image or path"
		exit(-1)

	# Compute number of rows and cols
	R = int(np.ceil(float(im.shape[0])/maskSize));
	C = int(np.ceil(float(im.shape[1])/maskSize));

	if args.verbose:
		print "Image size = ", im.shape
		print("Total cuts to process: %d x %d" % (R,C))

	# Softmax output
	softmax = np.zeros((2, im.shape[0], im.shape[1]))

	# Iterate over rows and cols
	for r in range(R):
		for c in range(C):
			# Compute bounding box coordinates
			x1, x2 = c * maskSize, (c + 1) * maskSize
			y1, y2 = r * maskSize, (r + 1) * maskSize
			cBsmall = BBOX(x1,x2,y1,y2)

			# Get larger tile with context
			im_cut = myTools.cutWithContext(im, cBsmall, context)

			# ?? Init NN input ??
			net.blobs['data'].reshape(1, 1, imSize, imSize)
			net.blobs['data'].data[0, 0, :, :] = im_cut.astype(np.float32)

			# ?? Compute NN output ??
			net.forward()

			# Copy output
			softmax[:, y1:min(y2, im.shape[0]), x1:min(x2, im.shape[1])] \
			= net.blobs['softmax'].data[0,:,:min(maskSize, im.shape[0] - y1), :min(maskSize, im.shape[1] - x1)]

	# Predict class with the highest probability
	prediction = np.argmax(softmax,axis=0)

	if hasattr(args, 'output'):
		plt.imsave(args.output, prediction, cmap='gray') # Save the segmentation map
	else:
		return prediction

###### if output filename is not given, script finishes here.

	### ?? The following code generates the output image ??

	if bool(args.truth) | args.verbose:
		root, ext = os.path.splitext(args.output)
		predictedBoundary = np.expand_dims(ndimage.morphology.morphological_gradient(prediction, size=(3,3), mode='reflect'), 2) * (1,1,1)

	if args.verbose:
		assert(bool(args.output))
		s0, s1 = softmax[0,:,:], softmax[1,:,:]
		plt.imsave(root+'-activation'+ext, s1, cmap = 'hot')
		img = np.expand_dims(im, 2) * (1-predictedBoundary) + predictedBoundary * (1,0,0)
		plt.imsave(root+'-overlay'+ext, img)

	if bool(args.truth):
		givenMask = myTools.readImage(args.truth, normalize=True)
		comp = (prediction[args.bbox[1]:args.bbox[1]+givenMask.shape[0], args.bbox[0]:args.bbox[0]+givenMask.shape[1]] == givenMask)
		accurate = np.sum(comp)
		falsePositives = np.sum(np.logical_and(np.logical_not(comp), np.logical_not(givenMask)))
		falseNegatives = np.sum(np.logical_and(np.logical_not(comp), givenMask))
		total = float(np.size(comp))
		print("Accuracy = %f%%" % (accurate*100/total,))
		print("False Positives = %f%%" % (falsePositives*100/total,))
		print("False Negatives = %f%%" % (falseNegatives*100/total,))
		mask_tiled = np.pad(givenMask, ((args.bbox[1], im.shape[0]-givenMask.shape[0]-args.bbox[1]), (args.bbox[0], im.shape[1]-givenMask.shape[1]-args.bbox[0])), mode='constant', constant_values = 2)
		trueBoundary = np.expand_dims(ndimage.morphology.morphological_gradient(mask_tiled, size=(3,3), mode='reflect'), 2) * (1,1,1)
		img = np.expand_dims(im, 2) * (1-trueBoundary) * (1-predictedBoundary) + trueBoundary * (0,1,0) + predictedBoundary * (1,0,0)
		font = cv2.FONT_HERSHEY_SIMPLEX
		cv2.putText(img, "Accuracy = %f" % (accurate/total,),(15,15), font, 0.5,(np.amax(img),np.amax(img),np.amax(img)))
		cv2.putText(img, "False Positives = %f" % (falsePositives/total,),(15,35), font, 0.5,(np.amax(img),np.amax(img),np.amax(img)))
		cv2.putText(img, "False Negatives = %f" % (falseNegatives/total,),(15,55), font, 0.5,(np.amax(img),np.amax(img),np.amax(img)))
		plt.imsave(root+'-comparison'+ext, img)
		outobj = OUTPUT(accurate, falsePositives, falseNegatives, int(total))
		return outobj

	