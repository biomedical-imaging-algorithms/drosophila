#!/usr/bin/env python

'''
@author Siddhant Ranade

Processes the accuracies and errors from the txt files created by testModel.py and saves data for GNUPlot in a single txt file.
'''

import csv
import numpy
import glob
import os

dirToProcess = "results_weight100/"

txtpath = dirToProcess+"*errs_*.txt"

w = open(dirToProcess+"errs.txt", 'w', 1)

class errStore:
	def __init__(self, i, c, fp, fn, t):
		self.iterNum, self.correct, self.falsePositives, self.falseNegatives, self.total = int(i),int(c),int(fp),int(fn),int(t)

def getKey(item):
	return item.iterNum

listing = glob.glob(txtpath)
errObjList = []
for item in listing:
	print item
	basename = os.path.splitext(os.path.basename(item))[0]
	iterNum = basename[basename.rfind('_')+1:]
	with open(item) as csvfile:
		r = csv.DictReader(csvfile, delimiter='\t')
		lastRow = None
		for lastRow in r: pass
		errObj = errStore(iterNum, lastRow['accurate'], lastRow['falsePositives'], lastRow['falseNegatives'], lastRow['total'])
		errObjList.append(errObj)

errObjList = sorted(errObjList, key=getKey)

w.write("\t".join(("Iteration", "Accuracy", "FalsePositives", "FalseNegatives"))+"\n")
for item in errObjList:
	i = str(item.iterNum)
	a = str(float(item.correct)/item.total*100.0)
	fp = str(float(item.falsePositives)/item.total*100.0)
	fn = str(float(item.falseNegatives)/item.total*100.0)
	w.write("\t".join((i, a, fp, fn))+"\n")
w.close()