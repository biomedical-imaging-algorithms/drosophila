csvFilename = '/mnt/medical/microscopy/drosophila/egg_segmentation/ovary_image_info_for_prague.csv';
cropTruthDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_2d/stage';
cropImDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_2d/stage';

maskBinDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_2d_binary/stage';

fileInfoArr = table2struct(readtable(csvFilename, 'Delimiter', '\t'));
close all
for ii = 43:numel(fileInfoArr)
%     subplot(8,8,ii);
    if fileInfoArr(ii).stage == 2
        continue
    end
    figure;
    ma = imread(strcat(cropTruthDir, int2str(fileInfoArr(ii).stage), '/mask', int2str(fileInfoArr(ii).image_id), '.png'));
    im = imread(strcat(cropImDir, int2str(fileInfoArr(ii).stage), '/train', int2str(fileInfoArr(ii).image_id), '.tif'));
    subplot(211); imagesc(double(im(:,:,1))/256); colormap(hot); axis off; colorbar;
    ma(ma==2 | ma==3) = 1;
    subplot(212); imagesc(ma); axis off; colorbar;
    
    ma2 = ordfilt2(ma, 1, ones(3));
    ma3 = ma; ma3(ma==4) = ma2(ma==4);
    se = strel('disk', 3);
    ma3 = imopen(ma3, se);
    subplot(212); imagesc(ma3); axis off; colorbar;
    imwrite(ma3, strcat(maskBinDir, int2str(fileInfoArr(ii).stage), '/mask', int2str(fileInfoArr(ii).image_id), '.png'));
    close;
end