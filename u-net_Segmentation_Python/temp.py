import myTools
import numpy as np
import csv
import matplotlib.pyplot as plt

csvFilename = '/mnt/medical/microscopy/drosophila/egg_segmentation/ovary_image_info_for_prague.csv'
fullTruthDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_2d_full/stage'
fullImDir = '/mnt/medical/microscopy/drosophila/egg_segmentation/slice/stage'

csvFilename2 = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/manually_corrected_3D_stacks_segmented.csv'
fullStackDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_3d_split/'
fullStackTruthDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_3d_binary/'

class BBOX:
	def __init__(self, x1, x2, y1, y2):
		self.x_1, self.x_2, self.y_1, self.y_2 = x1, x2, y1, y2
class INPUT_DATA:
	def __init__(self, fullImg, mask, wtmap, name, maxval):
		self.fullImg, self.mask, self.wtmap, self.name, self.maxval = fullImg, mask, wtmap, name, maxval

batchSize = 1
imSize = 700
maskSize = 516


with open(csvFilename) as csvfile:
	r = csv.DictReader(csvfile, delimiter='\t')
	for row in r:
		im = myTools.readImage(fullImDir+row['stage']+'/slice_'+row['image_id']+'.tif', normalize = False)

		assert np.isnan(im).any() == False
		
		truth = myTools.readImage(fullTruthDir+row['stage']+"/slice_"+row['image_id']+"-label.tif")
		wtmap = myTools.constructWeightMap(truth)
		print row['image_id']
		plt.matshow(im, cmap='hot'); plt.colorbar()
		plt.matshow(truth, cmap='hot'); plt.colorbar()
		plt.matshow(wtmap, cmap='hot'); plt.colorbar()
		plt.show()
		a, b, c = myTools.augmentNew(im, truth, wtmap, perturb_var = 30, theta=0, context=0)
		plt.matshow(a, cmap='hot'); plt.colorbar()
		plt.matshow(b, cmap='hot'); plt.colorbar()
		plt.matshow(c, cmap='hot'); plt.colorbar()
		plt.show()