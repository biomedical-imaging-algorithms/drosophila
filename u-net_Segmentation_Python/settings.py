# Settings file for various experiments
# This file will be used for training, by main-v5.py, and myTools.py will load some parameters from this.

gpu_id = 0 # Used in main-v5.py -- This gpu_id doesn't match the GPU ID in nvidia-smi (say)

w0, sigma = 20, 5 # Used in myTools.py for weight computation -- see the U-Net article or Siddhant's report for an explanation.

# For all input layers
batchSize = 1         # See the references for an explanation for batchSize

# Variables for augmentLayerPython and augmentLayerPythonTiling
imSize_noPad = 700    # Must be of the form 16 x N + 60
maskSize_noPad = 516  # Must be imSize_noPad - 184

# Variables for augmentLayerPythonPadded
imSize_pad = 512      # Must be of the form 2 ** k
maskSize_pad = 512    # Must be imSize_pad
