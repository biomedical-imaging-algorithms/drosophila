#!/usr/bin/env python

'''
@author Siddhant Ranade

Perform segmentation on the 3D stacks
'''

import numpy as np
import myTools
import time
import caffe
import argparse
import deploy
import libtiff
import glob
import os

imSize = 572
maskSize = 388

inputDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_3d_split/*_mag.tif'
# outputDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_3d_binary/'
outputDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_3d_binary_tmp/'

parser = argparse.ArgumentParser(description='Perform 3D segmentation on all 3D stacks using given model.')
parser.add_argument('-m', '--model', metavar='path/to/model.caffemodel', type=str, action='store', required=True,
	help='Path to the .caffemodel file to load')
parser.add_argument('-n', '--net', metavar='path/to/net.prototxt', type=str, action='store', default='test-v5.prototxt', required=True,
	help='Path to the net .prototxt to load')
parser.add_argument('-g', '--gpu_id', metavar='N', type=int, action='store', default=3,  # 0
	help='ID of the GPU to use (0,1,2,...)')
args = parser.parse_args()

caffe.set_device(args.gpu_id)
caffe.set_mode_gpu()
net = caffe.Net(args.net, args.model, caffe.TEST)

args.verbose = False

listing = glob.glob(inputDir)
for fname in listing:
	print fname
	img = myTools.load_image_tiff_3d(fname).astype(np.float64)
	img /= np.amax(img)
	# print img.shape

	prediction = np.zeros(img.shape)
	for l in range(img.shape[0]):
		args.inputImg = img[l,:,:]
		prediction[l,:,:] = deploy.tileAndSegment(net, args, imSize, maskSize)

	outfilename = outputDir + os.path.splitext(os.path.basename(fname))[0] + "_label.tif"
	tif = libtiff.TIFF.open(outfilename, mode='w')
	tif.write_image(prediction.astype(np.uint8))
	tif.close()
	print outfilename