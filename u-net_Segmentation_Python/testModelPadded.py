#!/usr/bin/env python

'''
@author Siddhant Ranade

Script to test a given caffe model (.caffemodel) on the fixed set of test images.  Performs segmentation, computes the accuracy from given ground truth, saves results.

Also outputs the accuracies and errors (per image and total) into a txt file.
'''

import cv2
import cv
import numpy as np
from scipy.interpolate import griddata
import scipy.ndimage as ndimage
import libtiff
import myTools
import csv
import time
import caffe
import matplotlib.pyplot as plt
import os
import argparse
import deploy

imSize = 384
maskSize = 384


csvFilename = '/mnt/medical/microscopy/drosophila/egg_segmentation/ovary_image_info_for_prague.csv'
cropImDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_2d/stage'
cropTruthDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_2d_binary/stage'
fullImDir = '/mnt/medical/microscopy/drosophila/egg_segmentation/slice/stage'

parser = argparse.ArgumentParser(description='Test the given model on the set of images described in the csv.')
parser.add_argument('-m', '--model', metavar='path/to/model.caffemodel', type=str, action='store', required=True,
	help='Path to the .caffemodel file to load')
parser.add_argument('-n', '--net', metavar='path/to/net.prototxt', type=str, action='store', default='test-v5.prototxt', required=True,
	help='Path to the net .prototxt to load')
parser.add_argument('-g', '--gpu_id', metavar='N', type=int, action='store', default=2,
	help='ID of the GPU to use (0,1,2,...)')
args = parser.parse_args()

args.verbose = True

caffe.set_device(args.gpu_id)
caffe.set_mode_gpu()

net = caffe.Net(args.net, args.model, caffe.TEST)

f=open("results_padded/errs_"+os.path.splitext(os.path.basename(args.model))[0]+'.txt', 'w', 1)
f.write('\t'.join(("image_id", "accurate", "falsePositives", "falseNegatives", "total")) + '\n')

for layer_name, param in net.params.iteritems():
	assert np.isnan(param[0].data).any() == False
	assert np.isnan(param[1].data).any() == False

accSum, fpSum, fnSum, totalSum = 0, 0, 0, 0

with open(csvFilename) as csvfile:
	r = csv.DictReader(csvfile, delimiter='\t')
	for row in r:
		args.bbox = (int(row['bb_begin_x']), int(row['bb_begin_y']))
		args.input = fullImDir+row['stage']+'/slice_'+row['image_id']+'.tif'
		args.truth = cropTruthDir+row['stage']+"/mask"+row['image_id']+".png"
		args.output = "results_padded/output_"+row['image_id']+".png"
		# print args.output
		outargs = deploy.tileAndSegment(net, args, imSize, maskSize)
		f.write('\t'.join((row['image_id'], str(outargs.accurate), str(outargs.falsePositives), str(outargs.falseNegatives), str(outargs.total))) + '\n')
		accSum, fpSum, fnSum, totalSum = accSum+outargs.accurate, fpSum+outargs.falsePositives, fnSum+outargs.falseNegatives, totalSum+outargs.total

f.write('\t'.join(("total", str(accSum), str(fpSum), str(fnSum), str(totalSum) )) + '\n')