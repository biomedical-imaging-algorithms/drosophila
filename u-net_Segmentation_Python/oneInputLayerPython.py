import caffe
import myTools
import numpy as np
import csv
import cv2
import libtiff
import matplotlib.pyplot as plt

csvFilename = '/mnt/medical/microscopy/drosophila/egg_segmentation/ovary_image_info_for_prague.csv';
cropImDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_2d/stage2/';
cropTruthDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_2d_binary/stage2/';
fullImDir = '/mnt/medical/microscopy/drosophila/egg_segmentation/slice/stage2/';

class BBOX:
	def __init__(self, x1, x2, y1, y2):
		self.x_1, self.x_2, self.y_1, self.y_2 = x1, x2, y1, y2
class INPUT_DATA:
	def __init__(self, fullImg, cB, mask, wtmap):
		self.fullImg, self.cB, self.mask, self.wtmap = fullImg, cB, mask, wtmap

batchSize = 1
imSize = 572
maskSize = 388

class oneInputLayer(caffe.Layer):
	def setup(self, bottom, top):
		if len(top) != 3:
			raise Exception('must have exactly three outputs')
		# if len(top) != 2:
		# 	raise Exception('must have exactly two outputs')
		self.inputList = []
		with open(csvFilename) as csvfile:
			r = csv.DictReader(csvfile, delimiter='\t')
			for row in r:
				if row['stage'] != "2":
					continue
				cB = BBOX(int(row['bb_begin_x']), int(row['bb_end_x']), int(row['bb_begin_y']), int(row['bb_end_y']))
				tif = libtiff.TIFF.open(fullImDir+'slice_'+row['image_id']+'.tif', mode='r')
				im = tif.read_image()
				im /= np.amax(im)

				assert np.isnan(im).any() == False
				
				truth = cv2.imread(cropTruthDir+"mask"+row['image_id']+".png", 0)
				wtmap = myTools.constructWeightMap(truth)
				self.inputList.append(INPUT_DATA(im, cB, truth, wtmap))
				break
		self.counter = 0;
		im = self.inputList[self.counter%len(self.inputList)].fullImg
		cB = self.inputList[self.counter%len(self.inputList)].cB
		mask = self.inputList[self.counter%len(self.inputList)].mask
		wtmap = self.inputList[self.counter%len(self.inputList)].wtmap

		self.im_aug, self.mask_aug, self.wtmap_aug = myTools.augment(im, cB, mask, wtmap, req_size = imSize, mask_size = maskSize, perturb_var=0, theta=0)

	def reshape(self, bottom, top):
		top[0].reshape(batchSize, 1, imSize, imSize)
		top[1].reshape(batchSize, 1, maskSize, maskSize)
		top[2].reshape(batchSize, 1, maskSize, maskSize)
		
	def forward(self, bottom, top):
		assert np.isnan(self.im_aug).any() == False
		assert np.isnan(self.mask_aug).any() == False
		assert np.isnan(self.wtmap_aug).any() == False

		for ii in range(top[0].shape[0]):
			# plt.matshow(wtmap_aug); plt.colorbar()
			# plt.matshow(mask_aug); plt.colorbar()
			# plt.matshow(im_aug); plt.colorbar()
			# plt.show()

			top[0].data[ii, 0, :, :] = self.im_aug.astype(np.float32)
			top[1].data[ii, 0, :, :] = self.mask_aug.astype(np.float32)
			top[2].data[ii, 0, :, :] = self.wtmap_aug.astype(np.float32)

			self.counter = self.counter + 1

	def backward(self, top, propagate_down, bottom):
		# no back propagation
		pass
