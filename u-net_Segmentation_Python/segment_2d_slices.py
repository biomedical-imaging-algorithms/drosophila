#!/usr/bin/env python

'''
@author Siddhant Ranade

Perform segmentation on the 3D stacks
'''

import numpy as np
import myTools
import time
import caffe
import argparse
import deploy
import libtiff
import glob
import os
import matplotlib.pyplot as plt
import csv

imSize = 572
maskSize = 388

csvFilename = '/mnt/medical/microscopy/drosophila/egg_segmentation/ovary_image_info_for_prague.csv'
inputDir = '/mnt/medical/microscopy/drosophila/egg_segmentation/slice/stage'
# outputDir = '/mnt/medical/microscopy/drosophila/egg_segmentation/mask_2d_full/stage'
outputDir = '/mnt/medical/microscopy/drosophila/egg_segmentation/mask_2d_full_tmp/stage'


parser = argparse.ArgumentParser(description='Perform 2D segmentation on all 2D slices using given model.')
parser.add_argument('-m', '--model', metavar='path/to/model.caffemodel', type=str, action='store', required=True,
	help='Path to the .caffemodel file to load')
parser.add_argument('-n', '--net', metavar='path/to/net.prototxt', type=str, action='store', default='test-v5.prototxt', required=True,
	help='Path to the net .prototxt to load')
parser.add_argument('-g', '--gpu_id', metavar='N', type=int, action='store', default=3,  # 1
	help='ID of the GPU to use (0,1,2,...)')
args = parser.parse_args()

caffe.set_device(args.gpu_id)
caffe.set_mode_gpu()
net = caffe.Net(args.net, args.model, caffe.TEST)

args.verbose = False

with open(csvFilename) as csvfile:
	r = csv.DictReader(csvfile, delimiter='\t')
	for row in r:
		args.input = inputDir+row['stage']+'/slice_'+row['image_id']+'.tif'

		# Segment the image
		output = deploy.tileAndSegment(net, args, imSize, maskSize)

		# outfilename = outputDir+row['stage']+'/mask'+row['image_id']+'.png'
		outfilename2 = outputDir+row['stage']+'/slice_'+row['image_id']+'-label.tif'
		# plt.imsave(outfilename, output.astype(np.uint8), vmin = 0, vmax = 255, cmap='gray')
		tif = libtiff.TIFF.open(outfilename2, mode='w')
		tif.write_image(output.astype(np.uint8))
		tif.close()
		print outfilename2