import cv2
import cv
import numpy as np
from scipy.interpolate import griddata
import libtiff
import myTools
import csv
import time
import caffe
import matplotlib.pyplot as plt

csvFilename = '/mnt/medical/microscopy/drosophila/ovary_image_info_for_prague.csv';
cropImDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_2d/stage2/';
cropTruthDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_2d_binary/stage2/';
fullImDir = '/mnt/medical/microscopy/drosophila/egg_segmentation/slice/stage2/';

class BBOX:
	def __init__(self, x1, x2, y1, y2):
		self.x_1, self.x_2, self.y_1, self.y_2 = x1, x2, y1, y2

caffe.set_device(0)
caffe.set_mode_gpu()

net = caffe.Net('SyntheticTest.prototxt', 'synthetic_train_iter_12000.caffemodel', caffe.TEST)

for layer_name, param in net.params.iteritems():
	assert np.isnan(param[0].data).any() == False
	assert np.isnan(param[1].data).any() == False


while True:
	net.forward()
	scores = np.zeros((2,388,388))
	print net.blobs['score'].data.shape
	scores[...] = net.blobs['score'].data[0,:,:,:]

	prediction = np.argmax(scores,axis=0)

	# cv2.imwrite('augmentation_results/augmented'+row['image_id']+'.png', np.dstack( ( (cut_aug*(255/np.amax(cut_aug))), truth_aug*127, truth_aug*127)))
	# plt.matshow(wtmap_aug); plt.colorbar()
	truth_aug = np.squeeze(net.blobs['label'].data[...])
	cut_aug = np.squeeze(net.blobs['data'].data[...])
	wtmap_aug = np.squeeze(net.blobs['sample_weights'].data[...])

	s0, s1 = np.exp(scores[0,:,:]), np.exp(scores[1,:,:])
	denom = s0 + s1
	s0, s1 = s0/denom, s1/denom
	p = np.ones(truth_aug.shape)
	p[truth_aug == 1] = s1[truth_aug == 1]
	p[truth_aug == 0] = s0[truth_aug == 0]
	count = np.sum(truth_aug != 2)
	err = np.nansum(wtmap_aug * - np.log(np.maximum(p, np.finfo(np.float32).eps)))
	print err, count, err/count
	print net.blobs['loss'].data[...]
	plt.matshow(truth_aug, cmap='hot'); plt.title('Ground Truth'); plt.colorbar()
	plt.matshow(cut_aug[94:482, 94:482], cmap='hot'); plt.title('Input Image'); plt.colorbar()
	plt.matshow(prediction, cmap='hot'); plt.title('Output (segmentation)'); plt.colorbar()
	# plt.matshow(s1); plt.colorbar()
	# plt.matshow(p); plt.colorbar()
	plt.show()