'''
@author Siddhant Ranade

Contains several functions to help in training the network -- including augmentation, creating weightmaps, reading images, etc.
'''

import cv2
import numpy as np
from scipy.interpolate import griddata
from scipy.interpolate import interpn
from scipy.interpolate import interp2d
import scipy.ndimage as ndimage
import math
import os
import libtiff
from PIL import Image
import matplotlib.pyplot as plt

def augment_old(img_full, cB, truth, wtmap, req_size = 572, mask_size = 388, full_size = 700):
	'''
	DO NOT USE -- SLOW!
	'''
	size_mask_x, size_mask_y = cB.x_2 - cB.x_1, cB.y_2 - cB.y_1	#MUST BE int
	perturb_var = 10
	
	origin_mask_x, origin_mask_y = (full_size - size_mask_x)/2, (full_size - size_mask_y)/2
	origin_im_x, origin_im_y = origin_mask_x - cB.x_1, origin_mask_y - cB.y_1

	img_tiled = img_full

	if origin_im_x < 0:
		img_tiled = img_tiled[:, -origin_im_x:]
		origin_im_x = 0
	if origin_im_y < 0:
		img_tiled = img_tiled[-origin_im_y:, :]
		origin_im_y = 0
	if origin_im_x + img_tiled.shape[1] > full_size:
		img_tiled = img_tiled[:, :full_size - origin_im_x]
	if origin_im_y + img_tiled.shape[0] > full_size:
		img_tiled = img_tiled[:full_size - origin_im_y, :]

	img_tiled = np.lib.pad(img_tiled, ((origin_im_y, full_size - img_tiled.shape[0] - origin_im_y), 
		(origin_im_x, full_size - img_tiled.shape[1] - origin_im_x)), 'symmetric')

	mask_tiled = np.lib.pad(truth, ((origin_mask_y, full_size - truth.shape[0] - origin_mask_y), 
		(origin_mask_x, full_size - truth.shape[1] - origin_mask_x)), 'constant', constant_values=2)

	wtmap_tiled = np.lib.pad(wtmap, ((origin_mask_y, full_size - truth.shape[0] - origin_mask_y), 
		(origin_mask_x, full_size - truth.shape[1] - origin_mask_x)), 'constant', constant_values=0)

	Nodes = np.linspace(0, full_size-1, 4)

	X_small,Y_small = np.meshgrid(Nodes, Nodes);
	X_full,Y_full = np.meshgrid(np.arange(full_size), np.arange(full_size));

	DX_small = perturb_var * np.random.randn(4,4);
	DY_small = perturb_var * np.random.randn(4,4);
	
	DX_full = griddata((X_small.flatten(), Y_small.flatten()), DX_small.flatten(), (X_full, Y_full), method="cubic", fill_value=0);
	DY_full = griddata((X_small.flatten(), Y_small.flatten()), DY_small.flatten(), (X_full, Y_full), method="cubic", fill_value=0);

	Xf_full = X_full+DX_full;
	Yf_full = Y_full+DY_full;
	theta = 2*math.pi*np.random.randn(1)

	query_Pts = np.arange((full_size-req_size)/2, (full_size+req_size)/2)
	query_Pts_mask = np.arange((full_size-mask_size)/2, (full_size+mask_size)/2)
	X_query_norotation, Y_query_norotation = np.meshgrid(query_Pts, query_Pts)
	X_query, Y_query = rotate2(X_query_norotation, Y_query_norotation, theta, full_size/2, full_size/2)
	X_query_mask_norotation, Y_query_mask_norotation = np.meshgrid(query_Pts_mask, query_Pts_mask)
	X_query_mask, Y_query_mask = rotate2(X_query_mask_norotation, Y_query_mask_norotation, theta, full_size/2, full_size/2)

	cut_aug = griddata((Xf_full.flatten(), Yf_full.flatten()), img_tiled.flatten(),  (X_query, Y_query), method="nearest");
	truth_aug = griddata((Xf_full.flatten(), Yf_full.flatten()), mask_tiled.flatten(), (X_query_mask, Y_query_mask), method="nearest");
	wtmap_tiled = griddata((Xf_full.flatten(), Yf_full.flatten()), wtmap_tiled.flatten(), (X_query_mask, Y_query_mask), method="nearest");

	return (cut_aug, truth_aug, wtmap_tiled)

def augment(img_full, cB, truth, wtmap, req_size = 572, mask_size = 388, full_size = 700, perturb_var = 10, theta = 2*math.pi*np.random.randn(1)):
	size_mask_x, size_mask_y = cB.x_2 - cB.x_1, cB.y_2 - cB.y_1	#MUST BE int
	
	origin_mask_x, origin_mask_y = (full_size - size_mask_x)/2, (full_size - size_mask_y)/2
	origin_im_x, origin_im_y = origin_mask_x - cB.x_1, origin_mask_y - cB.y_1

	img_tiled = img_full

	if origin_im_x < 0:
		img_tiled = img_tiled[:, -origin_im_x:]
		origin_im_x = 0
	if origin_im_y < 0:
		img_tiled = img_tiled[-origin_im_y:, :]
		origin_im_y = 0
	if origin_im_x + img_tiled.shape[1] > full_size:
		img_tiled = img_tiled[:, :full_size - origin_im_x]
	if origin_im_y + img_tiled.shape[0] > full_size:
		img_tiled = img_tiled[:full_size - origin_im_y, :]

	img_tiled = np.lib.pad(img_tiled, ((origin_im_y, full_size - img_tiled.shape[0] - origin_im_y), 
		(origin_im_x, full_size - img_tiled.shape[1] - origin_im_x)), 'symmetric')

	mask_tiled = np.lib.pad(truth, ((origin_mask_y, full_size - truth.shape[0] - origin_mask_y), 
		(origin_mask_x, full_size - truth.shape[1] - origin_mask_x)), 'constant', constant_values=2)

	wtmap_tiled = np.lib.pad(wtmap, ((origin_mask_y, full_size - truth.shape[0] - origin_mask_y), 
		(origin_mask_x, full_size - truth.shape[1] - origin_mask_x)), 'constant', constant_values=0)

	Nodes = np.linspace(0, full_size-1, 4)

	X_small,Y_small = np.meshgrid(Nodes, Nodes);
	X_full,Y_full = np.meshgrid(np.arange(full_size), np.arange(full_size));

	DX_small = perturb_var * np.random.randn(4,4);
	DY_small = perturb_var * np.random.randn(4,4);
	
	# DX_full = griddata((X_small.flatten(), Y_small.flatten()), DX_small.flatten(), (X_full, Y_full), method="cubic", fill_value=0);
	# DY_full = griddata((X_small.flatten(), Y_small.flatten()), DY_small.flatten(), (X_full, Y_full), method="cubic", fill_value=0);

	f1 = interp2d(Nodes, Nodes, DX_small.flatten(), kind="cubic")
	f2 = interp2d(Nodes, Nodes, DY_small.flatten(), kind="cubic")
	DX_full = f1(np.arange(full_size), np.arange(full_size))
	DY_full = f2(np.arange(full_size), np.arange(full_size))

	Xf_full = X_full+DX_full;
	Yf_full = Y_full+DY_full;
	
	X_query_norotation, Y_query_norotation = (Xf_full[(full_size-req_size)/2:(full_size+req_size)/2, (full_size-req_size)/2:(full_size+req_size)/2],
		Yf_full[(full_size-req_size)/2:(full_size+req_size)/2, (full_size-req_size)/2:(full_size+req_size)/2])

	X_query, Y_query = rotate2(X_query_norotation, Y_query_norotation, theta, full_size/2, full_size/2)
	X_query_mask_norotation, Y_query_mask_norotation = (Xf_full[(full_size-mask_size)/2:(full_size+mask_size)/2, (full_size-mask_size)/2:(full_size+mask_size)/2],
		Yf_full[(full_size-mask_size)/2:(full_size+mask_size)/2, (full_size-mask_size)/2:(full_size+mask_size)/2])

	X_query_mask, Y_query_mask = rotate2(X_query_mask_norotation, Y_query_mask_norotation, theta, full_size/2, full_size/2)

	cut_aug = interpn((np.arange(full_size), np.arange(full_size)), img_tiled,  (X_query, Y_query), method="nearest", bounds_error=False, fill_value=0);
	truth_aug = interpn((np.arange(full_size), np.arange(full_size)), mask_tiled, (X_query_mask, Y_query_mask), method="nearest", bounds_error=False, fill_value=2);
	wtmap_aug = interpn((np.arange(full_size), np.arange(full_size)), wtmap_tiled, (X_query_mask, Y_query_mask), method="nearest", bounds_error=False, fill_value=0);

	return (cut_aug, truth_aug, wtmap_aug)


def augmentNew(img, mask, wtmap, context=184, perturb_var = 10, theta = None):
	# pad_x, pad_y = (img.shape[1])/2, (img.shape[0])/2
	img_tiled = np.lib.pad(img, ((context/2, context/2), (context/2, context/2)), 'symmetric')
	mask_tiled = np.lib.pad(mask, ((context/2, context/2), (context/2, context/2)), 'constant', constant_values=2)
	wtmap_tiled = np.lib.pad(wtmap, ((context/2, context/2), (context/2, context/2)), 'constant', constant_values=0)

	if theta == None:
		theta = 2*math.pi*np.random.rand()

	size_full_x, size_full_y = int(img_tiled.shape[1]*abs(np.cos(theta)) + img_tiled.shape[0]*abs(np.sin(theta))), \
		int(img_tiled.shape[1]*abs(np.sin(theta)) + img_tiled.shape[0]*abs(np.cos(theta)))
	pad_x, pad_y = (size_full_x-img_tiled.shape[1])/2, (size_full_y-img_tiled.shape[0])/2

	print(theta, size_full_x, size_full_y)

	# img_tiled = np.lib.pad(img_tiled, ((pad_y, pad_y), (pad_x, pad_x)), 'symmetric')
	# mask_tiled = np.lib.pad(truth, ((pad_y, pad_y), (pad_x, pad_x)), 'constant', constant_values=2)
	# wtmap_tiled = np.lib.pad(wtmap, ((pad_y, pad_y), (pad_x, pad_x)), 'constant', constant_values=0)

	nodes_x = np.linspace(0, size_full_x-1, 4)
	nodes_y = np.linspace(0, size_full_y-1, 4)

	X_small,Y_small = np.meshgrid(nodes_x, nodes_y);
	X_full,Y_full = np.meshgrid(np.arange(size_full_x), np.arange(size_full_y));

	DX_small = perturb_var * np.random.randn(4,4);
	DY_small = perturb_var * np.random.randn(4,4);
	
	# DX_full = griddata((X_small.flatten(), Y_small.flatten()), DX_small.flatten(), (X_full, Y_full), method="cubic", fill_value=0);
	# DY_full = griddata((X_small.flatten(), Y_small.flatten()), DY_small.flatten(), (X_full, Y_full), method="cubic", fill_value=0);

	f1 = interp2d(nodes_x, nodes_y, DX_small, kind="cubic")
	f2 = interp2d(nodes_x, nodes_y, DY_small, kind="cubic")
	DX_full = f1(np.arange(size_full_x), np.arange(size_full_y))
	DY_full = f2(np.arange(size_full_x), np.arange(size_full_y))

	Xf_full = X_full+DX_full;
	Yf_full = Y_full+DY_full;
	

	X_query, Y_query = rotate2(Xf_full, Yf_full, theta, size_full_x/2, size_full_y/2)

	img_aug = interpn((np.arange(pad_y, pad_y + img_tiled.shape[0]), np.arange(pad_x, pad_x + img_tiled.shape[1])), img_tiled,  (Y_query, X_query), method="nearest", bounds_error=False, fill_value=0);
	mask_aug = interpn((np.arange(pad_y, pad_y + img_tiled.shape[0]), np.arange(pad_x, pad_x + img_tiled.shape[1])), mask_tiled, (Y_query, X_query), method="nearest", bounds_error=False, fill_value=2);
	wtmap_aug = interpn((np.arange(pad_y, pad_y + img_tiled.shape[0]), np.arange(pad_x, pad_x + img_tiled.shape[1])), wtmap_tiled, (Y_query, X_query), method="nearest", bounds_error=False, fill_value=0);

	return (img_aug, mask_aug, wtmap_aug)

def augmentPIL(img, mask, wtmap, context=184, perturb_var = 10, theta = None, DX = None, DY = None):
	img_tiled = np.lib.pad(img, ((context/2, context/2), (context/2, context/2)), 'symmetric')
	mask_tiled = np.lib.pad(mask, ((context/2, context/2), (context/2, context/2)), 'constant', constant_values=2)
	wtmap_tiled = np.lib.pad(wtmap, ((context/2, context/2), (context/2, context/2)), 'constant', constant_values=0)
	
	# print "Augmentation disp"
	# plt.matshow(img_tiled, cmap='hot'); plt.colorbar()
	# plt.matshow(mask_tiled, cmap='hot'); plt.colorbar()
	# plt.matshow(wtmap_tiled, cmap='hot'); plt.colorbar()
	# plt.show()
	# print "End augmentation disp"

	if theta == None:
		theta = 2 * np.pi * np.random.rand()
	if (DX == None) | (DY == None):
		DX = perturb_var * np.random.randn(4,4);
		DY = perturb_var * np.random.randn(4,4);

	size_full_x, size_full_y = img_tiled.shape[1], img_tiled.shape[0]

	print(theta, size_full_x, size_full_y)

	nodes_x = np.linspace(0, size_full_x-1, 4).astype(int)
	nodes_y = np.linspace(0, size_full_y-1, 4).astype(int)

	X,Y = np.meshgrid(nodes_x, nodes_y);
	Xf, Yf = X + DX, Y + DY

	# print X,Y

	# print Xf, Yf

	data = [((X[0,0], Y[0,0], X[1,1], Y[1,1]), (Xf[0,0],Yf[0,0], Xf[1,0],Yf[1,0], Xf[1,1],Yf[1,1], Xf[0,1],Yf[0,1])),
		((X[1,0], Y[1,0], X[2,1], Y[2,1]), (Xf[1,0],Yf[1,0], Xf[2,0],Yf[2,0], Xf[2,1],Yf[2,1], Xf[1,1],Yf[1,1])),
		((X[2,0], Y[2,0], X[3,1], Y[3,1]), (Xf[2,0],Yf[2,0], Xf[3,0],Yf[3,0], Xf[3,1],Yf[3,1], Xf[2,1],Yf[2,1])),
		((X[0,1], Y[0,1], X[1,2], Y[1,2]), (Xf[0,1],Yf[0,1], Xf[1,1],Yf[1,1], Xf[1,2],Yf[1,2], Xf[0,2],Yf[0,2])),
		((X[1,1], Y[1,1], X[2,2], Y[2,2]), (Xf[1,1],Yf[1,1], Xf[2,1],Yf[2,1], Xf[2,2],Yf[2,2], Xf[1,2],Yf[1,2])),
		((X[2,1], Y[2,1], X[3,2], Y[3,2]), (Xf[2,1],Yf[2,1], Xf[3,1],Yf[3,1], Xf[3,2],Yf[3,2], Xf[2,2],Yf[2,2])),
		((X[0,2], Y[0,2], X[1,3], Y[1,3]), (Xf[0,2],Yf[0,2], Xf[1,2],Yf[1,2], Xf[1,3],Yf[1,3], Xf[0,3],Yf[0,3])),
		((X[1,2], Y[1,2], X[2,3], Y[2,3]), (Xf[1,2],Yf[1,2], Xf[2,2],Yf[2,2], Xf[2,3],Yf[2,3], Xf[1,3],Yf[1,3])),
		((X[2,2], Y[2,2], X[3,3], Y[3,3]), (Xf[2,2],Yf[2,2], Xf[3,2],Yf[3,2], Xf[3,3],Yf[3,3], Xf[2,3],Yf[2,3]))]

	# for a,b in data:
	# 	print a,b

	i = Image.fromarray(img_tiled)
	m = Image.fromarray(mask_tiled.astype(np.float)-2)
	w = Image.fromarray(wtmap_tiled)

	# m.show()

	i2 = i.transform(i.size, Image.MESH, data, Image.BICUBIC, fill=0).rotate(int(theta*180.0/np.pi), Image.BICUBIC, expand=True)
	m2 = m.transform(m.size, Image.MESH, data, Image.BICUBIC, fill=2).rotate(int(theta*180.0/np.pi), Image.BICUBIC, expand=True)
	w2 = w.transform(w.size, Image.MESH, data, Image.BICUBIC, fill=0).rotate(int(theta*180.0/np.pi), Image.BICUBIC, expand=True)

	# m2.show()
	# m2.getcolors()

	img_aug = np.array(i2)
	mask_aug = np.array(m2)+2
	wtmap_aug = np.array(w2)

	# print "Augmentation disp2"
	# plt.matshow(img_aug, cmap='hot'); plt.colorbar()
	# plt.matshow(mask_aug, cmap='hot'); plt.colorbar()
	# plt.matshow(wtmap_aug, cmap='hot'); plt.colorbar()
	# plt.matshow(np.array(m2), cmap='hot'); plt.colorbar()
	# plt.show()

	# print img_aug.dtype, mask_aug.dtype, wtmap_aug.dtype
	# print "End augmentation disp2"

	return (img_aug, mask_aug, wtmap_aug)



def rotate2(x, y, theta, ox, oy):
	"""Rotate arrays of coordinates x and y by theta radians about the
	point (ox, oy).

	"""
	s, c = np.sin(theta), np.cos(theta)
	x, y = x - ox, y - oy
	return x * c - y * s + ox, x * s + y * c + oy

def constructWeightMap(truth):
	truth_copy = truth.copy()
	truth_copy[truth==2] = 1
	d1_f = ndimage.distance_transform_edt(truth_copy)
	truth_copy[truth==0] = 1
	truth_copy[truth==1] = 0
	d1_b = ndimage.distance_transform_edt(1-truth)
	d1 = np.maximum(d1_f, d1_b)

	truth_copy = truth.copy()
	truth_copy[truth==2] = 0

	labeled_array, num_features = ndimage.measurements.label(truth_copy, structure=None, output=None)

	tmp = np.empty_like(labeled_array)
	tmp[:] = labeled_array

	d2 = np.empty(labeled_array.shape)
	w_c = np.empty(labeled_array.shape)
	d2[:] = np.inf; w_c[:] = 0

	for ii in range(1,num_features+1):
		tmp[:] = labeled_array
		tmp[np.logical_or(tmp == ii, tmp == 0)] = -1	#fg
		tmp[tmp != -1] = 0
		tmp[tmp == -1] = 1
		#everything except current class and bg goes to bg, current class and bg goes to fg
		d2_ii = ndimage.distance_transform_edt(tmp)
		d2[labeled_array == ii] = d2_ii[labeled_array == ii]

	tmp[:] = 1
	d2b1 = np.empty(labeled_array.shape)
	d2b2 = np.empty(labeled_array.shape)
	d2b1[:] = np.inf; d2b2[:] = np.inf;
	for ii in range(1,num_features+1):
		tmp[labeled_array == ii] = 0
		db_ii = ndimage.distance_transform_edt(tmp)
		d2b1[labeled_array == 0], d2b2[labeled_array == 0] = np.minimum( db_ii[labeled_array == 0], d2b1[labeled_array == 0] ), \
		np.minimum(np.maximum( db_ii[labeled_array == 0], d2b1[labeled_array == 0] ), d2b2[labeled_array == 0] )
		tmp[labeled_array == ii] = 1

	w0, sigma = 20, 5
	d2[labeled_array == 0] = d2b2[labeled_array == 0]

	frac0 = np.sum(truth == 0)/float(truth.size)
	frac1 = np.sum(truth == 1)/float(truth.size)
	w_c[truth == 0] = 0.5/(frac0)
	# w_c[truth == 0] = 3/(2*frac+1)
	w_c[truth == 1] = 0.5/(frac1)
	# w_c[truth == 1] = 1/(2*frac+1)

	wtmap = w_c + w0 * np.exp(-np.square(d1+d2)/(2*sigma**2))
	wtmap[truth==2] = 0
	return wtmap

def cutWithContext(img_full, cB, context, padding = 'symmetric', constant_values = 0):
	# Extra 'context' pixels on all sides.
	size_mask_x, size_mask_y = cB.x_2 - cB.x_1, cB.y_2 - cB.y_1	#MUST BE int
	size_full_x, size_full_y = size_mask_x+2*context, size_mask_y+2*context

	origin_mask_x, origin_mask_y = (size_full_x - size_mask_x)/2, (size_full_y - size_mask_y)/2
	origin_im_x, origin_im_y = origin_mask_x - cB.x_1, origin_mask_y - cB.y_1

	img_tiled = img_full

	if origin_im_x < 0:
		img_tiled = img_tiled[:, -origin_im_x:]
		origin_im_x = 0
	if origin_im_y < 0:
		img_tiled = img_tiled[-origin_im_y:, :]
		origin_im_y = 0
	if origin_im_x + img_tiled.shape[1] > size_full_x:
		img_tiled = img_tiled[:, :size_full_x - origin_im_x]
	if origin_im_y + img_tiled.shape[0] > size_full_y:
		img_tiled = img_tiled[:size_full_y - origin_im_y, :]

	if padding == 'constant':
		img_tiled = np.lib.pad(img_tiled, ((origin_im_y, size_full_y - img_tiled.shape[0] - origin_im_y), 
			(origin_im_x, size_full_x - img_tiled.shape[1] - origin_im_x)), padding, constant_values = constant_values)
	else:
		img_tiled = np.lib.pad(img_tiled, ((origin_im_y, size_full_y - img_tiled.shape[0] - origin_im_y), 
			(origin_im_x, size_full_x - img_tiled.shape[1] - origin_im_x)), padding)
	return img_tiled

def readImage(filename, normalize=False):
	root, ext = os.path.splitext(filename)
	if ext.lower()==".tif" or ext.lower()==".tiff":
		tif = libtiff.TIFF.open(filename, mode='r')
		im = tif.read_image().astype(np.float64)
	else:
		im = cv2.imread(filename, 0).astype(np.float64)
	if normalize:
		im /= np.amax(im)
	return im

def load_image_tiff_3d(path_img):
	'''
	@author Jiri Borovec, Siddhant Ranade

	Modified from scripts/crop_ovary3d_train_images_template_matching.py
	'''
	if not os.path.exists(path_img):
		print(('given image "{}" does not exist!'.format(path_img)))
		return None
	tif = libtiff.TIFF.open(path_img, mode='r')
	img = []
	# to read all images in a TIFF file:
	img = list(tif.iter_images())
	tif.close()
	img = np.array(img)
	print(('image values {} -> {}'.format(np.amin(img), np.amax(img))))
	return img
