%%
% Rodrigo Nava
% Date: November, 2015.
% Return the pixel indexes within a circle with center [X Y] between radio(1) and radio(2).
% ===========================================================================

%%
function curveHistogram = getPixelsFromACurve(image, center, theta, A, B)

[YROWS, XCOLS] = size(image);   % The size of the slide.
[XGRID, YGRID] = meshgrid(1:XCOLS, 1:YROWS); % The grid to compute the indexes.
curveHistogram = zeros(4, 4);
% circle = (XGRID-center(1)).^2 + (YGRID-center(2)).^2 >= radio(1).^2 & ...
%   (XGRID-center(1)).^2 + (YGRID-center(2)).^2 <= radio(2).^2;

for indexCurve = 1:3
    pixels = ((XGRID-center(1))*cos(theta)+(YGRID-center(2))*sin(theta)).^2/(A.^2) + ...
        ((XGRID-center(1))*sin(theta)-(YGRID-center(2))*cos(theta)).^2/(B.^2) > 3*(indexCurve-1)/10 & ...
        ((XGRID-center(1))*cos(theta)+(YGRID-center(2))*sin(theta)).^2/(A.^2) + ...
        ((XGRID-center(1))*sin(theta)-(YGRID-center(2))*cos(theta)).^2/(B.^2) <= 3*indexCurve/10;
    
   curveHistogram(indexCurve,:)=  histcounts(image(pixels),[0,1,2,3,4]);
end






end
