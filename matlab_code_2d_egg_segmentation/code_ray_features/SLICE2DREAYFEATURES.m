%%
% Rodrigo Nava
% Date: October, 2015.
% Computes 2D ray features

%%
STAGE = 2;                       % We only consider "stage 2" slices.
sliceDirectory = dir(['train\segmented_slices\stage', num2str(STAGE), '_knn']);
sliceDirectory(1:2, :) = [];     % Erase '.' and '..' directories.
NUMOFSLICES = size(sliceDirectory, 1);
STEP = pi/32;                                           % This represents 64 rays.
load('eggPoints.mat')   % Load the points inside 'fullEggPoints' and outside 'fullOutsidePoints'.
  
for indexSlice = 1:NUMOFSLICES                       % For each slice segmented with KNN ...
    sliceName = sliceDirectory(indexSlice).name;
    sliceID = sliceName(11:find(sliceName == '.')-1);   % segmented_ - 10 words)
    sliceStringFile = ['train\segmented_slices\stage', num2str(STAGE), '_knn\segmented_', ...
        num2str(sliceID), '.tif'];
    segmentedSlice = double(imread(sliceStringFile));
    [YROWS, XCOLS] = size(segmentedSlice);      % Segmented image size.
    
    rayFeatureImage = segmentedSlice;           % Just to show the rays.
    eggPoints = fullEggPoints{indexSlice, 2};       % The order is [X,Y].
    %eggPoints = fullOutsidePoints{indexSlice, 2};   % The order is {[ID],[X,Y]}.
    
    NUMOFEGGPOINTS = size(eggPoints, 1);            % Sometimes a slice has several marked points
    
    for indexPoint = 1:NUMOFEGGPOINTS
        currentPoint = [eggPoints(indexPoint), ...
            eggPoints(indexPoint + NUMOFEGGPOINTS)];    % Indexing points.
        follicleCellIndexes = zeros(64, 1);             % The indexes of the follicle cells.
        distanceRayFeatures = zeros(64, 1);             % The distances of ray features.
        
        % We set the steps to cover the plane XY.
        angleStep = 1;
        for orientation = 0:STEP:2*pi-STEP
            rayIndexes = getRayPath(segmentedSlice, currentPoint, orientation);
            rayFeatureImage(rayIndexes) = 6;
            distanceRayFeatures(angleStep) = length(rayIndexes);
            follicleCellIndexes(angleStep) = rayIndexes(end); % Keep the last positions.
            angleStep = angleStep + 1;
        end
        
        MEAND = mean(distanceRayFeatures);
        maskDistances = distanceRayFeatures > 1.1*MEAND | distanceRayFeatures < 0.3*MEAND;
        follicleCellIndexes(maskDistances) = [];
        [YPOINTS, XPOINTS] = ind2sub([YROWS, XCOLS], follicleCellIndexes); % 64 points.
        figure(indexSlice), imagesc(rayFeatureImage); hold on;
        set(0,'DefaultFigureWindowStyle','docked');
        ellipse_t = fit_ellipse(XPOINTS, YPOINTS, figure(indexSlice));
    end

end
