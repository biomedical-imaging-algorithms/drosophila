%%
% Rodrigo Nava
% October 19th, 2015.
% Classification of Ray Feature Histograms

%%
rng default; %  Set the seed to replicate experiments
load('3DRayFeaturesInside.mat');     % Loading the distance histograms: training
load('3DRayFeaturesOutside.mat');      % Loading the distance histograms: test

numOfInsidersTrain = size(insideDataTrain{1}, 1);   % Computing the number of vectors
numOfOutsidersTrain = size(outsideDataTrain{1}, 1);
numOfInsidersTest = size(insideDataTest{1}, 1);
numOfOutsidersTest = size(outsideDataTest{1}, 1);

% Constructing the labels for the training dataset.
labeledHistograms = [ones(numOfInsidersTrain, 1); 2*ones(numOfOutsidersTrain, 1)];
trainHistograms = [insideDataTrain{1}; outsideDataTrain{1}];

% Random Forest implementation.
NUMBEROFTREES = 16;
BaggedEnsemble = TreeBagger(NUMBEROFTREES, trainHistograms, labeledHistograms, 'Prior', 'Uniform', ...
    'Method', 'classification', 'OOBPred', 'On');

%%
% ----------------------------------------
testHistograms = [insideDataTest{1}; outsideDataTest{1}]; % Classfication of test histograms.

[RESULTS, scores] = BaggedEnsemble.predict(testHistograms); % Classification
classification = str2double(RESULTS);

% knn_model = fitcknn(train_features, train_labels, 'NumNeighbors', 1, ...
% 'Distance', 'euclidean', 'Standardize', true);
% classification(IDXS) = predict(knn_model, samples);

%classification = multisvm(train, labels, test);
%svm_model = fitcsvm(train, labels, 'Standardize', false);
%classification = predict(svm_model, test);

%%
% ----------------------------------------
resultInside = [sum(classification(1:numOfInsidersTest, :) == 1), ...
    sum(classification(1:numOfInsidersTest, :) == 2)];
resultOutside = [sum(classification(numOfInsidersTest+1:end, :) == 1), ...
    sum(classification(numOfInsidersTest+1:end, :) == 2)];

confusionMatrix = [resultInside; resultOutside];
accuracyRate = sum(diag(confusionMatrix)) / sum(confusionMatrix(:));
confmatrix = cfmatrix2NEW(confusionMatrix', {'Inside', 'Outside'}, 0, 1);

%%
% ----------------------------------------

STAGE = 2;  % Only 2-stage volumes.
volumeDirectory = dir(['train\segmented_stacks\stage', num2str(STAGE)]);
volumeDirectory(1:2, :) = []; % erase '.' and '..' directories
NUMOFVOLS = size(volumeDirectory, 1);

indexVol = 1; % volume of interest.
volumeName = volumeDirectory(indexVol).name;
volumeStringFile = ['train\segmented_stacks\stage', num2str(STAGE), '\', volumeName];

% We use the bioinformatics toolbox to read volumes.
% reader = bfGetReader(volumeStringFile);  % This is for slices
volumeCell = bfOpen3DVolume(volumeStringFile); % This command opens the whole volume
currentVolume = volumeCell{1}{1}; % This is the full volume.

[YROWS, XCOLS, ZSTPS] = size(currentVolume);



% For the current annotated point, a number of extrapoints are randomly created.
extraPoints = getPointsAroundCircle([round(XCOLS/2), round(YROWS/2)], ...
    600, 50, false); % Center[X,Y], radio, NUM, flag
% Check boundaries for valid points.
invalidPoints = find(extraPoints(:,1) > XCOLS | extraPoints(:,1) <= 0 | ...
    extraPoints(:,2) > YROWS | extraPoints(:,2) <= 0);
if ~isempty(invalidPoints)
    extraPoints(invalidPoints,:) = []; % Remove invalid points.
end

coloredVolume = drawPoints3D(currentVolume, extraPoints, BaggedEnsemble);



%%
%--------------------------------------------------
fileName = 'volume.tiff';
for slice = 1:30
    if slice == 1
        imwrite(uint8(coloredVolume(:, :, slice)), fileName, 'Compression', 'none');
    else
        imwrite(uint8(coloredVolume(:, :, slice)), fileName, ...
            'Compression', 'none', 'WriteMode', 'append');
    end
end
