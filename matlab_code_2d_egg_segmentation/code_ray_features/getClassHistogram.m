% --------------------------------------
function [histogram, BINS] = getClassHistogram(values, BINS, display)

YCOLS = size(values, 1);  
NUMOFBINS = size(BINS,2);
histogram = zeros(YCOLS, NUMOFBINS-1);

% Compute histograms with adaptive bins.
for index = 1:YCOLS
    histogram(index,:) = histcounts(values(index,:), BINS);
end

if display
    aj = 0:NUMOFBINS-2;
    bj = 1:NUMOFBINS-1;
    cj = ( aj + bj ) ./ 2; 
    if YCOLS == 1
        figure, bar(cj, histogram, 'hist');
    else
        figure, bar(cj, mean(histogram), 'hist');
    end
    set(gca, 'XTick', cj, 'XTickLabel', round(rangeBins), 'FontSize',8);
    xlabel({'Distance from the center to the borders', '(In Pixels)'});
    ylabel('Frequency');
end

end
