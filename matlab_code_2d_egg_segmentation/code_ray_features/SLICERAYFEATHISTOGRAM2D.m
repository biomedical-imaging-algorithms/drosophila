%%
% Rodrigo Nava
% Date: October 19th, 2015.
% Compute ray feature histograms in 2D with adaptive bins.
% ===========================================================================

%%
rng default;                     % For replication
STAGE = 2;                       % We only consider "stage 2" slices.
sliceDirectory = dir(['train\segmented_slices\stage', num2str(STAGE), '_knn']);
sliceDirectory(1:2, :) = [];     % Erase '.' and '..' directories.
NUMOFSLICES = size(sliceDirectory, 1);

STEP = pi/32;                             % This step represents 64 rays along a circular path.
rayDistancesBySlice = cell(NUMOFSLICES, 1);         % Precompute memory for the distances.
rayLabeledClasesBySlice = cell(NUMOFSLICES, 1);     % Precompute memory for the classes.
circleLabeledClassesBySlice = cell(NUMOFSLICES, 1); % Precompute memory for the classes.

load('eggPoints.mat')   % Load the points inside 'fullEggPoints' and outside 'fullOutsidePoints'.

% ----------------------------------------------------
for indexSlice = 1:NUMOFSLICES
    % For each slice segmented with KNN
    sliceName = sliceDirectory(indexSlice).name;
    sliceID = sliceName(11:find(sliceName == '.')-1);   % segmented_ - 10 words)
    sliceStringFile = ['train\segmented_slices\stage', num2str(STAGE), '_knn\segmented_', ...
        num2str(sliceID), '.tif'];
    
    currentSlice = double(imread(sliceStringFile));     % The current slice.
    [YROWS, XCOLS] = size(currentSlice);                % Size of the slice
    
    %eggPoints = fullEggPoints{indexSlice, 2};    % The order is {[ID],[X,Y]}.
    eggPoints = fullOutsidePoints{indexSlice, 2};   % The order is {[ID],[X,Y]}.
    
    NUMOFEGGPOINTS = size(eggPoints, 1);            % Sometimes a slice has several points.
    rayDistancesByPoint = cell(NUMOFEGGPOINTS, 1);  % Distance matrix.
    rayClassesByPoint = cell(NUMOFEGGPOINTS, 1);    % Distance matrix.
    circleClassesByPoint = cell(NUMOFEGGPOINTS, 1); % Distance matrix.
    
    % ----------------------------------------------------
    for indexPoint = 1:NUMOFEGGPOINTS
        currentPoint = [eggPoints(indexPoint), eggPoints(indexPoint + NUMOFEGGPOINTS)];
        % For the current annotated point, a number of extrapoints are randomly created.
        extraPoints = getPointsAroundCircle(currentPoint, 20, 10, false); % [X,Y], radio, NUM, flag
        % Check boundaries for valid points.
        invalidPoints = find(extraPoints(:,1) > XCOLS | extraPoints(:,1) <= 0 | ...
            extraPoints(:,2) > YROWS | extraPoints(:,2) <= 0);
        if ~isempty(invalidPoints)
            extraPoints(invalidPoints,:) = []; % Remove invalid points.
        end
        
        NUMOFXTPNTS = size(extraPoints, 1);
        rayDistances = cell(NUMOFXTPNTS, 1);
        rayClasses = cell(NUMOFXTPNTS, 1);
        circleClasses = cell(NUMOFXTPNTS, 1);
        
        % ----------------------------------------------------
        for extraPointIndex = 1:NUMOFXTPNTS
            currentXtPnt = [extraPoints(extraPointIndex), extraPoints(extraPointIndex + NUMOFXTPNTS)];
            currentClassXtPnt = zeros(64, 4);   % To save classes of ray features.
            follicleCellIndexs = zeros(1, 64);    % Localization of the corresponding borders.
            currentDistances = zeros(1, 64);
            
            % We set the steps to cover the plane XY.
            angleStep = 1;
            for orientation = 0:STEP:2*pi-STEP
                rayIndexes = getRayPath(currentSlice, currentXtPnt, orientation);
                follicleCellIndexs(angleStep) = rayIndexes(end);    % Keep the last positions.
                currentClassXtPnt(angleStep, :) = histcounts(currentSlice(rayIndexes),[0,1,2,3,4]);
                currentDistances(angleStep) = length(rayIndexes);
                angleStep = angleStep + 1;          % Prepare the index for a new direction.
            end
            
            circleClasses{extraPointIndex} = getCurveHistogram(currentSlice, currentXtPnt, ...
                follicleCellIndexs, currentDistances);
            rayClasses{extraPointIndex} = sum(currentClassXtPnt);
            rayDistances{extraPointIndex} = currentDistances;
        end
        
        % Distances of a marked point and its randomly extra points are save here.
        rayDistancesByPoint{indexPoint} = rayDistances;
        rayClassesByPoint{indexPoint} = rayClasses;
        circleClassesByPoint{indexPoint} = circleClasses;
    end
    
    rayDistancesBySlice{indexSlice} = rayDistancesByPoint;
    rayLabeledClasesBySlice{indexSlice} = rayClassesByPoint;
    circleLabeledClassesBySlice{indexSlice} = circleClassesByPoint;
end

%%
% ===========================================================================
% Compute histograms with adaptive bins.

load('2DRayFeaturesInside.mat');
NUMOFSLICES = length(InRayDistancesBySlice);
indata = cell(2,1);
step = 1;
for slice = 1:NUMOFSLICES
    NUMOFPOINTS = length(InRayDistancesBySlice{slice});
    
    for point = 1: NUMOFPOINTS
        [InHistogramDistances, InRangeBinsDistances] = getNonUniformHistogram(...
            cell2mat(InRayDistancesBySlice{slice}{point}), 16, false);  % 16 is the number of bins
        InHistogramClasses  = cell2mat(InRayLabeledClasesBySlice{slice}{point});
     
        inCircles = cell2mat(InCircleLabeledClassesBySlice{slice}{point});
           indata{step} = inCircles;
          step = step +1;
        inCircleR1 = inCircles(1:4:end,:); 
        inCircleR2 = inCircles(2:4:end,:); 
        inCircleR3 = inCircles(3:4:end,:); 
        inCircleR4 = inCircles(4:4:end,:); 
    end
end

%%
load('2DRayFeaturesOutside.mat');
NUMOFSLICES = length(OutRayDistancesBySlice);
outdata = cell(2,1);
step = 1;
for slice = 1:NUMOFSLICES
    NUMOFPOINTS = length(OutRayDistancesBySlice{slice});
    
    for point = 1: NUMOFPOINTS
        [OutHistogramDistances, OutRangeBinsDistances] = getNonUniformHistogram( ...
            cell2mat(OutRayDistancesBySlice{slice}{point}), 16, false);  % 16 is the number of bins
          
        OutHistogramClasses  = cell2mat(OutRayLabeledClasesBySlice{slice}{point});
       
        outCircles = cell2mat(OutCircleLabeledClassesBySlice{slice}{point});
           outdata{step} = outCircles;
        step = step +1;
        outCircleR1 = outCircles(1:4:end,:); 
        outCircleR2 = outCircles(2:4:end,:); 
        outCircleR3 = outCircles(3:4:end,:); 
        outCircleR4 = outCircles(4:4:end,:); 
    end
end
