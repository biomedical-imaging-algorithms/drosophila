%%
% Rodrigo Nava
% Date: November 2015.
% Computes a non-uniform histogram. It display is set to true shows the mean histogram of values.

%%
function [histogram, rangeBins] = getNonUniformHistogram(values, NUMOFBINS, display)

YCOLS = size(values, 1);                        % Nuber of vectors. 
rangeBins = round([0 (1.6).^(1:NUMOFBINS)]);    % The non-uniform number of bins.
histogram = zeros(YCOLS, NUMOFBINS);

for index = 1:YCOLS
     histogram(index,:) = histcounts(values(index,:), rangeBins);
end

if display
    aj = 0:NUMOFBINS-1;
    bj = 1:NUMOFBINS;
    cj = ( aj + bj ) ./ 2; 
    if YCOLS == 1
        figure, bar(cj, histogram, 'hist');
    else
        figure, bar(cj, mean(histogram), 'hist');
    end
    set(gca, 'XTick', cj, 'XTickLabel', round(rangeBins), 'FontSize',8);
    xlabel({'Distance from the center to the borders', '(In Pixels)'});
    ylabel('Frequency');
end

end

%{
iso_histo2 = zeros(size(rayDistances,1), 15);
iso_histo3 = zeros(size(rayDistances1,1), 15);
pp = linspace(1,1036,17);
pp = round(logspace(log10(1),log10(2000),17));
iso_histo2 = zeros(size(rayDistances,1), 15);
for i = 1:size(rayDistances,1)
     iso_histo2(i,:) = histcounts(rayDistances(i,:),pp);
    iso_histo3(i,:) = histcounts(rayDistances1(i,:),pp);
end

aj = pp(1:end-1);     % bins lower edge
bj = pp(2:end);       % bins upper edge

aj = [0:14]
bj = [1:15]
cj = ( aj + bj ) ./ 2; 
%# plot histogram
figure(1), bar(cj,[mean(iso_histo2)' mean(iso_histo3)'], 'hist')
%set(gca, 'XTick',round(pp(1:4:end)), 'XLim',[pp(1) pp(end-1)], 'FontSize',8)
set(gca, 'XTick',cj, 'XTickLabel', round(pp()), 'FontSize',8)
%xlabel({'Distance from the center to the borders', '(In Pixels)'})
%ylabel('Frequency')

%%
iso_histo2 = zeros(size(rayDistances,1), 16);
iso_histo3 = zeros(size(rayDistances1,1), 16);
pp1 = round(logspace(log10(1),log10(2000),17));
%pp = linspace(1,1036,65);
for i = 1:size(rayDistances,1)
    iso_histo2(i,:) = histcounts(rayDistances(i,:),pp);
        iso_histo3(i,:) = histcounts(rayDistances1(i,:),pp);
end
%mu = 0;
%sigma = 1;
%pd = makedist('Normal', mu, sigma);
%lambda = 2;
%pd = makedist('Poisson',lambda);
%plot(cdf(pd,mean(iso_histo2), 'upper'));

%figure, stem(pp(1:end-1), mean([iso_histo;iso_histo1]), 'b');
aj = pp(1:end-1);     %# bins lower edge
bj = pp(2:end);       %# bins upper edge
cj = ( aj + bj ) ./ 2;      %# bins center
%# plot histogram
figure(2), bar( [mean(iso_histo2)' mean(iso_histo3)'],'hist');
set(gca, 'XTick' ,round(pp(1:4:end)), 'XLim',[pp(1) pp(end-1)], 'FontSize',8)
xlabel({'Distance from the center to the borders', '(In Pixels)'})
ylabel('Frequency')

%xlim([1 1036]);

%errorbar(mean(iso_histo1), mean(iso_histo1),'b')
%bar(mean(iso_histo1),'r');
%errorbar(mean(iso_histo1),std(iso_histo1), 'bo')

%%
iso_histo2 = zeros(size(ray_distances2,1),128);
pp = linspace(1,1036,129);
for i = 1:size(ray_distances2,1)
    iso_histo2(i,:) = histcounts(ray_distances2(i,:),pp);
end
%figure, stem(pp(1:end-1), mean(iso_histo1), 'b');
aj = pp(1:end-1);     %# bins lower edge
bj = pp(2:end);       %# bins upper edge
cj = ( aj + bj ) ./ 2;      %# bins center
%# plot histogram
figure(3), bar(cj, mean(iso_histo2),'hist')
set(gca, 'XTick' ,round(pp(1:4:end)), 'XLim',[pp(1) pp(end-1)], 'FontSize',8)
xlabel({'Distance from the center to the borders', '(In Pixels)'})
ylabel('Frequency')
%errorbar(mean(iso_histo2), mean(iso_histo2),'b')
%}
