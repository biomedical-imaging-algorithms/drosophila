%%
% Rodrigo Nava
% Date: October 19th, 2015.
% Compute points inside a circle randomly.

%%
function points = getPointsAroundCircle(center, radio, NUMOFPOINTS, display)

t = 2*pi*rand(NUMOFPOINTS, 1);
r1 = randi([10 radio], 1, NUMOFPOINTS)'; % 1 for only one column
r2 = randi([10 radio-5], 1, NUMOFPOINTS)'; % 1 for only one column
X = round(center(1) - r1.*cos(t));
Y = round(center(2) - r2.*sin(t));

points = unique([X, Y], 'rows');

%%
if display % Just for displaying porposes.
    plot(X, Y, '*', 'MarkerSize', 3)
    fontSize = 20;
    xlabel('X', 'FontSize', fontSize);
    ylabel('Y', 'FontSize', fontSize);
    title('Random Locations Within a Circle', 'FontSize', fontSize);
end

end
