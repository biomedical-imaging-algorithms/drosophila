%%
% Rodrigo Nava
% Date: November, 2015.
% Return the pixel indexes within a circle with center [X Y] between radio(1) and radio(2).
% ===========================================================================

%%
function pixels = getPixelsFromACircle(image, center, radio)

[YROWS, XCOLS] = size(image);   % The size of the slide.
[XGRID, YGRID] = meshgrid(1:XCOLS, 1:YROWS); % The grid to compute the indexes.

circle = (XGRID-center(1)).^2 + (YGRID-center(2)).^2 >= radio(1).^2 & ...
    (XGRID-center(1)).^2 + (YGRID-center(2)).^2 <= radio(2).^2;

 
pixels = find(circle == 1); % Getting the indexes.

end
