%%
% Rodrigo Nava
% Date: November, 2015.
%

%%
STAGE = 2;  % Only 2-stage volumes.
volumeDirectory = dir(['train\segmented_stacks\stage', num2str(STAGE)]);
volumeDirectory(1:2, :) = []; % erase '.' and '..' directories
NUMOFVOLS = size(volumeDirectory, 1);

XYSTEP = pi/24;     % Steps on the XY plane (theta) for 128 rays.
ZXSTEP = pi/8;      % Steps om the ZX plane (phi) for 64 rays.
vectorDistance = zeros(NUMOFVOLS, 48*16); % Precompute a size
rayFeatureSample = 1;

load('eggPoints.mat');          % Load the [X Y] points
load('eggPointsZplane.mat');    % Load the Z points.
randomPoints = cell(21,3);      % Cell to save random points.

for indexVol = 10:NUMOFVOLS
    volumeName = volumeDirectory(indexVol).name;
    volumeStringFile = ['train\segmented_stacks\stage', num2str(STAGE), '\', volumeName];
    
    % We use the bioinformatics toolbox to read volumes.
    % reader = bfGetReader(volumeStringFile);  % This is for slices
    volumeCell = bfOpen3DVolume(volumeStringFile); % This command opens the whole volume
    currentVolume = volumeCell{1}{1}; % This is the full volume.
    [YROWS, XCOLS, ZSTPS] = size(currentVolume);
    % displayRayFeatures = zeros(YROWS, XCOLS, ZSTPS); % This is the volume to save for displaying
    displayRayFeatures = 10*currentVolume;
    
    %% ------------------------------------------------------------------------------
    eggPoints = fullOutsidePoints{indexVol, 2}; % The order is [X,Y].
    % eggPoints = fullEggPoints{indexVol, 2}; % The order is [X,Y].
    NUMOFEGGPOINTS = size(eggPoints, 1); % Sometimes a slice has several marked points
    
    for indexPoint = 1:NUMOFEGGPOINTS
        currentPoint = [eggPoints(indexPoint), ...
            eggPoints(indexPoint + NUMOFEGGPOINTS)]; % Indexing points.
        
        % For the current annotated point, a number of extrapoints are randomly created.
        extraPoints = getPointsAroundCircle(currentPoint, 35, 20, false); % Center[X,Y], radio, NUM, flag
        % Check boundaries for valid points.
        invalidPoints = find(extraPoints(:,1) > XCOLS | extraPoints(:,1) <= 0 | ...
            extraPoints(:,2) > YROWS | extraPoints(:,2) <= 0);
        if ~isempty(invalidPoints)
            extraPoints(invalidPoints,:) = []; % Remove invalid points.
        end
        
        %--------------------------------------------------------
        randomPoints{indexVol, indexPoint} = extraPoints;
        
        NUMOFXTPNTS = size(extraPoints, 1);
        for extraPointIndex = 1:1%NUMOFXTPNTS % Computing ray features for extra points
            currentXtPnt = [extraPoints(extraPointIndex), ...
                extraPoints(extraPointIndex + NUMOFXTPNTS)]; % Indexing points.
            
            % We set the steps to cover the plane XY.
            angleStep = 1;
            for theta = 0:XYSTEP:2*pi-XYSTEP
                % for phi = 0:ZXSTEP:2*pi-ZXSTEP
                % This is an improvement to cover slices in a  better way.
                for phi = [0:ZXSTEP:pi/4, pi/4+ZXSTEP/2:ZXSTEP/2:3*pi/4, 3*pi/4+ZXSTEP:ZXSTEP:5*pi/4, ...
                        5*pi/4+ZXSTEP/2:ZXSTEP/2:7*pi/4, 7*pi/4+ZXSTEP:ZXSTEP:2*pi-ZXSTEP]
                    rayIndexes3D = getRayPath3D(currentVolume, [currentXtPnt, ZFOCUS(indexVol)], ...
                        theta, phi);
                    %-----------------------------------------------------------
                    displayRayFeatures(rayIndexes3D) = 255;  % For visualization
                    vectorDistance(rayFeatureSample, angleStep) = length(rayIndexes3D);
                    %-----------------------------------------------------------
                    angleStep = angleStep +1;
                end
            end
            
            rayFeatureSample = rayFeatureSample + 1; % Prepare the index for a new point.
        end
    end
    
    % figure, imshow(rayFeatureImage, []);
    % Compute histograms with adaptive bins.
    [histogram, rangeBins] = getNonUniformHistogram(vectorDistance, 16, false); % 16 is the number of bins
    
    % To free memory
    % randomPoints = randomPoints';
    % valid_blocks = randomPoints(~cellfun(@isempty, randomPoints));
    
    %%
    %{
    %--------------------------------------------------
    fileName = 'volume.tiff';
    for slice = 1:ZSTPS(end)
        if slice == 1
            imwrite(uint8(displayRayFeatures(:, :, slice)), fileName, 'Compression', 'none');
        else
            imwrite(uint8(displayRayFeatures(:, :, slice)), fileName, ...
                'Compression', 'none', 'WriteMode', 'append');
        end
    end
    %}
    
end
