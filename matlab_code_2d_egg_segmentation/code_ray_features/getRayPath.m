%%
% Rodrigo Nava
% Date: December 4th, 2014
% Computes the indexes from the 'center' to the closest 'border' in the direction of 'orientation'

%%
function  rayIndexes = getRayPath(image, center, orientation)

[YROWS, XCOLS] = size(image);
VALUE_BORDER = 1;       % This is the value used to stop the ray.
VALUE_BACKGROUND = 0;   % This is the value used to check holes.

% Faster version. First, the radius is constructed and then projected.
rho = round(sqrt(XCOLS^2 + YROWS^2));
x = center(1) + round(cos(orientation) * linspace(0, rho, rho+1));
y = center(2) - round(sin(orientation) * linspace(0, rho, rho+1));

MAXINDEX = min([find(y < 1, 1, 'first'), find(y > YROWS, 1, 'first'), ...
    find(x < 1, 1, 'first'), find(x > XCOLS, 1, 'first')]); % Checking boundaries

% Si 'MAXINDEX' esta vacio, quiere decir que es 45� y no se acortan X,Y, otro caso se
% proyecta sobre el �ngulo y se hace mas peque�o.
if ~isempty(MAXINDEX)
    y = y(1:MAXINDEX-1);
    x = x(1:MAXINDEX-1);
end

indexes = sub2ind([YROWS, XCOLS], y, x);
valuesOfIndexes = image(indexes);
borderIndex = find(valuesOfIndexes == VALUE_BORDER, 1, 'first'); % Primer borde

if isempty(borderIndex)
    % Can I improve this with the mean distance ...???
    rayIndexes = indexes; % Si no hay borde regresas todos los indices
else
    % Patch. This code avois holes.
    backgroundIndex = find(valuesOfIndexes(borderIndex+1:end) == VALUE_BACKGROUND, 1, 'first');
    if isempty(backgroundIndex)
        lastBorder = find(valuesOfIndexes == VALUE_BORDER, 1, 'last');
        rayIndexes = indexes(1:lastBorder);
    else
        newIndex = backgroundIndex + borderIndex;
        rayIndexes = indexes(1:newIndex-1);
        % rayIndexes = indexes(1:borderIndex);  % Si hay borde regresa solo los indices entre centro y borde
    end
end

end

%{
if orientation >= 0 && orientation <= pi/2
    rho = round(sqrt((center(1)-NCOLS)^2 + (center(2)-1)^2));
    %rho = round(sqrt((center(1)-1)^2 + (NCOLS-center(2))^2));
    x = center(1) + round(cos(orientation) * linspace(0, rho, rho+1));
    y = center(2) - round(sin(orientation) * linspace(0, rho, rho+1));
   
    MAXINDEX = min([find(y < 1, 1, 'first'), find(x > NCOLS, 1, 'first')]);
    
elseif orientation > pi/2 && orientation <= pi
    rho = round(sqrt((center(1)-1)^2 + (center(2)-1)^2));
    %rho = round(sqrt((center(1)-1)^2 + (center(2)-1)^2));
    x = center(1) + round(cos(orientation) * linspace(0, rho, rho+1));
    y = center(2) - round(sin(orientation) * linspace(0, rho, rho+1));
    
    MAXINDEX = min([find(y < 1, 1, 'first'), find(x < 1, 1, 'first')]);
    
elseif orientation > pi && orientation <= 3*pi/2
    rho = round(sqrt((center(1)-1)^2 + (center(2)-MROWS)^2));
    %rho = round(sqrt((MROWS-center(1))^2 + (center(2)-1)^2));
    x = center(1) + round(cos(orientation) * linspace(0, rho, rho+1));
    y = center(2) - round(sin(orientation) * linspace(0, rho, rho+1));
   
    MAXINDEX = min([find(y > MROWS, 1, 'first'), find(x < 1, 1, 'first')]);
    
else
    rho = round(sqrt((center(1)-NCOLS)^2 + (center(2)-MROWS)^2));
    %rho = round(sqrt((NCOLS-center(1))^2 + (NCOLS-center(2))^2));
    x = center(1) + round(cos(orientation) * linspace(0, rho, rho+1));
    y = center(2) - round(sin(orientation) * linspace(0, rho, rho+1));
    
    MAXINDEX = min([find(y > MROWS, 1, 'first'), find(x > NCOLS, 1, 'first')]);
end
%}



