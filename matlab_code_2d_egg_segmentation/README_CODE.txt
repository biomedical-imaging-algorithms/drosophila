README FILE!!!

/datagrid/Medical/microscopy/drosophila/rnava/train ->

mask/                        ground-truth of 2D isolated eggs cropped according to "all_ovary_image_info_for_prague.txt" The values are: Background=0, follicle cells=1;  nurse cells=2; cytoplasm = 3.
ovary/                        2D isolated egg chambers. The format file is UINT16
segmented_eggs/       isolated eggs segmented using supertextons with K-NN and Random Forest
segmented_slices/     full slices segmented with supertextons with K-NN and Random Forest. Every slice corresponds to the focused plane indicated in "all_ovary_image_info_for_prague.txt"
segmented_stacks/    3D volumes segmented with supertextons and K-NN
selected_images/       original isolated eggs: Two channels
selected_stacks/        original volumes, the egg chambers were extracted according "all_ovary_image_info_for_prague.txt"

% --------------------------------
This file describes how to run the code published in Nava R and Kybic J. "Supertexton-based segmentation in early Drosophila oogenesis," IEEE International Conference on Image Processing (ICIP), pp. 2656-2659, 2015.

Segmentation of 2D Drosophila egg chambers.

* Training images are stored at /train/ovary/stage2
* Masks are stored at /train/mask/stage2

TEST.m -> This is the main file. The first part of the code is used to generate textons and train the classifier.

getLabeledFeatures -> It computes features
createTextons -> It assigns features to textons

findLabels -> It classifies textons using leave-one-out




