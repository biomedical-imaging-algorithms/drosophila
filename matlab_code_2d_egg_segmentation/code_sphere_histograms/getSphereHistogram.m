%%
% Rodrigo Nava
% Date: December 8th, 2015
%

%%  ===========================================================================
function sphereHistogram = getSphereHistogram(volume, center, RADIO, NUMOFSPHERES)

[YROWS, XCOLS, ZSTEPS] = size(volume);                          % The size of the volume
[XGRID, YGRID, ZGRID] = meshgrid(1:XCOLS, 1:YROWS, 1:ZSTEPS);   % 
sphereHistogram = zeros(NUMOFSPHERES, 4);                       % Four classes
% showVolume = zeros(YROWS, XCOLS, ZSTEPS);
% showVolume = volume;

for indexCircle = 1:NUMOFSPHERES
    spherePixels = ...
        (XGRID-center(1)).^2 + (YGRID-center(2)).^2 + (ZGRID-center(3)).^2/(0.2)^2  >= ...
        ((indexCircle-1)*RADIO).^2 & ...
        (XGRID-center(1)).^2 + (YGRID-center(2)).^2 + (ZGRID-center(3)).^2/(0.2)^2  < ...
        (indexCircle*RADIO).^2;
    % showVolume(spherePixels) = indexCircle * 25;
    sphereHistogram(indexCircle, :) =  histcounts(volume(spherePixels),[0,1,2,3,4]); 
    % 0=Background, 1= Follicle cells, 2 = Nurse cells, and 3 = Cytoplasm. 
end

%% ----------------------------------------------------
%{
if (FLAG)
    fileName = 'volume.tiff';
    for slice = 1:ZSTEPS(end)
        if slice == 1
            imwrite(uint8(showVolume(:, :, slice)), fileName, 'Compression', 'none');
        else
            imwrite(uint8(showVolume(:, :, slice)), fileName, ...
                'Compression', 'none', 'WriteMode', 'append');
        end
    end
end
%}
end
