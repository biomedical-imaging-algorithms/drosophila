%%
% Rodrigo Nava
% Date: December 4th, 2014
%

%%
function curveHistogram = getCurveHistogram(image, center, borders, distances)

[YROWS, XCOLS] = size(image);
MEAND = mean(distances);
maskDistances = distances > 1.1*MEAND | distances < 0.3*MEAND;
borders(maskDistances) = [];
[YPOINTS, XPOINTS] = ind2sub([YROWS, XCOLS], borders); % 64 points.

ellipse_t = fit_ellipse(XPOINTS, YPOINTS); % , figure(1));
if ~isempty(ellipse_t)
    A = ellipse_t.a;
    B = ellipse_t.b;
    theta = -ellipse_t.phi;
    %XX=ellipse_t.X0_in;
    %YY=ellipse_t.Y0_in;
    [YROWS, XCOLS] = size(image);     % The size of the slide.
    [XGRID, YGRID] = meshgrid(1:XCOLS, 1:YROWS); % The grid to compute the indexes.
    curveHistogram = zeros(4, 4);
    % circle = (XGRID-center(1)).^2 + (YGRID-center(2)).^2 >= radio(1).^2 & ...
    %   (XGRID-center(1)).^2 + (YGRID-center(2)).^2 <= radio(2).^2;
  
    for indexCurve = 1:4
        pixels = ((XGRID-center(1))*cos(theta)+(YGRID-center(2))*sin(theta)).^2/(A.^2) + ...
            ((XGRID-center(1))*sin(theta)-(YGRID-center(2))*cos(theta)).^2/(B.^2) > 3*(indexCurve-1)/10 & ...
            ((XGRID-center(1))*cos(theta)+(YGRID-center(2))*sin(theta)).^2/(A.^2) + ...
            ((XGRID-center(1))*sin(theta)-(YGRID-center(2))*cos(theta)).^2/(B.^2) <= 3*indexCurve/10;
       
        curveHistogram(indexCurve,:)=  histcounts(image(pixels),[0,1,2,3,4]);
    end
else
    
    curveHistogram = [];
end
end
