%%
% Rodrigo Nava
% Date: November, 2015.
% Compute ray feature histograms in 3D with adaptive bins.
% ===========================================================================

%%
rng default;                        % For replication
STAGE = 2;                          % Only 2-stage volumes.
volumeDirectory = dir(['train\segmented_stacks\stage', num2str(STAGE)]);
volumeDirectory(1:2, :) = [];       % erase '.' and '..' directories
NUMOFVOLS = size(volumeDirectory, 1);

XYSTEP = pi/24;                                 % Steps in the XY plane (theta) for 48 rays.
ZXSTEP = pi/8;                                  % Steps in the ZX plane (phi) for 24 rays.
rayDistanceByVolume = cell(NUMOFVOLS, 1);       % Precompute memory for distances.
rayLabeledClassesByVolume = cell(NUMOFVOLS, 1);  % Precompure memory for classes.
randomPointsByVolume = cell(NUMOFVOLS, 1);      % Precompute memory for random points.
circleClassesByVolume = cell(NUMOFVOLS, 1);  

load('eggPoints.mat');          % Load the points inside 'fullEggPoints' and
load('eggPointsZplane.mat');    % outside 'fullOutsidePoints' in [X Y] format.
% Load the Z points. (Focused planes).

% ----------------------------------------------------
for indexVol = 1:NUMOFVOLS
    % For each volume segmented with KNN
    volumeName = volumeDirectory(indexVol).name;
    volumeStringFile = ['train\segmented_stacks\stage', num2str(STAGE), '\', volumeName];
    
    % We use the bioinformatics toolbox to read volumes.
    % reader = bfGetReader(volumeStringFile);           % This is for slices
    volumeCell = bfOpen3DVolume(volumeStringFile);      % This command opens the whole volume
    currentVolume = volumeCell{1}{1};                   % This is the full volume.
    [YROWS, XCOLS, ZSTPS] = size(currentVolume);
    
    % Display 3D Ray features or save the volume for displaying with other tool.
    displayRayFeatures = currentVolume;
    
    %eggPoints = fullOutsidePoints{indexVol, 2};  % The order is [X,Y].
    eggPoints = fullEggPoints{indexVol, 2};    % The order is [X,Y].
    NUMOFEGGPOINTS = size(eggPoints, 1); % Sometimes a slice has several marked points
    
    rayDistancesByPoint = cell(NUMOFEGGPOINTS, 1);  % Saving memory for distances.
    rayClassesByPoint = cell(NUMOFEGGPOINTS, 1);    % Saving memory for classes.
    randomPointsByPoint = cell(NUMOFEGGPOINTS, 1);  % Saving memory for points.
    circleClassesByPoint = cell(NUMOFEGGPOINTS, 1);
    
    % ----------------------------------------------------
    for indexPoint = 1:NUMOFEGGPOINTS
        currentPoint = [eggPoints(indexPoint), eggPoints(indexPoint + NUMOFEGGPOINTS)];
        % For the current annotated point, a number of extrapoints are randomly created.
        extraPoints = getPointsAroundCircle(currentPoint, 35, 20, false); % Center[X,Y], radio, NUM, flag
        % Check boundaries for valid points.
        invalidPoints = find(extraPoints(:,1) > XCOLS | extraPoints(:,1) <= 0 | ...
            extraPoints(:,2) > YROWS | extraPoints(:,2) <= 0);
        if ~isempty(invalidPoints)
            extraPoints(invalidPoints,:) = []; % Remove invalid points.
        end
        
        NUMOFXTPNTS = size(extraPoints, 1);
        rayDistances = zeros(NUMOFXTPNTS, 24*48);
        rayClasses = zeros(NUMOFXTPNTS, 4);
        randomPointsByPoint{indexPoint} = extraPoints;
        circleClassesXtPnt = cell(NUMOFXTPNTS, 1);
        
        % --------------------------------------------------------
        for extraPointIndex = 1:NUMOFXTPNTS
            currentXtPnt = [extraPoints(extraPointIndex), extraPoints(extraPointIndex + NUMOFXTPNTS)];
            currentClassXtPnt = zeros(24*48, 4); % To save classes of ray features.
          
            
            % We set the steps to cover the plane XY.
            angleStep = 1;
            for theta = 0:XYSTEP:2*pi-XYSTEP
                % for phi = 0:ZXSTEP:2*pi-ZXSTEP
                % This is an improvement to cover slices ina  better way.
                for phi = [0:ZXSTEP:pi/4, pi/4+ZXSTEP/2:ZXSTEP/2:3*pi/4, 3*pi/4+ZXSTEP:ZXSTEP:5*pi/4, ...
                        5*pi/4+ZXSTEP/2:ZXSTEP/2:7*pi/4, 7*pi/4+ZXSTEP:ZXSTEP:2*pi-ZXSTEP]
                    
                    rayIndexes3D = getRayPath3D(currentVolume, [currentXtPnt, ZFOCUS(indexVol)], ...
                        theta, phi);
                    displayRayFeatures(rayIndexes3D) = 255; % Painting the visual path.
                    
                    
                    %
                    
                    currentClassXtPnt(angleStep, :) = histcounts(currentVolume(rayIndexes3D),[0,1,2,3,4]);
                     
                    rayDistances(extraPointIndex, angleStep) = length(rayIndexes3D);
                    angleStep = angleStep +1;
                end
            end
            % Histogram of one point over all directions.
            rayClasses(extraPointIndex, :) = sum(currentClassXtPnt);
            circleClassesXtPnt{extraPointIndex} = getSphereHistogram(currentVolume, [currentXtPnt, ZFOCUS(indexVol)], ...
                5, 4, false);
        end
        % Distances and classes of all points are saving here.
        rayDistancesByPoint{indexPoint} = rayDistances;
        rayClassesByPoint{indexPoint} = rayClasses;
        circleClassesByPoint{indexPoint} = circleClassesXtPnt;
    end
    % Distances and classes of all volumes are saving here.
    rayDistanceByVolume{indexVol} = cell2mat(rayDistancesByPoint);
    rayLabeledClassesByVolume{indexVol} = cell2mat(rayClassesByPoint);
    randomPointsByVolume(indexVol) = {randomPointsByPoint};
    circleClassesByVolume{indexVol} = circleClassesByPoint;
end

%%
% figure, imshow(rayFeatureImage, []);
% Compute histograms with adaptive bins.
[histogram, rangeBins] = getNonUniformHistogram(cell2mat(rayDistanceByVolume), ...
    16, false); % 16 is the number of bins

% Compute classe histograms.
histogramClasses  = cell2mat(rayLabeledClassesByVolume);

% To free memory
% randomPoints = randomPoints';
% valid_blocks = randomPoints(~cellfun(@isempty, randomPoints));

%{
%%
%--------------------------------------------------
fileName = 'volume.tiff';
for slice = 1:ZSTPS(end)
    if slice == 1
        imwrite(uint8(displayRayFeatures(:, :, slice)), fileName, 'Compression', 'none');
    else
        imwrite(uint8(displayRayFeatures(:, :, slice)), fileName, ...
            'Compression', 'none', 'WriteMode', 'append');
    end
end
%}

%{
%%
NAME = 'volumenseg.tiff';
reader = bfGetReader(NAME);
volumenCell = bfOpen3DVolume(NAME);
volume = volumenCell{1}{1};
L1 = volume;
ROWS=1035; COLS=657; z=30; % meshgrid(1:1035,1:657,1:30);

index =1;
%[~, eggPoints] = fullEggPoints{index, :}; % Isolated Eggs points
[~, eggPoints] = fullOutsidePoints{index, :}; % Isolated Eggs points

NUMOFPOINTS = size(eggPoints, 1);
numdepunt = 1;
valordedistancia = 1;
for pointValues = 1 : NUMOFPOINTS
    points = getPointsAroundCircle([eggPoints(pointValues), eggPoints(pointValues + NUMOFPOINTS)], ...
        40, 30, false); %center, radio, NUM, flag
    n = find(points(:,1) > ROWS | points(:,1) <= 0| points(:,2) > COLS | points(:,2) <= 0);
    if ~isempty(n)
        points(n,:) = [];
    end
    NUMOFPS = size(points, 1);
    for numP = 1:NUMOFPS
        
        for theta = 0:pi/64:2*pi-(pi/64)
            for phi = 0:pi/16:2*pi-pi/16
                rayIndexes3D = getRayPath3D(volume, [points(numP), points(numP+NUMOFPS), 19], theta, phi);
                %L(rayIndexes3D) = 10;
                distancias2(numdepunt, valordedistancia) = length(rayIndexes3D);
                valordedistancia = valordedistancia +1;
            end
        end
        valordedistancia = 1;
        numdepunt = numdepunt +1;
    end
end
toc;


%%
fileName = 'volumenresl1.tiff';
for i = 1:30
    imwrite(uint8(L1(:,:,i)), fileName, 'Compression', 'none', 'WriteMode', 'append');
end


%%
scatter3(x(:),y(:),z(:),volume(:))



%%


AU42-39-48-60-73_f0019.tif
STAGE = 2;
%filesTest = dir(['train\ovary\stage', num2str(STAGE)]);
filesTest = dir(['train\slice\stage', num2str(STAGE)]);
filesTest(1:2, :) = []; % erase '.' and '..' directories

tagstruct.Compression = Tiff.Compression.None;
tagstruct.SampleFormat = Tiff.SampleFormat.UInt;
tagstruct.Photometric = Tiff.Photometric.MinIsBlack;
tagstruct.BitsPerSample = 8;
tagstruct.SamplesPerPixel = 1;
tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;



background = vertcat(textonData.background);
border = vertcat(textonData.border);
nurse = vertcat(textonData.nurse);
cytoplasm = vertcat(textonData.cyto);

[mrows, ncols] = size(image);

[L0, ~] = size(background);
[L1, ~] = size(border);
[L2, ~] = size(nurse);
[L3, ~] = size(cytoplasm);

trainFeatures = [background; border; nurse; cytoplasm];
labelTraining = [zeros(L0, 1); ones(L1, 1); 2*ones(L2, 1); 3*ones(L3,1)];
tic,
modelo = TreeBagger(18, trainFeatures, labelTraining, 'Prior', 'Uniform',...
    'Method', 'classification');
toc;
%%
for fileIndex = 17:length(filesTest)
    fileName = filesTest(fileIndex).name;
    %ID = fileName(6:find(fileName == '.')-1);
    %ovaryStringFile = ['train\ovary\stage', num2str(STAGE), '\train', num2str(ID), '.tif'];
    ID = fileName(7:find(fileName == '.')-1);
    ovaryStringFile = ['train\slice\stage', num2str(STAGE), '\slice_', num2str(ID), '.tif'];
    ovary = imread(ovaryStringFile); % ovary is uint16
    ovary = double(ovary(:, :, 1));
    
    tic;
    segmentedImage = eggSegmentation(ovary, modelo, 1);
    toc;
    
    tagstruct.ImageLength = size(ovary,1);
    tagstruct.ImageWidth = size(ovary,2);
    
    image_tiff = Tiff(['segmented_', num2str(ID), '.tif'],'w');
    image_tiff.setTag(tagstruct)
    image_tiff.write(uint8(segmentedImage));
    image_tiff.close();
    
end
%}
