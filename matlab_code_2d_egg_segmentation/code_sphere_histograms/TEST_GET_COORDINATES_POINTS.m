%%
% Rodrigo Nava
% Date: February, 2016.
% ..
% ===========================================================================

%%
%%rng default;                        % For replication purposes.
STAGE = 2;
fileTrain = dir(['train\slice\stage', num2str(STAGE)]);
fileTrain(1:2, :) = []; % Erase '.' and '..' directories

%load('eggPoints.mat');          % Load the points inside 'fullEggPoints' and
% outside 'fullOutsidePoints' in [X Y] format.
%load('eggPointsZplane.mat');    % Load the Z points. (Focused planes).

%%
for fileIndex = 28:length(fileTrain)
    fileName = fileTrain(fileIndex).name;
    NUMBER = fileName(7 : find(fileName == '.')-1);
    
    tic,
    ovaryStringFile = ['train\slice\stage', num2str(STAGE), '\slice_', NUMBER, '.tif'];
    ovary = imread(ovaryStringFile); % ovary uint16
    ovary = double(ovary(:, :, 1)); % keep only with green channel
    set(0,'DefaultFigureWindowStyle','docked')
    figure(str2num(NUMBER)), imagesc(ovary)
end
