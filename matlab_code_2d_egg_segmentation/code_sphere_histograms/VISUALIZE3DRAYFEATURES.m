%%
% Rodrigo Nava
% Date: November, 2015.
% Script to visualize ray features in 3D

%%
volume = 0.1*ones(80, 80, 50);      % Create an artificial volume data
[XCOLS, YROWS, ZSTEPS] = meshgrid(1:80, 1:80, 1:50);
XYSTEP = pi/24;     % Steps on the XY plane (theta) for 128 rays.
ZXSTEP = pi/8;     % Steps om the ZX plane (phi) for 64 rays.

displayRayFeatures = volume;    % For visualization
distances = 1;
vectorDistance = zeros(1, 16);  % Save all the distances and compute a histogram

for theta = 0:XYSTEP:2*pi-XYSTEP
    %for phi = 0:ZXSTEP:2*pi-ZXSTEP
    % This is an improvement to cover slices ina  better way.
    for phi = [0:ZXSTEP:pi/4, pi/4+ZXSTEP/2:ZXSTEP/2:3*pi/4, 3*pi/4+ZXSTEP:ZXSTEP:5*pi/4, ...
            5*pi/4+ZXSTEP/2:ZXSTEP/2:7*pi/4, 7*pi/4+ZXSTEP:ZXSTEP:2*pi-ZXSTEP] 
        rayIndexes3D = getRayPath3D(volume, [40, 40, 25], theta, phi);
        displayRayFeatures(rayIndexes3D) = 255;  % For visualization
        vectorDistance(distances) = length(rayIndexes3D);
        distances = distances +1;
    end
end

[histogram, rangeBins] = getNonUniformHistogram(vectorDistance, 16, true); % 16 is the number of bins

%%
% This part is for visualization only. If the array is small it is possible to show it here
% otherwise it is mandatory to use other application.

% For small volumes
%figure, scatter3(XCOLS(:), YROWS(:), ZSTEPS(:), displayRayFeatures(:));

% For a large volume you should save it and display it with other software. In uint8.
fileName = 'volume.tiff';
for slice = 1:ZSTEPS(end)
    if slice == 1
        imwrite(uint8(displayRayFeatures(:, :, slice)), fileName, 'Compression', 'none');
    else
        imwrite(uint8(displayRayFeatures(:, :, slice)), fileName, ...
            'Compression', 'none', 'WriteMode', 'append');
    end
end
