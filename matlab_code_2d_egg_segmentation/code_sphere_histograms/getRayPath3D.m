%%
% Rodrigo Nava
% Date: December 4th, 2015
% Computes the indexes from the 'center' to the closest 'border' or fillicle cell in the direction
% of 'theta' and 'phi'

%%
function  rayIndexes3D = getRayPath3D(volume, center, theta, phi)

[YROWS, XCOLS, ZSTPS] = size(volume); % The volume dimensions.
VALUE_BORDER = 1;       % This is the value used to stop the ray.
VALUE_BACKGROUND = 0;   % This is the value used to check holes.

%%
rho = round(sqrt(XCOLS^2 + YROWS^2 + ZSTPS^2));
x = center(1) + round(sin(phi)*cos(theta) * linspace(0, rho, rho+1));
y = center(2) - round(sin(phi)*sin(theta) * linspace(0, rho, rho+1));
z = center(3) + round(cos(phi)*linspace(0, rho, rho+1));

MAXINDEX = min([find(x < 1, 1, 'first'), find(x > XCOLS, 1, 'first'), find(y < 1, 1, 'first'), ...
    find(y > YROWS, 1, 'first'), find(z < 1, 1, 'first'), find(z > ZSTPS, 1, 'first')]);

% Si esta vacio, quiere decir que es 45� y no se acortan X,Y, otro caso se
% proyecta sobre el �ngulo y se hace mas peque�o.
if ~isempty(MAXINDEX)
    x = x(1:MAXINDEX-1);
    y = y(1:MAXINDEX-1);
    z = z(1:MAXINDEX-1);
end

indexes3D = sub2ind([YROWS, XCOLS, ZSTPS], y, x, z);
valuesPath3D = volume(indexes3D);
borderIndex = find(valuesPath3D == VALUE_BORDER, 1, 'first'); % Primer borde

if isempty(borderIndex)
    % Can I improve this with the mean distance ...???
    rayIndexes3D = indexes3D; % Si no hay borde regresas todos los indices
else
    % Patch
    backgroundIndex = find(valuesPath3D(borderIndex+1:end) == VALUE_BACKGROUND, 1, 'first');
    if isempty(backgroundIndex)
        lastBorder = find(valuesPath3D == VALUE_BORDER, 1, 'last');
        rayIndexes3D = indexes3D(1:lastBorder);
    else
        newIndex = backgroundIndex + borderIndex;
        rayIndexes3D = indexes3D(1:newIndex-1);
        % rayIndexes3D = indexes(1:borderIndex);  % Si hay borde regresas solo los indices entre centro y borde
    end
end

end
