%%
% Rodrigo Nava
% October 8th, 2015
% Script for segmenting volumes using a precomputed supertexton dictionary

%%
volumeDirectory = dir('selected_stacks');
volumeDirectory(1:2, :) = [];     % Erase '.' and '..' directories.
NOMOFVOLS = size(volumeDirectory, 1);

% ----------------------------------------------------
% Setting the .tiff structure to save the segmented slices: Images are saved as uint8
tagstruct.Compression = Tiff.Compression.None;
tagstruct.SampleFormat = Tiff.SampleFormat.UInt;
tagstruct.Photometric = Tiff.Photometric.MinIsBlack;
tagstruct.BitsPerSample = 8;
tagstruct.SamplesPerPixel = 1;
tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;

% ----------------------------------------------------
% Generate the classifier model using the supertexton dictionary
load('supertextondictionary.mat'); % Load the dictionary with supertextons

background = vertcat(dictionary.background);
follicle = vertcat(dictionary.follicle);
nurse = vertcat(dictionary.nurse);
cytoplasm = vertcat(dictionary.cytoplasm);

L0 = size(background, 1);
L1 = size(follicle, 1);
L2 = size(nurse, 1);
L3 = size(cytoplasm, 1);

supertextons = [background; follicle; nurse; cytoplasm];  % The supertexton dictionary.
supertextonLabels = [zeros(L0, 1); ones(L1, 1); 2*ones(L2, 1); 3*ones(L3, 1)]; % The labels.

% ----------------------------------------------------
% Construction of the classifier.
classifierModel = fitcknn(supertextons, supertextonLabels, 'NumNeighbors', 5, 'Standardize',1);
% classifierModel = TreeBagger(18, supertextons, supertextonLabels, 'Prior', 'Uniform',...
%    'Method', 'classification');


for indexVolume = [28, 29]%6:NOMOFVOLS % 38 y 41 no
    volumeName = volumeDirectory(indexVolume).name;
    volumenString = ['selected_stacks\' volumeName]; % The current volume string
    volumeCell = bfOpen3DVolume(volumenString);      % This command opens the whole volume
    currentVolume = double(volumeCell{1}{1});        % This is the full volume 2 channels
    currentVolume = currentVolume(:,:,2:2:end);
    [YROWS, XCOLS, ZSTPS] = size(currentVolume);     % and dimensions.
    
    fileName = ['volume', num2str(indexVolume), '.tiff']; % Name to be saved.
    for indexSlice = 1:ZSTPS
        tic;
        segmentedSlice = eggSegmentation(currentVolume(:,:,indexSlice), ...
            classifierModel); % The classifier model is KNN.
        toc;
        % This part saves the file.
        if indexSlice == 1
            imwrite(uint8(segmentedSlice), fileName, 'Compression', 'none');
        else
            imwrite(uint8(segmentedSlice), fileName, 'Compression', 'none', 'WriteMode', 'append');
        end
    end
end
