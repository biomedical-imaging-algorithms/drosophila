%%
% Rodrigo Nava
% Date: January, 2016.
% Test sphere feature histograms in 3D with adaptive bins.

%% ===========================================================================
tic,
load('ExtendedSphereHistograms_M_.mat');       % Load the feature vectors before reshaping
load('ExtendedSphereHistograms_U_.mat');  
%load('SphereHistograms.mat');              % Load the feature vectors before reshaping
NUMOFSAMPLES = 60;                             % Number of volumes available
RADIUS = 20;
NUMOFSPHERES = 6;

%% ===========================================================================
% Feature vectors

insideFeatures = eval(['U_sphereClassesByVolume_' num2str(RADIUS) '_' ...
    num2str(NUMOFSPHERES) '_Inside']);
outsideFeatures = eval(['M_sphereClassesByVolume_' num2str(RADIUS) '_' ...
    num2str(NUMOFSPHERES) '_Outside']);

% Cells to prepare data
inside = cell(NUMOFSAMPLES, 1);
outside = cell(NUMOFSAMPLES, 1);

for currentVolume = 1:NUMOFSAMPLES
    numOfPtVolFeatInside = size(insideFeatures{currentVolume}, 1);   % Num of points per volume inside
    numOfPtVolFeatOutside = size(outsideFeatures{currentVolume}, 1); % Num of points per volume outside
    
    tempCellInside = cell(numOfPtVolFeatInside, 1);
    tempCellOutside = cell(numOfPtVolFeatOutside, 1);
    
    for index = 1:numOfPtVolFeatInside
        numOfXtPt = size(insideFeatures{currentVolume}{index}, 1);
        tempCellInside{index}= reshape(cell2mat(insideFeatures{currentVolume}{index})', ...
            NUMOFSPHERES*4, numOfXtPt)';
    end
    for index = 1:numOfPtVolFeatOutside
        numOfXtPt = size(outsideFeatures{currentVolume}{index}, 1);
        tempCellOutside{index}= reshape(cell2mat(outsideFeatures{currentVolume}{index})', ...
            NUMOFSPHERES*4, numOfXtPt)';
    end
    
    inside{currentVolume} = cell2mat(tempCellInside);
    outside{currentVolume} = cell2mat(tempCellOutside);
end

insideMatrix = featureNormalization(cell2mat(inside));
outsideMatrix =  featureNormalization(cell2mat(outside));

% figure, stem(mean(insideMatrix), 'r'); hold on; stem(mean(outsideMatrix), 'b');
toc;

%% ===========================================================================

rng default;                        % For replication
STAGE = 2;                          % Only 2-stage volumes.
volumeDirectory = dir(['train\segmented_stacks\stage', num2str(STAGE)]);
volumeDirectory(1:2, :) = [];       % erase '.' and '..' directories
NUMOFVOLS = size(volumeDirectory, 1);
sphereClassesByVolume = cell(NUMOFVOLS, 1);

for indexVol = 1:1%NUMOFVOLS
    tic,
    % For each volume segmented with KNN
    volumeName = volumeDirectory(indexVol).name;
    volumeStringFile = ['train\segmented_stacks\stage', num2str(STAGE), '\', volumeName];
    
    % We use the bioinformatics toolbox to read volumes.
    % reader = bfGetReader(volumeStringFile);           % This is for slices
    volumeCell = bfOpen3DVolume(volumeStringFile);      % This command opens the whole volume
    currentVolume = volumeCell{1}{1};                   % This is the full volume.
    [YROWS, XCOLS, ZSTPS] = size(currentVolume);
    
    [X, Y, Z] = meshgrid(1:90:XCOLS, 1:90:YROWS, [10,20]);
    points = [X(:), Y(:), Z(:)];
    NUMOFXTPNTS = size(points, 1);
    sphereClassesXtPnt = zeros(NUMOFXTPNTS, NUMOFSPHERES*4);
    for indexPoint = 1:NUMOFXTPNTS
        currentXtPnt = points(indexPoint,:);
        % (volume, center, RADIO, NUMOFSPHERES, FLAG)
        sphereClassesXtPnt(indexPoint, :) = reshape(getSphereHistogram(currentVolume, ...
            currentXtPnt, RADIUS, NUMOFSPHERES)', NUMOFSPHERES*4, 1)';
    end
    toc;
    
    sphereClassesByVolume{indexVol} = sphereClassesXtPnt;
end

%% ===========================================================================

tic,
INTER = size(cell2mat(inside), 1);
EXTER = size(cell2mat(outside), 1);
train_features = [insideMatrix; outsideMatrix];     % Train features
train_labels = [zeros(INTER, 1); ones(EXTER, 1)];   % Labels

sample  = featureNormalization(cell2mat(sphereClassesByVolume));          % Samples to be classify

%knn_model = fitcknn(train_features, train_labels, 'NumNeighbors', 11, 'Distance', ...
%    'euclidean', 'Standardize', true);
%classification = predict(knn_model, sample);

%svm_model = fitcsvm(train_features, train_labels, 'Standardize', false);
%classification = predict(svm_model, sample);

BaggedEnsemble = TreeBagger(10, train_features, train_labels, 'Prior', 'Uniform',...
    'Method', 'classification', 'OOBPred','On');
[RESULTS, scores] = BaggedEnsemble.predict(sample);
classification = str2double(RESULTS);

toc;

%% ===========================================================================
tic,
displayVolume = currentVolume;
for index = 1:NUMOFXTPNTS
    spherePixels = getSpherePixels([XCOLS, YROWS, ZSTPS], points(index, :), ...
        30, 1);      % Radius = 4;
    if classification(index) == 1
        displayVolume(spherePixels) = 10;           % Non-egg samples
    else
        displayVolume(spherePixels) = 255;          % Positive Egg samples
    end
end
toc;

%% ===========================================================================

fileName = ['U1004_test_volume_', num2str(RADIUS), '_', num2str(NUMOFSPHERES), '.tiff'];
for slice = 1:ZSTPS(end)
    if slice == 1
        imwrite(uint8(displayVolume(:, :, slice)), fileName, 'Compression', 'none');
    else
        imwrite(uint8(displayVolume(:, :, slice)), fileName, ...
            'Compression', 'none', 'WriteMode', 'append');
    end
end

%% ===========================================================================

%{
result_0 = [sum(classification(1:M) == 0), sum(classification(1:M) == 1)];
result_1 = [sum(classification(M+1:end) == 0), sum(classification(M+1:end) == 1)];
confusion_matrix = [result_0; result_1];
accuracy_rate = sum(diag(confusion_matrix))/sum(confusion_matrix(:));
confmatrix = cfmatrix2NEW(confusion_matrix', {'0', '1'}, 0, 1);
%}
