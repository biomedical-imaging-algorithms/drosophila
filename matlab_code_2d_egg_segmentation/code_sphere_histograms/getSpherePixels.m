%%
% Rodrigo Nava
% Date: December 8th, 2015
%

%%  ===========================================================================
function spherePixels = getSpherePixels(dimensions, center, RADIO, NUMOFCIRCLE)

[XGRID, YGRID, ZGRID] = meshgrid(1:dimensions(1), 1:dimensions(2), 1:dimensions(3));

spherePixels = ...
        (XGRID-center(1)).^2 + (YGRID-center(2)).^2 + (ZGRID-center(3)).^2/(0.2)^2 >= ...
        ((NUMOFCIRCLE-1)*RADIO).^2 & ...   % Internal radio
        (XGRID-center(1)).^2 + (YGRID-center(2)).^2 + (ZGRID-center(3)).^2/(0.2)^2 < ...
        (NUMOFCIRCLE*RADIO).^2;            % External radio
 
%% -------------------------------------------------------
%{
fileName = 'volume6.tiff';
for slice = 1:ZGRID(end)
    if slice == 1
        imwrite(100*uint8(spherePixels(:, :, slice)), fileName, 'Compression', 'none');
    else
        imwrite(100*uint8(spherePixels(:, :, slice)), fileName, ...
            'Compression', 'none', 'WriteMode', 'append');
    end
end
%}
end
