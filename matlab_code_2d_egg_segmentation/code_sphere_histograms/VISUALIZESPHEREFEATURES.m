%%  ===========================================================================
% Rodrigo Nava
% Date: November, 2015.
% Compute sphere feature histograms in 3D with adaptive bins.
%

%%  ===========================================================================

rng default;                        % For replication
STAGE = 2;                          % Only 2-stage volumes.
volumeDirectory = dir(['train\segmented_stacks\stage', num2str(STAGE)]);
volumeDirectory(1:2, :) = [];       % erase '.' and '..' directories
NUMOFVOLS = size(volumeDirectory, 1);

sphereClassesByVolume = cell(NUMOFVOLS, 1);

load('eggPoints.mat');          % Load the points inside 'fullEggPoints' and
load('eggPointsZplane.mat');    % outside 'fullOutsidePoints' in [X Y] format.
% Load the Z points. (Focused planes).

% ----------------------------------------------------
for indexVol = 1:1%NUMOFVOLS
    tic,
    % For each volume segmented with KNN
    volumeName = volumeDirectory(indexVol).name;
    volumeStringFile = ['train\segmented_stacks\stage', num2str(STAGE), '\', volumeName];
    
    % We use the bioinformatics toolbox to read volumes.
    % reader = bfGetReader(volumeStringFile);           % This is for slices
    volumeCell = bfOpen3DVolume(volumeStringFile);      % This command opens the whole volume
    currentVolume = volumeCell{1}{1};                   % This is the full volume.
    displayVolume = currentVolume;
    [YROWS, XCOLS, ZSTPS] = size(currentVolume);
    
    %eggPoints = fullOutsidePoints{indexVol, 2};   % The order is [X,Y].
    eggPoints = fullEggPoints{indexVol, 2};         % The order is [X,Y].
    NUMOFEGGPOINTS = size(eggPoints, 1);            % Sometimes a slice has several marked points
    sphereClassesByPoint = cell(NUMOFEGGPOINTS, 1);
    
    % ----------------------------------------------------
    for indexPoint = 1:1%NUMOFEGGPOINTS
        currentPoint = [eggPoints(indexPoint), eggPoints(indexPoint + NUMOFEGGPOINTS)];
        % For the current annotated point, a number of extrapoints are randomly created.
        extraPoints = getPointsAroundCircle(currentPoint, 45, 5, false); % Center[X,Y], radio, NUM, flag
        % Check boundaries for valid points.
        invalidPoints = find(extraPoints(:,1) > XCOLS | extraPoints(:,1) <= 0 | ...
            extraPoints(:,2) > YROWS | extraPoints(:,2) <= 0);
        if ~isempty(invalidPoints)
            extraPoints(invalidPoints,:) = []; % Remove invalid points.
        end
        
        NUMOFXTPNTS = size(extraPoints, 1);
        sphereClassesXtPnt = cell(NUMOFXTPNTS, 1);
        
        % --------------------------------------------------------
        for extraPointIndex = 1:1%NUMOFXTPNTS
            RADIUS = 30;
            currentXtPnt = [extraPoints(extraPointIndex), extraPoints(extraPointIndex + NUMOFXTPNTS)];
            
            for NUMOFCIRCLE = 1:3
                % (volume, center, RADIO, NUMOFSPHERES, FLAG)
                spherePixels = getSpherePixels([XCOLS, YROWS, ZSTPS], [currentXtPnt, ZFOCUS(indexVol)], ...
                    RADIUS, NUMOFCIRCLE);
                displayVolume(spherePixels) = NUMOFCIRCLE*10;
            end
            sphereClassesXtPnt{extraPointIndex} = getSphereHistogram(currentVolume, ...
                [currentXtPnt, ZFOCUS(indexVol)], RADIUS, 5, false);
        end
        sphereClassesByPoint{indexPoint} = sphereClassesXtPnt;
    end
    
    sphereClassesByVolume =  sphereClassesByPoint;
end

%%
fileName = 'volume7.tiff';
for slice = 1:ZSTPS(end)
    if slice == 1
        imwrite(uint8(displayVolume(:, :, slice)), fileName, 'Compression', 'none');
    else
        imwrite(uint8(displayVolume(:, :, slice)), fileName, ...
            'Compression', 'none', 'WriteMode', 'append');
    end
end


