%% ===========================================================================
% Rodrigo Nava
% Date: November, 2015.
% Compute sphere feature histograms in 3D with adaptive bins.
%

%% ===========================================================================

rng default;                        % For replication
STAGE = 2;                          % Only 2-stage volumes.
volumeDirectory = dir(['train\segmented_stacks\stage', num2str(STAGE)]);
volumeDirectory(1:2, :) = [];       % erase '.' and '..' directories
NUMOFVOLS = size(volumeDirectory, 1);

sphereClassesByVolume = cell(NUMOFVOLS, 1);

load('ExtendedEggPoints.mat');          % Load the points inside 'fullEggPoints' and
load('eggPointsZplane.mat');            % outside 'fullOutsidePoints' in [X Y] format.
                                        % Load the Z points. (Focused planes).
% RADXTPT = 30;                           % Radius of extra points
% NUMXTPT = 2;                            % Number of extra points
RADSPHERE = 25;                         % Sphere radious
NUMOFCIRCLES = 5;                       % Number of spheres

% ----------------------------------------------------
for indexVol = 1:NUMOFVOLS
    tic,
    % For each volume segmented with KNN
    volumeName = volumeDirectory(indexVol).name;
    volumeStringFile = ['train\segmented_stacks\stage', num2str(STAGE), '\', volumeName];
    
    % We use the bioinformatics toolbox to read volumes.
    % reader = bfGetReader(volumeStringFile);           % This is for slices
    volumeCell = bfOpen3DVolume(volumeStringFile);      % This command opens the whole volume
    currentVolume = volumeCell{1}{1};                   % This is the full volume.
    [YROWS, XCOLS, ZSTPS] = size(currentVolume);
    
    % ----------------------------------------------------
    eggPoints = fullInsidePoints{indexVol, 2};         % The order is [X,Y].
    %eggPoints = fullOutsidePoints{indexVol, 2};   % The order is [X,Y].

    NUMOFEGGPOINTS = size(eggPoints, 1);            % Sometimes a slice has several marked points
    sphereClassesByPoint = cell(NUMOFEGGPOINTS, 1);
    
    % ----------------------------------------------------
    for indexPoint = 1:NUMOFEGGPOINTS
        currentPoint = [eggPoints(indexPoint), eggPoints(indexPoint + NUMOFEGGPOINTS)];
        % For the current annotated point, a number of extrapoints are randomly created.
        % extraPoints = getPointsAroundCircle(currentPoint, RADXTPT, ...
        %    NUMXTPT, false);                        % Center[X,Y], radio, NUM, flag
        % Check boundaries for valid points.
        % invalidPoints = find(extraPoints(:,1) > XCOLS | extraPoints(:,1) <= 0 | ...
        %    extraPoints(:,2) > YROWS | extraPoints(:,2) <= 0);
        % if ~isempty(invalidPoints)
        %    extraPoints(invalidPoints,:) = [];      % Remove invalid points.
        % end
        
        % NUMOFXTPNTS = size(extraPoints, 1);
        % EXTRAZFOCUS = randi([1 ZSTPS], 1, NUMOFXTPNTS)';
        % sphereClassesXtPnt = cell(NUMOFXTPNTS, 1);
        sphereClassesXtPnt = cell(1, 1);
        for extraPointIndex = 1:1%NUMOFXTPNTS
            %currentXtPnt = [extraPoints(extraPointIndex), extraPoints(extraPointIndex + NUMOFXTPNTS)];
            currentXtPnt = currentPoint;
            
            % ----------------------------------------------------
            % Inside (volume, center, RADIO, NUMOFSPHERES, FLAG)
            sphereClassesXtPnt{extraPointIndex} = getSphereHistogram(currentVolume, ...
                [currentXtPnt, ZFOCUS(indexVol)], RADSPHERE, NUMOFCIRCLES);
            
            % ----------------------------------------------------
            % Outside (volume, center, RADIO, NUMOFSPHERES, FLAG)
            %sphereClassesXtPnt{extraPointIndex} = getSphereHistogram(currentVolume, ...
            %    [currentXtPnt, EXTRAZFOCUS(extraPointIndex)], RADSPHERE, NUMOFCIRCLES);
        end
        
        sphereClassesByPoint{indexPoint} = sphereClassesXtPnt;          % Histograms per point
    end
    toc;
    
    sphereClassesByVolume{indexVol} = sphereClassesByPoint;             % Histograms per volume
end
