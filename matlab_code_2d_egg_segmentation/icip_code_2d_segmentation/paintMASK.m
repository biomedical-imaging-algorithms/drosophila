%%
%
%

%%
function colorMASK = paintMASK(im, mask, color)

mask = bwperim(mask, 8);
[nrows, mcols, channels] = size(im);
colorMASK = zeros([nrows, mcols, 3]);

for i = 1:3
    if (channels ~= 1)
        view = mat2gray(im(:,:,i));
    else
        view = mat2gray(im(:,:,1));
    end
    
    view(mask) = color(i);
    colorMASK(:,:,i) = view;
end

end


