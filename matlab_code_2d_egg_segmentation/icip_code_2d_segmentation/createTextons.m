%%
% Date: October 30th, 2014
%

%%
function [bcktexton, borTexton, nurTexton, cytTexton] = createTextons(back, border, nurse, cyto, K)

[a1, ~] = size(back);
[a2, ~] = size (border);
[a3, ~] = size (nurse);
[a4, ~] = size(cyto);

if ~exist('K','var') || isempty(K)
K = uint16(min([a1, a2, a3, a4])/2);
end

% X = [1 2; 1 2; 2 3];
% [I, C] = kmeans(X, 3, 'emptyaction', 'singleton');
% [I, C] = kmeans(X, 3, 'emptyaction', 'drop');
% [I, C] = kmeans(X, 3, 'emptyaction', 'error')

tic, 
[~, bcktexton] = kmeans(back, K, 'MaxIter', 70, 'emptyaction', 'singleton');%, 'Distance', 'city');
[~, borTexton] = kmeans(border, K,  'MaxIter', 70, 'emptyaction', 'singleton');%, 'Distance', 'city');
[~, nurTexton] = kmeans(nurse, K, 'MaxIter', 70, 'emptyaction', 'singleton');%, 'Distance', 'city');
[~, cytTexton] = kmeans(cyto, K,  'MaxIter', 70, 'emptyaction', 'singleton');%, 'Distance', 'city');
toc;

end
