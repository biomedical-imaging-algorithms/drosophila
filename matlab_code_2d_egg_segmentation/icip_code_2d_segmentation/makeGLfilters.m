%%
%
%

%%
function F = makeGLfilters

SUP  =  31;
SF = 5;
NF = 2 * SF;
F = zeros(SUP, SUP, NF);

for index = 1:SF
    
    std = index*sqrt(2);
    F(:, :, index) = normalise(fspecial('gaussian', SUP, std));
    F(:, :, index + SF) = normalise(fspecial('log', SUP, std));
end

end

%%
%
%

%%
function f = normalise(f)

f = f - mean(f(:));
f = f / sum(abs(f(:)));

end
