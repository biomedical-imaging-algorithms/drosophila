%%
% Date: October 22nd, 2014
%

%%
function [segments, borders] = getSuperpixels(image, regionSize, regularizer)
%{
%function [segments, borders] = getSuperpixels(ovary, regionSize, regularizer, filterSize, filtersdt)
% Gaussian filter to minimize noise effect
%filter = fspecial('gaussian', filterSize,  filtersdt);
%filter = filter ./ max(filter(:));
%ovaryGaussian = imfilter(ovary, filter, 'replicate');
%}

segments = vl_slic(single(image), regionSize, regularizer);
[sx, sy] = vl_grad(double(segments), 'type', 'forward') ;

borders = find(sx | sy) ;

end
