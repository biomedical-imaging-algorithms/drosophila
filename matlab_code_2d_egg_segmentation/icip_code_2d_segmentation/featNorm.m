%%
%
%

%%
function feat = featNorm(oldFeat)

[MROWS, NCOLS] = size(oldFeat);

feat = zeros(MROWS, NCOLS);

for index = 1:MROWS
    m = oldFeat(index, :);
     n = mean(m);
    v = std(m);
    feat(index,:) = (m + n) ./ (6*v+2);
 
end




end