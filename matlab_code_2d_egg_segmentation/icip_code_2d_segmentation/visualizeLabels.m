%%
% Date: November 15th, 2014
%

%%
function contImage = visualizeLabels(ovary, segmentedImage, label, color)

%se = strel('disk', 2);
follicleMASK = segmentedImage;
follicleMASK(follicleMASK ~= label) = 0; % Binary image
%follicleMASK = imopen(follicleMASK, se);
contImage = paintMASK(ovary, follicleMASK, color);

end
