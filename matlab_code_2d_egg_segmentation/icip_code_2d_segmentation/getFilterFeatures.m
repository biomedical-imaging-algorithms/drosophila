%%
% Date: November 3th, 2014
%

%%
function features = getFilterFeatures(patch, NORM, OP)

[MROWS, NCOLS] = size(patch);

% NORM flag.
if NORM == 1, im = mat2gray(patch);
else im = patch;
end

switch OP
    case 0, filters = makeLMfilters; % 54 filters
    case 1, filters = makeRFSfilters; % 38 filters
    case 2, filters = makeSfilters; % 13 filters
    case 3, filters = makeGLfilters; % 10 filters
    otherwise, filters = makeCGfilters; % 50 filters
end

%%
% compute features
FILTERS = size(filters, 3);
features = zeros(MROWS, NCOLS, FILTERS);

for coeff = 1:FILTERS
    responseFilter =  conv2(im, filters(:, :, coeff), 'same');
    Ln = sqrt(sum(responseFilter(:).^ 2));
    features(:, :, coeff) = (responseFilter * (log(1 + Ln) / 0.03)) / Ln;
end

end
