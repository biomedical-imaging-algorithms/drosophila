%%
% Date: November 10th, 2014
%

%%
function spxClass = drosoClassifier(listOfFeatures, trainFeatures, labelTraining, OP)

switch OP
    case 0, spxClass = classifySuperpixels(listOfFeatures, trainFeatures, labelTraining);
    case 1, spxClass = classifyKNN(listOfFeatures, trainFeatures, labelTraining);
    case 2
        [classifTrain, model] = adaboost('train', trainFeatures, labelTraining, 36);
        spxClass = adaboost('apply', listOfFeatures, model);
    case 3
        SVMModel = fitcsvm(trainFeatures, labelTraining);
        [spxClass, score] = predict(SVMModel, listOfFeatures);
    otherwise
        SVMModel = fitcsvm(trainFeatures, labelTraining, 'KernelFunction','rbf');
        [spxClass, score] = predict(SVMModel,listOfFeatures);
end

end
