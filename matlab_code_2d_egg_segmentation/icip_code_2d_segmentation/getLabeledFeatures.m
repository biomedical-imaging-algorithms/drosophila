%%
% Rodrigo Nava
% October 22nd, 2014
% Training stage
%

%%
function [background, border, nurse, cytoplasm] = getLabeledFeatures(ovaryStringFile, maskStringFile)

%% read files
ovary = imread(ovaryStringFile); % ovary uint16
ovary = double(ovary(:, :, 1)); % keep only the first channel (GREEN)
[MROWS, NCOLS] = size(ovary);
mask = imread(maskStringFile); % mask uint8

% compute superpixels for target image
regionSize = 10;
regularizer = 0.02;
[segments, ~] = getSuperpixels(mat2gray(ovary), regionSize, regularizer);
numOfSegments = unique(segments);


%%
% Steerable filter extraction
% 0:= LM filters, 54 filters
OP = 0;
NORM = 0;
numOfFilters = 18;
featureFilters = getFilterFeatures(ovary, NORM, OP);
invScales = zeros(MROWS, NCOLS, numOfFilters);

invScales(:, : , 1) = max(featureFilters(:, :, 1:6), [], 3);
invScales(:, : , 2) = max(featureFilters(:, :, 7:12), [], 3);
invScales(:, : , 3) = max(featureFilters(:, :, 13:18), [], 3);
invScales(:, : , 4) = max(featureFilters(:, :, 19:24), [], 3);
invScales(:, : , 5) = max(featureFilters(:, :, 25:30), [], 3);
invScales(:, : , 6) = max(featureFilters(:, :, 31:36), [], 3);
for index = 7:18
    invScales(:, :, index) = featureFilters(:, :, index + 30);
end


[Dx, Dy] = gradient(invScales);
G = Dx + Dy;
G = reshape(G, [], numOfFilters);

NSEG = length(numOfSegments);
%listOfFeatures = zeros(NSEG, mlabels);

spFiltered = reshape(invScales, [], numOfFilters);
% check if a segment is valid (it belongs to a single class)
% viewFeatures = zeros([size(ovary),2]);
for sgm = 1:NSEG
    index = find(segments == numOfSegments(sgm));
    feature = computeFeatures(G, index, spFiltered);
    valid = mask(index);
           
    A = length(valid(valid==0));
    B = length(valid(valid==1));
    C = length(valid(valid==2));
    D = length(valid(valid==3));
    
    [~, T] = max([A, B, C, D]);
    % the segment is valid
    %if length(valid) == 1
    
    % viewFeatures = viewFeatures + mapFeatures(ovary, index, feature);
    % viewFeatures(index) = feat(1);
    % vari(index) = feature(1);
    switch T
        case 1, background(sgm,:) = feature;
        case 2, border(sgm,:) = feature;
        case 3, nurse(sgm, :) = feature;
        otherwise, cytoplasm(sgm, :) = feature;
    end
    %end
end

%%
% removing null vectors
background(all(background==0, 2), :) = [];
border(all(border==0, 2), :) = [];
nurse(all(nurse==0, 2), :) = [];
cytoplasm(all(cytoplasm==0, 2), :) = [];

end
