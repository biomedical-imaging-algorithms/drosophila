%%
% Date: October 22nd, 2014
%

%%
function segmentedImage = findLabels(ovaryStringFile, labels, KTEXTONS)

%mlabels = 18*2;
%[nlabels, ~] = size(labels);
background = vertcat(labels.background);
border = vertcat(labels.border);
nurse = vertcat(labels.nurse);
cytoplasm = vertcat(labels.cyto);

%{
KTEXTONS = vertcat(textonData.background);
background = zeros(KTEXTONS, mlabels);
border = zeros(KTEXTONS, mlabels);
nurse = zeros(KTEXTONS, mlabels);
cytoplasm = zeros(KTEXTONS, mlabels);

entries = 1 : KTEXTONS : KTEXTONS*nlabels;

for structIndex = 1:nlabels
    val = entries(structIndex);
    background(val:val + KTEXTONS - 1, :) = labels(structIndex).background;
    border(val:val + KTEXTONS - 1, :) = labels(structIndex).border;
    nurse(val:val + KTEXTONS - 1, :) = labels(structIndex).nurse;
    cytoplasm(val:val + KTEXTONS - 1, :) = labels(structIndex).cyto;
end
%}
% read the target image
ovary = imread(ovaryStringFile); % ovary is uint16
ovary = double(ovary(:, :, 1)); % keep only with green channel
% ovary = ovary - mean(ovary(:)); % removing mean in order to work only with textural information
% ovary = mat2gray(ovary);
[mrows, ncols] = size(ovary);

%% Bilateral filter
%{
window = 3;
sigma = [3, 0.1];
ovary = bilateralFilter(ovary, window, sigma(1), sigma(2));
%}
[L0, ~] = size(background);
[L1, ~] = size(border);
[L2, ~] = size(nurse);
[L3, ~] = size(cytoplasm);

trainFeatures = [background; border; nurse; cytoplasm];

% compute superpixels for target image
regionSize = 10;
regularizer = 0.02;
%filterSize = 9;
%filtersdt = 2;
[segments, borders] = getSuperpixels(mat2gray(ovary), regionSize, regularizer);
numOfSegments = unique(segments);

% Steerable filter extraction
% filterFeats = getLogGaborFeatures(ovary, 0); % 24 filters (6 orientations and 4 scales)
% filterFeats = getRealGaborFeatures(ovary, 0); % 24 filters (6 orientations and 4 scales)
% filterFeats = getComplexGaborFeatures(ovary, 0); % 24 filters (6 orientations and 4 scales)
% op = 0; % 0:= LM filters, 1:= RF filters, and 2:= S filters
op = 0;
numOfFilters = 18;
featureFilters = getFilterFeatures(ovary, 0, op);
invScales = zeros(mrows, ncols, numOfFilters);
%{
invScales(:, : , 1) = max(featureFilters(:, :, 1:6), [], 3);
invScales(:, : , 2) = max(featureFilters(:, :, 7:12), [], 3);
invScales(:, : , 3) = max(featureFilters(:, :, 13:18), [], 3);
invScales(:, : , 4) = max(featureFilters(:, :, 19:24), [], 3);
invScales(:, : , 5) = max(featureFilters(:, :, 25:30), [], 3);
invScales(:, : , 6) = max(featureFilters(:, :, 31:36), [], 3);
for index = 7:8
    invScales(:, :, index) = featureFilters(:, :, index + 30);
end
%}

invScales(:, : , 1) = max(featureFilters(:, :, 1:6), [], 3);
invScales(:, : , 2) = max(featureFilters(:, :, 7:12), [], 3);
invScales(:, : , 3) = max(featureFilters(:, :, 13:18), [], 3);
invScales(:, : , 4) = max(featureFilters(:, :, 19:24), [], 3);
invScales(:, : , 5) = max(featureFilters(:, :, 25:30), [], 3);
invScales(:, : , 6) = max(featureFilters(:, :, 31:36), [], 3);
for index = 7:18
    invScales(:, :, index) = featureFilters(:, :, index + 30);
end
%{
[a,~,~] = FrangiFilter2D(ovary);
mm = cat(3,a,invScales);
%}
[Dx, Dy] = gradient(invScales);
G = Dx + Dy;
G = reshape(G, [], numOfFilters);

NSEG = length(numOfSegments);
%listOfFeatures = zeros(NSEG, mlabels);

spFiltered = reshape(invScales, [], numOfFilters);

%{
for sgm = 1:NSEG
    index = find(segments == numOfSegments(sgm));
    listOfFeatures(sgm, :) = computeFeatures(ovary, index, spFiltered);
end

%%
% Multiclass classifiers
labelTraining = [zeros(L0, 1); ones(L1, 1); 2*ones(L2, 1); 3*ones(L3,1)];
%labelTraining = [-1*ones(L0, 1); 1*ones(L1, 1); -1*ones(L2, 1); -1*ones(L3,1)];
%%
%Op = 0 (Linear Fisher Analysis), Op = 1 (Adaboost)
Op = 1;
spxClass = drosoClassifier(listOfFeatures, trainFeatures, labelTraining, Op);

%%
segmentedImage = zeros(mrows, ncols);
for sgm = 1:NSEG
    index = (segments == numOfSegments(sgm));
    segmentedImage(index) = spxClass(sgm);
end
%}

Op = 1;
labelTraining = [zeros(L0, 1); ones(L1, 1); 2*ones(L2, 1); 3*ones(L3,1)];
segmentedImage = zeros(mrows, ncols);
for sgm = 1:NSEG
    index = find(segments == numOfSegments(sgm));
    feat = featNorm(computeFeatures(G, index, spFiltered));
    spxClass = drosoClassifier(feat, trainFeatures, labelTraining, Op);
    segmentedImage(index) = spxClass;
end

end
