%%
% Date: October 22nd, 2014
%

%%
function features = computeFeatures(gradient, index, filters)

[~, N] = size(filters);
% current segment
% region = zeros(size(ovary));
% it is possible to include other features here
% measurements = regionprops(region, ovary, 'Area', 'Perimeter', 'Orientation');
% entropy
%valuesSgm = ovary(index);
% valuesNSgm = valuesSgm ./ sum(valuesSgm);
% valuesNSgm(valuesNSgm == 0) = []; % to avoid log(0)
% entro = -sum(valuesNSgm .* log2(valuesNSgm));

% mean of the filtered superpixel

%{
tic,
features = zeros(2*numOfFilters, 1);
count = 1;
for filNum = 1:numOfFilters
    spFiltered = filters(:, :, filNum);
    values = spFiltered(index);
    features(count) = mean(values); % superpixel mean
    features(count + 1) = std(values); % superpixel std
    count = count + 2;
end
features = features';
toc;
%}
currentValues = filters(index, :);
currentGradients = gradient(index, :);

if length(index) ~= 1
    feats_mean = mean(currentValues);
    feats_std = std(currentValues);
    feats_ener0 =  sum(currentValues .^ 2) / size(currentValues,1);
    feats_grad = mean(currentGradients);
else
    feats_mean = currentValues;
    feats_std = zeros(1,N);
    feats_ener0 = currentValues .^ 2;
    feats_grad = currentGradients;
end

features = reshape([feats_mean; feats_std; feats_grad; feats_ener0], 1, []);

%features = [mean(valuesSgm) std(valuesSgm), meanSp'];

% building the feature vector
% features = [(measurements.Perimeter^2) / measurements.Area, measurements.Orientation, ...
%     mean(valuesSgm), std(valuesSgm), kurtosis(valuesSgm), skewness(valuesSgm), entro, meanSp'];

end
