%%
% Rodrigo Nava
% Date: October 22nd, 2014
% Edit: Jiri Borovec, 19/09/2016
%

run('/datagrid/temporary/borovec/Repos/drosophila/matlab_code_2d_egg_segmentation/matlab/vlfeat-0.9.20/toolbox/vl_setup.m')

%%
STAGE = 2;
TRAIN_DIR = '/datagrid/Medical/microscopy/drosophila/rnava/train';
fileTrain = dir([TRAIN_DIR, '/ovary/stage', num2str(STAGE)]);
fileTrain(1:2, :) = []; % erase '.' and '..' directories
trainData2 = repmat(struct('background', [], 'border', [], 'nurse', [], 'cyto', []), length(fileTrain), 1);

for fileIndex = 1:length(fileTrain)
    fileName = fileTrain(fileIndex).name;
    disp(['getLabeledFeatures: ', num2str(fileIndex), ' of ', num2str(length(fileTrain)), ' for ', fileName])
    ID = fileName(6:find(fileName == '.')-1);
    tic,
    ovaryStringFile = [TRAIN_DIR, '/ovary/stage', num2str(STAGE), '/train', num2str(ID), '.tif'];
    maskStringFile = [TRAIN_DIR, '/mask/stage', num2str(STAGE), '/mask', num2str(ID), '.png'];
    [trainData2(fileIndex).background, trainData2(fileIndex).border, ...
        trainData2(fileIndex).nurse, trainData2(fileIndex).cyto] = getLabeledFeatures(ovaryStringFile, maskStringFile);
    toc;
end

%%
% trainData = [trainData1; trainData2; trainData3; trainData4; trainData5];
trainData = trainData2;

%%
textonData = repmat(struct('background', [], 'border', [], 'nurse', [], 'cyto', []), ...
    length(trainData), 1);
%KTEXTONS = 50;

for fileIndex = 1:length(trainData)
    disp(['createTextons: ', num2str(fileIndex), ' of ', num2str(length(trainData))])
    BACK = trainData(fileIndex).background;
    BOR = trainData(fileIndex).border;
    NUR = trainData(fileIndex).nurse;
    CYT = trainData(fileIndex).cyto;
    %{
    BACK (:,4:3:end)= [];
    BOR (:,4:3:end)= [];
    NUR (:,4:3:end)= [];
    CYT (:,4:3:end)= [];
    %}
    [textonData(fileIndex).background, textonData(fileIndex).border, ...
        textonData(fileIndex).nurse, textonData(fileIndex).cyto] = ...
        createTextons(featNorm(BACK),...
        featNorm(BOR), ...
        featNorm(NUR),...
        featNorm(CYT), []);
end

%%
% Classification
STAGE = 2;
filesTest = dir([TRAIN_DIR, '/ovary/stage', num2str(STAGE)]);
filesTest(1:2, :) = []; % erase '.' and '..' directories
Accuracy = zeros(54,1);
ConfusionMatrix = zeros(4,4,54);
Precision = zeros(54,4);
Sensitivity = zeros(54,4);
Ta = zeros(4,54);

for fileIndex = 1:1 % length(filesTest)
    tic,
    fileName = filesTest(fileIndex).name;
    ID = fileName(6:find(fileName == '.')-1);
    ovaryStringFile = [TRAIN_DIR, '/ovary/stage', num2str(STAGE), '/train', num2str(ID), '.tif'];
    ovary = imread(ovaryStringFile); % ovary is uint16
    ovary = double(ovary(:, :, 1));
    L1OTextonData = textonData;
    %L1OTextonData(fileIndex,:) = [];
    
    segmentedImage = findLabels(ovaryStringFile, L1OTextonData, []);
    
    conf_matrix = zeros(4,4);
    maskStringFile = [TRAIN_DIR, '/mask/stage', num2str(STAGE), '/mask', num2str(ID), '.png'];
    mask = imread(maskStringFile);
    
    for type = 0:3
        index = find(mask == type);
        conf_matrix(type + 1, 1) = sum(segmentedImage(index) == 0);
        conf_matrix(type + 1, 2) = sum(segmentedImage(index) == 1);
        conf_matrix(type + 1, 3) = sum(segmentedImage(index) == 2);
        conf_matrix(type + 1, 4) =  sum(segmentedImage(index) == 3);
        Ta(type + 1, fileIndex) = ... 
            sum(segmentedImage(index) == type) / (length(index) + length(find(segmentedImage == type))-sum(segmentedImage(index) == type));
    end
    
     [ConfusionMatrix(:,:,fileIndex), Accuracy(fileIndex), Precision(fileIndex,:), Sensitivity(fileIndex,:)] = ...
        cfmatrix2NEW(conf_matrix', {'BK', 'BOR', 'NUR', 'CYT'}, 1, 1);
    %cfmatrix2NEW(conf_matrix', {'BK', 'BOR', 'NUR', 'CYT'}, 1, 1);
    
    %  Just for visualization
    label = 1;
    contImage = visualizeLabels(ovary, segmentedImage, label, [1,1,0]);
    label = 2;
    contImage = visualizeLabels(contImage, segmentedImage, label, [0,1,1]);
    
%     figure(1), 
%     subplot(1,2,1), imshow(mat2gray(ovary)); 
%     subplot(1,2,2), imshow(contImage);    
%     figure(2), 
%     imagesc(segmentedImage);
    %imwrite(uint8(segmentedImage), ['test\segmentation\stage', num2str(STAGE), '\segm', num2str(ID), '.png'])
    toc;    
end
