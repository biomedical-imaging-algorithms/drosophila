function classif = classifyKNN(listOfFeatures, trainFeatures, labelTraining)

% https://github.com/antagomir/scripts/blob/master/matlab/knnclassify.m
% http://people.na.infn.it/~cavalier/Download/DSP_Informatica/TestsMatlab/knnclassify.m
% http://freesourcecode.net/matlabprojects/59395/knn-method--in-matlab#.V9_aOnWUcW1

% 23 is a prime number
classif = knnclassify(listOfFeatures, trainFeatures, labelTraining, 5);

end
