function V = readMultiPageTiff(fname)

% fprintf('Reading %s...\n', fname);

info = imfinfo(fname);

W = info(1).Width;
H = info(1).Height;
Z = numel(info);



if info(1).BitDepth == 16
    V = zeros(W,H,Z, 'uint16');
elseif info(1).BitDepth == 8
    V = zeros(W,H,Z, 'uint8');
else
    V = zeros(W,H,Z, 'single');
end

% pb = createProgressBar(Z);
for k = 1:Z
    I = imread(fname, k, 'Info', info);
    V(:,:,k) = I';
%     pb = updateProgessBar(pb, k);
end

% maxV = max(V(:));
% V = fix_alan_acquire(V, maxV);

% % check if we need to correct for microscope acquisition weirdness
% if max(V(:)) == 8288;
%     disp('...correcting for microscope acquisition weirdness.');
%     V = fix_alan_acquire(V);
% end
% 
% 
