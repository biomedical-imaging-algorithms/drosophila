

suppressRegion = [71 71 3];
ratio_min = .65;
value_ratio_min = .2;

path = fullfile(fileparts(mfilename('fullpath')), 'loci_tools.jar');
javaaddpath(path);

filename = 'MD-Experiment-2011-04-13-11914_Position(14).zvi';

% vol = bfopen(filename);

omemeta = vol{4};
%% Meta values
fprintf('Spacing (x,y,z): %f %s, %f %s, %f %s\n', ...
    double(omemeta.getPixelsPhysicalSizeX(0).value), char(omemeta.getPixelsPhysicalSizeX(0).unit.getSymbol()), ...
    double(omemeta.getPixelsPhysicalSizeY(0).value), char(omemeta.getPixelsPhysicalSizeY(0).unit.getSymbol()), ...
    double(omemeta.getPixelsPhysicalSizeZ(0).value), char(omemeta.getPixelsPhysicalSizeZ(0).unit.getSymbol()));
fprintf('Size (x,y,z,c): (%d, %d, %d, %d)\n', omemeta.getPixelsSizeX(0).getValue(), ...
    omemeta.getPixelsSizeY(0).getValue(), omemeta.getPixelsSizeZ(0).getValue(), omemeta.getPixelsSizeC(0).getValue());

stack = zeros(omemeta.getPixelsSizeY(0).getValue(), omemeta.getPixelsSizeX(0).getValue(), ...
    omemeta.getPixelsSizeZ(0).getValue(), omemeta.getPixelsSizeC(0).getValue());

for c=1:size(stack,4)
    stack(:,:,:,c) = cat(3, vol{1}{ 1+(c-1)*size(stack,3):c*size(stack,3) ,1 });
end

%%
i=11;
im = uint8(stack(:,:,i,2));
threshold = graythresh(im);

sr = (suppressRegion-1)/2;

figure(1);
for i=4:size(stack,3)
    fprintf('%d', i);
    
    im = uint8(stack(:,:,i,2));
    bw = im2bw(im,threshold);
    
    bw_cp = bw;
    st_cp = stack(:,:,i,2);
    bw_cp(st_cp<value_ratio_min*max(st_cp(:))) = 0;
    st_cp(st_cp<value_ratio_min*max(st_cp(:))) = 0;
%     while any(st_cp(:)>1)
%         [v,ind] = max(st_cp(:));
%         if v==0, break, end;
%         [a,b] = ind2sub(size(st_cp),ind);
%         
%         r = max(1,a-sr(1)):min(size(st_cp,1),a+sr(1));
%         c = max(1,b-sr(2)):min(size(st_cp,2),b+sr(2));
%         
%         reg = bw(r,c);
%         
% %         figure(2)
% %         imshow(st_cp); hold on;
% %         rectangle('Position',[c(1),r(1),suppressRegion(1),suppressRegion(2)],'EdgeColor',[0 0 1]);
% %         figure(3)
% %         imshow(bw_cp); hold on;
% %         rectangle('Position',[c(1),r(1),suppressRegion(1),suppressRegion(2)],'EdgeColor',[0 0 1]);
% %         drawnow;
% %         fprintf('%d, %d, %d, %.2f\n', a,b,v,sum(reg(:)==1)./numel(reg));
%         
%         L = bwlabel(st_cp,8);
%         if sum(reg(:)==1)<numel(reg)*ratio_min
%             bw_cp(L==L(a,b)) = 0;
% %             fprintf('sup, ');
%         end
%         st_cp(L==L(a,b)) = 0;
%     end
    
    subplot(2,3,1);
    imagesc(2.*bw_cp+bw); axis equal; axis off;
    
    bw = bw_cp;
    
    small = zeros(size(bw));
    for j=5:5:5
        small = logical(small + imopen(bw, strel('disk', j)));
    end

    subplot(2,3,2);
    filled = imfill(bw, 'holes');
%     filled = filled-small;
    imshow(filled);

    subplot(2,3,3);
    CHant = filled;
    CH = bwconvhull(filled, 'objects');
    imshow(CH);
    
    cleared = zeros(size(im));
    for j=20:5:80
        cleared = logical(cleared + imopen(CH, strel('disk', j)));
    end
    subplot(2,3,4);
    imshow(cleared);
    
    L = bwlabel(cleared,4);
    subplot(2,3,5);
    imagesc(L); axis equal; axis off;
    
    subplot(2,3,6);
    imshow(uint8(stack(:,:,i,2)));

    fprintf(', ');
    waitforbuttonpress;
end
fprintf('\n');






