
% addpath(genpath('.'));

path = fullfile(fileparts(mfilename('fullpath')), 'loci_tools.jar');
javaaddpath(path);

filename = 'MD-Experiment-2011-04-13-11914_Position(14).zvi';
filetiff = [filename(1:end-4) '_mag.tif'];

% vol = bfopen(filename);

omemeta = vol{4};
%% Meta values
fprintf('Spacing (x,y,z): %f %s, %f %s, %f %s\n', ...
    double(omemeta.getPixelsPhysicalSizeX(0).value), char(omemeta.getPixelsPhysicalSizeX(0).unit.getSymbol()), ...
    double(omemeta.getPixelsPhysicalSizeY(0).value), char(omemeta.getPixelsPhysicalSizeY(0).unit.getSymbol()), ...
    double(omemeta.getPixelsPhysicalSizeZ(0).value), char(omemeta.getPixelsPhysicalSizeZ(0).unit.getSymbol()));
fprintf('Size (x,y,z,c): (%d, %d, %d, %d)\n', omemeta.getPixelsSizeX(0).getValue(), ...
    omemeta.getPixelsSizeY(0).getValue(), omemeta.getPixelsSizeZ(0).getValue(), omemeta.getPixelsSizeC(0).getValue());

stack = zeros(omemeta.getPixelsSizeY(0).getValue(), omemeta.getPixelsSizeX(0).getValue(), ...
    omemeta.getPixelsSizeZ(0).getValue(), omemeta.getPixelsSizeC(0).getValue());

for c=1:size(stack,4)
    stack(:,:,:,c) = cat(3, vol{1}{ 1+(c-1)*size(stack,3):c*size(stack,3) ,1 });
end

imgreen = stack(:,:,:,1);
maxgreen = max(imgreen(:));
immagen = stack(:,:,:,2);
maxmagen = max(immagen(:));

for i=1:size(stack,3)
    im = zeros(size(stack,1), size(stack,2), 3);
    im(:,:,2) = 255*stack(:,:,i,1)./maxgreen;
    im(:,:,1) = 255*stack(:,:,i,2)./maxmagen;
    im(:,:,3) = 255*stack(:,:,i,2)./maxmagen;
    
    im2 = zeros(size(stack,1), size(stack,2), 3);
%     im2(:,:,2) = stack(:,:,i,1);
    im2(:,:,1) = stack(:,:,i,2);
    im2(:,:,3) = stack(:,:,i,2);
    
    im8 = uint8(im2);
    imshow(im8);
%     figure(1);
%     s=i; seg = vl_slic(single(stack(:,:,s,2)), 100, 50); subplot(1,2,1); imshow(uint8(stack(:,:,s,2))); subplot(1,2,2); imagesc(seg); colormap gray;
%     axis equal;
%     drawnow;
    imwrite(im8, filetiff, 'tif', 'Compression', 'none', 'WriteMode', 'append');
    
    fprintf('stack %d, max %d, max uint8 %d\n', i, max(im2(:)), max(im8(:)));
    
    waitforbuttonpress;
end


