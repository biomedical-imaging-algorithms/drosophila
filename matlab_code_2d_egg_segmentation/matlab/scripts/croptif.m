clear;

addpath(genpath('../toolbox'));

% filename = 'MD-Experiment-2011-02-21-11498_Position(3)';
filename = 'MD-Experiment-2011-02-21-11500_Position(1)';
% filename = 'MD-Experiment-2011-02-21-11524_Position(5)';
% filename = 'MD-Experiment-2011-02-24-11546_Position(14)';
% filename = 'MD-Experiment-2011-04-15-11971_Position(8)';
% filename = 'MD-Experiment-2011-04-15-11971_Position(28)';
% filename = 'MD-Experiment-2011-04-29-12250_Position(10)'; f = 0;
% filename = 'MD-Experiment-2011-04-29-12250_Position(10)'; f = 1;
% filename = 'MD-Experiment-2011-04-29-12250_Position(15)';

% filename = 'MD-Experiment-2011-02-25-11551_Position(10)'; f = 0;
% filename = 'MD-Experiment-2011-02-25-11551_Position(10)'; f = 1;
% filename = 'MD-Experiment-2011-02-25-11553_Position(14)'; f = 0;
% filename = 'MD-Experiment-2011-02-25-11553_Position(14)'; f = 1;
% filename = 'MD-Experiment-2011-02-25-11556_Position(1)';
% filename = 'MD-Experiment-2011-02-25-11557_Position(8)'; f = 0;
% filename = 'MD-Experiment-2011-02-25-11557_Position(8)'; f = 1;
% filename = 'MD-Experiment-2011-02-27-11558_Position(2)';
% filename = 'MD-Experiment-2011-02-27-11559_Position(2)';
% filename = 'MD-Experiment-2011-02-27-11564_Position(14)';
% filename = 'MD-Experiment-2011-02-28-11567_Position(6)';
% filename = 'MD-Experiment-2011-02-28-11569_Position(9)';

switch filename
    case 'MD-Experiment-2011-02-21-11498_Position(3)'
        cpx = 482:748;
        cpy = 309:612;
        out_filename = 'cropped_ovary_0001';
    case 'MD-Experiment-2011-02-21-11500_Position(1)'
        cpx = 454:718;
        cpy = 488:686;
        out_filename = 'cropped_ovary_0002';
    case 'MD-Experiment-2011-02-21-11524_Position(5)'
        cpx = 964:1257;
        cpy = 413:801;
        out_filename = 'cropped_ovary_0003';
    case 'MD-Experiment-2011-02-24-11546_Position(14)'
        cpx = 598:966;
        cpy = 345:608;
        out_filename = 'cropped_ovary_0004';
    case 'MD-Experiment-2011-04-15-11971_Position(8)'
        cpx = 376:614;
        cpy = 465:705;
        out_filename = 'cropped_ovary_0005';
    case 'MD-Experiment-2011-04-15-11971_Position(28)'
        cpx = 970:1292;
        cpy = 488:833;
        out_filename = 'cropped_ovary_0006';
    case 'MD-Experiment-2011-04-29-12250_Position(10)'
        if f==0
            cpx = 388:634;
            cpy = 430:709;
            out_filename = 'cropped_ovary_0007';
        elseif f==1
            cpx = 128:412;
            cpy = 634:905;
            out_filename = 'cropped_ovary_0008';
        end
    case 'MD-Experiment-2011-04-29-12250_Position(15)'
        cpx = 404:693;
        cpy = 293:556;
        out_filename = 'cropped_ovary_0009';
    case 'MD-Experiment-2011-02-25-11551_Position(10)'
        if f==0
            cpx = 446:745;
            cpy = 208:491;
            out_filename = 'cropped_ovary_0010';
        elseif f==1
            cpx = 708:941;
            cpy = 370:612;
            out_filename = 'cropped_ovary_0011';
        end
    case 'MD-Experiment-2011-02-25-11553_Position(14)'
        if f==0
            cpx = 713:1028;
            cpy = 370:620;
            out_filename = 'cropped_ovary_0012';
        elseif f==1
            cpx = 557:788;
            cpy = 577:821;
            out_filename = 'cropped_ovary_0013';
        end
    case 'MD-Experiment-2011-02-25-11556_Position(1)'
        cpx = 876:1132;
        cpy = 534:770;
        out_filename = 'cropped_ovary_0014';
    case 'MD-Experiment-2011-02-25-11557_Position(8)'
        if f==0
            cpx = 578:953;
            cpy = 582:905;
            out_filename = 'cropped_ovary_0015';
        elseif f==1
            cpx = 892:1130;
            cpy = 444:708;
            out_filename = 'cropped_ovary_0016';
        end
    case 'MD-Experiment-2011-02-27-11558_Position(2)'
        cpx = 325:609;
        cpy = 514:757;
        out_filename = 'cropped_ovary_0017';
    case 'MD-Experiment-2011-02-27-11559_Position(2)'
        cpx = 668:956;
        cpy = 144:448;
        out_filename = 'cropped_ovary_0018';
    case 'MD-Experiment-2011-02-27-11564_Position(14)'
        cpx = 826:1056;
        cpy = 237:556;
        out_filename = 'cropped_ovary_0019';
    case 'MD-Experiment-2011-02-28-11567_Position(6)'
        cpx = 512:825;
        cpy = 509:752;
        out_filename = 'cropped_ovary_0020';
    case 'MD-Experiment-2011-02-28-11569_Position(9)'
        cpx = 810:1097;
        cpy = 492:729;
        out_filename = 'cropped_ovary_0021';
end

path = fullfile(fileparts(mfilename('fullpath')), '../toolbox/bfmatlab/loci_tools.jar');
javaaddpath(path);

vol = bfopen(['../../../data/original_stacks/' filename '.zvi']);

omemeta = vol{4};

stack = zeros(omemeta.getPixelsSizeY(0).getValue(), omemeta.getPixelsSizeX(0).getValue(), ...
    omemeta.getPixelsSizeZ(0).getValue(), omemeta.getPixelsSizeC(0).getValue());

for c=1:size(stack,4)
    stack(:,:,:,c) = cat(3, vol{1}{ 1+(c-1)*size(stack,3):c*size(stack,3) ,1 });
end

crp_stack = stack(cpy,cpx,:,:);
crp_stack = uint16(crp_stack);

delete(['../../../data/cropped/' out_filename '_grn.tif']);
delete(['../../../data/cropped/' out_filename '_mag.tif']);
bfsave(crp_stack(:,:,:,1), ['../../../data/cropped/' out_filename '_grn.tif']);
bfsave(crp_stack(:,:,:,2), ['../../../data/cropped/' out_filename '_mag.tif']);

fprintf('Spacing (x,y,z): %f %s, %f %s, %f %s\n', ...
    double(omemeta.getPixelsPhysicalSizeX(0).value), char(omemeta.getPixelsPhysicalSizeX(0).unit.getSymbol()), ...
    double(omemeta.getPixelsPhysicalSizeY(0).value), char(omemeta.getPixelsPhysicalSizeY(0).unit.getSymbol()), ...
    double(omemeta.getPixelsPhysicalSizeZ(0).value), char(omemeta.getPixelsPhysicalSizeZ(0).unit.getSymbol()));
fprintf('%s\t\t%s\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\n', filename, out_filename, cpx(1), cpy(1), 1, length(cpx), length(cpy), size(crp_stack,3));
