clear;

f = fopen('all_ovary_image_info_for_prague.txt', 'r');

sline = fgets(f);
sline = fgets(f);

c = strsplit(sline, '\t');
sline = fgets(f);
dat = c;

while ischar(sline)
    c = strsplit(sline, '\t');
    
    dat = [dat; c];
    
    sline = fgets(f);
end

save('desc.mat', 'dat');