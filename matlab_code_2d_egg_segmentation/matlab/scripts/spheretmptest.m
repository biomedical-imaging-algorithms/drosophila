

% parallel = true;
parallel = false;

if strcmp(computer,'MACI64')
    filename = 'MD-Experiment-2011-04-13-11914_Position(14).zvi';
    if parallel, poolobj = parpool; end; %#ok<*UNRCH>
else
    addpath(genpath('../toolbox'));
    filename = '/datagrid/Medical/microscopy/drosophila/ovary_raw_stacks/MD-Experiment-2011-04-13-11914_Position(14).zvi';
    if parallel, matlabpool; end;
end

vol = bfopen(filename);
omemeta = vol{4};

c=2;
stackm = cat(3, vol{1}{ 1+(c-1)*omemeta.getPixelsSizeZ(0).getValue():c*omemeta.getPixelsSizeZ(0).getValue() ,1 });

% filetiff = 'spheretest.tif';

spacing = [.258 .258 3.00]; % in um/px

% center = [220 256 11];
radius = 20;
val = double(max(stackm(:)));
radius_px = radius./spacing;

rows = 1:size(stackm,1);
cols = 1:size(stackm,2);
zeds = 1:size(stackm,3);
[cols,rows,zeds] = meshgrid(cols,rows,zeds);

% inCircle = (((r-center(1))).^2 + (c-center(2)).^2) <= radius_px(1).^2;
inSphere = @(center) (((rows-center(1))./radius_px(2)).^2 + ((cols-center(2))./radius_px(1)).^2 + ((zeds-center(3))./radius_px(3)).^2) <= 1;

dist = zeros(size(stackm));
siz = [size(stackm,1) size(stackm,2)];

rd = radius_px;
sphere = zeros(ceil(2*rd)+1+mod(ceil(2*rd),2));
ctr = (size(sphere)+1)/2;
row = 1:size(sphere,1);
col = 1:size(sphere,2);
zed = 1:size(sphere,3);
[col,row,zed] = meshgrid(col,row,zed);
sphere = double((((row-ctr(1))./rd(2)).^2 + ((col-ctr(2))./rd(1)).^2 + ((zed-ctr(3))./rd(3)).^2) <= 1);
sphere(sphere==1) = 1./sum(sphere(:));

if parallel
    parfor_progress(length(floor(radius_px(3)):size(stackm,3)-floor(radius_px(3))));
    parfor z=floor(radius_px(3)):size(stackm,3)-floor(radius_px(3))
        d = zeros(siz);
        for c=1:1:size(stackm,2)
            for r=1:1:size(stackm,1)
                sph = (((rows-r)./rd(2)).^2 + ((cols-c)./rd(1)).^2 + ((zeds-z)./rd(3)).^2) <= 1;
                d(r,c) = sum(stackm(sph(:)))./(val.*sum(sph(:)));
            end
        end
        dist(:,:,z) = d;
        parfor_progress;
    end
    parfor_progress(0);
else
%     for z=floor(radius_px(3)):size(stackm,3)-floor(radius_px(3))
%         d = zeros(siz);
%         for c=1:1:size(stackm,2)
%             for r=1:1:size(stackm,1)
%                 tic;
%                 sph = (((rows-r)./rd(2)).^2 + ((cols-c)./rd(1)).^2 + ((zeds-z)./rd(3)).^2) <= 1;
%                 toc
%                 tic;
%                 d(r,c) = sum(stackm(sph(:)))./(val.*sum(sph(:)));
%                 toc
%             end
%         end
%         dist(:,:,z) = d;
%     end
    dist = convn(stackm,sphere,'full');
end

save('dist2.mat', 'dist');

% figure(2);
% for i=1:size(stackm,3)
%     mag = stackm(:,:,i);
%     mag(inSphere(:,:,i)) = 255;
%     im = uint8(cat(3,mag, stackg(:,:,i), mag));
%     imshow(im);
% %     imwrite(im, filetiff, 'tif', 'Compression', 'none', 'WriteMode', 'append');
%     drawnow;
%     pause(.5);
% end
if parallel
    if strcmp(computer, 'MACI64')
        delete(poolobj);
    else
        matlabpool close;
    end
end

