

% filename = 'MD-Experiment-2011-04-13-11914_Position(14)_mag.tif';
filename = 'MD-Experiment-2011-04-13-11914_Position(14).tif';

sv_size = 30; % in um
spacing = [.258 .258 3.00]; % in um/px
m=35;
max_it=10;
convergence=1;

info = imfinfo(filename);
W = info(1).Width;
H = info(1).Height;
Z = numel(info);

stackm = zeros(H,W,Z, 'single');
stackg = zeros(H,W,Z, 'single');
for k = 1:Z
    I = imread(filename, k, 'Info', info);
    stackm(:,:,k) = I(:,:,1);
    stackg(:,:,k) = I(:,:,2);
end

% for i=1:size(stack,3)
%     imshow(uint8(stack(:,:,i)));
%     drawnow;
%     pause(.1);
% end

sv_size_px = sv_size./spacing;

kvector = round([size(stackm,2) size(stackm,1) size(stackm,3)]./sv_size_px);

S = [size(stackm,2) size(stackm,1) size(stackm,3)]./kvector;

grr = (S(2)-1)/2+1:S(2):size(stackm,1)-(S(2)-1)/2;
grc = (S(1)-1)/2+1:S(1):size(stackm,2)-(S(1)-1)/2;
grz = (S(3)-1)/2+1:S(3):size(stackm,3)-(S(3)-1)/2;

[y,x,z] = meshgrid(grr,grc,grz);
x = x(:); y = y(:); z = z(:);

% imshow(im); hold on;
% plot(y, x, '.');

ind = sub2ind(size(stackm),round(y),round(x),round(z));

% init clusters
C = [stackm(ind) stackg(ind) zeros(length(x),1) x y z];

% G = zeros(size(stack));
% for x = 2:size(stack,2)-1
%     for y = 2:size(stack,1)-1
%         for z = 2:size(stack,3)-1
%             G(y,x,z) = sqrt(sum((stack(y,x+1,z)-stack(y,x-1,z)).^2)) + sqrt(sum((stack(y+1,x,z)-stack(y-1,x,z)).^2)) + sqrt(sum((stack(y,x,z+1)-stack(y,x,z-1)).^2));
%         end
%     end
% end

label = -1.*ones(size(stackm));
dists = Inf(size(stackm));

for it=1:max_it
    for k=1:size(C,1)
        regr = round(max(1,C(k,5)-2*S(2)):min(C(k,5)+2*S(2),size(stackm,1)));
        regc = round(max(1,C(k,4)-2*S(1)):min(C(k,4)+2*S(1),size(stackm,2)));
        regz = round(max(1,C(k,6)-2*S(3)):min(C(k,6)+2*S(3),size(stackm,3)));
        
        dc = sqrt((stackm(regr,regc,regz)-C(k,1)).^2 + (stackg(regr,regc,regz)-C(k,2)).^2);
        ds = sqrt(bsxfun(@plus, permute(bsxfun(@plus, ((regz-C(k,6)).*spacing(3)).^2, ((regr'-C(k,5)).*spacing(2)).^2),[1 3 2]), ((regc-C(k,4)).*spacing(1)).^2));
        D = sqrt(dc.^2 + m^2.*(ds./sv_size).^2);
        
        dreg = dists(regr,regc,regz);
        lreg = label(regr,regc,regz);
        
        lreg(D<dreg) = k;
        dreg(D<dreg) = D(D<dreg);
        
        label(regr,regc,regz) = lreg;
        dists(regr,regc,regz) = dreg;
        
        progressbar(k, size(C,1), 20);
    end
    figure(1);
    subplot(1,2,1);
    imshow(uint8(cat(3,stackm(:,:,11), stackg(:,:,11), stackm(:,:,11)))); freezeColors;
    subplot(1,2,2);
    imagesc(label(:,:,11)); axis equal; axis off; colormap colorcube;
    drawnow;
    
    old_cluster_position = C(:,4:6);
    for k=1:size(C,1)
        bw = (label == k);
        [r,c,z] = ind2sub(size(label),find(bw));
        C(k,4:6) = mean([c,r,z]);
        C(k,1) = stackm(round(C(k,5)), round(C(k,4)), round(C(k,6)));
        C(k,2) = stackg(round(C(k,5)), round(C(k,4)), round(C(k,6)));
        
        progressbar(k, size(C,1), 20);
    end
    E = sum(sqrt(sum((C(:,4:6)-old_cluster_position).^2,2)));
    
    fprintf('Iteration %d, Residual error %.2f\n', it, E);
    if E<convergence, break; end
end

%% Post-processing
fprintf('Post-processing...\n');
postlabels = label;
% conn = cat(3, [0 1 0; 1 1 1; 0 1 0],...
%               [1 1 1; 1 1 1; 1 1 1],...
%               [0 1 0; 1 1 1; 0 1 0]);
for k=1:size(C,1)
    bw = (postlabels == k);

    cc = bwconncomp(bw, 18);
    L8 = labelmatrix(cc);

    
    inds = find(and(L8~=L8(round(C(k,5)), round(C(k,4)), round(C(k,6))),L8>0));
    [r,c,z] = ind2sub(size(postlabels),inds);

    dsk = pdist2([c r z], double(C(:,4:6)));
    dsk(:,k) = Inf;
    
    [~,mins] = min(dsk,[],2);
    postlabels(inds) = mins;
    
    progressbar(k, size(C,1), 20);
end





