function x = describeRegion(imc, radius, params)

if all(imc(:)==0)
    im_norm = zeros(size(imc));
else
    im_norm = double(imc)./max(double(imc(:)));
end

rows = 1:size(imc,1);
cols = 1:size(imc,2);
zeds = 1:size(imc,3);
[cols,rows,zeds] = meshgrid(cols,rows,zeds);
c = (size(imc)-1)/2;
sph = (((rows-c(1))./radius(2)).^2 + ((cols-c(2))./radius(1)).^2 + ((zeds-c(3))./radius(3)).^2) <= 1;

im_sph = im_norm(sph);

x = hist(im_sph(:),params.bins);
x = x./sum(x);

end