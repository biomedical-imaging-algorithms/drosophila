

ds_train = {'cropped_ovary_0001','cropped_ovary_0002','cropped_ovary_0003',...
    'cropped_ovary_0004','cropped_ovary_0005','cropped_ovary_0006'};

no_seeds = 15*length(ds_train);
neg_samples = 4*no_seeds;
n_bins = 15;

if strcmp(computer,'MACI64')
    datadir = '../../../data/';
else
    datadir = '/datagrid/Medical/microscopy/drosophila/ovary_test_stacks/';
end

neg_samples_perim = ceil(neg_samples/length(ds_train));
samples = [];
class = {};

for i=1:length(ds_train)
    ds = ds_train{i};
    dscolor = [ds '_mag.tif'];
    
    filename = [datadir 'cropped/' dscolor];
    load([datadir 'seeds_mat/' ds '.mat']);
    load([datadir 'groundtruth/seeds_' ds '.mat']);
    
    params.bins = n_bins;
    stack = readMultiPageTiff(filename);
    
    if sum(params.sig_dif==0)
        nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius+params.sig_dif, params.spacing')';
    else
        nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius.*params.sig_dif, params.spacing')';
    end
    
    seeds_radii = zeros(size(seeds_gt_loc,1),1);
    
    % Positive samples
    for s=1:size(seeds_gt_loc,1)
        radius = nurseCellAllPixRadius(MS(seeds_gt_loc(s,1),seeds_gt_loc(s,2),seeds_gt_loc(s,3)),:);
        realradius = radius(1).*params.spacing(1);
        seeds_radii(s) = realradius;
        
        fsize = round(2.*radius);
        fsize = (fsize - mod(fsize,2))/2;
        
        cropr = min(max(1,seeds_gt_loc(s,1) + (-fsize(1):fsize(1))),size(stack,1));
        cropc = min(max(1,seeds_gt_loc(s,2) + (-fsize(2):fsize(2))),size(stack,2));
        cropz = min(max(1,seeds_gt_loc(s,3) + (-fsize(3):fsize(3))),size(stack,3));
        
        imc = stack(cropr,cropc,cropz);
%         imc = LoGMP(cropr,cropc,cropz);
        
        samples = [samples; describeRegion(imc, radius, params)]; %#ok<AGROW>
        class = [class; {'positive'}]; %#ok<AGROW>
        
%         return;
    end
    
    % Negative samples
    for s=1:neg_samples_perim
        radius = nurseCellAllPixRadius(randi(size(nurseCellAllPixRadius,1)),:);
        realradius = radius(1).*params.spacing(1);
        dist = Inf;
        
        realc = seeds_gt_loc(1,:).*params.spacing;
        
        while any(sqrt(sum(bsxfun(@minus, bsxfun(@times,seeds_gt_loc,params.spacing), realc).^2,2)) < seeds_radii)
            c = [randi(size(stack,1)) randi(size(stack,2)) randi(size(stack,3))];
            realc = c.*params.spacing;
        end
        
        fsize = round(2.*radius);
        fsize = (fsize - mod(fsize,2))/2;
        
        cropr = min(max(1,c(1) + (-fsize(1):fsize(1))),size(stack,1));
        cropc = min(max(1,c(2) + (-fsize(2):fsize(2))),size(stack,2));
        cropz = min(max(1,c(3) + (-fsize(3):fsize(3))),size(stack,3));
        
        imc = stack(cropr,cropc,cropz);
%         imc = LoGMP(cropr,cropc,cropz);
        
        samples = [samples; describeRegion(imc, radius, params)]; %#ok<AGROW>
        class = [class; {'negative'}]; %#ok<AGROW>
        
%         return;
    end
    
%     return;
end

SVMModel = fitcsvm(samples,class);

save('trainedModel.mat', 'SVMModel', 'ds_train', 'params');


