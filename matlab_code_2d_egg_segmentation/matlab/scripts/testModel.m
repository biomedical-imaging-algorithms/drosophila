clear;

ds_test = {'cropped_ovary_0001','cropped_ovary_0002','cropped_ovary_0003',...
    'cropped_ovary_0004','cropped_ovary_0005','cropped_ovary_0006',...
    'cropped_ovary_0007','cropped_ovary_0008','cropped_ovary_0009',...
    'cropped_ovary_0010','cropped_ovary_0011','cropped_ovary_0012',...
    'cropped_ovary_0013','cropped_ovary_0014','cropped_ovary_0015',...
    'cropped_ovary_0016','cropped_ovary_0017','cropped_ovary_0018',...
    'cropped_ovary_0019','cropped_ovary_0020','cropped_ovary_0021'};

n_bins = 15;

if strcmp(computer,'MACI64')
    datadir = '../../../data/';
else
    datadir = '/datagrid/Medical/microscopy/drosophila/ovary_test_stacks/';
end

for i=1:length(ds_test)
    ds = ds_test{i};
    dscolor = [ds '_mag.tif'];
    
    filename = [datadir 'cropped/' dscolor];
    load([datadir 'seeds_mat/' ds '.mat']);
    load([datadir 'groundtruth/seeds_' ds '.mat']);
    load('trainedModel.mat');
    stack = readMultiPageTiff(filename);
    
    tp=0; fp=0; tn=0; fn=0;
    
    if sum(params.sig_dif==0)
        nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius+params.sig_dif, params.spacing')';
    else
        nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius.*params.sig_dif, params.spacing')';
    end

    INLIER_MIN_DIST = params.nurseCellRealRadius;
    sds_gt_real_loc = bsxfun(@times, seeds_gt_loc, params.spacing);
    sds_real_loc = bsxfun(@times, seeds, params.spacing);
    inliers = 0;
    outliers = 0;
    already_found = zeros(1,size(sds_gt_real_loc,1));
    inlier_seeds = zeros(1,size(seeds,1));
    px_inlier_seeds = zeros(1,size(seeds,1));
    
    samples = zeros(size(seeds,1),params.bins);
    for s=1:size(sds_real_loc,1)
        
        radius = nurseCellAllPixRadius(MS(seeds(s,1),seeds(s,2),seeds(s,3)),:);
        realradius = radius(1).*params.spacing(1);
        
        fsize = round(2.*radius);
        fsize = (fsize - mod(fsize,2))/2;
        
        cropr = min(max(1,seeds(s,1) + (-fsize(1):fsize(1))),size(stack,1));
        cropc = min(max(1,seeds(s,2) + (-fsize(2):fsize(2))),size(stack,2));
        cropz = min(max(1,seeds(s,3) + (-fsize(3):fsize(3))),size(stack,3));
        
        imc = stack(cropr,cropc,cropz);
        samples(s,:) = describeRegion(imc, radius, params);
        
        
        dist = pdist2(sds_real_loc(s,:), sds_gt_real_loc);
        pxdist = pdist2(seeds(s,:), seeds_gt_loc);
        
%         dist(logical(already_found)) = Inf;
        k = find(dist<INLIER_MIN_DIST,1);
        if ~isempty(k)
            % Inlier
            if samples(s,:) * SVMModel.Beta + SVMModel.Bias < 0 % Negative
                fn=fn+1;
            else % Positive
                tp=tp+1;
            end
            inliers=inliers+1;
            already_found(k)=1;
            continue;
        end
        % Outlier
        if samples(s,:) * SVMModel.Beta + SVMModel.Bias < 0 % Negative
            tn=tn+1;
        else % Positive
            fp=fp+1;
        end
        inlier_seeds(s)=min(dist);
        px_inlier_seeds(s)=min(pxdist);
        outliers = outliers+1;
    end
    fprintf('%s, %d seeds detected. TP:%d, \tFP:%d, \tTN:%d, \tFN:%d, \tSens:%.3f, \tFPR:%.3f\n', ds, size(seeds,1), tp, fp, tn, fn, tp/(tp+fn), fp/(fp+tn));
    
%     return;
end




