function [seeds,LoGMP,MS,params] = detectSeeds(filename, params)

if nargin<2
    params.nurseCellRealRadius = 8.0;
    params.sig_dif = -2:1:2;
    params.spacing = [1 1 1];
    
    params.MIN_INTENSITY = .2;
    params.LoGMP_THRESHOLD = .8;
end

if ~isfield(params, 'nurseCellRealRadius')
    params.nurseCellRealRadius = 8.0;
end
if ~isfield(params, 'sig_dif')
    params.sig_dif = -2:1:2;
end
if ~isfield(params, 'spacing')
    params.spacing = [1 1 1];
end
if ~isfield(params, 'MIN_INTENSITY')
    params.MIN_INTENSITY = .2;
end
if ~isfield(params, 'LoGMP_THRESHOLD')
    params.LoGMP_THRESHOLD = .8;
end

h = (params.spacing(1)./params.spacing(3)).^2;

fprintf('#LoGMP_THRESHOLD = %.2f, MIN_INTENSITY = %.2f, sig_dif = \n', params.LoGMP_THRESHOLD, params.MIN_INTENSITY);
disp(['#' num2str(params.sig_dif)]);

if sum(params.sig_dif==0)
    nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius+params.sig_dif, params.spacing')';
else
    nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius.*params.sig_dif, params.spacing')';
end

stack = readMultiPageTiff(filename);
sigs = nurseCellAllPixRadius./sqrt(2);

stack(stack<params.MIN_INTENSITY.*max(stack(:))) = 0;

tic;
sig = sigs(1,:);
sec_dev = cat(3, [0 0 0;  0 -sig(1)^3 0;  0 0 0],...
                 [0 -h*sig(3)^3 0; -sig(2)^3 2*sig(1)^3+2*sig(2)^3+2*h*sig(3)^3 -sig(2)^3; 0 -h*sig(3)^3 0],...
                 [0 0 0;  0 -sig(1)^3 0;  0 0 0]);
G = GaussIm(stack, sigs(1,:).*4, sig);
LapGss = convn(G, sec_dev, 'same');

LoGMP = LapGss;
MS = ones(size(stack));
for s=2:size(sigs,1)
    sig = sigs(s,:);
    sec_dev = cat(3, [0 0 0;  0 -sig(1)^3 0;  0 0 0],...
                     [0 -h*sig(3)^3 0; -sig(2)^3 2*sig(1)^3+2*sig(2)^3+2*h*sig(3)^3 -sig(2)^3; 0 -h*sig(3)^3 0],...
                     [0 0 0;  0 -sig(1)^3 0;  0 0 0]);
    G = GaussIm(stack, sig.*4, sig);
    LapGss = convn(G, sec_dev, 'same');
    MS(LapGss>LoGMP) = s;
    LoGMP(LapGss>LoGMP) = LapGss(LapGss>LoGMP);
end
toc

neighgmp = imregionalmax(LoGMP,26);

%%
gmp = LoGMP; %#ok<*UNRCH>
gmp(:,:,1) = min(gmp(:));
gmp(:,:,size(gmp,3)) = min(gmp(:));
gmp(:,1,:) = min(gmp(:));
gmp(:,size(gmp,2),:) = min(gmp(:));
gmp(1,:,:) = min(gmp(:));
gmp(size(gmp,1),:,:) = min(gmp(:));

neighgmp = imregionalmax(gmp,26);

N = cumsum(hist(gmp(:),max(gmp(:))-min(gmp(:))+1))./numel(gmp);
valThreshold = find(N>params.LoGMP_THRESHOLD,1)+ min(gmp(:));

gmp(neighgmp==0) = min(gmp(:));

gmp(gmp<valThreshold) = min(gmp(:));
rows = 1:size(stack,1);
cols = 1:size(stack,2);
zeds = 1:size(stack,3);
[cols,rows,zeds] = meshgrid(cols,rows,zeds);
seeds = [];
while any(gmp(:)>min(gmp(:)))
    [~,maxind] = max(gmp(:));
    [r,c,z] = ind2sub(size(gmp),maxind);
    radius = nurseCellAllPixRadius( MS(r,c,z),: );
    seeds = [seeds; r c z]; %#ok<AGROW>
    sph = (((rows-r)./radius(2)).^2 + ((cols-c)./radius(1)).^2 + ((zeds-z)./radius(3)).^2) <= 1;
    gmp(sph) = min(gmp(:));
end




end