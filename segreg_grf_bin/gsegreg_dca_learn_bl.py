# coding=utf-8
""" Joint segmentation & registration and shape model learning

Model: Log-supermodular P^n Potts model with unary priors
Appearance: mixtures of multivariate normal distributions
Transformations: rigid body and scaling
author: Boris Flach
"""
import copy
import os
from gsegreg_dca_pn_bl import PnModel
from gsegreg_dca_image import CImage, GaussMVD, MixGauss, Appearance
from gsegreg_dca_transf import *
try:
    from gsegreg_dca_prms_local import DCAEMParms, PnModelParms, AppearParms
except ImportError:
    print("Warning (Main module): Found no local parameters")
    from gsegreg_dca_prms import DCAEMParms, PnModelParms, AppearParms


# =============================================================================
def marg_dist(marg1, marg2):
    """Compute Bhattacharyya distance of marginal distributions"""

    darr = np.sqrt(marg1 * marg2)
    darr += np.sqrt((1.0 - marg1) * (1.0 - marg2))
    darr = -np.log(darr)
    dist = np.sum(darr) / np.float64(marg1.size)

    return dist


# =============================================================================
def estimate_image_parms(img, pmarg, appear_init, transf_init, trange):
    """Estimate appearance and transformation for image"""

    zyk = 0
    iprms = PnModelParms()
    transf = np.copy(transf_init)
    appear = copy.deepcopy(appear_init)
    appear_new = copy.deepcopy(appear)

    while True:
        zyk += 1
        pmarg_tr = rbs_trans(pmarg, transf, img.size, iprms.MIN_PROB)
        appear_new.estimate(img.arr, pmarg_tr)
        ldiff = img.compute_appearance_ldiff(appear_new)
        transf_new = optimal_rbs_align_fft(pmarg, ldiff, trange, iprms.MIN_PROB)
        ttval = np.all(transf_new == transf)
        transf = np.copy(transf_new)
        appear = copy.deepcopy(appear_new)
        if ttval or (zyk > 5):
            break

    return appear, transf, zyk


# =============================================================================
def dca_for_image(imod, img, pmarg_init, appear_init, tr_init, trange, pos=None):
    """ Apply DC-algorithm for an image; estimate transformation, appearance model
    and foreground marginals.

    :param imod: MRF model
    :param img: image
    :param pmarg_init: initial posterior marginals
    :param appear_init: initial appearance model
    :param tr_init: initial transformation
    :param trange: range for angles and scales
    :param pos: process id needed for parallelisation
    :return pos process id needed for parallelisation
    :return pmarg: posterior foreground marginals
    :return ppot: posterior potentials
    :return appear: optimal appearance
    :return transf: optimal transformation
    :return zyk: number of EM cycles
    """

    zyk = 0
    sub_zyk = 0
    prms = DCAEMParms()
    transf = np.copy(tr_init)
    appear = copy.deepcopy(appear_init)
    pmarg = np.copy(pmarg_init)
    if prms.VERBOSE:
        print("Start DC algorithm for image")

    while True:
        zyk += 1
        # A-step
        ldiff = img.compute_appearance_ldiff(appear)
        minval = np.amin(ldiff)
        ldiff_tr = rbs_inv_trans(ldiff, transf, imod.size, minval)
        pmarg_new = imod.comp_marg_logsub(umod_e=ldiff_tr)
        dist = marg_dist(pmarg_new, pmarg)
        pmarg = np.copy(pmarg_new)

        if prms.VERBOSE:
            print("DCA_IM_Zyk = " + repr(zyk) + ", Transf = " + repr(transf) +
                  ", Dist = " + repr(dist) + ", Sub_zyk = " + repr(sub_zyk))
            name = "segm_" + repr(zyk) + ".png"
            img_out = CImage()
            img_out.size = img_out.size_orig = pmarg.shape
            img_out.arr = pmarg
            img_out.save(name, "L")

        if (zyk >= prms.EM_MaxZyk) or (dist < prms.EM_MinDist):
            break

        # B-step
        appear, transf, sub_zyk = estimate_image_parms(img, pmarg, appear, transf, trange)

    if pos is None:
        return pmarg, appear, transf, zyk
    else:
        return pos, pmarg, appear, transf, zyk


# =============================================================================
def init_appearance():
    """
    Init mixture of Gaussians for foreground & background
    :return: appearance
    """
    aprms = AppearParms()
    dim = aprms.DIMENSION
    gd = GaussMVD(dim, aprms.MIN_EIG)
    appear = Appearance()
    mean = np.ones(dim, dtype=np.float64) * 0.5
    cov = np.eye(dim, dim, dtype=np.float64) * 0.3

    # background
    aprob = 1.0 / float(aprms.COMPONENTS[0])
    mixb = MixGauss(dim)
    gd.modify(mean, cov)
    mixb.add_component(gd, aprob)
    for i in range(1, aprms.COMPONENTS[0]):
        gd.modify(mean + (np.random.random_sample(dim) - 0.5) * 0.1, cov)
        mixb.add_component(gd, aprob)
    appear.add_comp(mixb)
    # foreground
    aprob = 1.0 / float(aprms.COMPONENTS[1])
    mixf = MixGauss(dim)
    gd.modify(mean, cov)
    mixf.add_component(gd, aprob)
    for i in range(1, aprms.COMPONENTS[1]):
        gd.modify(mean + (np.random.random_sample(dim) - 0.5) * 0.1, cov)
        mixf.add_component(gd, aprob)
    appear.add_comp(mixf)

    return appear


# =============================================================================
def init_appear_transf_from_image(image, imod, trange):
    """estimate initial appearance and transformation (darker components - foreground)"""

    # estimate one Gaussian
    aprms = AppearParms()
    dim = aprms.DIMENSION
    numb = aprms.COMPONENTS[0]
    numf = aprms.COMPONENTS[1]
    weights = np.ones(image.size, dtype=np.float64)
    gd = GaussMVD(dim, aprms.MIN_EIG)
    gd.estimate(image.arr, weights)

    # split into numf + numb components
    mix = MixGauss(dim)
    mean = gd.mean
    cov = gd.cov
    w, v = np.linalg.eigh(np.matrix(cov))
    w = np.array(w)
    v = np.array(v)
    delta = v[:, -1]
    delta *= w[-1] * 0.5
    if np.sum(delta * np.ones(delta.shape, dtype=np.float64)) < 0.0:
        delta = -delta
    for k in range(numb):
        gd.modify(mean + float(k+1) * delta, cov)
        mix.add_component(gd, 1.0 / float(numb + numf))
    for k in range(numf):
        gd.modify(mean - float(k+1) * delta, cov)
        mix.add_component(gd, 1.0 / float(numb + numf))
    mix.estimate(image.arr, weights)

    # initialise appearance
    appear = Appearance()
    bmix = MixGauss(dim)
    baprob = np.sum(mix.aprobs[0:numb])
    for k in range(numb):
        bmix.add_component(mix.glist[k], mix.aprobs[k] / baprob)

    fmix = MixGauss(dim)
    faprob = np.sum(mix.aprobs[numb:])
    for k in range(numb, numb+numf):
        fmix.add_component(mix.glist[k], mix.aprobs[k] / faprob)

    appear.add_comp(bmix)
    appear.add_comp(fmix)

    # estimate transformation
    ldiff = image.compute_appearance_ldiff(appear)
    ldiff[ldiff > imod.mps.MAX_U] = imod.mps.MAX_U
    ldiff[ldiff < imod.mps.MIN_U] = imod.mps.MIN_U
    beta = np.copy(imod.fprob)
    tran = optimal_rbs_align_fft(beta, ldiff, trange, imod.mps.MIN_PROB)
    transf = np.copy(tran)

    return appear, transf


# =============================================================================
def add_segm_overlay(im_arr, seg_arr):
    """ Add a segmenation overlay to an image
    :param im_arr: color image array
    :param seg_arr: segmentation array
    :return: image with overlay
    """

    segm = copy.deepcopy(seg_arr)
    img = copy.deepcopy(im_arr)
    if len(img.shape) == 2:
        img = np.repeat(img[..., np.newaxis], 3, len(img.shape))
    segm[segm >= 0.5] = 1.0
    segm[segm < 0.5] = 0.0
    edge_hor = ndimage.sobel(segm, 0)
    edge_ver = ndimage.sobel(segm, 1)
    edge = np.hypot(edge_hor, edge_ver)

    img[edge > 0.0, :] = np.array([1.0, 0.0, 0.0])

    return img


# =============================================================================
def add_model_overlay(seg_arr, mod_arr):
    """ Add model overlay to segmentation

    :param seg_arr: segmentation array
    :param mod_arr: shape model array
    :return: segmentation with overlay as color image array
    """

    model = copy.deepcopy(mod_arr)
    segm = np.repeat(seg_arr[..., np.newaxis], 3, len(seg_arr.shape))
    model[model >= 0.5] = 1.0
    model[model < 0.5] = 0.0
    edge_hor = ndimage.sobel(model, 0)
    edge_ver = ndimage.sobel(model, 1)
    edge = np.hypot(edge_hor, edge_ver)

    segm[edge > 0.0, :] = np.array([1.0, 0.0, 0.0])

    return segm


# =============================================================================
def main():
    import glob
    import pickle
    from multiprocessing import Pool
    import argparse
    import time

    start = time.time()

    #  Parse parameters
    parser = argparse.ArgumentParser(description='Joint segmentation & registration')
    parser.add_argument('--wdir', help='working directory')
    parser.add_argument('--mod_in', required=True, help='shape model')
    parser.add_argument('--mod_out', required=True, help='shape model')
    parser.add_argument('--img_bname', required=True, help='image base name')
    parser.add_argument('--drange', type=int, nargs='+', required=True, help='differential range for angles')
    parser.add_argument('--srange', type=int, nargs='+', required=True, help='differential range for scale')
    parser.add_argument('--size', type=int, nargs='+', required=True, help='size of the model')
    parser.add_argument('--parms', help='appearance models & transformations')
    parser.add_argument('--cores', type=int, help='number of cores')
    parser.add_argument('--save', action='store_true', help='save images')

    args = parser.parse_args()
    # number of cores for parallel execution
    if args.cores is None:
        cores = 1
    else:
        cores = args.cores

    # change to working directory
    if args.wdir is not None:
        os.chdir(args.wdir)

    dcaprms = DCAEMParms()
    size = args.size

    #  load all images
    image_list = []
    imname_list = sorted(glob.glob(args.img_bname))

    for name in imname_list:
        img = CImage()
        img.load(name, "RGB", size)
        image_list.append(img)

    if len(image_list) == 0:
        raise Exception("No images loaded")

    # initialise and load P^n model
    imod = PnModel(size)
    if args.mod_in.rsplit('.', 1)[1] == "npy":
        imod.load_probs(args.mod_in)
    else:
        imod.load_probs_from_image(args.mod_in)
    rad = int(imod.mps.RADIUS)
    for x in range(-rad, rad+1):
        for y in range(-rad, rad+1):
            if math.sqrt(x*x + y*y) <= rad:
                imod.edges.append((y, x))

    imod.estimate_upots_logsub()

    # initialise fields for posterior potentials and marginals
    pmarg_list = []

    for name in imname_list:
        # pmarg = np.array(imod.fprob, dtype=np.float64)
        pmarg = np.ones(imod.size, dtype=np.float64)
        pmarg_list.append(pmarg)

    # initialise appearance models and transformations
    if args.parms is None:
        appear_list = []
        transf_list = []
        for i in range(len(image_list)):
            appear = init_appearance()
            appear_list.append(appear)
            transf_list.append(np.array([0, 0, 0, 0], dtype=np.int))
    else:
        # load initial appearance models and transformations from file
        with open(args.parms, 'r') as infile:
            appear_list = pickle.load(infile)
            transf_list = pickle.load(infile)
        if (len(appear_list) != len(image_list)) or (len(transf_list) != len(image_list)):
            raise Exception("Length mismatch of loaded lists!")
        # rescale translations (down sampling!)
        for i in range(len(image_list)):
            size_factors = np.array(image_list[i].size_orig, dtype=np.float_) / np.array(size, dtype=np.float_)
            transf_list[i][0:2] /= size_factors
            transf_list[i] = list(map(int, transf_list[i]))

    # differential range for rotation angles and scale
    drange = args.drange
    srange = args.srange
    #  Run the DCA algorithm
    zyk = 0
    probs = np.zeros(imod.size, dtype=np.float_)

    print("Start DC-Algorithm")
    while True:
        zyk += 1
        #  A-STEP
        # run EM algorithm for all images
        probs.fill(0.0)
        avzyk = np.float_(0)

        # compute list of absolute search ranges (transformations)
        trange_li = []
        for i in range(len(image_list)):
            trange = np.copy(drange + srange)
            trange[0:2] += transf_list[i][2]
            trange[2:] += transf_list[i][3]
            trange_li.append(trange)

        # construct a pool of workers and run the em-algorithm for each image
        pool = Pool(processes=cores)
        result_out = [pool.apply_async(dca_for_image, args=(
            imod, image_list[i], pmarg_list[i], appear_list[i], transf_list[i], trange_li[i], i))
                      for i in range(len(image_list))]

        # collect the results
        results = [p.get() for p in result_out]
        results.sort()
        # strip the position number
        results = [r[1:] for r in results]

        for i in range(len(image_list)):
            avzyk += results[i][3]
            pmarg_list[i] = results[i][0]
            appear_list[i] = results[i][1]
            transf_list[i] = results[i][2]
            #  accumulate to new model (probs)
            probs += pmarg_list[i]

        # normalise
        probs /= np.float64(len(image_list))
        avzyk /= np.float64(len(image_list))

        imod_old = copy.deepcopy(imod)
        imod.set_fprob(probs)
        dist = imod.marg_dist(imod_old)

        #  B-step
        imod.estimate_upots_logsub()

        # save intermediate model
        mod_name = "modelz_" + str(zyk).zfill(2) + ".png"
        imod.save_probs_as_image(mod_name)
        mod_name = "modelz_" + str(zyk).zfill(2) + ".npy"
        imod.save_probs(mod_name)

        print("DCA-Zyk: " + repr(zyk) + " , MargDist: " + repr(dist) + " , AvEmZyk: " + repr(avzyk))
        if (zyk >= dcaprms.DCA_MaxZyk) or (dist < dcaprms.DCA_MinDist):
            break

    # save final model
    imod.save_probs_as_image(args.mod_out)
    name = args.mod_out.rsplit('.', 1)[0]
    imod.save_probs(name)

    # save segmentations & transformed images

    if args.save:
        print("Save segmentations")
        for i in range(len(image_list)):
            rname = imname_list[i].rsplit('.', 1)[0]
            # save posterior marginals foreground prob's in reference frame
            beta = pmarg_list[i]
            beta_overl = add_model_overlay(beta, imod.fprob)
            img = copy.deepcopy(image_list[i])
            img.set_values(beta_overl)
            img.save(rname + "_seg_rf.png", "RGB", True)

            # save posterior marginals foreground prob's in image frame
            beta = pmarg_list[i]
            beta_tr = rbs_trans(beta, transf_list[i], image_list[i].size, imod.mps.MIN_PROB)
            img = copy.deepcopy(image_list[i])
            img.set_values(beta_tr)
            img.save(rname + "_seg_if.png", "L", True)

            # transform and save images in reference frame
            if len(image_list[i].arr.shape) == 2:
                pval = np.float64(1.0)
            else:
                pval = np.ones(image_list[i].arr.shape[2], dtype=np.float64)
            tarr = rbs_inv_trans(image_list[i].arr, transf_list[i], image_list[i].size, pval)
            tarr_overl = add_segm_overlay(tarr, beta)
            image_list[i].set_values(tarr_overl)
            image_list[i].save(rname + "_tra.png", "RGB", True)

    # scale back translations
    if args.size is not None:
        for i in range(len(image_list)):
            size_factors = np.array(image_list[i].size_orig, dtype=np.float_) / np.array(size, dtype=np.float_)
            transf_list[i][0:2] = size_factors * transf_list[i][0:2].astype(np.float64)
            transf_list[i] = list(map(int, transf_list[i]))

    # save estimated theta parameters and transformations
    with open("theta_parm_out.txt", 'wb') as output:
        pickle.dump(appear_list, output)
        pickle.dump(transf_list, output)

    end = time.time()
    print("Done in " + repr((end - start) / 60) + " minutes.")

    return imod.fprob, transf_list, appear_list


# =============================================================================
if __name__ == "__main__":
    main()
