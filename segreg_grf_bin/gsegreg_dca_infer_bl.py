# coding=utf-8
""" Joint segmentation & registration and shape model learning

Model: Log-supermodular Ising model with unary priors
Appearance: mixtures of multivariate normal distributions
Transformations: rigid body and scaling
author: Boris Flach
"""

import copy
import os

from gsegreg_dca_pn_bl import PnModel
from gsegreg_dca_image import CImage
from gsegreg_dca_learn_bl import init_appearance, dca_for_image
from gsegreg_dca_learn_bl import add_segm_overlay, add_model_overlay
from gsegreg_dca_transf import *
try:
    from gsegreg_dca_prms_local import DCAEMParms, PnModelParms, AppearParms
except ImportError:
    print("Warning: Found no local parameters")
    from gsegreg_dca_prms import DCAEMParms, PnModelParms, AppearParms


# =============================================================================
def main():
    import pickle
    import argparse

    #  Parse parameters
    parser = argparse.ArgumentParser(description='Joint segmentation & registration')
    parser.add_argument('--wdir', help='working directory')
    parser.add_argument('--mod_in', required=True, help='shape model')
    parser.add_argument('--img', required=True, help='image name')
    parser.add_argument('--drange', type=int, nargs='+', required=True, help='differential range for angles')
    parser.add_argument('--srange', type=int, nargs='+', required=True, help='differential range for scale')
    parser.add_argument('--size', type=int, nargs='+', required=True, help='size of the model')

    args = parser.parse_args()

    # change to working directory
    if args.wdir is not None:
        os.chdir(args.wdir)

    size = args.size

    #  load image
    img = CImage()
    img.load(args.img, "RGB", size)


    # initialise and load P^n model
    imod = PnModel(size)
    if args.mod_in.rsplit('.', 1)[1] == "npy":
        imod.load_probs(args.mod_in)
    else:
        imod.load_probs_from_image(args.mod_in)

    rad = int(imod.mps.RADIUS)
    for x in range(-rad, rad+1):
        for y in range(-rad, rad+1):
            if math.sqrt(x*x + y*y) <= rad:
                imod.edges.append((y, x))

    imod.estimate_upots_logsub()
    pmarg = np.ones(imod.size, dtype=np.float64)

    # differential range for rotation angles and scalings
    drange = args.drange
    srange = args.srange
    trange = np.copy(drange + srange)

    # initialise appearance models and transformations
    # appear_init, transf_init = init_appear_transf_from_image(image=img, imod=imod, trange=trange)
    appear_init = init_appearance()
    transf_init = np.array([0, 0, 0, 0], dtype=np.int)

    trange[0:2] += transf_init[2]
    trange[2:] += transf_init[3]

    #  run dca-algorithm
    beta, appear, transf, imzyk = dca_for_image(imod, img, pmarg, appear_init, transf_init, trange)

    # save posterior marginals foreground prob's in reference frame
    beta_overl = add_model_overlay(beta, imod.fprob)
    rname = args.img.replace(".png", "_seg_rf.png")
    img_out = copy.deepcopy(img)
    img_out.set_values(beta_overl)
    img_out.save(rname, "RGB", True)

    # save posterior marginals foreground prob's in image frame
    rname = args.img.replace(".png", "_seg_if.png")
    beta_tr_if = rbs_trans(beta, transf, img.size, imod.mps.MIN_PROB)
    img_out = copy.deepcopy(img)
    img_out.set_values(beta_tr_if)
    img_out.save(rname, "L", True)

    # transform and save the image in the refererence frame
    if len(img.arr.shape) == 2:
        pval = np.float64(1.0)
    else:
        pval = np.ones(img.arr.shape[2], dtype=np.float64)
    tarr = rbs_inv_trans(img.arr, transf, img.size, pval)
    tarr_overl = add_segm_overlay(tarr, beta)
    img.set_values(tarr_overl)
    rname = args.img.replace(".png", "_tra.png")
    img.save(rname, "RGB", True)

    # scale back translations
    if args.size is not None:
        size_factors = np.array(img.size_orig, dtype=np.float_) / np.array(size, dtype=np.float_)
        transf[0:2] = size_factors * transf[0:2].astype(np.float64)

    # save estimated theta parameters and transformations
    rname = args.img.replace(".png", "_parms.txt")
    with open(rname, 'wb') as output:
        pickle.dump(appear, output)
        pickle.dump(transf, output)

    return imod.fprob, transf, appear

# =============================================================================
if __name__ == "__main__":
    main()
