""" Joint segmentation & registration and shape model learning

Model: Log-supermodular P^n model with unary priors, binary labelling
Computes: unary marginals, potentials by supermodular-polyhedron approximation
Autor: Boris Flach
"""
import numpy as np
import copy
import scipy.ndimage as ndimage
try:
    from gsegreg_dca_prms_local import PnModelParms
except ImportError:
    print("Warning (PnModel module): Found no local parameters")
    from gsegreg_dca_prms import PnModelParms


# =============================================================================
class PnModel:
    """ P^n model with unaries """
    mps = PnModelParms()

    # ===============================================
    def __init__(self, size, umod=None, edges=None):
        self.size = tuple(size)

        if umod is None:
            self.umod = np.zeros(self.size, dtype=np.float64)
        else:
            if not umod.shape == self.size:
                raise Exception("PN model: shape mismatch!")
            self.umod = np.array(umod, dtype=np.float64)
        if edges is None:
            self.edges = []
        else:
            self.edges = copy.deepcopy(edges)
        self.fprob = np.zeros(self.size)

    # ===============================================
    def set_umod(self, umod):
        if not umod.shape == self.size:
            raise Exception("PN model: shape mismatch!")
        self.umod = np.array(umod, dtype=np.float64)

        self.umod[self.umod < self.mps.MIN_U] = self.mps.MIN_U
        self.umod[self.umod > self.mps.MAX_U] = self.mps.MAX_U

    # ===============================================
    def set_fprob(self, prob):
        if not prob.shape == self.size:
            raise Exception("PN model: shape mismatch!")
        self.fprob = np.array(prob, dtype=np.float64)

        self.fprob[self.fprob < self.mps.MIN_PROB] = self.mps.MIN_PROB
        self.fprob[self.fprob > self.mps.MAX_PROB] = self.mps.MAX_PROB

    # ===============================================
    def add_edge(self, edge=None):
        x = copy.deepcopy(edge)
        self.edges.append(x)

    # ===============================================
    def load_probs_from_image(self, img_name):
        from PIL import Image

        im = Image.open(img_name).convert("L")
        # resize if necessary
        if not im.size == self.size[::-1]:
            im = im.resize(self.size[::-1])

        # put the image into an np-array
        arr = np.array(im, dtype=np.float64) / 255.0
        self.set_fprob(arr)

        return None

    # ===============================================
    def save_probs_as_image(self, name, size=None):
        """ Save as an image, optionally resize."""
        from PIL import Image

        # check maximal/minimal value
        if (self.fprob.min() < 0.0) or (self.fprob.max() > 1.0):
            print("min = " + repr(self.fprob.min()) + ", max = " + repr(self.fprob.max()))
            raise Exception("Save image" + repr(name) + ": array values not in [0.0,1.0]")

        # scale, transform to int
        i_arr = np.uint8(self.fprob * 255)

        img = Image.fromarray(i_arr, "L")

        # resize if requested
        if size is not None:
            img = img.resize(size[::-1])
            # save
        img.save(name)

        return None

    # ===============================================
    def save_probs(self, name):
        np.save(name, self.fprob)

    # ===============================================
    def load_probs(self, name):
        arr = np.load(name)
        if not len(arr.shape) == 2:
            raise Exception("Pn Model: load_probs: wrong dimension")
        if arr.shape == self.size:
            self.set_fprob(arr)
        else:
            zoom = np.array(arr.shape, np.float64) / np.array(self.size, dtype=np.float64)
            arr = ndimage.affine_transform(arr, zoom, output_shape=self.size)
            self.set_fprob(arr)

    # ===============================================
    def save_umod(self, name):
        np.save(name, self.umod)

    # ===============================================
    def load_umod(self, name):
        arr = np.load(name)
        if not len(arr.shape) == 2:
            raise Exception("Pn Model: load_umod: wrong dimension")
        if arr.shape == self.size:
            self.set_umod(arr)
        else:
            zoom = np.array(arr.shape, np.float64) / np.array(self.size, dtype=np.float64)
            arr = ndimage.affine_transform(arr, zoom, output_shape=self.size)
            self.set_umod(arr)

    # ===============================================
    def marg_dist(self, imod):
        """ Compute Bhattacharyya distance of marginal distributions"""

        darr = np.sqrt(self.fprob * imod.fprob)
        darr += np.sqrt((1.0 - self.fprob) * (1.0 - imod.fprob))
        darr = -np.log(darr)
        dist = np.sum(darr) / np.float64(self.fprob.size)
        return dist

    # ===============================================
    def find_lext(self, probs):
        """ find Lovasz extension

        :param probs: marginal probabilities
        """

        eshape = self.size + (len(self.edges),)
        ext_smod = np.empty(eshape, dtype=np.float64)

        y = np.zeros(self.size, dtype=np.float64)
        if self.mps.PNOTTS == 0.0:
            return y

        # extend windows in additional dimension
        for i in range(len(self.edges)):
            dv = np.roll(probs, self.edges[i][0], axis=0)
            dv = np.roll(dv, self.edges[i][1], axis=1)
            ext_smod[:, :, i] = dv

        # maxima / minima per window
        ext_ext = np.zeros((eshape[0] * eshape[1], eshape[2]), dtype=np.float64)
        md = np.argmax(ext_smod, axis=-1)
        ext_ext[np.arange(int(eshape[0] * eshape[1])), md.flatten()] -= 1.0
        md = np.argmin(ext_smod, axis=-1)
        ext_ext[np.arange(int(eshape[0] * eshape[1])), md.flatten()] += 1.0
        ext_ext = np.reshape(ext_ext, eshape)

        # roll back & accumulate
        for i in range(len(self.edges)):
            dv = np.roll(ext_ext[:, :, i], -self.edges[i][0], axis=0)
            dv = np.roll(dv, -self.edges[i][1], axis=1)
            y += dv

        y *= self.mps.PNOTTS

        return y

    # ===============================================
    def estimate_upots_logsub(self, probs=None):
        """ estimate unary potentials of a P^n model by log-supermodular approximation

        :param probs: marginal probabilities
        """

        if probs is None:
            smod = self.fprob
        else:
            if not probs.shape == self.size:
                raise Exception("PN model: shape mismatch!")
            smod = probs

        y = self.find_lext(smod)

        u = np.log(smod / (1 - smod)) - y
        self.set_umod(u)

        return None

    # ===============================================
    def comp_marg_logsub(self, umod_e=None, v_init=None):
        """ compute marginals of P^n model by log submodular approximation

        :param: umod_e: additional external unaries (log-domain)
        :param: v_init: initial estimate of marginals (log-domain)
        :return: unary marginals
        """

        y = np.empty(self.umod.shape, dtype=np.float64)
        umod = np.copy(self.umod)

        if umod_e is not None:
            umod += np.array(umod_e, dtype=np.float64)

        umod[umod < self.mps.MIN_U] = self.mps.MIN_U
        umod[umod > self.mps.MAX_U] = self.mps.MAX_U

        if v_init is None:
            v = np.copy(umod)
        else:
            v = np.array(v_init, dtype=np.float64)

        dval = 0.0
        zyk = 0

        # Frank-Wolfe algorithm
        while True:
            zyk += 1
            grad = np.exp(v) / (1 + np.exp(v))
            y.fill(0.0)
            y += umod
            y += self.find_lext(grad)

            dval = np.sum(grad * (v - y))
            if dval < 0.0:
                raise Exception("Error in Frank-Wolfe algorithm!")

            gamma = 2.0 / (float(zyk) + 2.0)
            v += gamma * (y - v)
            if (zyk > self.mps.MAX_STEPS) or (dval < self.mps.EPSILON):
                break

        if self.mps.VERBOSE:
            print("PNZyk = " + repr(zyk) + ", dval = " + repr(dval))

        v[v < self.mps.MIN_U] = self.mps.MIN_U
        v[v > self.mps.MAX_U] = self.mps.MAX_U

        return np.exp(v) / (1 + np.exp(v))
