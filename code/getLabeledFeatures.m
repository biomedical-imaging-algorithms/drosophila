%%
% Date: October 22nd, 2014
% Training stage
%

%%
function [background, border, nurse, cytoplasm] = getLabeledFeatures(stage, number)

% read files
ovaryStringFile = ['train\ovary\stage', num2str(stage), '\train', num2str(number), '.tif'];
maskStringFile = ['train\mask\stage', num2str(stage), '\mask', num2str(number), '.png'];
ovary = imread(ovaryStringFile); % ovary is in uint16
ovary = double(ovary(:, :, 1)); % keep only the green channel
ovary = ovary - mean(ovary(:)); % removing mean in order to work only with textural information
[mrows, ncols] = size(ovary);
mask = imread(maskStringFile); % mask is in uint8

% setting parameters for superpixel segmentation
regionSize = 4;
regularizer = 0.01;
filterSize = 9;
filterStd = 2;
[segments, borders] = getSuperpixels(ovary, regionSize, regularizer, filterSize, filterStd);
numOfSegments = unique(segments);

% Steerable filter extraction
% filterFeats = getLogGaborFeatures(ovary, 0); % 24 filters (6 orientations and 4 scales)
% filterFeats = getRealGaborFeatures(ovary, 0); % 24 filters (6 orientations and 4 scales)
% filterFeats = getComplexGaborFeatures(ovary, 0); % 24 filters (6 orientations and 4 scales)
% 0:= LM filters, 1:= RF filters, 2:= S filters, and 3:= CG filters
op = 3;
featureFilters = getFilterFeatures(ovary, 0, op);
invScales = zeros(mrows, ncols, 8);
invScales(:, : , 1) = sum(featureFilters(:, :, 1:6), 3);
invScales(:, : , 2) = sum(featureFilters(:, :, 7:12), 3);
invScales(:, : , 3) = sum(featureFilters(:, :, 13:18), 3);
invScales(:, : , 4) = sum(featureFilters(:, :, 19:24), 3);
invScales(:, : , 5) = sum(featureFilters(:, :, 25:30), 3);
invScales(:, : , 6) = sum(featureFilters(:, :, 31:36), 3);
invScales(:, : , 7) = featureFilters(:, :, 37);
invScales(:, : , 8) = featureFilters(:, :, 38);

%%
% Tengo que pensar mejor esta parte!!!
NSEG = length(numOfSegments);
background = zeros(NSEG, 9);
border = zeros(NSEG, 9);
nurse = zeros(NSEG, 9);
cytoplasm = zeros(NSEG, 9);

% check if a segment is valid (it belongs to a single class)
tic,
vari = zeros(size(ovary));
for sgm = 1:NSEG
    index = find(segments == numOfSegments(sgm));
    valid = unique(mask(index));
    
    % the segment is valid
    if length(valid) == 1
        feature = computeFeatures(ovary, index, invScales);
        %viewFeatures = viewFeatures + mapFeatures(ovary, index, feat);
        % viewFeatures(index) = feat(4);
        vari(index) = feature(1);
        switch valid
            case 0, background(sgm,:) = feature;
            case 1, border(sgm,:) = feature;
            case 2, nurse(sgm, :) = feature;   
            otherwise, cytoplasm(sgm, :) = feature;
        end
    end
end

% removing null vectors
[~, index] = ismember(background, zeros(1, 9), 'rows');
background(index == 1, : ) = [];
[~, index] = ismember(border, zeros(1, 9), 'rows');
border(index == 1, :) = [];
[~, index] = ismember(nurse, zeros(1, 9), 'rows');
nurse(index == 1, :) = [];
[~, index] = ismember(cytoplasm, zeros(1, 9), 'rows');
cytoplasm(index == 1, :) = [];
toc;

end
