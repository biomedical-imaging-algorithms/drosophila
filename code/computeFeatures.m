%%
% Date: October 22nd, 2014
%

%%
function features = computeFeatures(ovary, index, filters)

% current segment
% region = zeros(size(ovary));
% it is possible to include other features here
% measurements = regionprops(region, ovary, 'Area', 'Perimeter', 'Orientation');
% entropy
valuesSgm = ovary(index);
% valuesNSgm = valuesSgm ./ sum(valuesSgm);
% valuesNSgm(valuesNSgm == 0) = []; % to avoid log(0)
% entro = -sum(valuesNSgm .* log2(valuesNSgm));

% mean of the filtered superpixel 
numOfFilters = size(filters, 3);
meanSp = zeros(numOfFilters, 1);
for filNum = 1:numOfFilters
    spFiltered = filters(:, :, filNum);
    meanSp(filNum) = mean(spFiltered(index)); % superpixel mean
end
features = [std(valuesSgm), meanSp'];
% building the feature vector
% features = [(measurements.Perimeter^2) / measurements.Area, measurements.Orientation, ...
%     mean(valuesSgm), std(valuesSgm), kurtosis(valuesSgm), skewness(valuesSgm), entro, meanSp'];

end
