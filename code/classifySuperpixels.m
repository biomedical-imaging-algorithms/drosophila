%%
%

%%
function classif = classifySuperpixels(listOfFeatures, trainFeatures, labelTraining)

[L_0, ~] = size(trainFeatures(labelTraining == 0));
[L_1, ~] = size(trainFeatures(labelTraining == 1));
[L_2, ~] = size(trainFeatures(labelTraining == 2));
[L_3, ~] = size(trainFeatures(labelTraining == 3));

M_0 = mean(trainFeatures(labelTraining == 0, :))';
M_1 = mean(trainFeatures(labelTraining == 1, :))';
M_2 = mean(trainFeatures(labelTraining == 2, :))';
M_3 = mean(trainFeatures(labelTraining == 3, :))';

S_0 = cov(trainFeatures(labelTraining == 0, :));
S_1 = cov(trainFeatures(labelTraining == 1, :));
S_2 = cov(trainFeatures(labelTraining == 2, :));
S_3 = cov(trainFeatures(labelTraining == 3, :));

M_T = (M_0 + M_1 + M_2 + M_3) ./ 4;
S_W = S_0 + S_1 + S_2 + S_3;

S_B = L_0*(M_0 - M_T)*(M_0 - M_T)' + L_1*(M_1 - M_T)*(M_1 - M_T)' + ...
     L_2*(M_2 - M_T)*(M_2 - M_T)' + L_3*(M_3 - M_T)*(M_3 - M_T)';

inv_S_W_by_S_B = S_W \ S_B;
[values, ~] = eig(inv_S_W_by_S_B, 'nobalance');

W_0 = real(values(:,1));
W_1 = real(values(:,2));
W_2 = real(values(:,3));

ALPHA_0 = W_0' * trainFeatures';
ALPHA_1 = W_1' * trainFeatures';
ALPHA_2 = W_2' * trainFeatures';

training = [ALPHA_0', ALPHA_1', ALPHA_2'];

BETA_0 = W_0' * listOfFeatures';
BETA_1 = W_1' * listOfFeatures';
BETA_2 = W_2' * listOfFeatures';

test = [BETA_0', BETA_1', BETA_2'];

classif = knnclassify(test, training, labelTraining, 40);

end
