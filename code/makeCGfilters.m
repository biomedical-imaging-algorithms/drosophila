%%
%
%

%%
function F = makeCGfilters

SUP  =  49;
NF = 2 * 18;
NROTINV = 2;
Fs = SUP + 1;
OCTAVES = 1;
F = zeros(SUP, SUP, NF + NROTINV);
N = (-SUP/2:SUP/2-1)/Fs;
M = (-SUP/2:SUP/2-1)/Fs;

count = 1;
for scale = 2:4
    for angle = 1:6
        
        if mod(scale, 2) == 0, theta = (angle - 1)*pi/6;
        else theta =  (angle - 1)*pi/6 + pi/12;
        end
        
        u0 = 2^(scale - 1)*sqrt(2);
        gamma = 1;
        phi = 0;
        sigma = ((2^OCTAVES+1) / (2^OCTAVES-1)) * sqrt(log(2)) / (sqrt(2)*pi) / u0;
        gReal = zeros(SUP, SUP);
        gIm = zeros(SUP, SUP);
        
        xx = 1;
        yy = 1;
        for x = M
            for y = N
                xp = x*cos(theta) - y*sin(theta);
                yp = x*sin(theta) + y*cos(theta);
                
                gReal(yy, xx) =  (1/(2*pi*sigma*(sigma/gamma)))* ...
                    exp(-0.5*(xp^2 + gamma^2*yp^2)/sigma^2) * cos(2*pi*xp*u0 + phi);
                gIm(yy, xx) =  (1/(2*pi*sigma*(sigma/gamma)))* ...
                    exp(-0.5*(xp^2 + gamma^2*yp^2)/sigma^2) * sin(2*pi*xp*u0 + phi);
                
                yy = yy + 1;
            end
            yy = 1;
            xx = xx + 1 ;
        end
        
        gReal = gReal - mean(gReal(:));
        %gReal = gReal ./ sum(abs(gReal(:)));
        gReal = gReal ./ max(gReal(:));
        
        gIm = gIm - mean(gIm(:));
        %gIm = gIm ./ sum(abs(gIm(:)));
        gIm = gIm ./ max(gIm(:));
        
        F(:, :, count) = gReal;
        F(:, :, count + 18) = gIm;
        count = count + 1;
    end
end

F(:, :, NF + 1) = normalise(fspecial('gaussian', SUP, 10));
F(:, :, NF + 2) = normalise(fspecial('log', SUP, 10));

end

%%
%
%

%%
function f = normalise(f)

f = f - mean(f(:));
f = f / sum(abs(f(:)));

end
