%%
% Date: October 30th, 2014
%

%%
function [bcktexton, borTexton, nurTexton, cytTexton] = createTextons(back, border, nurse, cyto, K)

% back(:,1) = [];
% border(:,1) = [];
% nurse(:,1) = [];
% cyto(:,1) = [];

tic, 
[~, bcktexton] = kmeans(back, K, 'MaxIter', 40);
[~, borTexton] = kmeans(border, K, 'MaxIter', 40);
[~, nurTexton] = kmeans(nurse, K, 'MaxIter', 40);
[~, cytTexton] = kmeans(cyto, K, 'MaxIter', 40);
toc;

end
