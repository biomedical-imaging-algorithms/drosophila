%%
% Date: October 22nd, 2014
%

%%
function segmentedImage = findLabels(stage, number, background, border, nurse, cytoplasm)

% read the target image
ovaryStringFile = ['test\stage', num2str(stage), '\test', num2str(number), '.tif'];
ovary = imread(ovaryStringFile); % ovary is uint16
ovary = double(ovary(:, :, 1)); % keep only with green channel
ovary = ovary - mean(ovary(:)); % removing mean in order to work only with textural information
[mrows, ncols] = size(ovary);

[L0, ~] = size(background); 
[L1, ~] = size(border);
[L2, ~] = size(nurse);
[L3, ~] = size(cytoplasm);

trainFeatures = [background; border; nurse; cytoplasm];
labelTraining =[zeros(L0, 1); ones(L1, 1); 2 * ones(L2, 1); 3 * ones(L3,1)];

% compute superpixels for target image
regionSize = 4;
regularizer = 0.01;
filterSize = 9;
filtersdt = 2;
[segments, borders] = getSuperpixels(ovary, regionSize, regularizer, filterSize, filtersdt);
numOfSegments = unique(segments);

% Steerable filter extraction
% filterFeats = getLogGaborFeatures(ovary, 0); % 24 filters (6 orientations and 4 scales)
% filterFeats = getRealGaborFeatures(ovary, 0); % 24 filters (6 orientations and 4 scales)
% filterFeats = getComplexGaborFeatures(ovary, 0); % 24 filters (6 orientations and 4 scales)
% op = 0; % 0:= LM filters, 1:= RF filters, and 2:= S filters
op = 1;
featureFilters = getFilterFeatures(ovary, 0, op);
invScales = zeros(mrows, ncols, 8);
invScales(:, : , 1) = sum(featureFilters(:, :, 1:6), 3);
invScales(:, : , 2) = sum(featureFilters(:, :, 7:12), 3);
invScales(:, : , 3) = sum(featureFilters(:, :, 13:18), 3);
invScales(:, : , 4) = sum(featureFilters(:, :, 19:24), 3);
invScales(:, : , 5) = sum(featureFilters(:, :, 25:30), 3);
invScales(:, : , 6) = sum(featureFilters(:, :, 31:36), 3);
invScales(:, : , 7) = featureFilters(:, :, 37);
invScales(:, : , 8) = featureFilters(:, :, 38);

NSEG = length(numOfSegments);
listOfFeatures = zeros(NSEG, 9);
tic,
for sgm = 1:NSEG
    index = (segments == numOfSegments(sgm));
    listOfFeatures(sgm, :) = computeFeatures(ovary, index, invScales);
end

% listOfFeatures(:,1) = [];
classif = classifySuperpixels(listOfFeatures, trainFeatures, labelTraining);
%classif = classifyNonLinearSuperpixels(listOfFeatures, trainFeatures, labelTraining);
toc;

tic,
segmentedImage = zeros(mrows, ncols);
for sgm = 1:NSEG
    index = (segments == numOfSegments(sgm));
    segmentedImage(index) = classif(sgm);
end
toc;

% visualizing results
se = strel('disk', 4);
follicleMASK = segmentedImage;
follicleMASK(follicleMASK ~= 1) = 0; % Take follicle cells
follicleMASK = imopen(follicleMASK, se);
FM = paintMASK(ovary, follicleMASK, [1,1,0]);

nurseMASK = segmentedImage;
nurseMASK(nurseMASK ~= 2) = 0;
nurseMASK = imopen(nurseMASK, se);
NM = paintMASK(ovary, nurseMASK, [0,0,1]);

figure(1), imshow(FM);
figure(2), imshow(NM);
figure(3), imshow(mat2gray(ovary));

end
