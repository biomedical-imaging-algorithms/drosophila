%%
% Date: October 22nd, 2014
%

%%
function [segments, borders] = getSuperpixels(ovary, regionSize, regularizer, filterSize, filtersdt)

% Gaussian filter to minimize noise effect
filter = fspecial('gaussian', filterSize,  filtersdt);
filter = filter ./ max(filter(:));
ovaryGaussian = imfilter(ovary, filter, 'replicate');

tic,
segments = vl_slic(single(mat2gray(ovaryGaussian)), regionSize, regularizer);
[sx, sy] = vl_grad(double(segments), 'type', 'forward') ;
toc;

borders = find(sx | sy) ;

end
