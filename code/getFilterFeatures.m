%%
% Date: November 3th, 2014
%

%%
function features = getFilterFeatures(patch, NORM, op, MIM)

% NORM flag para normalizar.
if NORM == 1, im = mat2gray(patch);
else im = patch;
end

[nrows, mcols] = size(patch);

% Get filters;
switch op
    case 0, filters = makeLMfilters; % 48 filters
    case 1, filters = makeRFSfilters; % 38 filters
    case 2, filters = makeSfilters; % 13 filters
    otherwise, filters = makeCGfilters; % 38 filters
end

% compute features
FILTERS =  size(filters, 3);
features = zeros(nrows, mcols, FILTERS);
for coeff = 1:FILTERS
    responseFilter =  conv2(im, filters(:, :, coeff), 'same');
    Ln = sqrt(sum(responseFilter(:) .^ 2));
    responseFilter = (responseFilter * (log(1 + Ln) / 0.03)) / Ln;
    features(:, :, coeff) = responseFilter;
end

%%
% Por qu� fall� esto????? Tengo que revisarlo
% IM = fft2(im, nrows, mcols);
% for coeff = 1:size(filters, 3)
%     filter = fft2(filters(:,:,coeff), nrows, mcols);
%     features(:,:,coeff) = abs(ifft2(IM.*filter));
% end

end
