function  writeMultiPageTiff(V, fname)
%writeMultiPageTiff(V, fname)
%
%
%

fprintf('Writing %s...\n', fname);

% if the file already exists, delete it so we do not append to it!
if exist(fname, 'file')
    delete(fname);
end

pb = createProgressBar(size(V,3));
for i = 1:size(V,3)
    imwrite(V(:,:,i), fname ,'tif', 'Compression', 'none', 'WriteMode', 'append');
    pb = updateProgessBar(pb, i);
end
