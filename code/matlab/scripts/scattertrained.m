
load trainedsamples2.mat;

pos = cellfun(@(x) strcmp(x,'positive'), class);
neg = cellfun(@(x) strcmp(x,'negative'), class);

figure(1); hold on;
% [H,AX,BigAx,P,PAx] = plotmatrix(samples(pos,:));
% for i=1:numel(H)
%     H(i).Color = [0 1 0];
% end
% [H,AX,BigAx,P,PAx] = plotmatrix(samples(pos,:),samples(neg,:));

f = figure(1);
categories = [sprintf('%12s', 'mean'); sprintf('%12s', 'variance'); ...
    sprintf('%12s', 'LoGMP'); sprintf('%12s', 'radius'); sprintf('%12s', 'phi(x)'); ...
    sprintf('%12s', 'sphericity'); sprintf('%12s', 'est. radius')];
gplotmatrix(samples,samples,class,...
    'br', '..',[],'on','', categories, categories);
lh=findall(gcf,'tag','legend');
set(lh,'position',[0.90,0.13,0.03,0.03]);

saveas(f, '/Users/amavel/Documents/drosophila/docs/map-ncseg/figs/scatterplot.png');