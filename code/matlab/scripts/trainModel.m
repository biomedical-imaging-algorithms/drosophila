

ds_train = {'cropped_ovary_0001','cropped_ovary_0002','cropped_ovary_0003',...
    'cropped_ovary_0004','cropped_ovary_0005','cropped_ovary_0006','cropped_ovary_0007'};

no_seeds = 15*length(ds_train);
neg_samples = 5*no_seeds;
n_bins = 15;
norm_perc = .99;

% model = 'nonnormalized';
% model = 'normalized';
% model = 'var_comp';
% model = 'w_sphericity';
model = 'w_rad';
% model = 'test';

if strcmp(computer,'MACI64')
    datadir = '../../../data/';
else
    datadir = '/datagrid/Medical/microscopy/drosophila/ovary_test_stacks/';
end
    
load([datadir 'seeds_mat/' ds_train{1} '.mat']);
sig_grad = 2./sqrt(2)./params.spacing;
sig_kernel = 1;

gradr = cat(3, [ 1  3  1;  0  0  0; -1 -3 -1],...
               [ 3  6  3;  0  0  0; -3 -6 -3],...
               [ 1  3  1;  0  0  0; -1 -3 -1]);
gradc = cat(3, [ 1  0 -1;  3  0 -3;  1  0 -1],...
               [ 3  0 -3;  6  0 -6;  3  0 -3],...
               [ 1  0 -1;  3  0 -3;  1  0 -1]);
gradz = cat(3, [ 1  3  1;  3  6  3;  1  3  1],...
               [ 0  0  0;  0  0  0;  0  0  0],...
               [-1 -3 -1; -3 -6 -3; -1 -3 -1]);

neg_samples_perim = ceil(neg_samples/length(ds_train));
samples = [];
class = {};

for l=1:length(ds_train)
    ds = ds_train{l};
    dscolor = [ds '_mag.tif'];
    
    filename = [datadir 'cropped/' dscolor];
    load([datadir 'seeds_mat/' ds '.mat']);
    load([datadir 'groundtruth/seeds_' ds '.mat']);
    
    params.bins = n_bins;
    params.norm_perc = .99;
    stack = readMultiPageTiff(filename);
    
    if ~strcmp(model, 'trainedModel_nonnormalized')
        N = cumsum(hist(double(stack(:)),double(max(stack(:)))-double(min(stack(:)))+1))./numel(stack);
        valThreshold = find(N>params.norm_perc,1)+ min(stack(:));
        stack = double(stack);
        stack(stack(:)>valThreshold) = valThreshold;
        stack = stack./max(stack(:));
    end
    
    N = cumsum(hist(double(LoGMP(:)),double(max(LoGMP(:)))-double(min(LoGMP(:)))+1))./numel(LoGMP);
    valThreshold = find(N>params.norm_perc,1)+ min(LoGMP(:));
    LoGMP = double(LoGMP);
    LoGMP(LoGMP(:)>valThreshold) = valThreshold;
    LoGMP = LoGMP./max(LoGMP(:));
    
    if sum(params.sig_dif==0)
        nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius+params.sig_dif, params.spacing')';
    else
        nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius.*params.sig_dif, params.spacing')';
    end
    
    seeds_radii = zeros(size(seeds_gt_loc,1),1);
    
    % Positive samples
    for s=1:size(seeds_gt_loc,1)
        radius = nurseCellAllPixRadius(MS(seeds_gt_loc(s,1),seeds_gt_loc(s,2),seeds_gt_loc(s,3)),:);
        realradius = radius(1).*params.spacing(1);
        seeds_radii(s) = realradius;
        
        fsize = round(3.*radius);
        fsize = (fsize - mod(fsize,2))/2;
        
        cropr = min(max(1,seeds_gt_loc(s,1) + (-fsize(1):fsize(1))),size(stack,1));
        cropc = min(max(1,seeds_gt_loc(s,2) + (-fsize(2):fsize(2))),size(stack,2));
        cropz = min(max(1,seeds_gt_loc(s,3) + (-fsize(3):fsize(3))),size(stack,3));
        
        imc = stack(cropr,cropc,cropz);
        gmp = LoGMP(cropr,cropc,cropz);
        
        if ~any(cellfun(@(x) strcmp(x,model), {'nonnormalized','normalized'}))
            [bw,phi] = segmentsingleNC(imc, realradius, sig_kernel, sig_grad, gradr, gradc, gradz, params);
        else
            bw=[]; phi=[];
        end
        
        samples = [samples; describeRegion(imc, gmp, bw, phi, radius, model, params)]; %#ok<AGROW>
        class = [class; {'positive'}]; %#ok<AGROW>
        
%         return;
    end
    
    % Negative samples
    for s=1:neg_samples_perim
        radius = nurseCellAllPixRadius(randi(size(nurseCellAllPixRadius,1)),:);
        realradius = radius(1).*params.spacing(1);
        dist = Inf;
        
        realc = seeds_gt_loc(1,:).*params.spacing;
        
        while any(sqrt(sum(bsxfun(@minus, bsxfun(@times,seeds_gt_loc,params.spacing), realc).^2,2)) < seeds_radii)
            c = [randi(size(stack,1)) randi(size(stack,2)) randi(size(stack,3))];
            realc = c.*params.spacing;
        end
        
        fsize = round(3.*radius);
        fsize = (fsize - mod(fsize,2))/2;
        
        cropr = min(max(1,c(1) + (-fsize(1):fsize(1))),size(stack,1));
        cropc = min(max(1,c(2) + (-fsize(2):fsize(2))),size(stack,2));
        cropz = min(max(1,c(3) + (-fsize(3):fsize(3))),size(stack,3));
        
        imc = stack(cropr,cropc,cropz);
        gmp = LoGMP(cropr,cropc,cropz);
        
        if ~any(cellfun(@(x) strcmp(x,model), {'nonnormalized','normalized'}))
            [bw,phi] = segmentsingleNC(imc, realradius, sig_kernel, sig_grad, gradr, gradc, gradz, params);
        else
            bw=[]; phi=[];
        end
        
        samples = [samples; describeRegion(imc, gmp, bw, phi, radius, model, params)]; %#ok<AGROW>
        class = [class; {'negative'}]; %#ok<AGROW>
        
%         return;
    end
    
%     return;
    progressbar(l,length(ds_train), 20);
end

SVMModel = fitcsvm(samples,class);
ScoreSVMModel = fitSVMPosterior(SVMModel);

save([datadir 'trainedModel/trainedModel_' model '.mat'], 'SVMModel', 'ScoreSVMModel', 'ds_train', 'params', 'samples', 'class');

