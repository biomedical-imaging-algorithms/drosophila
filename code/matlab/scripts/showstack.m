function showstack(stack)

stack = double(stack);
stack = stack-min(stack(:));
stack = 255.*stack./max(stack(:));
stack = uint8(stack);

figure;
for i=1:size(stack,3)
    imshow(stack(:,:,i));
    drawnow;
    pause(.5);
end