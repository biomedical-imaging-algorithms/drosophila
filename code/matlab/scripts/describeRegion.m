function x = describeRegion(imc, gmp, bw, phi, radius, model, params)

rows = 1:size(imc,1);
cols = 1:size(imc,2);
zeds = 1:size(imc,3);
[cols,rows,zeds] = meshgrid(cols,rows,zeds);
c = (size(imc)+1)/2;
sph = (((rows-c(1))./radius(2)).^2 + ((cols-c(2))./radius(1)).^2 + ((zeds-c(3))./radius(3)).^2) <= 1;

im_sph = imc(sph);


if any(cellfun(@(x) strcmp(x,model), {'nonnormalized','normalized'}))
    x = hist(double(im_sph(:)),params.bins);
    x = x./sum(x);
else
    ph_sph = phi(sph);
    i=1;
    x(i) = mean(im_sph); i=i+1;
    x(i) = var(im_sph); i=i+1;
    x(i) = gmp(c(1), c(2), c(3)); i=i+1;
    x(i) = radius(1).*params.spacing(1); i=i+1;
    x(i) = sum(ph_sph(:))./numel(ph_sph); i=i+1;
    if strcmp(model,'var_comp'), return; end;
    x(i) = (sum(bw(:)==1).^(1.5))./numel(bw); i=i+1;
    if strcmp(model,'w_sphericity'), return; end;
    x(i) = estimateDiameter(bw,params);
    if strcmp(model,'w_rad'), return; end;
end


end