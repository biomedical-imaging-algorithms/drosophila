
clear;

ds = 'cropped_ovary_0001';
% ds = 'cropped_ovary_0002';
% ds = 'cropped_ovary_0003';
% ds = 'cropped_ovary_0004';
% ds = 'cropped_ovary_0005';
% ds = 'cropped_ovary_0006';
% ds = 'cropped_ovary_0007';
% ds = 'cropped_ovary_0008';
% ds = 'cropped_ovary_0009';
% ds = 'cropped_ovary_0010';
% ds = 'cropped_ovary_0011';
% ds = 'cropped_ovary_0012';
% ds = 'cropped_ovary_0013';
% ds = 'cropped_ovary_0014';
% ds = 'cropped_ovary_0015';
% ds = 'cropped_ovary_0016';
% ds = 'cropped_ovary_0017';
% ds = 'cropped_ovary_0018';
% ds = 'cropped_ovary_0019';
% ds = 'cropped_ovary_0020';
% ds = 'cropped_ovary_0021';

dscolor = [ds '_mag.tif'];

if strcmp(computer,'MACI64')
    datadir = '../../../data/';
else
    datadir = '/datagrid/Medical/microscopy/drosophila/ovary_test_stacks/';
end

if exist('ds','var')
    only_one = true;
else
    only_one = false;
end

list = dir([datadir 'cropped/*_mag.tif']);
for l = 1:length(list)
    if ~only_one
        dscolor = list(l).name;
        ds = dscolor(1:end-8);
    else
        dscolor = [ds '_mag.tif'];
    end
    filename = [datadir 'cropped/' dscolor];
    load([datadir 'groundtruth/seeds_' ds '.mat']);
    load([datadir 'seeds_mat/' ds '.mat']);
    
    sig_grad = 2./sqrt(2)./params.spacing;
    sig_kernel = 1;
    
    gradr = cat(3, [ 1  3  1;  0  0  0; -1 -3 -1],...
                   [ 3  6  3;  0  0  0; -3 -6 -3],...
                   [ 1  3  1;  0  0  0; -1 -3 -1]);
    gradc = cat(3, [ 1  0 -1;  3  0 -3;  1  0 -1],...
                   [ 3  0 -3;  6  0 -6;  3  0 -3],...
                   [ 1  0 -1;  3  0 -3;  1  0 -1]);
    gradz = cat(3, [ 1  3  1;  3  6  3;  1  3  1],...
                   [ 0  0  0;  0  0  0;  0  0  0],...
                   [-1 -3 -1; -3 -6 -3; -1 -3 -1]);
    
%     grad = cat(3, [-1  0  1; -6 -6  0; -3 -6 -1],...
%                   [ 0  6  6; -6  0  6; -6 -6  0],...
%                   [ 1  6  3;  0  6  6; -1  0  1]);
    
    stack = readMultiPageTiff(filename);
    segm = zeros(size(stack));
    segmbw = zeros(size(stack));
    segmbg = zeros(size(stack));
    
    if sum(params.sig_dif==0)
        nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius+params.sig_dif, params.spacing')';
    else
        nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius.*params.sig_dif, params.spacing')';
    end
    
    seeds = seeds_gt_loc;
    for s=1:size(seeds,1)
        radius = nurseCellAllPixRadius(MS(seeds(s,1),seeds(s,2),seeds(s,3)),:);
        realradius = radius(1).*params.spacing(1);
        
        fsize = round(3.*radius);
        fsize = (fsize - mod(fsize,2))/2;
        
        center = fsize+1;
        
        cropr = min(max(1,seeds(s,1) + (-fsize(1):fsize(1))),size(stack,1));
        cropc = min(max(1,seeds(s,2) + (-fsize(2):fsize(2))),size(stack,2));
        cropz = min(max(1,seeds(s,3) + (-fsize(3):fsize(3))),size(stack,3));
        
        imc = stack(cropr,cropc,cropz);
        
        G = GaussIm(imc,4.*sig_grad,sig_grad);
        gxr = convn(G, gradr, 'same');
        gxc = convn(G, gradc, 'same');
        gxz = convn(G, gradz, 'same');
        gx = [gxr(:),  gxc(:),  gxz(:)]; 
%         gx = convn(G,grad,'same');
        
        [i,j,k] = ndgrid(1:size(imc,1),1:size(imc,2),1:size(imc,3));
        
        dsijk = bsxfun(@times, [center(1)-i(:),center(2)-j(:),center(3)-k(:)], params.spacing);
        
        d_norms = arrayfun(@(idx) norm(dsijk(idx,:)), (1:size(dsijk,1))');
        nsijk = dsijk./repmat(d_norms,1,size(dsijk,2));
        
        g_norms = arrayfun(@(idx) norm(gx(idx,:)), (1:size(gx,1))');
        ngx = gx./repmat(g_norms,1,size(gx,2));
        
        phi = .5 .* ( 1 + dot(ngx,nsijk,2) );
        % phi_im = reshape(phi, size(imc,1), size(imc,2), size(imc,3));
        
%         wsijk = sign(realradius-d_norms);
        wsijk = max(sign(realradius-d_norms),exp(-(realradius-d_norms).^2./(2.*sig_kernel.^2)));
        
        weighted_phi = phi.*wsijk;
        weighted_phi = reshape(weighted_phi, size(imc,1), size(imc,2), size(imc,3));
        
        level = graythresh(weighted_phi);
        BW = zeros(size(imc));
        BW(weighted_phi>level) = 1;
        
        patch = segmbw(cropr,cropc,cropz);
        patch(BW==1) = BW(BW==1);
        segmbw(cropr,cropc,cropz) = patch;
        patch(BW==1) = phi(BW==1);
        segm(cropr,cropc,cropz) = patch;
    end
    segmbg(segmbw>0) = max(stack(:));
%     segmbg(segm>0) = segm(segm>0);
    segmbg(segmbw==0) = stack(segmbw==0);
    
    if only_one
        return;
    end
end