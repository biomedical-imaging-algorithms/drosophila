

% [(1:size(seeds,1))' seeds(:,2) seeds(:,1) seeds(:,3) LoGMP(sub2ind(size(MS),seeds(:,1),seeds(:,2),seeds(:,3)))*.001 MS(sub2ind(size(MS),seeds(:,1),seeds(:,2),seeds(:,3)))]

stack = readMultiPageTiff(filename);
stk = double(stack);
stk = stk-min(stk(:));
stk = 255.*stk./max(stk(:));
stk = uint8(stk);

tr = [];

for s=1:size(seeds,1)
    
    figure(1); clf;
    imshow(stk(:,:,seeds(s,3))); hold on;
    plot(seeds(s,2), seeds(s,1), 'b.', 'MarkerSize', 30);
    
    fprintf('seed %d x:%d y:%d z:%d\n', s, seeds(s,2), seeds(s,1), seeds(s,3));
    
    k = getkey;
    while k~=121 && k~=110 && k~=113 % y, n, q respectively
        k = getkey;
    end
    
    if k==113
        break;
    elseif k==121
        tr = [tr s]; %#ok<AGROW>
    end
    
    if ~isempty(tr)
        fprintf('tr = [%d', tr(1));
        for i=2:length(tr), fprintf(' %d', tr(i)); end;
        fprintf(']; length=%d\n', length(tr));
    end
    
end

if k~=113
    seeds_gt_loc = seeds(tr,:);
    save([datadir 'groundtruth/seeds_' ds '.mat'],'seeds_gt_loc')
end