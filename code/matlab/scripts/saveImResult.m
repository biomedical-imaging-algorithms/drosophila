function saveImResult(input_filename, output_filename, seeds, spacing)

if nargin<4
    spacing=[1 1 1];
end
if nargin<3
    seeds=[];
end


stack = readMultiPageTiff(input_filename);
stk = double(stack);
stk = stk-min(stk(:));
stk = 255.*stk./max(stk(:));
stk = uint8(stk);
rad = 1.5./spacing;

rows = 1:size(stk,1);
cols = 1:size(stk,2);
zeds = 1:size(stk,3);
[cols,rows,zeds] = meshgrid(cols,rows,zeds);
inSphere = @(center) (((rows-center(1))./rad(2)).^2 + ((cols-center(2))./rad(1)).^2 + ((zeds-center(3))./rad(3)).^2) <= 1;
spheres = zeros(size(stk));
for i=1:size(seeds,1)
    sph = inSphere(seeds(i,:));
    spheres(sph) = size(seeds,1)-i+1;
end

delete(output_filename);

for i=1:size(stk,3)
    im = uint8(zeros(size(stk,1),size(stk,2),3));
    im(:,:,1) = stk(:,:,i).*uint8(1-logical(spheres(:,:,i)));
    im(:,:,2) = stk(:,:,i).*uint8(1-logical(spheres(:,:,i))) + uint8(255.*logical(spheres(:,:,i)) - 205.*(spheres(:,:,i)./size(seeds,1)));
    im(:,:,3) = stk(:,:,i) + uint8(245.*(spheres(:,:,i)./size(seeds,1)) + 10.*logical(spheres(:,:,i)));
    imwrite(im, output_filename, 'tif', 'Compression', 'none', 'WriteMode', 'append');
end


end