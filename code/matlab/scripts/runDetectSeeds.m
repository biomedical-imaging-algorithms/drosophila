
clear;

% ds = 'cropped_ovary_0001';
% ds = 'cropped_ovary_0002';
% ds = 'cropped_ovary_0003';
% ds = 'cropped_ovary_0004';
% ds = 'cropped_ovary_0005';
% ds = 'cropped_ovary_0006';
ds = 'cropped_ovary_0007';
% ds = 'cropped_ovary_0008';
% ds = 'cropped_ovary_0009';
% ds = 'cropped_ovary_0010';
% ds = 'cropped_ovary_0011';
% ds = 'cropped_ovary_0012';
% ds = 'cropped_ovary_0013';
% ds = 'cropped_ovary_0014';
% ds = 'cropped_ovary_0015';
% ds = 'cropped_ovary_0016';
% ds = 'cropped_ovary_0017';
% ds = 'cropped_ovary_0018';
% ds = 'cropped_ovary_0019';
% ds = 'cropped_ovary_0020';
% ds = 'cropped_ovary_0021';
% ds = 'cropped_ovary_0022';
% ds = 'cropped_ovary_0023';
% ds = 'cropped_ovary_0024';
% ds = 'cropped_ovary_0025';
% ds = 'cropped_ovary_0026';

if strcmp(computer,'MACI64')
    datadir = '../../../data/';
else
    datadir = '/datagrid/Medical/microscopy/drosophila/ovary_test_stacks/';
end

if exist('ds','var')
    only_one = true;
else
    only_one = false;
end

list = dir([datadir 'cropped/*_mag.tif']);
for l = 1:length(list)
    if ~only_one
        dscolor = list(l).name;
        ds = dscolor(1:end-8);
    else
        dscolor = [ds '_mag.tif'];
    end
    filename = [datadir 'cropped/' dscolor];

    %% Parameters
    params.nurseCellRealRadius = 6.2;
    params.sig_dif = -2:1:2; % must change below to addition
%     params.sig_dif = -1:.5:1; % must change below to addition
%     params.sig_dif = cumprod(sqrt(2).*ones(1,5))/(sqrt(2)*2); % must change below to multiplication

%     params.LoGMP_THRESHOLD = .95; %Relative to maximum
    params.LoGMP_THRESHOLD = .8; %Relative to maximum
%     params.LoGMP_THRESHOLD = .6; %Relative to maximum
    
    params.MIN_INTENSITY = .4; %Relative to maximum
%     params.MIN_INTENSITY = .2; %Relative to maximum

    params.spacing = [.258 .258 3.00]; % in um/px

    switch ds
        case 'cropped_ovary_0001'
            params.nurseCellRealRadius = 6.2; 
        case 'cropped_ovary_0002'
            params.sig_dif = -1:.5:1;
            params.nurseCellRealRadius = 4.1; 
        case 'cropped_ovary_0003'
            params.nurseCellRealRadius = 7.0; 
        case 'cropped_ovary_0004'
            params.nurseCellRealRadius = 7.0; 
        case 'cropped_ovary_0005'
            params.nurseCellRealRadius = 6.8; 
        case 'cropped_ovary_0006'
        case 'cropped_ovary_0007'
            params.sig_dif = -1:.5:1;
            params.nurseCellRealRadius = 4.8; 
            params.MIN_INTENSITY = .3;
        case 'cropped_ovary_0008'
            params.nurseCellRealRadius = 5.2; 
            params.MIN_INTENSITY = .2;
        case 'cropped_ovary_0009'
            params.sig_dif = -1:.5:1;
            params.nurseCellRealRadius = 5.5;
            params.MIN_INTENSITY = .2;
        case 'cropped_ovary_0010'
            params.nurseCellRealRadius = 6.8;
        case 'cropped_ovary_0011'
            params.nurseCellRealRadius = 5.2;
        case 'cropped_ovary_0012'
            params.nurseCellRealRadius = 7.7;
            params.MIN_INTENSITY = .3;
        case 'cropped_ovary_0013'
            params.nurseCellRealRadius = 5.2;
        case 'cropped_ovary_0014'
            params.nurseCellRealRadius = 5.2;
        case 'cropped_ovary_0015'
            params.nurseCellRealRadius = 7.8;
            params.MIN_INTENSITY = .2;
        case 'cropped_ovary_0016'
            params.nurseCellRealRadius = 5.2;
        case 'cropped_ovary_0017'
            params.MIN_INTENSITY = .2;
        case 'cropped_ovary_0018'
            params.sig_dif = -1:.5:1;
            params.nurseCellRealRadius = 5.8;
            params.MIN_INTENSITY = .2;
        case 'cropped_ovary_0019'
        case 'cropped_ovary_0020'
            params.sig_dif = -1:.5:1;
            params.nurseCellRealRadius = 5.4;
            params.MIN_INTENSITY = .3;
        case 'cropped_ovary_0021'
        case 'cropped_ovary_0022'
            params.nurseCellRealRadius = 7.7;
            params.MIN_INTENSITY = .3;
        case 'cropped_ovary_0023'
            params.MIN_INTENSITY = .3;
        case 'cropped_ovary_0024'
            params.sig_dif = -1:.5:1;
            params.nurseCellRealRadius = 8.0;
            params.MIN_INTENSITY = .15;
        case 'cropped_ovary_0025'
            params.MIN_INTENSITY = .3;
        case 'cropped_ovary_0026'
            params.sig_dif = -1:.5:1;
            params.nurseCellRealRadius = 5.2;
    end

    %% Run seed detection
    fprintf('\n#### %s (%.2f) ####\n', ds, params.nurseCellRealRadius);

    [seeds,LoGMP,MS,params] = detectSeeds(filename, params);

    %% Evaluate seed detection if ground truth exists
    INLIER_MIN_DIST = params.nurseCellRealRadius;
    if exist([datadir 'groundtruth/seeds_' ds '.mat'],'file')
        load([datadir 'groundtruth/seeds_' ds '.mat']);
        sds_gt_real_loc = bsxfun(@times, seeds_gt_loc, params.spacing);
        sds_real_loc = bsxfun(@times, seeds, params.spacing);
        inliers = 0;
        outliers = 0;
        already_found = zeros(1,size(sds_gt_real_loc,1));
        inlier_seeds = zeros(1,size(seeds,1));
        px_inlier_seeds = zeros(1,size(seeds,1));
        for i=1:size(sds_real_loc,1)
            dist = pdist2(sds_real_loc(i,:), sds_gt_real_loc);
            pxdist = pdist2(seeds(i,:), seeds_gt_loc);
            dist(logical(already_found)) = Inf;
            k = find(dist<INLIER_MIN_DIST,1);
            if ~isempty(k), inliers=inliers+1; already_found(k)=1; continue; end;
            inlier_seeds(i)=min(dist);
            px_inlier_seeds(i)=min(pxdist);
            outliers = outliers+1;
        end
        fprintf('Found %d out of %d seeds. %d outliers detected\n', inliers, size(seeds_gt_loc,1), outliers);
        fprintf('###################################\n\n');
    end

    %%
    filetiff = [datadir 'seeds/' ds '.tif'];
    
    saveImResult(filename, filetiff, seeds, params.spacing);

    save([datadir 'seeds_mat/' ds '.mat'], 'LoGMP', 'MS', 'seeds', 'params');

    if only_one
        return;
    end
end

return;
%% Extra small functions
%%
stk = double(stack);
stk = stk-min(stk(:));
stk = 255.*stk./max(stk(:));
stk = uint8(stk);
figure(1);
for i=1:size(stk,3)
    inds = find(seeds(:,3)==i);
    imshow(stk(:,:,i)); hold on;
    for s=1:length(inds)
        if inlier_seeds(inds(s))==0
            plot(seeds(inds(s),2),seeds(inds(s),1),'g.','MarkerSize',20);
        else
            plot(seeds(inds(s),2),seeds(inds(s),1),'r.','MarkerSize',20);
        end
    end
    drawnow;
    pause(2);
end

%%
stk = double(LoGMP);
stk = stk-min(stk(:));
stk = 255.*stk./max(stk(:));
stk = uint8(stk);

figure(1);
for i=1:size(stk,3)
    [r,c] = find(neighgmp(:,:,i)==0);
    imshow(stk(:,:,i)); hold on;
    plot(c,r,'.','MarkerSize',20);
    drawnow;
    pause(2);
end

%%
stk = double(stack);
stk = stk-min(stk(:));
stk = 255.*stk./max(stk(:));
stk = uint8(stk);
rows = 1:size(stack,1);
cols = 1:size(stack,2);
zeds = 1:size(stack,3);
[cols,rows,zeds] = meshgrid(cols,rows,zeds);
inSphere = @(center) (((rows-center(1))./nurseCellPixelRadius(2)).^2 + ((cols-center(2))./nurseCellPixelRadius(1)).^2 + ((zeds-center(3))./nurseCellPixelRadius(3)).^2) <= 1;
tmp = LoGMP;
figure(1);
for m=1:20
    [~,maxind] = max(tmp(:));
    [r,c,z] = ind2sub(size(tmp),maxind);
    imshow(stk(:,:,z)); hold on; plot(c,r,'.','MarkerSize',20);
    drawnow; pause(2);
    sph = inSphere([r,c,z]);
    tmp(sph) = min(tmp(:));
end




