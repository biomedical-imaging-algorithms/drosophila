clear;

% ds_test = {'cropped_ovary_0001','cropped_ovary_0002','cropped_ovary_0003',...
%     'cropped_ovary_0004','cropped_ovary_0005','cropped_ovary_0006',...
%     'cropped_ovary_0007','cropped_ovary_0008','cropped_ovary_0009',...
%     'cropped_ovary_0010','cropped_ovary_0011','cropped_ovary_0012',...
%     'cropped_ovary_0013','cropped_ovary_0014','cropped_ovary_0015',...
%     'cropped_ovary_0016','cropped_ovary_0017','cropped_ovary_0018',...
%     'cropped_ovary_0019','cropped_ovary_0020','cropped_ovary_0021',...
%     'cropped_ovary_0022','cropped_ovary_0023','cropped_ovary_0024',...
%     'cropped_ovary_0025','cropped_ovary_0026'};
ds_test = {'cropped_ovary_0008','cropped_ovary_0009',...
    'cropped_ovary_0010','cropped_ovary_0011','cropped_ovary_0012',...
    'cropped_ovary_0013','cropped_ovary_0014','cropped_ovary_0015',...
    'cropped_ovary_0016','cropped_ovary_0017','cropped_ovary_0018',...
    'cropped_ovary_0019','cropped_ovary_0020','cropped_ovary_0021',...
    'cropped_ovary_0022','cropped_ovary_0023','cropped_ovary_0024',...
    'cropped_ovary_0025','cropped_ovary_0026'};
% ds_test = {'cropped_ovary_0014'};

n_bins = 15;

% model = 'nonnormalized';
% model = 'normalized';
% model = 'var_comp';
% model = 'w_sphericity';
model = 'w_rad';
% model = 'test';

if strcmp(computer,'MACI64')
    datadir = '../../../data/';
else
    datadir = '/datagrid/Medical/microscopy/drosophila/ovary_test_stacks/';
end

load([datadir 'seeds_mat/' ds_test{1} '.mat']);
sig_grad = 2./sqrt(2)./params.spacing;
sig_kernel = 1;

gradr = cat(3, [ 1  3  1;  0  0  0; -1 -3 -1],...
               [ 3  6  3;  0  0  0; -3 -6 -3],...
               [ 1  3  1;  0  0  0; -1 -3 -1]);
gradc = cat(3, [ 1  0 -1;  3  0 -3;  1  0 -1],...
               [ 3  0 -3;  6  0 -6;  3  0 -3],...
               [ 1  0 -1;  3  0 -3;  1  0 -1]);
gradz = cat(3, [ 1  3  1;  3  6  3;  1  3  1],...
               [ 0  0  0;  0  0  0;  0  0  0],...
               [-1 -3 -1; -3 -6 -3; -1 -3 -1]);

TP = zeros(1,length(ds_test)); TN = zeros(1,length(ds_test));
FP = zeros(1,length(ds_test)); FN = zeros(1,length(ds_test));
TP15 = zeros(1,length(ds_test)); TN15 = zeros(1,length(ds_test));
FP15 = zeros(1,length(ds_test)); FN15 = zeros(1,length(ds_test));
for l=1:length(ds_test)
    ds = ds_test{l};
    dscolor = [ds '_mag.tif'];
    
    filename = [datadir 'cropped/' dscolor];
    load([datadir 'seeds_mat/' ds '.mat']);
    load([datadir 'groundtruth/seeds_' ds '.mat']);
    load([datadir 'trainedModel/trainedModel_' model '.mat']);
    
%     if ~exist('ScoreSVMModel','var')
%         ScoreSVMModel = fitSVMPosterior(SVMModel);
%         save([datadir 'trainedModel/' model '.mat'], '-append', 'ScoreSVMModel');
%     end
    
    stack = readMultiPageTiff(filename);
    
    N = cumsum(hist(double(stack(:)),double(max(stack(:)))-double(min(stack(:)))+1))./numel(stack);
    valThreshold = find(N>params.norm_perc,1)+ min(stack(:));
    stack = double(stack);
    
    stack(stack(:)>valThreshold) = valThreshold;
    stack = stack./max(stack(:));
    
    N = cumsum(hist(double(LoGMP(:)),double(max(LoGMP(:)))-double(min(LoGMP(:)))+1))./numel(LoGMP);
    valThreshold = find(N>params.norm_perc,1)+ min(LoGMP(:));
    LoGMP = double(LoGMP);
    
    LoGMP(LoGMP(:)>valThreshold) = valThreshold;
    LoGMP = LoGMP./max(LoGMP(:));
    
    tp=0; fp=0; tn=0; fn=0;
    
    if sum(params.sig_dif==0)
        nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius+params.sig_dif, params.spacing')';
    else
        nurseCellAllPixRadius = bsxfun(@rdivide, params.nurseCellRealRadius.*params.sig_dif, params.spacing')';
    end

    INLIER_MIN_DIST = params.nurseCellRealRadius;
    sds_gt_real_loc = bsxfun(@times, seeds_gt_loc, params.spacing);
    sds_real_loc = bsxfun(@times, seeds, params.spacing);
    inliers = 0;
    outliers = 0;
    already_found = zeros(1,size(sds_gt_real_loc,1));
    inlier_seeds = zeros(1,size(seeds,1));
    px_inlier_seeds = zeros(1,size(seeds,1));
    
    samples = zeros(size(seeds,1),length(SVMModel.Beta));
    for s=1:size(sds_real_loc,1)
        
        radius = nurseCellAllPixRadius(MS(seeds(s,1),seeds(s,2),seeds(s,3)),:);
        realradius = radius(1).*params.spacing(1);
%         INLIER_MIN_DIST = realradius;
        
        fsize = round(3.*radius);
        fsize = (fsize - mod(fsize,2))/2;
        
        center = fsize+1;
        
        cropr = min(max(1,seeds(s,1) + (-fsize(1):fsize(1))),size(stack,1));
        cropc = min(max(1,seeds(s,2) + (-fsize(2):fsize(2))),size(stack,2));
        cropz = min(max(1,seeds(s,3) + (-fsize(3):fsize(3))),size(stack,3));
        
        imc = stack(cropr,cropc,cropz);
        gmp = LoGMP(cropr,cropc,cropz);
        
        if ~any(cellfun(@(x) strcmp(x,model), {'nonnormalized','normalized'}))
            [bw,phi] = segmentsingleNC(imc, realradius, sig_kernel, sig_grad, gradr, gradc, gradz, params);
        else
            bw=[]; phi=[];
        end
        
        samples(s,:) = describeRegion(imc, gmp, bw, phi, radius, model, params);
        
        dist = pdist2(sds_real_loc(s,:), sds_gt_real_loc);
        pxdist = pdist2(seeds(s,:), seeds_gt_loc);
        
%         dist(logical(already_found)) = Inf;
        k = find(dist<INLIER_MIN_DIST,1);
        if ~isempty(k)
            % Inlier
            if samples(s,:) * SVMModel.Beta + SVMModel.Bias < 0 % Negative
                fn=fn+1;
            else % Positive
                tp=tp+1;
            end
            inliers=inliers+1;
            already_found(k)=1;
            continue;
        end
        % Outlier
        if samples(s,:) * SVMModel.Beta + SVMModel.Bias < 0 % Negative
            tn=tn+1;
        else % Positive
            fp=fp+1;
        end
        inlier_seeds(s)=min(dist);
        px_inlier_seeds(s)=min(pxdist);
        outliers = outliers+1;
    end
    [c,p] = predict(ScoreSVMModel, samples);
    [~,inds] = sortrows(p, -2);
    tp15=0; fp15=0; tn15=0; fn15=0;
    pseeds = [];
    for s=1:size(sds_real_loc,1)
        radius = nurseCellAllPixRadius(MS(seeds(s,1),seeds(s,2),seeds(s,3)),:);
        realradius = radius(1).*params.spacing(1);
%         INLIER_MIN_DIST = realradius;
        dist = pdist2(sds_real_loc(s,:), sds_gt_real_loc);
        pxdist = pdist2(seeds(s,:), seeds_gt_loc);
        
%         dist(logical(already_found)) = Inf;
        k = find(dist<INLIER_MIN_DIST,1);
        if ~isempty(k)
            pseeds = [pseeds; seeds(s,:)];
            i = find(s==inds);
            if i<=15
                tp15=tp15+1;
            else
                fn15=fn15+1;
            end
        else
            i = find(s==inds);
            if i<=15
                fp15=fp15+1;
            else
                tn15=tn15+1;
            end
        end
    end
    
    TP(l) = tp; TN(l) = tn; FP(l) = fp; FN(l) = fn;
    TP15(l) = tp15; TN15(l) = tn15; FP15(l) = fp15; FN15(l) = fn15;
    fprintf('%s, %d seeds detected. TP:%d, \tFP:%d, \tTN:%d, \tFN:%d, \tSens:%.3f, \tFPR:%.3f\n', ds, size(seeds,1), TP(l), FP(l), TN(l), FN(l), TP(l)/(TP(l)+FN(l)), FP(l)/(FP(l)+TN(l)));
    fprintf('\t\t\tsecond method: TP:%d, \tFP:%d, \tTN:%d, \tFN:%d, \tSens:%.3f, \tFPR:%.3f\n', tp15, fp15, tn15, fn15, tp15/(tp15+fn15), fp15/(fp15+tn15));
%     return;
end

list = dir([datadir 'results/*.mat']);

save([datadir 'results/svm_' model '.mat'], 'TP', 'TN', 'FP', 'FN', 'TP15', 'TN15', 'FP15', 'FN15');
% save([datadir 'results/svm_hist_normalized.mat'], 'TP', 'TN', 'FP', 'FN', 'TP15', 'TN15', 'FP15', 'FN15');
% save([datadir 'results/svm_var_comp.mat'], 'TP', 'TN', 'FP', 'FN', 'TP15', 'TN15', 'FP15', 'FN15');
% save([datadir 'results/svm_w_sphericity.mat'], 'TP', 'TN', 'FP', 'FN', 'TP15', 'TN15', 'FP15', 'FN15');
% save([datadir 'results/svm_w_rad.mat'], 'TP', 'TN', 'FP', 'FN', 'TP15', 'TN15', 'FP15', 'FN15');
% save([datadir 'results/svm_test2.mat'], 'TP', 'TN', 'FP', 'FN', 'TP15', 'TN15', 'FP15', 'FN15');

sumTP=zeros(1,length(list)+1); sumFP=zeros(1,length(list)+1); sumTN=zeros(1,length(list)+1); sumFN=zeros(1,length(list)+1);
sumSens=zeros(1,length(list)+1); sumFPR=zeros(1,length(list)+1);
sumTP15=zeros(1,length(list)+1); sumFP15=zeros(1,length(list)+1); sumTN15=zeros(1,length(list)+1); sumFN15=zeros(1,length(list)+1);
sumSens15=zeros(1,length(list)+1); sumFPR15=zeros(1,length(list)+1);

fprintf('\n:: Comparison with older results ::\n');
% fprintf('%30s ', 'this one');
% fprintf(' %d & %d & %d & %d & %.3f & %.3f \\\\\n', ...
%     sum(TP), sum(FP), sum(TN), sum(FN), sum(TP)/(sum(TP)+sum(FN)), sum(FP)/(sum(FP)+sum(TN)));
% fprintf('%30s ', 'this one, 2nd method');
% fprintf(' %d & %d & %d & %d & %.3f & %.3f \\\\\n', ...
%     sum(TP15), sum(FP15), sum(TN15), sum(FN15), sum(TP15)/(sum(TP15)+sum(FN15)), sum(FP15)/(sum(FP15)+sum(TN15)));
thisTP = TP; thisTN = TN; thisFP = FP; thisFN = FN;
if length(TP)>19
    inds = [false(1,7) true(1,19)];
else
    inds = true(1,length(TP));
end
l=0;
sumTP(l+1) = sum(TP(inds)); sumFP(l+1) = sum(FP(inds)); sumTN(l+1) = sum(TN(inds)); sumFN(l+1) = sum(FN(inds));
sumSens(l+1) = sum(TP(inds))/(sum(TP(inds))+sum(FN(inds))); sumFPR(l+1) = sum(FP(inds))/(sum(FP(inds))+sum(TN(inds)));
sumTP15(l+1) = sum(TP15(inds)); sumFP15(l+1) = sum(FP15(inds)); sumTN15(l+1) = sum(TN15(inds)); sumFN15(l+1) = sum(FN15(inds));
sumSens15(l+1) = sum(TP15(inds))/(sum(TP15(inds))+sum(FN15(inds))); sumFPR15(l+1) = sum(FP15(inds))/(sum(FP15(inds))+sum(TN15(inds)));
for l=1:length(list)
    load([datadir 'results/' list(l).name]);
    if length(TP)>19
        inds = [false(1,7) true(1,19)];
    else
        inds = true(1,length(TP));
    end
    sumTP(l+1) = sum(TP(inds)); sumFP(l+1) = sum(FP(inds)); sumTN(l+1) = sum(TN(inds)); sumFN(l+1) = sum(FN(inds));
    sumSens(l+1) = sum(TP(inds))/(sum(TP(inds))+sum(FN(inds))); sumFPR(l+1) = sum(FP(inds))/(sum(FP(inds))+sum(TN(inds)));
    sumTP15(l+1) = sum(TP15(inds)); sumFP15(l+1) = sum(FP15(inds)); sumTN15(l+1) = sum(TN15(inds)); sumFN15(l+1) = sum(FN15(inds));
    sumSens15(l+1) = sum(TP15(inds))/(sum(TP15(inds))+sum(FN15(inds))); sumFPR15(l+1) = sum(FP15(inds))/(sum(FP15(inds))+sum(TN15(inds)));
    
%     fprintf('%30s ', list(l).name);
%     fprintf(' %d & %d & %d & %d & %.3f & %.3f \\\\\n', ...
%         sum(TP(inds)), sum(FP(inds)), sum(TN(inds)), sum(FN(inds)), ...
%         sum(TP(inds))/(sum(TP(inds))+sum(FN(inds))), sum(FP(inds))/(sum(FP(inds))+sum(TN(inds))));
end
TP = thisTP; TN = thisTN; FP = thisFP; FN = thisFN;

for l=1:length(list)+1
    if l==1
        fprintf('%30s ', '\textbf{this one}');
    else
        switch list(l-1).name
            case 'svm_nonnormalized.mat'
                fprintf('%30s ', '\textbf{Baseline}');
            case 'svm_normalized.mat'
                fprintf('%30s ', '\textbf{Normalized}');
            case 'svm_var_comp.mat'
                fprintf('%30s ', '\textbf{Various comp.}');
            case 'svm_w_sphericity.mat'
                fprintf('%30s ', '\textbf{w/ sphericity}');
            case 'svm_w_rad.mat'
                fprintf('%30s ', '\textbf{w/ radius estimation}');
            otherwise
                fprintf('%30s ', ['\textbf{' list(l-1).name '}']);
        end
    end
    if sumTP(l)==max(sumTP), fprintf(' & \\textbf{%d} ', sumTP(l)); else fprintf(' & %d ', sumTP(l)); end;
    if sumFP(l)==min(sumFP), fprintf(' & \\textbf{%d} ', sumFP(l)); else fprintf(' & %d ', sumFP(l)); end;
    if sumTN(l)==max(sumTN), fprintf(' & \\textbf{%d} ', sumTN(l)); else fprintf(' & %d ', sumTN(l)); end;
    if sumFN(l)==min(sumFN), fprintf(' & \\textbf{%d} ', sumFN(l)); else fprintf(' & %d ', sumFN(l)); end;
    if sumSens(l)==max(sumSens), fprintf(' & \\textbf{%.3f} ', sumSens(l)); else fprintf(' & %.3f ', sumSens(l)); end;
    if sumFPR(l)==min(sumFPR), fprintf(' & \\textbf{%.3f} ', sumFPR(l)); else fprintf(' & %.3f ', sumFPR(l)); end;
    fprintf('\\\\\n');
end
fprintf('\n');
for l=1:length(list)+1
    if l==1
        fprintf('%30s ', '\textbf{this one}');
    else
        switch list(l-1).name
            case 'svm_nonnormalized.mat'
                fprintf('%30s ', '\textbf{Baseline}');
            case 'svm_normalized.mat'
                fprintf('%30s ', '\textbf{Normalized}');
            case 'svm_var_comp.mat'
                fprintf('%30s ', '\textbf{Various comp.}');
            case 'svm_w_sphericity.mat'
                fprintf('%30s ', '\textbf{w/ sphericity}');
            case 'svm_w_rad.mat'
                fprintf('%30s ', '\textbf{w/ radius estimation}');
            otherwise
                fprintf('%30s ', ['\textbf{' list(l-1).name '}']);
        end
    end
    if sumTP15(l)==max(sumTP15), fprintf(' & \\textbf{%d} ', sumTP15(l)); else fprintf(' & %d ', sumTP15(l)); end;
    if sumFP15(l)==min(sumFP15), fprintf(' & \\textbf{%d} ', sumFP15(l)); else fprintf(' & %d ', sumFP15(l)); end;
    if sumTN15(l)==max(sumTN15), fprintf(' & \\textbf{%d} ', sumTN15(l)); else fprintf(' & %d ', sumTN15(l)); end;
    if sumFN15(l)==min(sumFN15), fprintf(' & \\textbf{%d} ', sumFN15(l)); else fprintf(' & %d ', sumFN15(l)); end;
    if sumSens15(l)==max(sumSens15), fprintf(' & \\textbf{%.3f} ', sumSens15(l)); else fprintf(' & %.3f ', sumSens15(l)); end;
    if sumFPR15(l)==min(sumFPR15), fprintf(' & \\textbf{%.3f} ', sumFPR15(l)); else fprintf(' & %.3f ', sumFPR15(l)); end;
    fprintf('\\\\\n');
end
fprintf('\n');














