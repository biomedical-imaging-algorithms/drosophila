function [bw,phi] = segmentsingleNC(imc,realradius,sig_kernel,sig_grad,gradr,gradc,gradz,params)

center = (size(imc)+1)./2;

G = GaussIm(imc,4.*sig_grad,sig_grad);
gxr = convn(G, gradr, 'same');
gxc = convn(G, gradc, 'same');
gxz = convn(G, gradz, 'same');
gx = [gxr(:),  gxc(:),  gxz(:)];

[i,j,k] = ndgrid(1:size(imc,1),1:size(imc,2),1:size(imc,3));

dsijk = bsxfun(@times, [center(1)-i(:),center(2)-j(:),center(3)-k(:)], params.spacing);

d_norms = arrayfun(@(idx) norm(dsijk(idx,:)), (1:size(dsijk,1))');
d_norms((length(d_norms)+1)/2) = 1;
nsijk = dsijk./repmat(d_norms,1,size(dsijk,2));
nsijk((length(d_norms)+1)/2) = 0;

g_norms = arrayfun(@(idx) norm(gx(idx,:)), (1:size(gx,1))');
ngx = gx./repmat(g_norms,1,size(gx,2));

phi = .5 .* ( 1 + dot(ngx,nsijk,2) );

wsijk = max(sign(realradius-d_norms),exp(-(realradius-d_norms).^2./(2.*sig_kernel.^2)));

weighted_phi = phi.*wsijk;
weighted_phi = reshape(weighted_phi, size(imc,1), size(imc,2), size(imc,3));

level = graythresh(weighted_phi);
bw = zeros(size(imc));
bw(weighted_phi>level) = 1;
phi = reshape(phi, size(imc,1), size(imc,2), size(imc,3));

end