
addpath(genpath('../toolbox'));

% filename = '../../MD-Experiment-2011-02-28-11569_Position\(9\).zvi';
% filename = '../../../data/original_stacks/MD-Experiment-2011-02-28-11569_Position\(9\).zvi';

filename = strrep(filename, '\(','(');
filename = strrep(filename, '\)',')');

filetiff = [filename(1:end-4) '.tif'];

path = fullfile(fileparts(mfilename('fullpath')), '../toolbox/bfmatlab/loci_tools.jar');
javaaddpath(path);

vol = bfopen(filename);

omemeta = vol{4};

stack = zeros(omemeta.getPixelsSizeY(0).getValue(), omemeta.getPixelsSizeX(0).getValue(), ...
    omemeta.getPixelsSizeZ(0).getValue(), omemeta.getPixelsSizeC(0).getValue());

for c=1:size(stack,4)
    stack(:,:,:,c) = cat(3, vol{1}{ 1+(c-1)*size(stack,3):c*size(stack,3) ,1 });
end

for i=1:size(stack,3)
    
    im2 = zeros(size(stack,1), size(stack,2), 3);
    im2(:,:,2) = stack(:,:,i,1);
    im2(:,:,1) = stack(:,:,i,2);
    im2(:,:,3) = stack(:,:,i,2);
    
    im8 = uint8(im2);
    imwrite(im8, filetiff, 'tif', 'Compression', 'none', 'WriteMode', 'append');
end


