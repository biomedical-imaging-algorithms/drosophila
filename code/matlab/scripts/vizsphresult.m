r = 621;
c = 621;
z = 8;

rows = 1:size(stackm,1);
cols = 1:size(stackm,2);
zeds = 1:size(stackm,3);
[cols,rows,zeds] = meshgrid(cols,rows,zeds);

spacing = [.258 .258 3.00];
radius = 20;
radius_px = radius./spacing;

st = stackm;
sph = (((rows-r)./radius_px(2)).^2 + ((cols-c)./radius_px(1)).^2 + ((zeds-z)./radius_px(3)).^2) <= 1;

insph = [];
for i=1:size(stackm,3)
    st16 = stackm(:,:,i);
    st = uint8(stackm(:,:,i));
    st(sph(:,:,i)) = st(sph(:,:,i))./2+90;
    insph = [insph; st16(sph(:,:,i))];
    im = cat(3,st,stackm(:,:,i),st);
    imshow(im);
    pause(.5);
end

% rows = 1:size(stackm,1);
% cols = 1:size(stackm,2);
% zeds = 1:size(stackm,3);
% [cols,rows,zeds] = meshgrid(cols,rows,zeds);
% 
% spacing = [.258 .258 3.00];
% radius = 20;
% radius_px = radius./spacing;
% 
% dd = dist;
% for m=1:30
%     [~,ind] = max(dd(:));
%     [r,c,z] = ind2sub(size(dd),ind);
%     clf;
%     imshow(uint8(stackm(:,:,z)));
%     hold on;
%     plot(c,r,'.','MarkerSize',20);
%     
%     sph = (((rows-r)./radius_px(2)).^2 + ((cols-c)./radius_px(1)).^2 + ((zeds-z)./radius_px(3)).^2) <= 1;
%     dd(sph)=0;
%     
%     fprintf('%d %d %d %d\n', r, c, z, m);
%     waitforbuttonpress;
% end