function diam = estimateDiameter(segm, params)

rows = (1:size(segm,1)).*params.spacing(1);
cols = (1:size(segm,2)).*params.spacing(2);
zeds = (1:size(segm,3)).*params.spacing(3);
[cols,rows,zeds] = meshgrid(cols,rows,zeds);

brd = zeros(size(segm));
for i=1:size(segm,3)
    bd = bwboundaries(segm(:,:,i));
    for k=1:length(bd)
        bb=bd{k};
        inds = sub2ind(size(brd(:,:,i)),bb(:,1),bb(:,2));
        slice=brd(:,:,i);
        slice(inds)=1;
        brd(:,:,i)=slice;
    end
end

center = ((size(segm)+1)/2).*params.spacing;

dists = brd.*sqrt((rows-center(1)).^2 + (cols-center(2)).^2 + (zeds-center(3)).^2);
diam = mean(dists(dists>0));
end