function imout = GaussIm(im, fsize, sig)
% imout = GaussIm(im, stds)

fsize   = (fsize-1)/2;
[x,y,z] = ndgrid(-fsize(1):fsize(1),-fsize(2):fsize(2),-fsize(3):fsize(3));
h = exp(-(x.*x/2/sig(1)^2 + y.*y/2/sig(2)^2 + z.*z/2/sig(3)^2));
h = h/sum(h(:));
imout = convn(im,h,'same');

end