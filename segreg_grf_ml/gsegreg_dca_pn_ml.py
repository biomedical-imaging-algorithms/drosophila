""" Joint segmentation & registration and shape model learning

Model: Log-supermodular P^n model with unary priors, multi-valued labelling
Computes: unary marginals, potentials by supermodular-polyhedron approximation
Autor: Boris Flach
"""
import numpy as np
import copy
import scipy.ndimage as ndimage

from gsegreg_dca_prms import PnModelParms

# try:
#     from gsegreg_dca_prms_local import PnModelParms
# except ImportError:
#     print("Warning: Found no local parameters - gsegreg_dca_prms_local")
#     from gsegreg_dca_prms import PnModelParms


# =============================================================================
class PnMlModel:
    """ P^n model for multi-valued labellings with unaries """
    # mps = PnModelParms()

    # ===============================================
    def __init__(self, size, lnum=None, umod=None, edges=None, params=None):
        self.size = tuple(size)
        if params is not None:
            self.mps = params
        else:
            self.mps = PnModelParms()
        if lnum is None:
            lnum = self.mps.LABELS
        self.lnum = lnum
        self.fsize = self.size + (lnum,)

        if umod is None:
            self.umod = np.zeros(self.fsize, dtype=np.float64)
        else:
            if not umod.shape == self.fsize:
                raise Exception("PN model: shape mismatch!")
            self.umod = np.array(umod, dtype=np.float64)
        if edges is None:
            self.edges = []
        else:
            self.edges = copy.deepcopy(edges)
        self.mprob = np.zeros(self.fsize, dtype=np.float64)

    # ===============================================
    def set_umod(self, umod):
        if not umod.shape == self.fsize:
            raise Exception("PN model: shape mismatch!")
        self.umod = np.array(umod, dtype=np.float64)

        self.umod[self.umod < self.mps.MIN_U] = self.mps.MIN_U
        self.umod[self.umod > self.mps.MAX_U] = self.mps.MAX_U

    # ===============================================
    def set_mprob(self, prob):
        if not prob.shape == self.fsize:
            raise Exception("PN model: shape mismatch!")
        self.mprob = np.array(prob, dtype=np.float64)

        self.mprob[self.mprob < self.mps.MIN_PROB] = self.mps.MIN_PROB
        self.mprob[self.mprob > self.mps.MAX_PROB] = self.mps.MAX_PROB
        self.normalise_mprobs()

    # ===============================================
    def add_edge(self, edge=None):
        x = copy.deepcopy(edge)
        self.edges.append(x)

    # ===============================================
    def load_probs_from_images(self, img_names):
        """ Load marginal probabilities from images
        :param img_names: dictionary {label : name}
        images must be given for all or for all but one label
        """
        from PIL import Image

        if len(img_names) not in [self.lnum, self.lnum - 1]:
            raise Exception("PN model: wrong number of images")

        labels = list(img_names.keys())
        if len(set(range(self.lnum)) - set(labels)) > 1:
            raise Exception("PN model: not enough images given")

        v = np.zeros(self.fsize, dtype=np.float64)

        for label in labels:
            im = Image.open(img_names[label]).convert("L")
            # resize if necessary
            if not im.size == self.size[::-1]:
                im = im.resize(self.size[::-1])

            # put the image into an np-array
            v[:, :, label] = np.array(im, dtype=np.float64) / 255.0

        if len(labels) is not self.lnum:
            last = set(range(self.lnum)) - set(labels)
            label = last.pop()
            v[:, :, label] = 1.0 - np.sum(v, axis=2)

        self.set_mprob(v)

        return None

    # ===============================================
    def save_probs_as_images(self, img_names, size=None):
        """ Save as an image, optionally resize.
        :param: img_names: dictionary {label : name}
        :param: size: alternative image size
        """
        from PIL import Image

        if len(img_names) is not self.lnum:
            raise Exception("PN model: wrong number of images")

        # check maximal/minimal value
        if (self.mprob.min() < 0.0) or (self.mprob.max() > 1.0):
            print("min = " + repr(self.mprob.min()) + ", max = " + repr(self.mprob.max()))
            raise Exception("Save probs as image: array values not in [0.0,1.0]")

        # scale, transform to int
        i_arr = np.uint8(self.mprob * 255)

        labels = list(img_names.keys())
        for l in labels:
            img = Image.fromarray(i_arr[:, :, l], "L")
            # resize if requested
            if size is not None:
                img = img.resize(size[::-1])
            # save
            img.save(img_names[l])

        return None

    # ===============================================
    def save_probs_as_image(self, name, size=None):
        """ Save as an image, optionally resize."""
        from PIL import Image

        if self.lnum is not 3:
            raise Exception("Can not save marginals as clour image")

        # check maximal/minimal value
        if (self.mprob.min() < 0.0) or (self.mprob.max() > 1.0):
            print("min = " + repr(self.mprob.min()) + ", max = " + repr(self.mprob.max()))
            raise Exception("Save probs as image: array values not in [0.0,1.0]")

        for l in range(self.lnum):
            # scale, transform to int
            i_arr = np.uint8(self.mprob * 255)
            img = Image.fromarray(i_arr, "RGB")

            # resize if requested
            if size is not None:
                img = img.resize(size[::-1])
            # save
            img.save(name)

        return None

    # ===============================================
    def save_mprobs(self, name):
        np.save(name, self.mprob)

    # ===============================================
    def load_probs(self, name):
        arr = np.load(name)
        if not len(arr.shape) == 3:
            raise Exception("Pn Model: load_probs: wrong dimension")
        if arr.shape[0:2] == self.size:
            self.set_mprob(arr)
        else:
            zoom = np.array(arr.shape, np.float64) / np.array(self.fsize, dtype=np.float64)
            zoom[2] = 1.0
            arr = ndimage.affine_transform(arr, zoom, output_shape=self.fsize)
            self.set_mprob(arr)

    # ===============================================
    def save_umod(self, name):
        np.save(name, self.umod)

    # ===============================================
    def load_umod(self, name):
        arr = np.load(name)
        if not len(arr.shape) == 3:
            raise Exception("Pn Model: load_umod: wrong dimension")
        if arr.shape == self.size:
            self.set_umod(arr)
        else:
            zoom = np.array(self.size, dtype=np.float64) / np.array(arr.shape, np.float64)
            zoom[2] = 1.0
            arr = ndimage.affine_transform(arr, zoom, output_shape=self.size)
            self.set_umod(arr)

    # ===============================================
    def marg_dist(self, imod):
        """ Compute Bhattacharyya distance of marginal distributions"""

        darr = np.sqrt(self.mprob * imod.mprob)
        sarr = np.sum(darr, axis=-1)
        sarr = -np.log(sarr)
        dist = np.sum(sarr) / np.float64(sarr.size)

        if dist < 0.0:
            raise Exception("Negative Bhattacharyya distance!")

        return dist

    # ===============================================
    def normalise_mprobs(self):
        sarr = np.sum(self.mprob, axis=-1, keepdims=True)
        self.mprob /= sarr

    # ===============================================
    def find_lext(self, probs):
        """ find Lovasz extension

        :param probs: marginal probabilities
        """

        if not probs.shape == self.fsize:
            raise Exception("PN model: shape mismatch!")

        eshape = self.size + (len(self.edges),)
        ext_smod = np.empty(eshape, dtype=np.float64)
        lprobs = np.zeros(self.size, dtype=np.float64)
        y = np.zeros(self.fsize, dtype=np.float64)

        if self.mps.PNOTTS == 0.0:
            return y

        for l in range(self.lnum - 1):
            lprobs += probs[:, :, l]

            # extend windows in additional dimension
            for i in range(len(self.edges)):
                dv = np.roll(lprobs, self.edges[i][0], axis=0)
                dv = np.roll(dv, self.edges[i][1], axis=1)
                ext_smod[:, :, i] = dv

            # maxima / minima per window
            ext_ext = np.zeros((eshape[0] * eshape[1], eshape[2]), dtype=np.float64)
            md = np.argmax(ext_smod, axis=-1)
            ext_ext[np.arange(int(eshape[0] * eshape[1])), md.flatten()] += 1.0
            md = np.argmin(ext_smod, axis=-1)
            ext_ext[np.arange(int(eshape[0] * eshape[1])), md.flatten()] -= 1.0
            ext_ext = np.reshape(ext_ext, eshape)

            # roll back & accumulate
            ly = np.zeros(self.size, dtype=np.float64)
            for i in range(len(self.edges)):
                dv = np.roll(ext_ext[:, :, i], -self.edges[i][0], axis=0)
                dv = np.roll(dv, -self.edges[i][1], axis=1)
                ly += dv

            ly *= self.mps.PNOTTS
            for lup in range(l+1, self.lnum):
                y[:, :, lup] += ly

        return y

    # ===============================================
    def estimate_upots_logsub(self, probs=None):
        """ estimate unary potentials of a P^n model by log-supermodular approximation

        :param probs: marginal probabilities
        """

        if probs is None:
            smod = self.mprob
        else:
            if not probs.shape == self.fsize:
                raise Exception("PN model: shape mismatch!")
            smod = probs

        y = self.find_lext(smod)

        u = np.log(smod) - y

        if self.mps.ONE_FOREGROUND:
            # set all foreground components equal
            u[:, :, 1:] = np.mean(u[:, :, 1:], axis=-1, keepdims=True)

        self.set_umod(u)

        return None

    # ===============================================
    def comp_marg_logsub(self, umod_e=None, v_init=None):
        """ compute marginals of P^n model by log submodular approximation

        :param: umod_e: additional external unaries (log-domain)
        :param: v_init: initial estimate of marginals (log-domain)
        :return: unary marginals
        """

        y = np.empty(self.fsize, dtype=np.float64)
        umod = np.copy(self.umod)
        nb_pxls = np.prod(self.fsize[:-1]).astype(float)

        if umod_e is not None:
            umod += np.array(umod_e, dtype=np.float64)

        if v_init is None:
            v = np.copy(umod)
        else:
            v = np.array(v_init, dtype=np.float64)

        dval = 0.0
        zyk = 0

        # Frank-Wolfe algorithm
        while True:
            zyk += 1
            grad = np.exp(v) / np.sum(np.exp(v), axis=-1, keepdims=True)
            y.fill(0.0)
            y += umod
            y += self.find_lext(grad)

            dval = np.sum(grad * (v - y))

            if dval < 0.0:
                raise Exception("Error in Frank-Wolfe algorithm!")
            gamma = 2.0 / (float(zyk) + 2.0)
            v += gamma * (y - v)
            dval_norm = dval / nb_pxls / self.mps.PNOTTS / self.mps.RADIUS

            if self.mps.VERBOSE:
                print("FW_Zyk = " + repr(zyk) + "; dval_norm = " + repr(dval_norm))
            if (zyk > self.mps.MAX_STEPS) or (dval_norm < self.mps.EPSILON):
                break

        v[v < self.mps.MIN_U] = self.mps.MIN_U
        v[v > self.mps.MAX_U] = self.mps.MAX_U

        print("FW_Zyk = " + repr(zyk) + ", dval = " + repr(dval))

        return np.exp(v) / np.sum(np.exp(v), axis=-1, keepdims=True)
