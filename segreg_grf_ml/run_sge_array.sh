#! /bin/bash
# EXECUTE:
# export PATH=/datagrid/sge/bin/lx-amd64:$PATH
# export SGE_ROOT=/datagrid/sge
# qsub run_sge_array.sh
# qstat

#$ -S /bin/bash
#$ -N DrosEgg-BF-all-images
#$ -t 1-50108:1
#$ -tc 250
#$ -cwd
#$ -q offline
#$ -V
#$ -o /datagrid/Medical/microscopy/drosophila/TEMPORARY_OVARY/segment-ovary_BF-all-images/tmp/
#$ -e /datagrid/Medical/microscopy/drosophila/TEMPORARY_OVARY/segment-ovary_BF-all-images/tmp/

NAME=`awk "NR==$SGE_TASK_ID" list.txt`

python3 ~/Repos/drosophila/segreg_grf_ml/gsegreg_dca_simple_infer_ml.py \
	--wdir "/datagrid/Medical/microscopy/drosophila/TEMPORARY_OVARY/segment-ovary_BF-all-images" \
	--unar ${NAME}

