""" Joint registration & segmentation by DCA with EM (parallel version)

Model: Ising model with unary priors
Appearance: multivariate normal distributions
Transformations: rigid body
Author: Boris Flach
"""

import numpy as np


class PnModelParms:
    def __init__(self):
        # Model constants
        self.LABELS = 3
        self.RADIUS = 2
        self.PNOTTS = 3.0
        self.MIN_PROB = 0.0001
        self.MAX_PROB = 0.9999
        self.MIN_U = - np.log(1 / self.MIN_PROB - 1)
        self.MAX_U = - np.log(1 / self.MAX_PROB - 1)
        self.ONE_FOREGROUND = True

        # Frank-Wolfe algorithm constants
        self.MAX_STEPS = 100
        self.EPSILON = 1.0
        self.VERBOSE = False


class DCAEMParms:
    def __init__(self):
        # EM constants
        self.EM_MaxZyk = 100
        self.EM_MinDist = 1.0e-6  # Bhattacharyya distance

        # DCA constants
        self.DCA_MaxZyk = 100  # maximal number of DCA iterations
        self.DCA_MinDist = 5.0e-6  # Stopping criterion for DCA (Avg. Bhattacharyya distance of marginals)
        self.VERBOSE = True


class AppearParms:
    def __init__(self):
        self.COMPONENTS = (1, 1)
        self.DIMENSION = 3
        self.MIN_EIG = 0.002
        self.MIN_DVAL = 1e-50  # smallest value for probability density
        self.MIX_EM_ZYK = 100
        self.MIX_EM_DIST = 0.001
        self.MIX_EM_VERBOSE = False
