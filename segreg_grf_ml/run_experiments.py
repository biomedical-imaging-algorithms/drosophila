"""
>> python run_experiments.py \
    --wdir "/datagrid/Medical/microscopy/drosophila/TEMPORARY_OVARY/segment-ovary_BF-all-images"

"""

import os
import argparse
import multiprocessing as mproc
from functools import partial

import tqdm
import pandas as pd

import gsegreg_dca_simple_infer_ml

NB_THREADS = mproc.cpu_count()


def parse_args():
    #  Parse parameters
    parser = argparse.ArgumentParser(description='Image segmentation')
    parser.add_argument('--wdir', type=str, help='working directory')
    parser.add_argument('--unar', type=str, required=False,
                        default=None, help='unaries name')
    args = parser.parse_args()
    return args


def wrapper_execute(path_unar, args):
    args.unar = str(path_unar[0])
    # print (repr(args))
    gsegreg_dca_simple_infer_ml.main(args)


def main(args):
    path_list = os.path.join(args.wdir, 'list.txt')
    assert os.path.isfile(path_list), 'missing %s' % path_list
    df_list = pd.read_csv(path_list, index_col=None, header=None)

    tqdm_bar = tqdm.tqdm(total=len(df_list))
    pool = mproc.Pool(NB_THREADS)
    for _ in pool.imap_unordered(partial(wrapper_execute, args=args),
                                 (row for idx, row in df_list.iterrows())):
        tqdm_bar.update()
    pool.close()
    pool.join()


if __name__ == '__main__':
    args = parse_args()
    main(args)