""" Joint segmentation & registration and shape model learning

Models: Image and appearance models (mixtures of multivariate Gaussians)
Computes: appearance probabilities, parameter estimation
Autor: Boris Flach
"""
import numpy as np
import copy

try:
    from gsegreg_dca_prms_local import AppearParms
except ImportError:
    print("Warning (Image module): Found no local parameters")
    from gsegreg_dca_prms import AppearParms


# =============================================================================
class CImage:
    mps = AppearParms()

    # ===============================================
    def __init__(self):
        self.size = None
        self.size_orig = None
        self.arr = None

    # ===============================================
    def load(self, img_name, im_type, size=None):
        """ load image, resize, transform into numpy array. """

        from PIL import Image

        im = Image.open(img_name).convert(im_type)
        self.size_orig = im.size[::-1]
        #  resize if requested
        if size is not None:
            # bh = size[0]
            # hpercent = (bh/float(im.size[1]))
            # bw = int((float(im.size[0])*float(hpercent)))
            # im = im.resize((bw, bh), resample=Image.BICUBIC)
            im = im.resize(size[::-1], resample=Image.BICUBIC)
        self.size = im.size[::-1]
        #  put the image into an np-array
        self.arr = np.float64(np.array(im))
        if im_type in ('L', 'RGB'):
            self.arr /= 255.0
        if im_type in ('I', 'F'):
            self.arr /= np.amax(self.arr)
        if im_type not in ('L', 'RGB', 'I', 'F'):
            raise Exception("Load image: image type not supported")

        return None

    # ===============================================
    def save(self, name, im_type, orig_size=True):
        """ Save as an image, optionally resize."""
        from PIL import Image

        # check maximal/minimal value
        if (self.arr.min() < 0.0) or (self.arr.max() > 1.0):
            print("min = " + repr(self.arr.min()) + ", max = " + repr(self.arr.max()))
            raise Exception("Save image" + repr(name) + ": array values not in [0.0,1.0]")

        i_arr = None
        # scale, transform to int
        if im_type in ('L', 'RGB'):
            i_arr = np.uint8(self.arr * 255)
        if im_type in ('I', 'F'):
            i_arr = np.uint16(self.arr * np.iinfo(np.uint16).max)
        if im_type not in ('L', 'RGB', 'I', 'F'):
            raise Exception("Save image: image type not supported")

        img = Image.fromarray(i_arr, im_type)

        # resize if requested
        if (orig_size is True) and (self.size != self.size_orig):
            img = img.resize(self.size_orig[::-1], resample=Image.BICUBIC)

        # save
        img.save(name)

        return None

    # ===============================================
    def set_values(self, array):
        if not array.shape[0:2] == self.size:
            raise Exception("Image: array shape mismatch")
        self.arr = np.array(array, dtype=np.float64)

        return None

    # ===============================================
    def compute_appearance_ldiff(self, appear):
        """compute log difference of foreground/ background appearance prob's

        :param appear: appaerance models
        :return: difference of logarithms of appearance probs
        """
        bg = appear.glist[0].compute_probs(self.arr)
        fg = appear.glist[1].compute_probs(self.arr)
        bg[bg < self.mps.MIN_DVAL] = self.mps.MIN_DVAL
        fg[fg < self.mps.MIN_DVAL] = self.mps.MIN_DVAL

        return np.log(fg) - np.log(bg)

    # ===============================================
    def compute_appearance_lprobs(self, appear):
        """Compute log appearance probabilities"""

        fsize = self.size + (len(appear.glist),)
        lprobs = np.empty(fsize, dtype=np.float64)

        for l in range(len(appear.glist)):
            arr = appear.glist[l].compute_probs(self.arr)
            arr[arr < self.mps.MIN_DVAL] = self.mps.MIN_DVAL
            lprobs[:, :, l] = arr

        lprobs = np.log(lprobs)
        return lprobs


# =============================================================================
class GaussMVD:
    """ Multivariate normal distribution """

    # ===============================================
    def __init__(self, dim, min_eigen):
        self.dim = int(dim)
        self.min_eigen = float(min_eigen)
        self.mean = np.zeros(self.dim, dtype=np.float64)
        self.cov = np.eye(self.dim, dtype=np.float64)
        self.det = np.linalg.det(np.matrix(self.cov))
        self.normc = 1.0 / np.sqrt(np.power((2.0 * np.pi), self.dim) * self.det)

    # ===============================================
    def modify(self, mean, cov):
        if not ((mean.shape == (self.dim,)) and (cov.shape == (self.dim, self.dim))):
            raise Exception("Gaussian: shape mismatch!")

        self.mean = np.array(mean, dtype=np.float64)
        self.cov = np.array(cov, dtype=np.float64)

        # if an eigenvalue is smaller than min_eigen set it to min_eigen
        w, v = np.linalg.eigh(np.matrix(self.cov))
        w[w < self.min_eigen] = self.min_eigen
        wd = np.matrix(np.diag(w))
        self.cov = np.asarray(v * wd * v.transpose())

        self.det = np.linalg.det(np.matrix(self.cov))

        self.normc = 1.0 / np.sqrt(np.power((2.0 * np.pi), self.dim) * self.det)

        return None

    # ===============================================
    def compute_probs(self, arr):
        """ compute probabilities for an array of values """

        inv_cov = np.asarray(np.linalg.inv(np.matrix(self.cov)))
        if self.dim == 1:
            darr = arr[..., np.newaxis] - self.mean
        else:
            darr = arr - self.mean

        varr = np.sum(darr * np.inner(darr, inv_cov), axis=-1)
        varr = - varr * 0.5
        varr = np.exp(varr) * self.normc

        return varr

    # ===============================================
    def estimate(self, arr, weight):
        """ estimate parameters from data (array of values & array of weights) """
        ebeta = weight[..., np.newaxis]
        wsum = np.sum(weight)

        if self.dim == 1:
            dimlist = list(range(len(arr.shape)))
            arre = arr[..., np.newaxis]
        else:
            dimlist = list(range(len(arr.shape) - 1))
            arre = arr
        dimtup = tuple(dimlist)

        # estimate mean
        mean = np.sum(ebeta * arre, axis=dimtup) / wsum

        # estimate covariance
        darr = arre - mean
        cov = np.tensordot(darr, darr * ebeta, axes=(dimlist, dimlist)) / wsum

        self.modify(mean, cov)

        return None

    # ===============================================
    def compute_distance(self, mvgd):
        """ Bhattacharyya distance """

        ccov = (self.cov + mvgd.cov) / 2.0
        inv_ccov = np.asarray(np.linalg.inv(np.matrix(ccov)))
        d_ccov = np.linalg.det(np.matrix(ccov))
        cmean = self.mean - mvgd.mean
        v1 = np.dot(cmean, np.tensordot(inv_ccov, cmean, 1)) / 8.0
        v2 = np.log(d_ccov / np.sqrt(self.det * mvgd.det)) / 2.0

        return v1 + v2

    # ===============================================
    def write(self):
        print("Mean, covariance:")
        print(repr(self.mean))
        print(repr(self.cov))


# =============================================================================
class MixGauss:
    """ Mixture of components (multivariate Gaussians) """

    mps = AppearParms()

    # ===============================================
    def __init__(self, dim):
        self.dim = dim
        self.n = 0
        self.glist = []
        self.aprobs = []

    # ===============================================
    def add_component(self, mvgd, prob=0.0):
        if not self.dim == mvgd.dim:
            raise Exception("Mixture of Gaussians: wrong dimension!")
        x = copy.deepcopy(mvgd)
        self.glist.append(x)
        self.aprobs.append(np.float64(prob))
        self.n += 1

    # ===============================================
    def modify_component(self, i, mean, cov, prob):
        self.glist[i].modify(mean, cov)
        self.aprobs[i] = np.float64(prob)

    # ===============================================
    def normalise_aprobs(self):
        self.aprobs /= np.sum(self.aprobs)
        self.aprobs = list(self.aprobs)

    # ===============================================
    def compute_probs(self, val):
        """ compute probabilities for an array of values """
        tval = self.glist[0].compute_probs(val) * self.aprobs[0]

        for k in range(1, self.n):
            tval += self.glist[k].compute_probs(val) * self.aprobs[k]

        return tval

    # ===============================================
    def compute_distance(self, mix):
        maxd = self.glist[0].compute_distance(mix.glist[0])

        for k in range(1, self.n):
            maxd = max(maxd, self.glist[k].compute_distance(mix.glist[k]))

        return maxd

    # ===============================================
    def write(self):
        for k in range(self.n):
            print("p, mean, sdev = {0}, {1}, \n {2}".format(repr(self.aprobs[k]), repr(self.glist[k].mean),
                                                            repr(self.glist[k].cov)))

    # ===============================================
    def estimate(self, val, weight):
        """ Estimate mixture by em-algorithm """

        # if mixture consists of one component only
        if self.n == 1:
            self.glist[0].estimate(val, weight)
            self.aprobs[0] = np.float64(1.0)
            return None

        # if mixture consists of several components apply em algorithm
        sumweight = np.sum(weight)
        zyk = 0
        if self.dim == 1:
            post = np.empty((self.n,) + val.shape, dtype=np.float64)
            postsum = np.empty(val.shape, dtype=np.float64)
        else:
            post = np.empty((self.n,) + val.shape[:-1], dtype=np.float64)
            postsum = np.empty(val.shape[:-1], dtype=np.float64)

        mdist = 0.0
        while True:
            zyk += 1
            # compute component posteriors
            postsum.fill(0.0)
            for k in range(len(self.glist)):
                post[k] = self.glist[k].compute_probs(val) * self.aprobs[k]
                postsum += post[k]
            # normalize
            # post /= postsum[np.newaxis, :]
            post[:, postsum > 0.0] /= postsum[np.newaxis, postsum > 0.0]

            # change the components
            mdist = 0.0
            for k in range(len(self.glist)):
                gmold = copy.deepcopy(self.glist[k])
                self.glist[k].estimate(val, post[k] * weight)
                self.aprobs[k] = np.sum(post[k] * weight) / sumweight
                mdist = max(mdist, self.glist[k].compute_distance(gmold))

            self.normalise_aprobs()

            if (zyk > self.mps.MIX_EM_ZYK) or (mdist < self.mps.MIX_EM_DIST):
                break

        if self.mps.MIX_EM_VERBOSE:
            print("MIX_EM_Zyk, mdist = " + repr(zyk) + ", " + repr(mdist))

        return None


# =============================================================================
class Appearance:
    def __init__(self):
        self.glist = []

    # ===============================================
    def add_comp(self, gmix):
        x = copy.deepcopy(gmix)
        self.glist.append(x)

    # ===============================================
    def estimate(self, arr, weights):
        # background
        self.glist[0].estimate(arr, 1.0 - weights)
        # foreground
        self.glist[1].estimate(arr, weights)

    # ===============================================
    def estimate_ml(self, arr, weights):
        for l in range(len(self.glist)):
            self.glist[l].estimate(arr, weights[:, :, l])

    # ===============================================
    def distance(self, app):
        dist = 0.0
        for gmix1, gmix2 in zip(self.glist, app.glist):
            dist += gmix1.compute_distance(gmix2)
        return dist
