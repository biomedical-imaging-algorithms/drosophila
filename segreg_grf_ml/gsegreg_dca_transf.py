""" Joint segmentation & registration and shape model learning

Transformations: rigid body and scaling
author: Boris Flach
"""

import numpy as np
import scipy.ndimage as ndimage
import scipy.signal as signal
import math


# =============================================================================
def rbs_trans_2d(arr, transf, out_shape2d, pval):
    """ Apply rigid body & scale transform to 2D array

    :param arr: input array
    :param transf: transformation vector (0,1 - translation, 2 - rotation cclws, 3 scale
    :param out_shape2d: shape (2D) of the output
    :param pval: padding value
    :return: transformed array
    """
    zf = 1.2
    c_in = 0.5 * np.array(arr.shape)
    c_out = 0.5 * np.array(out_shape2d)

    a = transf[2] * np.pi / 180.0
    z = math.pow(zf, transf[3])
    matrix = np.matrix(np.array([[np.cos(a), -np.sin(a)], [np.sin(a), np.cos(a)]]))
    matrix = matrix.dot(np.diag([z, z]))
    imatrix = np.array(np.linalg.inv(matrix))
    offs = c_in - np.dot(imatrix, c_out)

    tarr = ndimage.affine_transform(arr, imatrix, offset=offs, output_shape=out_shape2d, order=1, cval=pval)

    tarr = np.roll(tarr, transf[0], axis=0)
    tarr = np.roll(tarr, transf[1], axis=1)

    return tarr


# =============================================================================
def rbs_inv_trans_2d(arr, transf, out_shape2d, pval):
    """ Apply inverse rigid body & scale transform to 2D array

    :param arr: input array
    :param transf: transformation vector (0,1 - translation, 2 - rotation cclws, 3 scale
    :param out_shape2d: shape (2D) of the output
    :param pval: padding value
    :return: transformed array
    """

    itransf = -transf
    zf = 1.2
    c_in = 0.5 * np.array(arr.shape)
    c_out = 0.5 * np.array(out_shape2d)

    a = itransf[2] * np.pi / 180.0
    z = math.pow(zf, itransf[3])
    matrix = np.matrix(np.array([[np.cos(a), -np.sin(a)], [np.sin(a), np.cos(a)]]))
    matrix = matrix.dot(np.diag([z, z]))
    imatrix = np.array(np.linalg.inv(matrix))
    offs = c_in - np.dot(imatrix, c_out)

    tarr = np.roll(arr, itransf[0], axis=0)
    tarr = np.roll(tarr, itransf[1], axis=1)

    arr_out = ndimage.affine_transform(tarr, imatrix, offset=offs, output_shape=out_shape2d, order=1, cval=pval)

    return arr_out


# =============================================================================
def rbs_trans(arr, transf, out_shape2d, pval):
    """ Apply rigid body & scale transform to 2D or 3D array

    :param arr: input array
    :param transf: transformation vector (0,1 - translation, 2 - rotation cclws, 3 scale
    :param out_shape2d: shape (2D) of the output
    :param pval: padding value(s)
    :return: transformed array
    """

    if len(arr.shape) is 2:
        tarr = rbs_trans_2d(arr, transf, out_shape2d, pval)
    else:
        out_shape = out_shape2d + (arr.shape[2],)
        tarr = np.zeros(out_shape, dtype=np.float64)

        for l in range(arr.shape[2]):
            tarr[:, :, l] = rbs_trans_2d(arr[:, :, l], transf, out_shape2d, pval[l])

    return tarr


# =============================================================================
def rbs_inv_trans(arr, transf, out_shape2d, pval):
    """ Apply inverse rigid body & scale transform to 2D or 3D array

    :param arr: input array
    :param transf: transformation vector (0,1 - translation, 2 - rotation cclws, 3 scale
    :param out_shape2d: shape (2D) of the output
    :param pval: padding value
    :return: transformed array
    """

    if len(arr.shape) is 2:
        tarr = rbs_inv_trans_2d(arr, transf, out_shape2d, pval)
    else:
        out_shape = out_shape2d + (arr.shape[2],)
        tarr = np.zeros(out_shape, dtype=np.float64)

        for l in range(arr.shape[2]):
            tarr[:, :, l] = rbs_inv_trans_2d(arr[:, :, l], transf, out_shape2d, pval[l])

    return tarr


# =============================================================================
def optimal_translation_2d(arr1, arr2):
    """Estimate best translation via FFT for 2D arrays"""

    if not arr1.shape == arr2.shape:
        raise Exception("optimal translation: shape mismatch")
    if len(arr1.shape) is not 2:
        raise Exception("optimal translation: wrong dimension")

    out = signal.fftconvolve(arr1, arr2[::-1, ::-1], mode='full')
    tval = np.max(out)
    [i, j] = -np.array(np.unravel_index(np.argmax(out), out.shape)) + np.array(arr2.shape) - [1, 1]

    return [i, j, tval]


# =============================================================================
def optimal_translation_3d(arr1, arr2):
    """Estimate best translation via FFT for 3D arrays"""

    if not arr1.shape == arr2.shape:
        raise Exception("optimal translation: shape mismatch")
    if len(arr1.shape) is not 3:
        raise Exception("optimal translation: wrong dimension")

    arr1_layer = arr1[:, :, 0]
    arr2_layer = arr2[:, :, 0]
    out = signal.fftconvolve(arr1_layer, arr2_layer[::-1, ::-1], mode='full')

    for l in range(1, arr1.shape[2]):
        arr1_layer = arr1[:, :, l]
        arr2_layer = arr2[:, :, l]
        out += signal.fftconvolve(arr1_layer, arr2_layer[::-1, ::-1], mode='full')

    tval = np.max(out)
    [i, j] = -np.array(np.unravel_index(np.argmax(out), out.shape)) + np.array(arr1_layer.shape) - [1, 1]

    return [i, j, tval]


# =============================================================================
def optimal_rbs_align_fft(mprob, umod, trange, pval):
    """ Compute optimal alignment of mprob and umod by fft.
    :param mprob: marginal probs
    :param umod: potentials
    :param trange: range for angles and scales (0,1 - angle, 2,3 - scale)
    :param pval: padding value(s) for marginal probs
    :return: optimal transformation
    """

    i = 0
    j = 0
    a = trange[0]
    s = trange[2]
    maxval = np.NINF
    shape_2d = umod.shape[0:2]

    for ds in np.arange(trange[2], trange[3] + 1):
        # scale
        for da in np.arange(trange[0], trange[1] + 1):
            # rotate & scale
            transf = [0, 0, da, ds]
            mprob_transf = rbs_trans(mprob, transf, shape_2d, pval)

            if len(mprob.shape) is 2:
                [di, dj, tval] = optimal_translation_2d(mprob_transf, umod)
            else:
                [di, dj, tval] = optimal_translation_3d(mprob_transf, umod)
            if tval > maxval:
                maxval = tval
                i = di
                j = dj
                a = da
                s = ds

    return [i, j, a, s]


# =============================================================================
def optimal_rs_align_fft(mprob, umod, trange, pval):
    """ Compute optimal alignment of mprob and umod without translation.
    :param mprob: marginal probs
    :param umod: potentials
    :param trange: range for angles and scales (0,1 - angle, 2,3 - scale)
    :param pval: padding value(s) for marginal probs
    :return: optimal transformation
    """

    a = trange[0]
    s = trange[2]
    maxval = np.NINF
    shape_2d = umod.shape[0:2]

    for ds in np.arange(trange[2], trange[3] + 1):
        # scale
        for da in np.arange(trange[0], trange[1] + 1):
            # rotate & scale
            transf = [0, 0, da, ds]
            mprob_transf = rbs_trans(mprob, transf, shape_2d, pval)
            tval = np.sum(mprob_transf * umod)

            if tval > maxval:
                maxval = tval
                a = da
                s = ds

    return [0, 0, a, s]
