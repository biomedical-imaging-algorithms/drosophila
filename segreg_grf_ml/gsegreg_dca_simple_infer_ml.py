""" Joint segmentation & registration and shape model learning

Model: Log-supermodular P^n model with unary priors, multi-valued labelling
Computes: unary marginals, potentials by supermodular-polyhedron approximation
Autor: Boris Flach

USED PARAMETERS:
* RADIUS: the size of the cliques on which the potentials are defined
* PNOTTS: the strength of the potentials.
The formula is
    PNOTTS * [max_{i in C) s_i - min_{i in C} s_i]
where s is the labelling, and C denotes a clique with radius RADIUS.

The parameter EPSILON is a technical one: it defines the stopping criterion
    for the Frank-Wolfe algorithm.

SAMPLE run in SGE:
-------------------------------------------------------------------------------
#! /bin/bash
# export PATH=/datagrid/sge/bin/lx-amd64:$PATH
# export SGE_ROOT=/datagrid/sge
# qsub run_sge_array.sh
# qstat

#$ -S /bin/bash
#$ -N DrosEgg-BF-selected-slices
#$ -t 1-3:1
#$ -tc 100
#$ -cwd
#$ -q offline
#$ -V
#$ -o drosophila/TEMPORARY_OVARY/segment-ovary_BF-selected-slices/tmp/
#$ -e drosophila/TEMPORARY_OVARY/segment-ovary_BF-selected-slices/tmp/

NAME=`awk "NR==$SGE_TASK_ID" list.txt`

python3 ~/Repos/drosophila/segreg_grf_ml/gsegreg_dca_simple_infer_ml.py \
	--wdir "drosophila/TEMPORARY_OVARY/segment-ovary_BF-selected-slices" \
	--unar ${NAME}
--------------------------------------------------------------------------------

"""


import os
import numpy as np
import math
import json
import argparse
from PIL import Image
import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt

from gsegreg_dca_pn_ml import PnMlModel
from gsegreg_dca_prms import PnModelParms

LOCAL_CONFIG = 'PnModelParms.json'
DIR_LABELING = 'labeling'
DIR_SEGMENT = 'segmentation'
DIR_IMAGE_RGB = 'segment-RGB'
FORCE_RECOMPUTE = False

class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)


def load_params():
    if os.path.isfile(LOCAL_CONFIG):
        with open(LOCAL_CONFIG, 'r') as fp:
            params = json.load(fp)
        pnprms = Struct(**params)
    else:
        print("Warning: Found no local parameters - %s" % LOCAL_CONFIG)
        pnprms = PnModelParms()
        with open(LOCAL_CONFIG, 'w') as fp:
            json.dump(dict(vars(pnprms)), fp)
    return pnprms


def export_results(name, post_probs, max_label=None):
    if not os.path.isdir(DIR_LABELING):
        os.mkdir(DIR_LABELING)
    np.save(os.path.join(DIR_LABELING, name), post_probs)

    if not os.path.isdir(DIR_SEGMENT):
        os.mkdir(DIR_SEGMENT)
    labels = np.argmax(post_probs, axis=-1)
    img = Image.fromarray(labels.astype(dtype=np.uint8))
    img.save(os.path.join(DIR_SEGMENT, name + '.png'))

    if not os.path.isdir(DIR_IMAGE_RGB):
        os.mkdir(DIR_IMAGE_RGB)
    if max_label is None:
        max_label = np.max(labels)
    plt.imsave(os.path.join(DIR_IMAGE_RGB, name + '.png'), labels,
               vmin=0, vmax=max_label, cmap=plt.cm.jet)
    # colours = np.array([[0, 0, 0],
    #                     [255, 0, 0],
    #                     [0, 255, 0],
    #                     [0, 0, 255]])
    # imarr = np.take(colours, labels, axis=0)
    # img = Image.fromarray(imarr.astype(dtype=np.uint8), 'RGB')
    # img.save(os.path.join(DIR_IMAGE_RGB, name + '.png'), format='PNG')


# =============================================================================
def main(args):

    # change to working directory
    if args.wdir is not None:
        os.chdir(args.wdir)

    pnprms = load_params()

    # load unaries
    assert os.path.isfile(args.unar), 'missing "%s"' % args.unar
    name = os.path.splitext(os.path.basename(args.unar))[0]
    path_segm = os.path.join(DIR_SEGMENT, name + '.png')
    if os.path.isfile(path_segm) and not FORCE_RECOMPUTE:
        print('segmentation "%s" already exists and rerun is %s'
              % (path_segm, FORCE_RECOMPUTE))
        return

    f = np.load(args.unar)
    unaries = f['arr_0']

    # change label order
    unaries = unaries[:, :, [0, 1, 3, 2]]
    size = unaries.shape[0:2]
    unaries[unaries < pnprms.MIN_PROB] = pnprms.MIN_PROB
    unaries[unaries > pnprms.MAX_PROB] = pnprms.MAX_PROB
    unaries = np.log(unaries)

    # initialise P^n model
    imod = PnMlModel(size, params=pnprms)

    rad = int(imod.mps.RADIUS)
    for x in range(-rad, rad + 1):
        for y in range(-rad, rad + 1):
            if math.sqrt(x*x + y*y) <= rad:
                imod.edges.append((y, x))

    aprobs = np.ones(imod.fsize, dtype=np.float64)
    aprobs /= float(imod.lnum)
    imod.set_mprob(aprobs)
    imod.estimate_upots_logsub()

    # compute marginals
    post_probs = imod.comp_marg_logsub(umod_e=unaries)

    # change back label order
    post_probs = post_probs[:, :, [0, 1, 3, 2]]

    # save result
    export_results(name, post_probs, max_label=(unaries.shape[-1] - 1))


def parse_args():
    #  Parse parameters
    parser = argparse.ArgumentParser(description='Image segmentation')
    parser.add_argument('--wdir', help='working directory')
    # parser.add_argument('--mod_in', required=True, help='shape model')
    parser.add_argument('--unar', required=True, help='unaries name')

    args = parser.parse_args()
    return args


# =============================================================================
if __name__ == "__main__":
    args = parse_args()
    main(args)
