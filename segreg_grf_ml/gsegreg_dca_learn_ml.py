""" Joint segmentation & registration and shape model learning

Model: Log-supermodular P^n model with unary priors for multi-value labellings
Appearance: mixtures of multivariate normal distributions
Transformations: rigid body and scaling
author: Boris Flach
"""
import copy
import os
from gsegreg_dca_pn_ml import PnMlModel
from gsegreg_dca_image import CImage, GaussMVD, MixGauss, Appearance
from gsegreg_dca_transf import *
try:
    from gsegreg_dca_prms_local import DCAEMParms, PnModelParms, AppearParms
except ImportError:
    print("Warning: Found no local parameters - gsegreg_dca_prms_local")
    from gsegreg_dca_prms import DCAEMParms, PnModelParms, AppearParms


# =============================================================================
def marg_dist(marg1, marg2):
    """ Compute Bhattacharyya distance of marginal distributions"""

    darr = np.sqrt(marg1 * marg2)
    sarr = np.sum(darr, axis=-1)
    sarr = -np.log(sarr)
    dist = np.sum(sarr) / np.float64(sarr.size)

    if dist < 0.0:
        raise Exception("Negative Bhattacharyya distance!")
    return dist


# =============================================================================
def init_appearance():
    """
    Init mixture of Gaussians for foreground & background
    :return: appearance
    """

    aprms = AppearParms()
    dim = aprms.DIMENSION
    gd = GaussMVD(dim, aprms.MIN_EIG)
    appear = Appearance()
    mean = np.ones(dim, dtype=np.float64) * 0.5
    cov = np.eye(dim, dim, dtype=np.float64) * 0.3

    for ncomps in aprms.COMPONENTS:
        mix = MixGauss(dim)
        aprob = 1.0 / float(ncomps)
        for i in range(ncomps):
            gd.modify(mean + (np.random.random_sample(dim) - 0.5) * 0.1, cov)
            mix.add_component(gd, aprob)
        appear.add_comp(mix)

    return appear


# =============================================================================
def estimate_image_parms(img, pmarg, appear_init, transf_init, trange):
    """Estimate appearance and transformation for image"""

    zyk = 0
    iprms = PnModelParms()
    transf = np.copy(transf_init)
    appear = copy.deepcopy(appear_init)
    appear_new = copy.deepcopy(appear)
    pvals = np.ones(pmarg.shape[-1], dtype=np.float64) * iprms.MIN_PROB
    pvals[0] = iprms.MAX_PROB

    while True:
        zyk += 1
        # estimate appearance
        pmarg_tr = rbs_trans(pmarg, transf, img.size, pvals)
        appear_new.estimate_ml(img.arr, pmarg_tr)
        laprobs = img.compute_appearance_lprobs(appear_new)

        # reduce background components (zero padding of fftconvolve)
        pmarg_red = pmarg[:, :, 1:]
        laprobs_red = laprobs[:, :, 1:] - laprobs[:, :, 0, np.newaxis]

        # find new transformation
        transf_new = optimal_rbs_align_fft(pmarg_red, laprobs_red, trange, pvals[1:])

        ttval = np.all(transf_new == transf)
        transf = np.copy(transf_new)
        appear = copy.deepcopy(appear_new)
        if ttval or (zyk > 5):
            break

    return appear, transf, zyk


# =============================================================================
def dca_for_image(imod, img, pmarg_init, appear_init, tr_init, trange, pos=None):
    """ Apply EM-algirithm for an image; estimates transformation and appearance model.

    :param imod: Ising model
    :param img: image
    :param pmarg_init: initial posterior marginals
    :param appear_init: initial appearance model
    :param tr_init: initial transformation
    :param trange: range for angles and scales
    :param pos: process id needed for parallelisation
    :return pos process id needed for parallelisation
    :return beta: posterior foreground marginals
    :return appear: optimal appearance
    :return tran: optimal transformation
    :return zyk: number of EM cycles
    """

    zyk = 0
    sub_zyk = 0
    prms = DCAEMParms()
    transf = np.copy(tr_init)
    appear = copy.deepcopy(appear_init)
    pmarg = np.copy(pmarg_init)
    pvals = np.ones(imod.lnum, dtype=np.float64) * imod.mps.MIN_PROB
    pvals[0] = imod.mps.MAX_PROB
    if prms.VERBOSE:
        print("Start DC algorithm for image")

    while True:
        zyk += 1
        # A-step
        laprobs = img.compute_appearance_lprobs(appear)
        lvals = np.amin(np.amin(laprobs, axis=0), axis=0)
        lvals[0] = np.amax(np.amax(laprobs, axis=0), axis=0)[0]

        laprobs_tr = rbs_inv_trans(laprobs, transf, imod.size, lvals)
        pmarg_new = imod.comp_marg_logsub(umod_e=laprobs_tr)
        dist = marg_dist(pmarg_new, pmarg)
        pmarg = np.copy(pmarg_new)

        if prms.VERBOSE:
            print("DCA_IM_Zyk = " + repr(zyk) + ", Transl = " + repr(transf) +
                  ", Dist = " + repr(dist) + ", Sub_zyk = " + repr(sub_zyk))
            if pos is None:
                name = "segm_" + str(zyk).zfill(2) + ".png"
            else:
                name = "segm_" + str(pos).zfill(2) + "_" + str(zyk).zfill(2) + ".png"
            img_out = CImage()
            img_out.size = img_out.size_orig = pmarg.shape[0:2]
            img_out.arr = pmarg
            img_out.save(name, "RGB")

        if (zyk >= prms.EM_MaxZyk) or (dist < prms.EM_MinDist):
            break

        # B-step
        appear, transf, sub_zyk = estimate_image_parms(img, pmarg, appear, transf, trange)

    if pos is None:
        return pmarg, appear, transf, zyk
    else:
        return pos, pmarg, appear, transf, zyk


# =============================================================================
def add_segm_overlay(im_arr, seg_arr):
    """ Add a segmenation overlay to an image
    :param im_arr: color image array
    :param seg_arr: segmentation array
    :return: image with overlay
    """

    # segm = seg_arr[:, :, 1] + seg_arr[:, :, 2]
    segm = np.argmax(seg_arr, -1)
    img = copy.deepcopy(im_arr)
    if len(img.shape) == 2:
        img = np.repeat(img[..., np.newaxis], 3, len(img.shape))
    for k in range(seg_arr.shape[-1]):
        colour = np.array([0.0, 0.0, 0.0])
        colour[k % 2] = 1.0
        pos = np.copy(segm)
        pos[pos != k] = 0
        pos[pos > 0] = 1
        edge_hor = ndimage.sobel(pos, 0)
        edge_ver = ndimage.sobel(pos, 1)
        edge = np.hypot(edge_hor, edge_ver)
        img[edge > 0.0, :] = colour

    return img


# =============================================================================
def main():
    import glob
    import pickle
    from multiprocessing import Pool
    import argparse
    import time

    start = time.time()

    #  Parse parameters
    parser = argparse.ArgumentParser(description='Joint segmentation & registration')
    parser.add_argument('--wdir', help='working directory')
    parser.add_argument('--mod_in', required=True, help='shape model (leading part)')
    parser.add_argument('--mod_out', required=True, help='shape model (leading part)')
    parser.add_argument('--img_bname', required=True, help='image base name')
    parser.add_argument('--drange', type=int, nargs='+', required=True, help='differential range for angles')
    parser.add_argument('--srange', type=int, nargs='+', required=True, help='differential range for scale')
    parser.add_argument('--size', type=int, nargs='+', required=True, help='size of the model')
    parser.add_argument('--parms', help='appearance models & transformations')
    parser.add_argument('--cores', type=int, help='number of cores')
    parser.add_argument('--save', action='store_true', help='save images')

    args = parser.parse_args()
    # number of cores for parallel execution
    if args.cores is None:
        cores = 1
    else:
        cores = args.cores

    # change to working directory
    if args.wdir is not None:
        os.chdir(args.wdir)

    dcaprms = DCAEMParms()
    size = args.size

    #  load all images
    image_list = []
    imname_list = sorted(glob.glob(args.img_bname))
    for name in imname_list:
        img = CImage()
        img.load(name, "RGB", size)
        image_list.append(img)

    if len(image_list) == 0:
        raise Exception("No images loaded")

    # initialise and load P^n model
    pnprms = PnModelParms()
    imod = PnMlModel(size, pnprms.LABELS)
    if args.mod_in.rsplit('.', 1)[1] == "npy":
        imod.load_probs(args.mod_in)
    else:
        bname = args.mod_in.rsplit('.', 1)[0]
        suff = args.mod_in.rsplit('.', 1)[1]
        names = {}
        for l in range(1, imod.lnum):
            names[l] = bname + repr(l) + '.' + suff
        imod.load_probs_from_images(names)

    rad = int(imod.mps.RADIUS)
    for x in range(-rad, rad+1):
        for y in range(-rad, rad+1):
            if math.sqrt(x*x + y*y) <= rad:
                imod.edges.append((y, x))

    imod.estimate_upots_logsub()

    # initialise fields for posterior potentials and marginals
    pmarg_list = []

    for name in imname_list:
        pmarg = np.ones(imod.fsize, dtype=np.float64) / np.float64(imod.lnum)
        pmarg_list.append(pmarg)

    # initialise appearance models and transformations
    if args.parms is None:
        appear_list = []
        transf_list = []
        for i in range(len(image_list)):
            appear = init_appearance()
            appear_list.append(appear)
            transf_list.append(np.array([0, 0, 0, 0], dtype=np.int))
    else:
        # load initial appearance models and transformations from file
        with open(args.parms, 'rb') as infile:
            appear_list = pickle.load(infile)
            transf_list = pickle.load(infile)
        if (len(appear_list) != len(image_list)) or (len(transf_list) != len(image_list)):
            raise Exception("Length mismatch of loaded lists!")
        # rescale translations if necessary (downsampling!)
        for i in range(len(image_list)):
            size_factors = np.array(image_list[i].size_orig, dtype=np.float_) / np.array(size, dtype=np.float_)
            transf = transf_list[i].astype(np.float64)
            transf[0:2] /= size_factors
            transf_list[i] = transf.astype(int)

    # differential range for rotation angles and scale
    drange = args.drange
    srange = args.srange
    #  Run the DCA algorithm
    zyk = 0
    probs = np.zeros(imod.fsize, dtype=np.float64)

    print("Start DC-Algorithm")
    while True:
        zyk += 1
        #  A-STEP
        # run EM algortihm for all images
        probs.fill(0.0)
        avzyk = np.float_(0)

        # compute list of absolute search ranges (transformations)
        trange_li = []
        for i in range(len(image_list)):
            trange = np.copy(drange + srange)
            trange[0:2] += transf_list[i][2]
            trange[2:] += transf_list[i][3]
            trange_li.append(trange)

        # construct a pool of workers and run the em-algorithm for each image
        pool = Pool(processes=cores)
        result_out = [pool.apply_async(dca_for_image, args=(
            imod, image_list[i], pmarg_list[i], appear_list[i], transf_list[i], trange_li[i], i))
                      for i in range(len(image_list))]

        # collect the results
        results = [p.get() for p in result_out]
        results.sort()
        # strip the position number
        results = [r[1:] for r in results]

        for i in range(len(image_list)):
            avzyk += results[i][3]
            pmarg_list[i] = np.copy(results[i][0])
            appear_list[i] = copy.deepcopy(results[i][1])
            transf_list[i] = np.copy(results[i][2])
            #  transform and accumulate to new model (probs)
            probs += pmarg_list[i]

        # normalise
        probs /= np.float64(len(image_list))
        avzyk /= np.float64(len(image_list))

        imod_old = copy.deepcopy(imod)
        imod.set_mprob(probs)
        dist = imod.marg_dist(imod_old)

        #  B-step
        imod.estimate_upots_logsub()

        # save intermediate model
        mod_name = "modelz_" + str(zyk).zfill(2) + ".png"
        imod.save_probs_as_image(mod_name)
        mod_name = "modelz_" + str(zyk).zfill(2) + ".npy"
        imod.save_mprobs(mod_name)

        print("DCA-Zyk: " + repr(zyk) + " , MargDist: " + repr(dist) + " , AvEmZyk: " + repr(avzyk))
        if (zyk >= dcaprms.DCA_MaxZyk) or (dist < dcaprms.DCA_MinDist):
            break

    # save final model
    imod.save_probs_as_image(args.mod_out)
    name = args.mod_out.rsplit('.', 1)[0]
    imod.save_mprobs(name)
    # save segmentations & transformed images

    if args.save:
        print("Save segmentations")
        for i in range(len(image_list)):
            rname = imname_list[i].rsplit('.', 1)[0]
            # save posterior marginal prob's in reference frame
            beta = pmarg_list[i]
            img = copy.deepcopy(image_list[i])
            img.set_values(beta)
            img.save(rname + "_seg_rf.png", "RGB", True)

            # save posterior marginals prob's in image frame
            pvals = np.zeros(imod.lnum, dtype=np.float64)
            pvals[0] = 1.0
            beta = pmarg_list[i]
            beta_tr = rbs_trans(beta, transf_list[i], image_list[i].size, pvals)
            img = copy.deepcopy(image_list[i])
            img.set_values(beta_tr)
            img.save(rname + "_seg_if.png", "RGB", True)

            # transform and save images in reference frame
            if len(image_list[i].arr.shape) == 2:
                pval = np.float64(1.0)
            else:
                pval = np.ones(image_list[i].arr.shape[2], dtype=np.float64)
            tarr = rbs_inv_trans(image_list[i].arr, transf_list[i], image_list[i].size, pval)
            tarr_overl = add_segm_overlay(tarr, beta)
            image_list[i].set_values(tarr_overl)
            image_list[i].save(rname + "_tra.png", "RGB", True)

    # scale back translations
    if args.size is not None:
        for i in range(len(image_list)):
            size_factors = np.array(image_list[i].size_orig, dtype=np.float_) / np.array(size, dtype=np.float_)
            transf = transf_list[i].astype(np.float64)
            transf[0:2] *= size_factors
            transf_list[i] = transf.astype(int)

    # save estimated theta parameters and transformations
    with open("theta_parm_out.txt", 'wb') as output:
        pickle.dump(appear_list, output)
        pickle.dump(transf_list, output)

    end = time.time()
    print("Done in " + repr((end - start) / 60) + " minutes.")

    return imod.mprob, transf_list, appear_list


# =============================================================================
if __name__ == "__main__":
    main()
