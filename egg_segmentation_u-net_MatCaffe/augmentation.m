im = imread('cameraman.tif');
im = im(1:128, :);
sz = size(im); sz = sz(1:2);
yNodes = linspace(1, size(im,1), 4);
xNodes = linspace(1, size(im,2), 4);
[X,Y] = meshgrid(xNodes, yNodes);
[Xq,Yq] = meshgrid(1:size(im, 2), 1:size(im, 1));
DX = 10 * randn(4);
DY = 10 * randn(4);

DXq = interp2(X, Y, DX, Xq, Yq, 'cubic', 0);
DYq = interp2(X, Y, DY, Xq, Yq, 'cubic', 0);

% (DXq,DYq) gives the translation at each point.

Xf = Xq+DXq; Yf = Yq+DYq;
imW = interp2(Xq, Yq, double(im)/256,  Xf, Yf, 'linear', 0);
imshow(imW)