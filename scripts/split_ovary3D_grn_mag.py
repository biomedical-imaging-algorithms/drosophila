"""
@author: Jiri Borovec

taking the input csv file we load all 3D stack and export the two channels

"""

import os
import logging

import pandas as pd
import crop_ovary3d_train_images_template_matching as my_img_crop

logger = logging.getLogger(__name__)

PATH_DATA = '/datagrid/Medical/microscopy/drosophila'
PATH_SEGM = os.path.join(PATH_DATA, 'egg_segmentation')
PATH_IMG_SPLIT = os.path.join(PATH_SEGM, 'ovary_3d_split')
PATH_IMG_RAW = os.path.join(PATH_DATA, 'ovary_raw_stacks')
PATH_DESC_NEW = os.path.join(PATH_SEGM,
                             'ovary_image_info_for_prague.csv')
CSV_COLUMN = 'stack_path'


def split_images(data_path=PATH_IMG_RAW, out_path=PATH_IMG_SPLIT, path_csv=PATH_DESC_NEW):
    assert os.path.exists(path_csv)
    df = pd.DataFrame.from_csv(path_csv, sep='\t')
    assert os.path.exists(os.path.dirname(out_path))
    if not os.path.exists(out_path):
        os.mkdir(out_path)
    for i, img_name in enumerate(df['stack_path']):
        img_path = os.path.join(data_path, img_name)
        logger.info('processing %i / %i in %s', i + 1, len(df), img_path)
        assert os.path.exists(img_path), '%s' % img_path
        img_mag, img_grn = my_img_crop.load_img_volume_double_band_split(img_path)
        img_path_out = os.path.join(out_path, os.path.splitext(img_name)[0])
        my_img_crop.save_images(img_path_out, img_mag, img_grn)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    split_images()

    logger.info('DONE')
