"""
@author: Jiri Borovec

assuming we have 2D images with their annotation and we want to have them in 3D
We walk over all input 2D images and find related 3D stack and then using
template matching detect the 2D image in large 2D plane  (we assume that
the original manual selection was made from the middle plane of the 3D stack)

>> python run_crop_ovary3d_train_images_template_matching.py \
    -in /datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_2d/stage1 \
    -out /datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_3d_cut/stage1

>> python run_crop_ovary3d_train_images_template_matching.py \
    -in /datagrid/Medical/microscopy/drosophila/ovary_selected_images \
    -out /datagrid/Medical/microscopy/drosophila/ovary_selected_images_3d_cut

"""

import os
import sys
import glob
import re
import gc
import time
import logging
import argparse
import traceback
import multiprocessing as mproc
from functools import partial

import numpy as np
import pandas as pd
import libtiff
import tqdm
from skimage.feature import match_template
import matplotlib.pyplot as plt

sys.path.append(os.path.abspath(os.path.join('..','..')))
import src.own_utils.tool_data_io as tl_data


NB_THREADS = int(mproc.cpu_count() * .7)
PATH_DATA = '/datagrid/Medical/microscopy/drosophila'
PATH_RAW = os.path.join(PATH_DATA, 'ovary_raw_stacks')
PATH_DESC = os.path.join(PATH_DATA, 'all_ovary_image_info_for_prague.txt')
NAME_DESC_NEW = 'ovary_image_info_for_prague.csv'
IMG_NAME_TEMPLATE = '*.tif'
IMG_POSIX = '.tif'
POSIX_MAG = '_mag'
POSIX_GRN = '_grn'


def create_args_parser():
    """ create simple arg parser with default values (input, output, dataset)

    :return: object argparse<in, out, ant, name>
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-in', '--path_in', type=str, required=True,
                        help='path, folder with image templates')
    parser.add_argument('-out', '--path_out', type=str, required=True,
                        help='path, results of 3D cuts')
    parser.add_argument('-imgs', '--path_imgs', type=str, required=False,
                        help='path, folder with original 3D images',
                        default=PATH_RAW)
    parser.add_argument('-csv', '--path_csv', type=str, required=False,
                        help='path, csv/txt with decriptions', default=PATH_DESC)
    parser.add_argument('--img_pattern', type=str, required=False, default='*.png',
                        help='template image pattern name')
    parser.add_argument('--nb_jobs', type=int, required=False, default=NB_THREADS,
                        help='number of processes running in parallel')
    return parser


def parse_arg_params(parser):
    """ parse basic args and return as dictionary

    :param parser: argparse
    :return: {str: ...}
    """
    args = vars(parser.parse_args())
    for n in (k for k in args if 'path' in k and args[k] is not None):
        args[n] = os.path.abspath(os.path.expanduser(args[n]))
        assert os.path.exists(args[n]), '%s' % args[n]
    return args


def get_list_segmented_images(path_segm, im_temp=IMG_NAME_TEMPLATE):
    list_imgs = glob.glob(os.path.join(path_segm, im_temp))
    logging.debug('loaded images: %s', repr([os.path.basename(n)
                                            for n in list_imgs]))
    return list_imgs


def parse_image_indexes(list_img):
    idxs = []
    for n in list_img:
        search_obj = re.search('(\d+)', os.path.basename(n))
        if not search_obj:
            logging.warning('image "%s" does not contain an ID', n)
            continue
        id = int(search_obj.groups()[0])
        idxs.append(id)
    logging.debug('loaded indexes: %s', repr(idxs))
    return idxs


def load_image_details(img_templates, path_info):
    assert os.path.exists(path_info), \
        'description file "%s" doe not exist' % path_info
    ids = parse_image_indexes(img_templates)
    df_all = pd.DataFrame().from_csv(path_info, sep='\t')
    logging.debug('extracted columns: %s', repr(df_all.columns.values.tolist()))
    df_cut = df_all[df_all['image_id'].isin(ids)]

    df_temp = pd.DataFrame(zip(ids, (os.path.basename(p) for p in img_templates)))
    df_temp.columns = ['image_id', 'image_templates']
    df_temp.set_index('image_id', inplace=True)

    df_cut = pd.merge(df_cut, df_temp,
                      left_on='image_id', right_index=True, how='inner')
    logging.debug(df_cut.head())
    return df_cut


def save_images(path_out, img_mag, img_grn):
    logging.info('saving images %s in path: %s', repr(img_mag.shape), path_out)
    logging.debug('image type: %s of %s in range %d -> %d', type(img_mag),
                 type(img_mag[0, 0, 0]), np.min(img_mag), np.max(img_mag))
    # to write a image to tiff file
    tif = libtiff.TIFF.open(path_out + POSIX_MAG + IMG_POSIX, mode='w')
    tif.write_image(img_mag.astype(np.uint16))
    tif.close()
    # to write a image to tiff file
    tif = libtiff.TIFF.open(path_out + POSIX_GRN + IMG_POSIX, mode='w')
    tif.write_image(img_grn.astype(np.uint16))
    tif.close()


def get_bounding_box_desc(row):
    logging.info('crop by information in the description file')
    # ant_x  ant_y  post_x  post_y  lat_x  lat_y
    # vecX = [row['ant_x'], row['post_x'], row['lat_x']]
    vec_x = [row['ant_x'], row['lat_x']]
    begin_x, end_x = min(vec_x), max(vec_x)
    # vecY = [row['ant_y'], row['post_y'], row['lat_y']]
    vec_y = [row['ant_y'], row['lat_y']]
    begin_y, end_y = min(vec_y), max(vec_y)
    return begin_x, end_x, begin_y, end_y


def crop_image_bounding_box(img, begin_x, end_x, begin_y, end_y):
    logging.debug('cutting X: {} -> {}, Y: {} -> {}'.format(begin_x, end_x,
                                                           begin_y, end_y))
    img = img[:, begin_y:end_y, begin_x:end_x]
    return img


def find_position_image_template(img, im_template, path_fig=None):
    # http://scikit-image.org/docs/dev/auto_examples/plot_template.html?highlight=template
    logging.debug('crop by template image of size %s', repr(im_template.shape))
    result = match_template(img, im_template)
    pos = np.unravel_index(np.argmax(result), result.shape)
    logging.debug('found position of given template is %s', repr(pos))
    begin_x, end_x = pos[1], pos[1] + im_template.shape[1] - 1
    begin_y, end_y = pos[0], pos[0] + im_template.shape[0] - 1
    if path_fig is not None:
        fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(24, 16))
        ax[0, 0].set_title('whole image')
        ax[0, 0].imshow(img)  # , ax[0, 0].axis('off')
        ax[0, 1].set_title('template match')
        ax[0, 1].imshow(result)
        ax[1, 0].set_title('template image')
        ax[1, 0].imshow(im_template)
        ax[1, 1].set_title('cute image')
        ax[1, 1].imshow(img[begin_y:end_y, begin_x:end_x])
        fig.savefig(path_fig, bbox_inches='tight')
        plt.close(fig)
    return begin_x, end_x, begin_y, end_y


def cut_image_from_stack(row, path_img, path_out, path_template=None):
    assert os.path.exists(path_img)
    img_mag, img_grn = tl_data.load_img_volume_double_band_split(path_img)
    logging.info('loaded image "%s" with %s', os.path.basename(path_img),
                 repr(img_mag.shape))
    logging.debug('image type: %s of %s in range %d -> %d', type(img_mag),
                  type(img_mag[0, 0, 0]), np.min(img_mag), np.max(img_mag))
    if path_template is not None and os.path.exists(path_template):
        tif = libtiff.TIFF.open(path_template, mode='r')
        if row['slice_index'] >= img_mag.shape[0]:
            row['slice_index'] = img_mag.shape[0] / 2
        img = img_mag[row['slice_index'], :, :]
        im_template = tif.read_image()[:, :, 0]
        try:
            bounding_box = find_position_image_template(img, im_template,
                                                path_out + '_template_match.png')
            # crop by information in the description file
            img_mag = crop_image_bounding_box(img_mag, *bounding_box)
            img_grn = crop_image_bounding_box(img_grn, *bounding_box)
            save_images(path_out, img_mag, img_grn)
        except:
            logging.error(traceback.format_exc())
            bounding_box = [-1] * 4
    else:
        logging.warning('template image "%s" is not defined or does not exist',
                        path_template)
        bounding_box = [-1] * 4
    return bounding_box


def cut_image_by_template(idx_row, path_raw, path_templates, path_out):
    idx, row = idx_row
    path_img = os.path.join(path_raw, row['stack_path'])
    logging.debug('#%i image (%i) <- "%s"', idx,
                 os.path.exists(path_img), path_img)
    if os.path.splitext(row['stack_path'])[1] != IMG_POSIX:
        logging.warning('not valid image format "%s"', row['stack_path'])
        begin_x, end_x, begin_y, end_y = [-1] * 4
    else:
        path_template = os.path.join(path_templates, row['image_templates'])
        path_img_cut = os.path.join(path_out, os.path.splitext(row['image_path'])[0])
        begin_x, end_x, begin_y, end_y = cut_image_from_stack(row, path_img,
                                                  path_img_cut, path_template)
        gc.collect(), time.sleep(1)
    row_new = dict(row)
    row_new.update(dict(zip(['bb_begin_x', 'bb_end_x', 'bb_begin_y', 'bb_end_y'],
                            [begin_x, end_x, begin_y, end_y])))
    logging.debug(row_new)
    return idx, row_new


def mproc_wrapper(mp_tuple):
    return cut_image_by_template(*mp_tuple)


def main_cut_all_images(path_info, path_img_raw, path_img_templates,
                        path_out, img_names, nb_jobs=NB_THREADS):
    logging.info('for all 2D templates and cut 3D image from original 3D volume')
    list_imgs = get_list_segmented_images(path_img_templates)
    df_details = load_image_details(list_imgs, path_info)
    path_info_new = os.path.join(path_out, NAME_DESC_NEW)

    wrapper_cut_image_by_template = partial(cut_image_by_template,
                                            path_templates=path_img_templates,
                                            path_raw=path_img_raw,
                                            path_out=path_out)
    mproc_pool = mproc.Pool(nb_jobs)
    df_new = pd.DataFrame()

    tqdm_bar = tqdm.tqdm(total=len(df_details))
    if nb_jobs > 1:
        for idx, row_new in mproc_pool.imap_unordered(wrapper_cut_image_by_template,
                                           df_details.iterrows()):
            df_new = df_new.append(pd.DataFrame(row_new, index=[idx]))
            df_new.to_csv(path_info_new, sep='\t')
            tqdm_bar.update(1)
    else:
        for idx_row in df_details.iterrows():
            idx, row_new = wrapper_cut_image_by_template(idx_row)
            df_new = df_new.append(pd.DataFrame(row_new, index=[idx]))
            df_new.to_csv(path_info_new, sep='\t')
            tqdm_bar.update(1)
    mproc_pool.close()
    mproc_pool.join()

    df_new.to_csv(path_info_new, sep='\t')
    logging.debug(df_new)


def main():
    logging.basicConfig(level=logging.INFO)
    logging.info('running...')

    parser = create_args_parser()
    params = parse_arg_params(parser)
    main_cut_all_images(params['path_csv'], params['path_imgs'],
                        params['path_in'], params['path_out'],
                        params['img_pattern'], params['nb_jobs'])

    logging.info('DONE')


if __name__ == "__main__":
    main()