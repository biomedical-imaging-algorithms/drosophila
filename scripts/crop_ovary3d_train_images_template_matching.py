"""
@author: Jiri Borovec

assuming we have 2D images with their annotation and we want to have them in 3D
We walk over all input 2D images and find related 3D stack and then using
template matching detect the 2D image in large 2D plane  (we assume that
the original manual selection was made from the middle plane of the 3D stack)

"""

import os, sys
import glob, re
import logging

import numpy as np
import pandas as pd
import libtiff
from skimage.feature import match_template
import matplotlib.pyplot as plt

logger = logging.getLogger(__name__)

PATH_DATA = '/datagrid/Medical/microscopy/drosophila'
PATH_SEGM = os.path.join(PATH_DATA, 'egg_segmentation')
PATH_IMG_2D = os.path.join(PATH_SEGM, 'ovary_2d')
PATH_IMG_CUT = os.path.join(PATH_SEGM, 'ovary_3d_cut')
PATH_RAW = os.path.join(PATH_DATA, 'ovary_raw_stacks')
PATH_DESC = os.path.join(PATH_DATA, 'all_ovary_image_info_for_prague.txt')
PATH_DESC_NEW = os.path.join(PATH_SEGM,
                             'ovary_image_info_for_prague.csv')
IMG_POSIX = '.tif'
POSIX_MAG = '_mag'
POSIX_GRN = '_grn'


def load_image_tiff_3d(path_img):
    if not os.path.exists(path_img):
        logger.error('given image "{}" does not exist!'.format(path_img))
        return None
    tif = libtiff.TIFF.open(path_img, mode='r')
    img = []
    # to read all images in a TIFF file:
    for im in tif.iter_images():
        img.append(im)
    tif.close()
    img = np.array(img)
    # img = img / np.max(img) * 255
    logger.debug('image values {} -> {}'.format(np.min(img), np.max(img)))
    return img


def load_img_volume_double_band_split(path_img):
    img = load_image_tiff_3d(path_img)
    img_b1 = np.array(img[0::2])
    img_b2 = np.array(img[1::2])
    return img_b2, img_b1


def get_list_segmented_images(path_segm=PATH_SEGM, im_posix=IMG_POSIX):
    list_imgs = glob.glob(os.path.join(path_segm, '*', '*' + im_posix))
    logger.debug('loaded images: %s', repr([os.path.basename(n)
                                            for n in list_imgs]))
    return list_imgs


def parse_image_indexes(list_img):
    idxs = []
    for n in list_img:
        search_obj = re.search('(\d+)', os.path.basename(n))
        if not search_obj:
            logger.warning('image "%s" does not contain an ID', n)
            continue
        id = int(search_obj.groups()[0])
        idxs.append(id)
    logger.debug('loaded indexes: %s', repr(idxs))
    return idxs


def load_image_details(ids, path_info=PATH_DESC):
    assert os.path.exists(path_info), \
        'description file "%s" doe not exist' % path_info
    df = pd.DataFrame().from_csv(path_info, sep='\t')
    logger.debug('extracted columns: %s', repr(df.columns.values.tolist()))
    df_cut = df[df['image_id'].isin(ids)]
    logger.debug(df_cut.head())
    return df_cut


def save_images(path_out, img_mag, img_grn):
    logger.info('saving images %s in path: %s', repr(img_mag.shape), path_out)
    logger.debug('image type: %s of %s in range %d -> %d', type(img_mag),
                 type(img_mag[0, 0, 0]), np.min(img_mag), np.max(img_mag))
    # to write a image to tiff file
    tif = libtiff.TIFF.open(path_out + POSIX_MAG + IMG_POSIX, mode='w')
    tif.write_image(img_mag.astype(np.uint16))
    tif.close()
    # to write a image to tiff file
    tif = libtiff.TIFF.open(path_out + POSIX_GRN + IMG_POSIX, mode='w')
    tif.write_image(img_grn.astype(np.uint16))
    tif.close()


def get_bounding_box_desc(row):
    logger.info('crop by information in the description file')
    # ant_x  ant_y  post_x  post_y  lat_x  lat_y
    # vecX = [row['ant_x'], row['post_x'], row['lat_x']]
    vec_x = [row['ant_x'], row['lat_x']]
    begin_x, end_x = min(vec_x), max(vec_x)
    # vecY = [row['ant_y'], row['post_y'], row['lat_y']]
    vec_y = [row['ant_y'], row['lat_y']]
    begin_y, end_y = min(vec_y), max(vec_y)
    return begin_x, end_x, begin_y, end_y


def crop_image_bounding_box(img, begin_x, end_x, begin_y, end_y):
    logger.debug('cutting X: {} -> {}, Y: {} -> {}'.format(begin_x, end_x,
                                                           begin_y, end_y))
    img = img[:, begin_y:end_y, begin_x:end_x]
    return img


def find_position_image_template(img, im_template, path_fig=None):
    # http://scikit-image.org/docs/dev/auto_examples/plot_template.html?highlight=template
    logger.debug('crop by template image of size %s', repr(im_template.shape))
    result = match_template(img, im_template)
    pos = np.unravel_index(np.argmax(result), result.shape)
    logger.info('found position of given template is %s', repr(pos))
    begin_x, end_x = pos[1], pos[1] + im_template.shape[1] - 1
    begin_y, end_y = pos[0], pos[0] + im_template.shape[0] - 1
    if path_fig is not None:
        fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(24, 16))
        ax[0, 0].set_title('whole image')
        ax[0, 0].imshow(img)  # , ax[0, 0].axis('off')
        ax[0, 1].set_title('template match')
        ax[0, 1].imshow(result)
        ax[1, 0].set_title('template image')
        ax[1, 0].imshow(im_template)
        ax[1, 1].set_title('cute image')
        ax[1, 1].imshow(img[begin_y:end_y, begin_x:end_x])
        fig.savefig(path_fig, bbox_inches='tight')
    return begin_x, end_x, begin_y, end_y


def cut_image_from_stack(row, path_img, path_out, path_template=None):
    assert os.path.exists(path_img)
    img_mag, img_grn = load_img_volume_double_band_split(path_img)
    logger.info('loaded image "%s" with %s', os.path.basename(path_img), repr(img_mag.shape))
    logger.debug('image type: %s of %s in range %d -> %d', type(img_mag),
                 type(img_mag[0, 0, 0]), np.min(img_mag), np.max(img_mag))
    if path_template is not None and os.path.exists(path_template):
        tif = libtiff.TIFF.open(path_template, mode='r')
        img = img_mag[row['slice_index'], :, :]
        im_template = tif.read_image()[:, :, 0]
        bounding_box = find_position_image_template(img, im_template,
                                            path_out + '_template_match.png')
    else:
        logger.warning('template image "%s" is not defined or does not exist', path_template)
        bounding_box = get_bounding_box_desc(row)
    # crop by information in the description file
    img_mag = crop_image_bounding_box(img_mag, *bounding_box)
    img_grn = crop_image_bounding_box(img_grn, *bounding_box)
    save_images(path_out, img_mag, img_grn)
    return bounding_box


def cut_images_by_template(path_info=PATH_DESC, path_dir_templates=PATH_IMG_2D,
                           path_out=PATH_IMG_CUT, path_raw=PATH_RAW):
    logger.info('for all 2D templates and cut 3D image from original 3D volume')
    assert os.path.exists(path_info) and os.path.exists(path_dir_templates)
    assert os.path.exists(path_out)
    list_imgs = get_list_segmented_images(path_dir_templates)
    ids = parse_image_indexes(list_imgs)
    df_details = load_image_details(ids, path_info)
    df_details_new = pd.DataFrame()

    for i, (idx, row) in enumerate(df_details.iterrows()):
        logger.info('loading %i / %i', i + 1, len(df_details))
        path_img = os.path.join(path_raw, row['stack_path'])
        logger.debug('#%i image (%i) <- "%s"', idx,
                     os.path.exists(path_img), path_img)

        path_template = os.path.join(path_dir_templates,
                                     'stage{}'.format(row['stage']),
                                     'train{}.tif'.format(row['image_id']))
        path_out_dir = os.path.join(path_out, 'stage{}'.format(row['stage']))
        if not os.path.exists(path_out_dir):
            os.makedirs(path_out_dir)
        path_img_cut = os.path.join(path_out_dir, 'train{}'.format(row['image_id']))
        begin_x, end_x, begin_y, end_y = cut_image_from_stack(row, path_img,
                                                  path_img_cut, path_template)

        row_new = dict(row)
        row_new.update({'bb_begin_x': begin_x, 'bb_begin_y': begin_y,
                        'bb_end_x': end_x, 'bb_end_y': end_y})
        logger.debug(row_new)
        df_details_new = df_details_new.append(pd.DataFrame(row_new, index=[idx]))
        df_details_new.to_csv(PATH_DESC_NEW, sep='\t')

    logger.debug(df_details_new)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    cut_images_by_template()

    logger.info('DONE')
