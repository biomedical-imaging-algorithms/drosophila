'''
@author Siddhant Ranade

Contains several functions to help in training the network -- including augmentation, creating weightmaps, reading images, etc.
'''

import cv2
import numpy as np
from scipy.interpolate import griddata
from scipy.interpolate import interpn
from scipy.interpolate import interp2d
from scipy.interpolate import RegularGridInterpolator
import scipy.ndimage as ndimage
import math
import os
import libtiff
from PIL import Image
import matplotlib.pyplot as plt

def augmentPIL(img, mask, wtmap, context=184, perturb_var = 10, theta = None, DX = None, DY = None):
	img_tiled = np.lib.pad(img, ((context/2, context/2), (context/2, context/2)), 'symmetric')
	mask_tiled = np.lib.pad(mask, ((context/2, context/2), (context/2, context/2)), 'constant', constant_values=2)
	wtmap_tiled = np.lib.pad(wtmap, ((context/2, context/2), (context/2, context/2)), 'constant', constant_values=0)
	
	if theta == None:
		theta = 2 * np.pi * np.random.rand()
	if (DX == None) | (DY == None):
		DX = perturb_var * np.random.randn(4,4);
		DY = perturb_var * np.random.randn(4,4);

	size_full_x, size_full_y = img_tiled.shape[1], img_tiled.shape[0]

	# print size_full_x, size_full_y

	nodes_x = np.linspace(0, size_full_x-1, 4).astype(int)
	nodes_y = np.linspace(0, size_full_y-1, 4).astype(int)

	X,Y = np.meshgrid(nodes_x, nodes_y);
	Xf, Yf = X + DX, Y + DY

	# print X,Y

	# print Xf, Yf

	data = [((X[0,0], Y[0,0], X[1,1], Y[1,1]), (Xf[0,0],Yf[0,0], Xf[1,0],Yf[1,0], Xf[1,1],Yf[1,1], Xf[0,1],Yf[0,1])),
		((X[1,0], Y[1,0], X[2,1], Y[2,1]), (Xf[1,0],Yf[1,0], Xf[2,0],Yf[2,0], Xf[2,1],Yf[2,1], Xf[1,1],Yf[1,1])),
		((X[2,0], Y[2,0], X[3,1], Y[3,1]), (Xf[2,0],Yf[2,0], Xf[3,0],Yf[3,0], Xf[3,1],Yf[3,1], Xf[2,1],Yf[2,1])),
		((X[0,1], Y[0,1], X[1,2], Y[1,2]), (Xf[0,1],Yf[0,1], Xf[1,1],Yf[1,1], Xf[1,2],Yf[1,2], Xf[0,2],Yf[0,2])),
		((X[1,1], Y[1,1], X[2,2], Y[2,2]), (Xf[1,1],Yf[1,1], Xf[2,1],Yf[2,1], Xf[2,2],Yf[2,2], Xf[1,2],Yf[1,2])),
		((X[2,1], Y[2,1], X[3,2], Y[3,2]), (Xf[2,1],Yf[2,1], Xf[3,1],Yf[3,1], Xf[3,2],Yf[3,2], Xf[2,2],Yf[2,2])),
		((X[0,2], Y[0,2], X[1,3], Y[1,3]), (Xf[0,2],Yf[0,2], Xf[1,2],Yf[1,2], Xf[1,3],Yf[1,3], Xf[0,3],Yf[0,3])),
		((X[1,2], Y[1,2], X[2,3], Y[2,3]), (Xf[1,2],Yf[1,2], Xf[2,2],Yf[2,2], Xf[2,3],Yf[2,3], Xf[1,3],Yf[1,3])),
		((X[2,2], Y[2,2], X[3,3], Y[3,3]), (Xf[2,2],Yf[2,2], Xf[3,2],Yf[3,2], Xf[3,3],Yf[3,3], Xf[2,3],Yf[2,3]))]

	# for a,b in data:
	# 	print a,b

	i = Image.fromarray(img_tiled)
	m = Image.fromarray(mask_tiled-2)
	w = Image.fromarray(wtmap_tiled)
	i2 = i.transform(i.size, Image.MESH, data, Image.BICUBIC, fill=0).rotate(int(theta*180.0/np.pi), Image.BICUBIC, expand=True)
	m2 = m.transform(m.size, Image.MESH, data, Image.BICUBIC, fill=2).rotate(int(theta*180.0/np.pi), Image.BICUBIC, expand=True)
	w2 = w.transform(w.size, Image.MESH, data, Image.BICUBIC, fill=0).rotate(int(theta*180.0/np.pi), Image.BICUBIC, expand=True)
	img_aug = np.array(i2)
	mask_aug = np.array(m2)+2
	wtmap_aug = np.array(w2)

	return (img_aug, mask_aug, wtmap_aug)


def augment3D(img, mask, wtmap, perturb_var = 10, theta = None):
	if theta == None:
		theta = 2*math.pi*np.random.rand()

	size_full_x, size_full_y, size_full_z = img.shape[2],img.shape[1],img.shape[0]
	
	nodes_x = np.linspace(0, size_full_x-1, 4)
	nodes_y = np.linspace(0, size_full_y-1, 4)
	nodes_z = np.linspace(0, size_full_z-1, 4)

	X_small,Y_small = np.meshgrid(nodes_x, nodes_y);
	X_stack,Y_stack,Z_stack = np.meshgrid(nodes_x, nodes_y, np.arange(size_full_z));

	DX_small = perturb_var * np.random.randn(4,4,4);
	DY_small = perturb_var * np.random.randn(4,4,4);

	print "Calculating positions"
	
	DX_stack = interpn((nodes_y,nodes_x,nodes_z), DX_small, (Y_stack,X_stack,Z_stack), method="linear", bounds_error=False, fill_value=0)
	print "Calculating positions 1/2"
	DY_stack = interpn((nodes_y,nodes_x,nodes_z), DY_small, (Y_stack,X_stack,Z_stack), method="linear", bounds_error=False, fill_value=0)
	print "Calculating positions 2/2"

	print DX_stack.shape, DY_stack.shape
	
	img_aug=[]
	mask_aug=[]
	wtmap_aug=[]
	for i in xrange(size_full_z):
		x,y,z = augmentPIL(img[i,:,:].astype(np.float64), mask[i,:,:].astype(np.float64), wtmap[i,:,:].astype(np.float64), context=0, theta=theta, DX=DX_stack[:,:,i], DY=DY_stack[:,:,i])
		img_aug.append(x)
		mask_aug.append(y)
		wtmap_aug.append(z)

	return (np.array(img_aug), np.array(mask_aug), np.array(wtmap_aug))


def rotate2(x, y, theta, ox, oy):
	"""Rotate arrays of coordinates x and y by theta radians about the
	point (ox, oy).

	"""
	s, c = np.sin(theta), np.cos(theta)
	x, y = x - ox, y - oy
	return x * c - y * s + ox, x * s + y * c + oy

def constructWeightMap(truth):
	d1_f = ndimage.distance_transform_edt(truth)
	d1_b = ndimage.distance_transform_edt(1-truth)
	d1 = np.maximum(d1_f, d1_b)

	truth_copy = truth.copy()
	truth_copy[truth==2] = 0

	labeled_array, num_features = ndimage.measurements.label(truth_copy, structure=None, output=None)

	tmp = np.empty_like(labeled_array)
	tmp[:] = labeled_array

	d2 = np.empty(labeled_array.shape)
	w_c = np.empty(labeled_array.shape)
	d2[:] = np.inf; w_c[:] = 0

	for ii in xrange(1,num_features+1):
		tmp[:] = labeled_array
		tmp[np.logical_or(tmp == ii, tmp == 0)] = -1	#fg
		tmp[tmp != -1] = 0
		tmp[tmp == -1] = 1
		#everything except current class and bg goes to bg, current class and bg goes to fg
		d2_ii = ndimage.distance_transform_edt(tmp)
		d2[labeled_array == ii] = d2_ii[labeled_array == ii]

	tmp[:] = 1
	d2b1 = np.empty(labeled_array.shape)
	d2b2 = np.empty(labeled_array.shape)
	d2b1[:] = np.inf; d2b2[:] = np.inf;
	for ii in xrange(1,num_features+1):
		tmp[labeled_array == ii] = 0
		db_ii = ndimage.distance_transform_edt(tmp)
		d2b1[labeled_array == 0], d2b2[labeled_array == 0] = np.minimum( db_ii[labeled_array == 0], d2b1[labeled_array == 0] ), \
		np.minimum(np.maximum( db_ii[labeled_array == 0], d2b1[labeled_array == 0] ), d2b2[labeled_array == 0] )
		tmp[labeled_array == ii] = 1

	w0, sigma = 20, 5
	d2[labeled_array == 0] = d2b2[labeled_array == 0]

	frac0 = np.sum(truth == 0)/float(truth.size)
	frac1 = np.sum(truth == 1)/float(truth.size)
	w_c[truth == 0] = 0.5/(frac0)
	# w_c[truth == 0] = 3/(2*frac+1)
	w_c[truth == 1] = 0.5/(frac1)
	# w_c[truth == 1] = 1/(2*frac+1)

	wtmap = w_c + w0 * np.exp(-np.square(d1+d2)/(2*sigma**2))
	wtmap[truth==2] = 0
	return wtmap

def constructWeightMap3D(truth3D):
	wtmap = np.zeros(truth3D.shape)
	for l in range(truth3D.shape[0]):
		wtmap[l,:,:] = constructWeightMap(truth3D[l,:,:])
	return wtmap

def cutWithContext(img_full, cB, context, padding = 'symmetric', constant_values = 0):
	# Extra 'context' pixels on all sides.
	size_mask_x, size_mask_y = cB.x_2 - cB.x_1, cB.y_2 - cB.y_1	#MUST BE int
	size_full_x, size_full_y = size_mask_x+2*context, size_mask_y+2*context

	origin_mask_x, origin_mask_y = (size_full_x - size_mask_x)/2, (size_full_y - size_mask_y)/2
	origin_im_x, origin_im_y = origin_mask_x - cB.x_1, origin_mask_y - cB.y_1

	img_tiled = img_full

	if origin_im_x < 0:
		img_tiled = img_tiled[:, -origin_im_x:]
		origin_im_x = 0
	if origin_im_y < 0:
		img_tiled = img_tiled[-origin_im_y:, :]
		origin_im_y = 0
	if origin_im_x + img_tiled.shape[1] > size_full_x:
		img_tiled = img_tiled[:, :size_full_x - origin_im_x]
	if origin_im_y + img_tiled.shape[0] > size_full_y:
		img_tiled = img_tiled[:size_full_y - origin_im_y, :]

	if padding == 'constant':
		img_tiled = np.lib.pad(img_tiled, ((origin_im_y, size_full_y - img_tiled.shape[0] - origin_im_y), 
			(origin_im_x, size_full_x - img_tiled.shape[1] - origin_im_x)), padding, constant_values = constant_values)
	else:
		img_tiled = np.lib.pad(img_tiled, ((origin_im_y, size_full_y - img_tiled.shape[0] - origin_im_y), 
			(origin_im_x, size_full_x - img_tiled.shape[1] - origin_im_x)), padding)
	return img_tiled

def cutWithContext3D(img_full, cB, context, padding = 'symmetric', constant_values = 0):
	# Extra 'context' pixels on all sides.
	size_mask_x, size_mask_y = cB.x_2 - cB.x_1, cB.y_2 - cB.y_1	#MUST BE int
	size_full_x, size_full_y = size_mask_x+2*context, size_mask_y+2*context

	origin_mask_x, origin_mask_y = (size_full_x - size_mask_x)/2, (size_full_y - size_mask_y)/2
	origin_im_x, origin_im_y = origin_mask_x - cB.x_1, origin_mask_y - cB.y_1

	img_tiled = img_full.copy()

	if origin_im_x < 0:
		img_tiled = img_tiled[:,:, -origin_im_x:]
		origin_im_x = 0
	if origin_im_y < 0:
		img_tiled = img_tiled[:,-origin_im_y:, :]
		origin_im_y = 0
	if origin_im_x + img_tiled.shape[2] > size_full_x:
		img_tiled = img_tiled[:,:, :size_full_x - origin_im_x]
	if origin_im_y + img_tiled.shape[1] > size_full_y:
		img_tiled = img_tiled[:,:size_full_y - origin_im_y, :]

	if padding == 'constant':
		img_tiled = np.lib.pad(img_tiled, ((0,0), (origin_im_y, size_full_y - img_tiled.shape[1] - origin_im_y), 
			(origin_im_x, size_full_x - img_tiled.shape[2] - origin_im_x)), padding, constant_values = constant_values)
	else:
		img_tiled = np.lib.pad(img_tiled, ((0,0), (origin_im_y, size_full_y - img_tiled.shape[1] - origin_im_y), 
			(origin_im_x, size_full_x - img_tiled.shape[2] - origin_im_x)), padding)
	return img_tiled

def readImage(filename, normalize=False):
	root, ext = os.path.splitext(filename)
	if ext.lower()==".tif" or ext.lower()==".tiff":
		tif = libtiff.TIFF.open(filename, mode='r')
		im = tif.read_image().astype(np.float64)
	else:
		im = cv2.imread(filename, 0).astype(np.float64)
	if normalize:
		im /= np.amax(im)
	return im

def load_image_tiff_3d(path_img):
	'''
	@author Jiri Borovec

	Modified from scripts/crop_ovary3d_train_images_template_matching.py
	'''
	if not os.path.exists(path_img):
		print('given image "{}" does not exist!'.format(path_img))
		return None
	tif = libtiff.TIFF.open(path_img, mode='r')
	img = []
	# to read all images in a TIFF file:
	img = list(tif.iter_images())
	tif.close()
	img = np.array(img)
	# img2 = np.zeros(img.shape)
	# img2[...] = img
	# img = img / np.max(img) * 255
	print('image values {} -> {}'.format(np.min(img), np.max(img)))
	return img
