import cv2
import cv
import numpy as np
from scipy.interpolate import griddata
import libtiff
import myTools
import csv
import time
import caffe
import matplotlib.pyplot as plt
import datetime

# import signal
import sys

caffe.set_device(3)
caffe.set_mode_gpu()

solver = caffe.SGDSolver('solver.prototxt')
solver.restore('train/u-net_train_v5_iter_40000.solverstate')
# solver.restore('/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/Training Backups/train - latest - correct/u-net_train_v5_iter_40000.solverstate')
net = solver.net
# net.copy_from('train/u-net_train_v5_iter_36000.caffemodel')

errs = []
f = open('errsList'+datetime.datetime.now().strftime("%Y%m%d-%H%M%S")+'.txt', 'w', 1)

while True:
	solver.step(1)

	errs.append(net.blobs['loss'].data[()])
	f.write(str(net.blobs['loss'].data[()])+"\n")
	l1a = np.squeeze(net.params['conv_d0a-b'][0].data)
	# l1b = np.squeeze(net.params['conv_d0b-c'][0].data)
	# l2a = np.squeeze(net.params['conv_d1a-b'][0].data)
	# l2b = np.squeeze(net.params['conv_d1b-c'][0].data)
	# l3a = np.squeeze(net.params['conv_d2a-b'][0].data)
	# l3b = np.squeeze(net.params['conv_d2b-c'][0].data)
	# l4a = np.squeeze(net.params['conv_d3a-b'][0].data)
	# l4b = np.squeeze(net.params['conv_d3b-c'][0].data)

	# assert np.isnan(l1a).any() == False
	# assert np.isnan(l1b).any() == False
	# assert np.isnan(l2a).any() == False
	# assert np.isnan(l2b).any() == False
	# assert np.isnan(l3a).any() == False
	# assert np.isnan(l3b).any() == False
	# assert np.isnan(l4a).any() == False
	# assert np.isnan(l4b).any() == False

	for layer_name, param in net.params.iteritems():
		assert np.isnan(param[0].data).any() == False
		assert np.isnan(param[1].data).any() == False

	# plt.matshow(l1a); plt.colorbar()
	# plt.matshow(l1b); plt.colorbar()
	# plt.matshow(l2a); plt.colorbar()
	# plt.matshow(l2b); plt.colorbar()
	# plt.matshow(l3a); plt.colorbar()
	# plt.matshow(l3b); plt.colorbar()
	# plt.matshow(l4a); plt.colorbar()
	# plt.matshow(l4b); plt.colorbar()
	# plt.show()

	# scores = np.zeros((2,388,388))
	# scores[...] = net.blobs['score'].data[0,:,:,:]
	# mask_tiled = np.squeeze(net.blobs['label'].data)
	# wtmap_tiled = np.squeeze(net.blobs['sample_weights'].data)
	# s0, s1 = np.squeeze(np.exp(scores[0,:,:])), np.squeeze(np.exp(scores[1,:,:]))
	# denom = s0 + s1
	# s0, s1 = s0/denom, s1/denom
	# plt.matshow(s1, cmap='hot'); plt.colorbar()
	# plt.matshow(np.squeeze(net.blobs['label'].data[0,:,:,:]), cmap='hot'); plt.colorbar()
	# p = np.squeeze(np.zeros(s1.shape))
	# p[mask_tiled == 1] = s1[mask_tiled == 1]
	# p[mask_tiled == 0] = s0[mask_tiled == 0]
	# lossContrib = wtmap_tiled * - np.log(np.maximum(p, np.finfo(np.float32).eps))
	# plt.matshow(lossContrib, cmap='hot'); plt.colorbar()
	# count = np.sum(mask_tiled != 2)
	# err = np.nansum(lossContrib)
	# print err, count, err/count
	# print net.blobs['loss'].data[...]
	# plt.show()

	tif = libtiff.TIFF.open("l1a.tif", mode='w'); tif.write_image(l1a); del tif
	# f = libtiff.TIFF.open("l1b.tif", mode='w'); f.write_image(l1b); del f
	# f = libtiff.TIFF.open("l2a.tif", mode='w'); f.write_image(l2a); del f
	# f = libtiff.TIFF.open("l2b.tif", mode='w'); f.write_image(l2b); del f
	# f = libtiff.TIFF.open("l3a.tif", mode='w'); f.write_image(l3a); del f
	# f = libtiff.TIFF.open("l3b.tif", mode='w'); f.write_image(l3b); del f
	# f = libtiff.TIFF.open("l4a.tif", mode='w'); f.write_image(l4a); del f
	# f = libtiff.TIFF.open("l4b.tif", mode='w'); f.write_image(l4b); del f