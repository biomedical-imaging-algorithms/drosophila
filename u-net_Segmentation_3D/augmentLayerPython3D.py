import caffe
import myTools
import numpy as np
import csv
import cv2
import libtiff
import matplotlib.pyplot as plt

csvFilename2 = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/manually_corrected_3D_stacks_segmented.csv'
fullStackDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_3d_split/'
fullStackTruthDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_3d_binary/'

class BBOX:
	def __init__(self, x1, x2, y1, y2):
		self.x_1, self.x_2, self.y_1, self.y_2 = x1, x2, y1, y2
class INPUT_DATA:
	def __init__(self, fullImg, mask, wtmap, name, maxval):
		self.fullImg, self.mask, self.wtmap, self.name, self.maxval = fullImg, mask, wtmap, name, maxval

batchSize = 1
imSize = 256
maskSize = 256

class augmentLayer(caffe.Layer):
	def setup(self, bottom, top):
		if len(top) != 3:
			raise Exception('must have exactly three outputs')
		# if len(top) != 2:
		# 	raise Exception('must have exactly two outputs')
		self.inputList = []
		
		with open(csvFilename2) as csvfile:
			r = csv.DictReader(csvfile, delimiter='\t')
			for row in r:
				im3D = myTools.load_image_tiff_3d(fullStackDir+row['filename']+'.tif').astype(np.float64)
				# im3D /= np.amax(im3D)

				assert np.isnan(im3D).any() == False
				
				truth3D = myTools.load_image_tiff_3d(fullStackTruthDir+row['filename']+'_label.tif')
				wtmap3D = myTools.constructWeightMap3D(truth3D)
					
				self.inputList.append(INPUT_DATA(im3D, truth3D, wtmap3D, row['filename'], np.amax(im3D)))
				# plt.matshow(im, cmap='hot'); plt.colorbar()
				# plt.matshow(truth, cmap='hot'); plt.colorbar()
				# plt.matshow(wtmap, cmap='hot'); plt.colorbar()
				# plt.show()
				break

		self.counter = 0
		self.queue = []

	def reshape(self, bottom, top):
		top[0].reshape(batchSize, 1, 30, imSize, imSize)
		top[1].reshape(batchSize, 1, 30, maskSize, maskSize)
		top[2].reshape(batchSize, 1, 30, maskSize, maskSize)
		
	def forward(self, bottom, top):
		if len(self.queue) == 0:
			im = self.inputList[self.counter%len(self.inputList)].fullImg
			mask = self.inputList[self.counter%len(self.inputList)].mask
			wtmap = self.inputList[self.counter%len(self.inputList)].wtmap
			maxval = self.inputList[self.counter%len(self.inputList)].maxval

			# plt.matshow(im, cmap='hot'); plt.colorbar()
			# plt.matshow(mask, cmap='hot'); plt.colorbar()
			# plt.matshow(wtmap, cmap='hot'); plt.colorbar()
			# plt.show()
			
			im_aug, mask_aug, wtmap_aug = myTools.augment3D(im, mask, wtmap)
			im_aug /= maxval

			name = self.inputList[self.counter%len(self.inputList)].name
			# print maxval, np.amax(im), np.amax(im_aug), im_aug.dtype
			# print im_aug.shape, mask_aug.shape, wtmap_aug.shape

			# plt.matshow(im_aug, cmap='hot'); plt.colorbar()
			# plt.matshow(mask_aug, cmap='hot'); plt.colorbar()
			# plt.matshow(wtmap_aug, cmap='hot'); plt.colorbar()
			# plt.show()

			print name

			assert np.isnan(im_aug).any() == False
			assert np.isnan(mask_aug).any() == False
			assert np.isnan(wtmap_aug).any() == False

			R = int(np.ceil(float(im_aug.shape[1])/maskSize));
			C = int(np.ceil(float(im_aug.shape[2])/maskSize));

			for r in range(R):
				for c in range(C):
					x1, x2 = c * maskSize, (c + 1) * maskSize
					y1, y2 = r * maskSize, (r + 1) * maskSize
					cBsmall = BBOX(x1,x2,y1,y2)

					im_cut = myTools.cutWithContext3D(im_aug, cBsmall, (imSize - maskSize)/2)
					mask_cut = myTools.cutWithContext3D(mask_aug, cBsmall, 0, padding = 'constant', constant_values = 2)
					wtmap_cut = myTools.cutWithContext3D(wtmap_aug, cBsmall, 0, padding = 'constant', constant_values = 0)

					# print im_cut.shape, mask_cut.shape, wtmap_cut.shape
					# plt.imsave('check/'+ name +'-1.jpg', im_cut.astype(np.float32), cmap='hot', vmin=0, vmax=1); #plt.colorbar()
					# plt.imsave('check/'+ name +'-2.jpg', mask_cut.astype(np.float32), cmap='hot', vmin=0, vmax=2); #plt.colorbar()
					# plt.imsave('check/'+ name +'-3.jpg', wtmap_cut.astype(np.float32), cmap='hot', vmin=0, vmax=255); #plt.colorbar()

					if np.sum(mask_cut !=2) > 0.3 * mask_cut.size:
						self.queue.append((im_cut, mask_cut, wtmap_cut))
			print "Queue length = ", len(self.queue)

			self.counter = self.counter + 1

		elem = self.queue.pop()
		# print "Popped"
		top[0].data[0, 0, :, :, :] = elem[0].astype(np.float32)	# image
		# print "1/3: ", elem[0].shape
		top[1].data[0, 0, :, :, :] = elem[1].astype(np.float32)	# mask
		# print "2/3: ", np.amax(elem[1])
		top[2].data[0, 0, :, :, :] = elem[2].astype(np.float32)	# weight map
		# print "3/3: ", elem[2].shape
		# plt.matshow(elem[0].astype(np.float32), cmap='hot'); plt.colorbar()
		# plt.matshow(elem[1].astype(np.float32), cmap='hot'); plt.colorbar()
		# plt.matshow(elem[2].astype(np.float32), cmap='hot'); plt.colorbar()
		# plt.show()
		assert np.isnan(top[0].data).any() == False
		assert np.isnan(top[1].data).any() == False
		assert np.isnan(top[2].data).any() == False

	def backward(self, top, propagate_down, bottom):
		# no back propagation
		pass
