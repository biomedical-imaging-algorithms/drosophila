\title{Drosophila Egg Segmentation in 2-D using the U-Net Architechture}
\author{Prof. Jan Kybic \\CVUT \and Siddhant Ranade \\IITB}
\date{\today}

\documentclass[a4paper,11pt]{article}

\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amssymb}
\usepackage[colorlinks]{hyperref}
\usepackage{ulem,color}
\usepackage{mdwlist}
\usepackage{url}
\usepackage[margin=1in]{geometry}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{float}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[section]{placeins}
% \usepackage{subfig}

\graphicspath{{../}}

\begin{document}
\maketitle

\tableofcontents
\listoffigures
\listoftables
\listoftodos

\section{Introduction}
\subsection{Problem Definition}
In this work, we plan to perform image segmentation on (2D) Fluorescence Microscopy scans of drosophila eggs.  Basically, given an input image \ref{subfig:inputSlice}, we would like to get image \ref{subfig:outputSeg} as output.  We do this using Deep Convolutional Neural Networks (CNNs), in particular the U-Net architecture\cite{U-Net}.

\begin{figure}
\centering	
\begin{subfigure}{.5\textwidth}
	\centering
	\includegraphics[width=.99\textwidth]{imgs/slice_4174.jpg}
	\caption{Input slice}
	\label{subfig:inputSlice}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
	\centering
	\includegraphics[width=.99\textwidth]{imgs/slice_4174-label.jpg}
	\caption{Desired output segmentation}
	\label{subfig:outputSeg}
\end{subfigure}
\caption{Problem definition}
\end{figure}

\subsection{U-Nets}
\label{sec:U-Nets}
The U-Net is a fully convolutional neural network (FCN), i.e. it has no fully connected layers.  Also, all kernels used in the architecture are small\footnote{Mainly $3\times 3$, some $2\times 2$}.  As  a result, it has only a few parameters\footnote{Roughly 30M parameters. ImageNet has 60M.}.  However, the novel idea behind U-Net is that we have an upsampling (deconvolution) path following the usual convolution-pooling path.  This upsampling allows us to have a high resolution segmentation map (output), i.e. it helps us localize.  This upsampling path can be thought of as the reflected version of the downsampling path.

\begin{figure}[!ht]
	\includegraphics[width=\textwidth]{imgs/u-net-architecture}
	\caption[U-Net architecture]{The U-Net architecture. Image from \cite{U-Net} (\url{lmb.informatik.uni-freiburg.de/people/ronneber/u-net/u-net-architecture.png})}
	\label{fig:architecture}
\end{figure}

Figure \ref{fig:architecture} shows the U-Net architecture.  The green upwards-pointing arrows represent the ``Deconvolution'' (also known as ``Upconvolution'') operation.  Deconvolution is just like interpolation, except the weights are parameters of the neural network, and are tuned.  In all convolution layers, there is no zero-padding, i.e. only the valid parts of the convolutions are used. Therefore, there is an overhead here, because of the context information required at the boundaries.

For training, we use a weighted version of the softmax-loss function (see section \ref{subsec:WSMWL}), in order to give more importance to the pixels that are close to the separation boundaries of two cells.  The weights here are the sample weights, pre-computed and supplied to the network as input.  These are NOT tuned(see section \ref{subsec:wtmap}).  We also make extensive use of data augmentation (see \ref{subsec:augmentation}) while training the U-Net.

Because of the lower number of parameters and the use of data augmentation, the U-Net is able to learn from very few training images\footnote{In this work, we used 75 training images}.

The paper specifies that they used the Stochastic Gradient Descent \uline{(SGD Solver)}, with a \uline{high momentum of $0.99$} so that ``a large number of the previously seen training samples determine the update in the current optimization step.''\cite{U-Net}  They also specify that they use as large tiles as possible (to reduce the overhead of context information), and hence the \uline{batchsize is $1$}.



\hrule

\section{Work Done}
A test net is provided with the paper, along with a trained model, and code to segment 2 example images.  However, no code is provided for training.  Hence, we had to code the following ourselves (see Table \ref{table:WorkDone}).

\begin{table}[!ht]
	\begin{center}
		\begin{tabular}{|p{6cm}|l|p{4cm}|}
		\hline
		Brief Description				& Filename/Function name		&	Type	\\ \hline
		Generate Weightmaps for training (given the ground truth) (See \ref{subsec:wtmap})		&		\path{myTools.py > constructWeightMap}	&		Python Function \\ 
		Data augmentation (See \ref{subsec:augmentation})		&		\path{myTools.py > augment}	&		Python Function	\\
		U-Net training Net (See \ref{subsec:Net})		&		\path{phseg_v5-train.prototxt}	&		Prototxt File\\
		Python Input Layer for training (See \ref{subsec:InputLayer})		&		\path{augmentLayerPython.py > augmentLayer}	&	Python Class (PythonLayer for Caffe) \\
		Python code for training (See \ref{subsec:MainFile})				&		\path{main-v5.py} 			&		Python Script\\
		Solver file (See \ref{subsec:Solver})			&			\path{solver-v5.prototxt}				&		Prototxt file\\
		Test net 				&			\path{test-v5.prototxt}				&		Prototxt file\\
		Function to perform segmentation on given image (See \ref{subsec:deploy})	&	\path{deploy.py > tileAndSegment}	&	Python Function\\
		Python script to segment a given 2D image and save the output. (See \ref{subsec:deployMain})	&	\path{deployMain.py}&	Python Script\\
		Python script for testing a given model (See \ref{subsec:testModel})	&		\path{testModel.py} 			&		Python Script\\
		Python script for segmenting 2D slices (See \ref{subsec:segmentSlice})		&		\path{segment_3d_stack.py}		&	Python Script\\
		Python script for segmenting 3D stacks layer-by-layer (See \ref{subsec:segmentStack})	&	\path{segment_3d_stack.py}	&	Python Script\\
		\hline
		\end{tabular}
	\end{center}
	\caption{Table describing the work done}
	\label{table:WorkDone}
\end{table}


% \begin{itemize*}
% 	\item SGD solver
% 	\item Large tiles, batchsize = $1$
% 	\item momentum = $0.99$ \todo[inline,color=red!40]{Delete this!}
% \end{itemize*}

% \subsection{Some concerns}
% \begin{itemize*}
% 	\item Some implementation details missing? Where's the \texttt{solver.prototxt}?\\
% 	Wrote my own solver using the details above and some heuristics.
% 	\item Weighted Soft-max with loss --- How to implement this?\\
% 	Wrote my own WeightedSoftMaxWithLoss Layer, inspired by \url{http://deepdish.io/2014/11/04/caffe-with-weighted-samples/}. The relevant files are:
% 	\begin{enumerate*}
% 		\item Header file: \path{/home.guest/qqranade/caffe/include/caffe/layers/weighted_softmax_loss_layer.hpp}
% 		\item CPU: \path{/home.guest/qqranade/caffe/src/caffe/layers/weighted_softmax_loss_layer.cpp}
% 		\item GPU: \path{/home.guest/qqranade/caffe/src/caffe/layers/weighted_softmax_loss_layer.cu}
% 	\end{enumerate*}
% 	WeightedSoftMaxLossLayer inherits from WeightedLossLayer, the relevant files for this are:
% 	\begin{enumerate*}
% 		\item Header file: \path{/home.guest/qqranade/caffe/include/caffe/layers/weighted_loss_layer.hpp}
% 		\item Source: \path{/home.guest/qqranade/caffe/src/caffe/layers/weighted_loss_layer.cpp}
% 	\end{enumerate*}
% \end{itemize*}

\section{Implementation Details}
\subsection{Compute Weightmaps}\label{subsec:wtmap}
This function takes the ground truth as input and outputs the computed weightmaps.  Using sample weightmaps serves two purposes: the first is to compensate for the difference in the frequencies of the foreground and the background in the training data, and the second is to force the network to learn to correctly label very narrow regions separating two cells. Accordingly, the weightmap has two terms:
\begin{equation}\label{eqn:wtmap}
	w(\textbf{x}) = w_c(\textbf{x}) + w_0\cdot\exp\left(-\frac{(d_1(\textbf{x}) + d_2(\textbf{x}))^2}{2\sigma^2} \right),
\end{equation}
Here, $w_c(\textbf{x})$ is higher for the class with the lower frequency, and vice versa.  The exact formula (which is not specified in \cite{U-Net}) differs as per the experiment\todo[color=blue!20]{For experiments}. The terms $d_1(\textbf{x})$ and $d_2(\textbf{x})$ are respectively the distances from each pixel to the first and second nearest cell boundary.  They are computed using distance transforms.

% \begin{itemize*}
% 	\item Start with the ground truth, use distance transform to get the distance from each pixel to the first and second nearest cell boundary.
% 	\item Compute the ``frequencies'' of both (interior, exterior) classes.
% 	\item Calculate the weights according to equation (\ref{eqn:wtmap}),
% 	\begin{equation}\label{eqn:wtmap}
% 		w(\textbf{x}) = w_c(\textbf{x}) + w_0\cdot\exp\left(-\frac{(d_1(\textbf{x}) + d_2(\textbf{x}))^2}{2\sigma^2} \right),
% 	\end{equation}
% 	where $w_c(\textbf{x})$ higher for the class with lower frequency and vice versa, $w_0 = 20$ and $\sigma = 5$.\todo[inline,color=red!40]{Delete this!}
% \end{itemize*}

\subsection{Data Augmentation}\label{subsec:augmentation}
Augmentation is done in two stages: (random) elastic deformation followed by (random) rotation.  We first get a $700\times 700$ image from the full slice such that the available ground truth is in the centre.  Symmetric padding is used as needed.  We then create two $4\times 4$ matrices of std 10 gaussian random values.  These are treated as the displacements (in $x,y$ directions) at the ``control points'' (placing the corners of the matrix at the corners of the image.)  We use bicubic interpolation to compute the displacements at all points, and compute the final positions.  We then generate a (uniform) random angle ($\in [0,2\pi)$) and rotate the final positions about the centre by that angle.  We use nearest neighbour interpolation to augment the image (using the rotated final position for each pixel).  The exact same deformation is applied to the weightmap and the ground truth as well.  We then get a crop of the appropriate size from the centre of the image, weightmap, and the ground truth. These sizes also differ as per the experiment.\todo[color=blue!20]{For experiments}
% \begin{itemize*}
% 	\item Augmentation is done in 2 stages: (random) elastic deformation followed by (random) rotation.
% 	\item Get the $700\times 700$ image from the full slice such that the available ground truth is in the centre. Use symmetric padding if needed.
% 	\item Create two $4\times 4$ matrix of std 10 gaussian random values. Treat these as the perturbations in $x,y$ directions at the control points (placing the corners of the matrix at the corners of the image.)
% 	\item Use bicubic interpolation to compute the perturbations at all points.
% 	\item Calculate the final positions of all these points.
% 	\item Get a random angle ($\in [0,2\pi)$) and rotate the final positions about the centre.
% 	\item Apply the same exact deformation to the ground truth and the weightmap.
% 	\item Interpolate to get the augmented image. Get a $572\times 572$ cut from the centre for the image, and $388\times 388$ cuts for the weightmap and ground truth.\todo[inline,color=red!40]{Delete this!}
% \end{itemize*}

\subsection{Training Net}\label{subsec:Net}
We basically used the net provided by the authors of \cite{U-Net} along with the paper, with some suitable modifications as follows.  We set the weight initializers correctly as described in \cite{U-Net} (but not in the file they provide), using Gaussian initialization (instead of Xavier's algorithm) with standard deviation of $\sqrt{2/N}$, where $N$ denotes the total number of incoming nodes of one neuron. We manually computed these values and put them in the \texttt{prototxt} file.  To handle data input (with augmentation), we wrote our own \texttt{PythonLayer} (see \ref{subsec:InputLayer} below). We also implemented our own \texttt{WeightedSoftMaxWithLoss} Layer for loss computation in the final layer (see \ref{subsec:WSMWL} below). The original \texttt{SoftMaxWithLoss} layer in Caffe doesn't allow the samples to be weighted.

\subsection{Weighted Soft-max With Loss Layer}\label{subsec:WSMWL}
Weighted softmax-loss is a variant of the softmax-loss function, defined as follows:
\begin{align}
p_j(\mathbf {x} ) &= {\frac {e^{x_{j}}}{\sum _{k=1}^{K}e^{x_{k}}}}, \text{ for } j = 1,\ldots,K. \tag{softmax}\\
E &= -\sum _{\mathbf {x} \in \Omega}\log\left({p_{l(\mathbf {x})}}(\mathbf {x})\right) \tag{softmax-loss}\\
E &= -\sum _{\mathbf {x} \in \Omega}w(\mathbf {x})\log\left({p_{l(\mathbf {x})}}(\mathbf {x})\right) \tag{weighted softmax-loss}\\
&(l(\mathbf {x}) \text{ is the correct class of } \mathbf {x}) \nonumber
\end{align}

Wrote my own WeightedSoftMaxWithLoss Layer in C++, inspired by \url{http://deepdish.io/2014/11/04/caffe-with-weighted-samples/}. The relevant files are:
\begin{enumerate*}
	\item Header file: \path{/home.guest/qqranade/caffe/include/caffe/layers/weighted_softmax_loss_layer.hpp}
	\item CPU: \path{/home.guest/qqranade/caffe/src/caffe/layers/weighted_softmax_loss_layer.cpp}
	\item GPU: \path{/home.guest/qqranade/caffe/src/caffe/layers/weighted_softmax_loss_layer.cu}\todo[color=yellow!100]{Make a table instead?}
\end{enumerate*}
WeightedSoftMaxLossLayer inherits from WeightedLossLayer, the relevant files for this are:
\begin{enumerate*}
	\item Header file: \path{/home.guest/qqranade/caffe/include/caffe/layers/weighted_loss_layer.hpp}
	\item Source: \path{/home.guest/qqranade/caffe/src/caffe/layers/weighted_loss_layer.cpp}\todo[color=yellow!100]{Make a table instead?}
\end{enumerate*}

\subsection{Data Input Layer (PythonLayer)}\label{subsec:InputLayer}
During setup, this layer reads all ($75$) image slices and ground truths. It then computes the weightmaps, and stores everything in memory.  During each iteration, it picks one image, does the augmentation, and feeds it to Caffe.  It goes over all the images in the dataset and then loops back, starting with the first image again.  Because these augmented images are used only once, augmentation is done on the fly and the augmented images are never stored.

We use all the images in \path{/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/}, of all stages.  The \textcolor{blue}{input images} (full slices) \textcolor{blue}{are in \path{/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/ovary_2d}}, and the \textcolor{blue}{ground truths}, which I created myself by modifying \path{/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_2d}, \textcolor{blue}{are in \path{/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/mask_2d_binary}}.

Refer to the PythonLayer file (see table \ref{table:WorkDone}), or \url{http://stackoverflow.com/questions/34996075/caffe-data-layer-example-step-by-step}, or \path{caffe/python/caffe/test/test_python_layer.py} for help with PythonLayers.
% \begin{itemize*}
% 	\item Internally reads the CSV file, gets the relevant image slices, computes the weightmaps, and feeds the data to Caffe.
% 	\item Refer to the PythonLayer file (see table above), or \url{http://stackoverflow.com/questions/34996075/caffe-data-layer-example-step-by-step}, or \path{caffe/python/caffe/test/test_python_layer.py} for help with PythonLayers.\todo[inline,color=red!40]{Delete this!}
% \end{itemize*}

\subsection{Python Main File for Training}\label{subsec:MainFile}
Calls Caffe, sets up the net and starts the computation.

\subsection{Solver File}\label{subsec:Solver}
Contains parameters for the solver -- the Net to use, the name of the algorithm, learning rate policy, and so on.

\subsection{Function to perform segmentation on given image}\label{subsec:deploy}
Takes a net object (\texttt{net}) (has to be set up before) and another object (\texttt{args}), and also the image size and mask size to use when tiling. The object \texttt{args} contains either an input image (array) or an input filename, a flag specifying verbosity level (true or false), optionally an output filename to save to (otherwise the segmentation map is returned), and some other optional arguments, such as the ground truth for comparison.

\subsection{Python script to segment a given 2D image and save the output}\label{subsec:deployMain}
Applies the given model to the given input image and saves the output segmentation map. Call with \texttt{-h} or \verb|--help| flag to see help.

\subsection{Script to Test a given trained model}\label{subsec:testModel}
Tests the given model -- performs segmentation, compares it with ground truths, computes accuracy and saves the results.  Call with \texttt{-h} or \verb|--help| flag to see help.

\subsection{Script to Perform segmentation on 3D stacks layer-by-layer}\label{subsec:segmentSlice}
Applies the given model on all the 2D slices and saves the segmentation outputs (masks).  Call with \texttt{-h} or \verb|--help| flag to see help.

\subsection{Script to Perform segmentation on 3D stacks layer-by-layer}\label{subsec:segmentStack}
Applies the given model on all the 3D stacks (slice-by-slice) and saves the segmentation outputs (masks).  Call with \texttt{-h} or \verb|--help| flag to see help.

\hrule

\section{Experiments and Results}
\todo[inline,color=green!60]{Experiments with synthetic data to be added?}
We did several different experiments, which are described in the sections to follow, starting with the best results.

% \subsection{}
% We used $w_c(\textbf{x}) = \dfrac{1}{\text{frequency}(l(\textbf{x}))}$, $w_0 = 10$ and $\sigma = 5$ and trained the network for $40k$ iterations. The results were very similar to those in the next experiment

% \subsection{}
% We used
% \begin{equation}
% w_c(\textbf{x}) =
% \begin{cases}
% 	\frac{3}{2 \times \text{frequency}(\text{background}) + 1} & \text{if } l(\textbf{x}) = \text{background} \\
% 	\frac{1}{2 \times \text{frequency}(\text{background}) + 1} & \text{if } l(\textbf{x}) = \text{foreground}
% \end{cases},
% \end{equation}
% (i.e. background carries thrice as much weight as foreground, because most training images contain a lot more foreground than background, and in most test images it's the other way around) along with $w_0 = 20$ and $\sigma = 5$. We initialized the network with weights from the previous experiment, and let the training continue for $70k$ more iterations. Not much of a difference was observed in the results. The results of testing (with differently augmented images as input) are shown in figure \ref{fig:res1}.

% \begin{figure}[!htb]
% 	\centering
% 	\begin{subfigure}[t]{0.4\textwidth}
% 	\includegraphics[width=\textwidth]{results-1/out-51-2}
% 	\end{subfigure}
% 	~
% 	\begin{subfigure}[t]{0.4\textwidth}
% 	\includegraphics[width=\textwidth]{results-1/out-49-2}
% 	\end{subfigure}

% 	\begin{subfigure}[t]{0.4\textwidth}
% 	\includegraphics[width=\textwidth]{results-1/out-46-2}
% 	\end{subfigure}
% 	~
% 	\begin{subfigure}[t]{0.4\textwidth}
% 	\includegraphics[width=\textwidth]{results-1/out-42-2}
% 	\end{subfigure}
% 	\caption[Results]{Some results. The green outline represents the true segmentation, the red one is the output of our trained neural network}
% 	\label{fig:res1}
% \end{figure}

% \subsection{}
% Because there wasn't much of a difference in the results after changing the weightmaps, we decided to train from scratch with the new weightmaps for $70k$ iterations, the results are shown in figure 

% \begin{figure}[!htb]
% 	\centering
% 	\begin{subfigure}[t]{0.4\textwidth}
% 	\includegraphics[width=\textwidth]{results-2/out-51-2}
% 	\end{subfigure}
% 	~
% 	\begin{subfigure}[t]{0.4\textwidth}
% 	\includegraphics[width=\textwidth]{results-2/out-49-2}
% 	\end{subfigure}

% 	\begin{subfigure}[t]{0.4\textwidth}
% 	\includegraphics[width=\textwidth]{results-2/out-46-2}
% 	\end{subfigure}
% 	~
% 	\begin{subfigure}[t]{0.4\textwidth}
% 	\includegraphics[width=\textwidth]{results-2/out-42-2}
% 	\end{subfigure}
% 	\caption[More results]{Some more results.}
% 	\label{fig:res2}
% \end{figure}

\subsection{Best Results}
We used 
\begin{equation}
w_c(\textbf{x}) = \dfrac{1}{\text{frequency}(l(\textbf{x}))},
\end{equation}
with $w_0 = 20$ and $\sigma = 5$ and trained the network for $20k$ iterations.  The learning rate is initially $0.001$ and falls off by a factor of $10$ every 8k iterations.

The network, to a large extent, does learn to deal with narrow separation borders correctly. On inspection, it appears that the network has learnt to recognise the follicle cells.\todo[color=green!60]{Explain this}.  The results are in figure \ref{fig:resultsBest}.

% \begin{figure}[!htb]
% 	\centering
% 	\begin{subfigure}[t]{0.5\textwidth}
% 	\includegraphics[width=.99\textwidth]{results-3/out-59-2}
% 	\end{subfigure}%
% 	\begin{subfigure}[t]{0.5\textwidth}
% 	\includegraphics[width=.99\textwidth]{results-3/out-49-2}
% 	\end{subfigure}

% 	\begin{subfigure}[t]{0.5\textwidth}
% 	\includegraphics[width=.99\textwidth]{results-3/out-46-2}
% 	\end{subfigure}%
% 	\begin{subfigure}[t]{0.5\textwidth}
% 	\includegraphics[width=.99\textwidth]{results-3/out-42-2}
% 	\end{subfigure}
% 	\caption[Even more results]{Even more results.}
% 	\label{fig:res3}
% \end{figure}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-best/output_4846-overlay.png}
	\end{subfigure}%
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-best/output_7035-overlay.png}
	\end{subfigure}

	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-best/output_7047-overlay.png}
	\end{subfigure}%
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-best/output_20266-overlay.png}
	\end{subfigure}
	\caption[Results]{Results: the red boundaries are obtained by computing the morphological gradient of the output segmentation.  This has been done for illustration purposes.}
	\label{fig:resultsBest}
\end{figure}

\subsection{Evolution of the Output Segmentation with iteration number}
Here, we use the same parameters as above, but also look at the results obtained using intermediate models, to gain some insight into how the output evolves.  See figures \ref{fig:resultsEvolution1} and \ref{fig:resultsEvolution2}.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-evolution1/iter_1000_output_4174-overlay.png}
	\caption{1000 iterations}
	\end{subfigure}%
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-evolution1/iter_2000_output_4174-overlay.png}
	\caption{2000 iterations}
	\end{subfigure}

	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-evolution1/iter_4000_output_4174-overlay.png}
	\caption{4000 iterations}
	\end{subfigure}%
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-evolution1/iter_8000_output_4174-overlay.png}
	\caption{8000 iterations}
	\end{subfigure}
	\caption[Results - Evolution]{Evolution of output with iteration number, for a particular image}
	\label{fig:resultsEvolution1}
\end{figure}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-evolution2/iter_1000_output_4695-overlay.png}
	\caption{1000 iterations}
	\end{subfigure}%
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-evolution2/iter_2000_output_4695-overlay.png}
	\caption{2000 iterations}
	\end{subfigure}

	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-evolution2/iter_4000_output_4695-overlay.png}
	\caption{4000 iterations}
	\end{subfigure}%
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth]{res-evolution2/iter_8000_output_4695-overlay.png}
	\caption{8000 iterations}
	\end{subfigure}
	\caption[More results - Evolution]{Evolution of output with iteration number, for another image}
	\label{fig:resultsEvolution2}
\end{figure}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[t]{\textwidth}
	\includegraphics[width=.49\textwidth]{res-evolution1/iter_1000_output_4174-overlay.png}
	\includegraphics[width=.49\textwidth]{res-evolution2/iter_1000_output_4695-overlay.png}
	\caption{1000 iterations}
	\end{subfigure}
	\begin{subfigure}[t]{\textwidth}
	\includegraphics[width=.49\textwidth]{res-evolution1/iter_2000_output_4174-overlay.png}
	\includegraphics[width=.49\textwidth]{res-evolution2/iter_2000_output_4695-overlay.png}
	\caption{2000 iterations}
	\end{subfigure}

	\begin{subfigure}[t]{\textwidth}
	\includegraphics[width=.49\textwidth]{res-evolution1/iter_4000_output_4174-overlay.png}
	\includegraphics[width=.49\textwidth]{res-evolution2/iter_4000_output_4695-overlay.png}
	\caption{4000 iterations}
	\end{subfigure}
	\begin{subfigure}[t]{\textwidth}
	\includegraphics[width=.49\textwidth]{res-evolution1/iter_8000_output_4174-overlay.png}
	\includegraphics[width=.49\textwidth]{res-evolution2/iter_8000_output_4695-overlay.png}
	\caption{8000 iterations}
	\end{subfigure}
	\caption[Results - Evolution]{Evolution of output with iteration number, for two particular images - ALTERNATE ARRANGEMENT\todo[inline,color=yellow!100]{Alternate arrangement preferred?}.}
	\label{fig:resultsEvolution}
\end{figure}

The test accuracy and errors (measured over all test images) evolve as shown in figure \ref{fig:AccErrIter}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[t]{\textwidth}
	\centering
	\includegraphics[width=\textwidth,trim={0 1cm 0 1cm},clip]{imgs/accuracy-vs-iteration.pdf}
	\end{subfigure}
	\begin{subfigure}[t]{\textwidth}
	\centering
	\includegraphics[width=\textwidth,trim={0 1cm 0 1cm},clip]{imgs/error-vs-iteration.pdf}
	\end{subfigure}
	\caption{Accuracy, Error v/s Iteration number}
	\label{fig:AccErrIter}
\end{figure}

\subsection{Effect of Boundary weights}
In this experiment, we used $w_0\in\{5, 10, 20, 40\}$, keeping everything else the same.  As expected, the segmentation becomes better as we increase $w_0$, until it gets to $20$. After this, going from $20$ to $40$, the segmentation becomes worse, and the accuracy decreases.  Perhaps this is because of overfitting.  The network sees the very high weights for the boundary, and everything else is effectively ignored.  Results are presented in figure \ref{fig:resultsWeightVarying}.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth,trim={6cm 1cm 5.5cm 0},clip]{res-weightVarying/iter_20000_output_7336-overlay-weight5.png}
	\caption{$w_0=5$}
	\end{subfigure}%
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth,trim={6cm 1cm 5.5cm 0},clip]{res-weightVarying/iter_20000_output_7336-overlay-weight10.png}
	\caption{$w_0=10$}
	\end{subfigure}

	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth,trim={6cm 1cm 5.5cm 0},clip]{res-weightVarying/iter_20000_output_7336-overlay-weight20.png}
	\caption{$w_0=20$}
	\end{subfigure}%
	\begin{subfigure}[t]{0.5\textwidth}
	\includegraphics[width=.99\textwidth,trim={6cm 1cm 5.5cm 0},clip]{res-weightVarying/iter_20000_output_7336-overlay-weight40.png}
	\caption{$w_0=40$}
	\end{subfigure}
	\caption{Results - varying the weight parameter $w_0$}
	\label{fig:resultsWeightVarying}
\end{figure}

\subsection{Training with a single image}
Seeing that we were able to train the network with 75 images, we decided to train with just one image, starting from scratch, and some results are presented in figure \ref{fig:resultsOneImage}.  Also, the training is really fast in the case of a single image, roughly 3000 iterations.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[b]{0.525\textwidth}
	\includegraphics[width=.99\textwidth,trim={1cm 0 16cm 2cm},clip]{res-oneImage/iter_2800_output_21125-overlay.png}
	\end{subfigure}%
	\begin{subfigure}[b]{0.475\textwidth}
	\includegraphics[width=.99\textwidth,trim={2cm 0 6cm 2cm},clip]{res-oneImage/iter_2600_output_14811-overlay.png}
	\includegraphics[width=.99\textwidth,trim={8cm 0 2cm 1cm},clip]{res-oneImage/iter_4600_output_4844-overlay.png}
	\end{subfigure}
	\caption{Results - varying the weight parameter $w_0$}
	\label{fig:resultsOneImage}
\end{figure}

Of course, this isn't perfect, and the segmentation fails, sometimes spectacularly when the input image looks significantly different (say the cell is of a different biological stage), and at boundaries (see figure \ref{fig:resultsOneImageBad}).

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[b]{0.475\textwidth}
	\includegraphics[width=.99\textwidth,trim={0 2cm 0 0},clip]{res-oneImage/iter_2800_output_43727-overlay.png}
	\end{subfigure}%
	\begin{subfigure}[b]{0.525\textwidth}
	\includegraphics[width=.99\textwidth,trim={2cm 0 0 0},clip]{res-oneImage/iter_2600_output_20267-overlay.png}
	\includegraphics[width=.99\textwidth,trim={4cm 4cm 0 0},clip]{res-oneImage/iter_3000_output_7034-overlay.png}
	\end{subfigure}
	\caption{Results - varying the weight parameter $w_0$}
	\label{fig:resultsOneImageBad}
\end{figure}

\hrule

\section{Future Work -- Extension to 3D}
\subsection{Extending the 2D network to 3D data}
\subsubsection{Using trained 2D network on 3D data}
In all the experiments with 2D slices thus far, we have only used the middle slices, i.e. the slices from the middle of the eggs.  To extend to 3D, this is perhaps the simplest idea -- simply use the network trained on the middle slices to segment all other slices.  This predictably doesn't work so well, because these slices look considerably different. 

For instance, in figure \ref{fig:resultsNonMiddle}, as we go from \ref{subfig:centreSlice} to \ref{subfig:offSlice} to \ref{subfig:farSlice}, the slices look more and more different, and the segmentations become worse and worse.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[t]{0.33\textwidth}
	\includegraphics[width=.99\textwidth,trim={10cm 5cm 0 5cm},clip]{res-nonMiddle/AU10-81_f0001_mag-0017.png}
	\caption{Centre slice}
	\label{subfig:centreSlice}
	\includegraphics[width=.99\textwidth,trim={10cm 5cm 0 5cm},clip]{res-nonMiddle/AU10-81_f0001_mag_label-0017.png}
	\end{subfigure}%
	\begin{subfigure}[t]{0.33\textwidth}
	\includegraphics[width=.99\textwidth,trim={10cm 5cm 0 5cm},clip]{res-nonMiddle/AU10-81_f0001_mag-0012.png}
	\caption{Off-centre slice}
	\label{subfig:offSlice}
	\includegraphics[width=.99\textwidth,trim={10cm 5cm 0 5cm},clip]{res-nonMiddle/AU10-81_f0001_mag_label-0012.png}
	\end{subfigure}%
	\begin{subfigure}[t]{0.33\textwidth}
	\includegraphics[width=.99\textwidth,trim={10cm 5cm 0 5cm},clip]{res-nonMiddle/AU10-81_f0001_mag-0010.png}
	\caption{Slice far from centre}
	\label{subfig:farSlice}
	\includegraphics[width=.99\textwidth,trim={10cm 5cm 0 5cm},clip]{res-nonMiddle/AU10-81_f0001_mag_label-0010.png}
	\end{subfigure}
	\caption{Applying the trained model on all slices}
	\label{fig:resultsNonMiddle}
\end{figure}

\subsubsection{Using off-centre slices in training of 2D network}
Because the previous experiment wasn't so successful, we tried to include all the slices in the 3D stacks for training the 2D network, slice-by-slice.  We first used the results from the previous part and manually corrected those to get 3D ground truths.  We then included them in the training.  However the network is not able to learn anything useful here, and the outputs go haywire, eventually settling at all zeroes\footnote{For obvious reasons, the author feels that this part might need more work.}.

\subsection{Extending the Network to 3D}
Since both of the previous ideas didn't seem to work, we would like to extend the network to 3D.  This would mean 3D convolutional kernels, 3D pooling layers, etc.  However, there are some concerns regarding this.

First, the resolution of the microscope is anisotropic.  The $z$-resolution is roughly 1/11\textsuperscript{th} of the $x,y$-resolutions.  In this case, it wouldn't make sense to do 3D convolutions.  To deal with this, we keep the convolutions in the first few layers 2D, and do pooling only in $x,y$ directions.  After 3 pooling operations, both resolutions turn out to be close, and so we switch to 3D kernels here.

Even after doing this, however we still have the problem of context information (described in section \ref{sec:U-Nets}) to deal with -- the amount required is too much for the $z$-direction.  The scans themselves aren't big enough.

However, even if they were big enough, we would have issues with memory consumption (we would need roughly 60 times the memory for 2D).  Thus, we use zero-padding in all the convlutions, so that the input and output sizes are the same.  It is important to note here that our background values really are close to zero, so this approximation is not too bad.

We will also need 3D image augmentation for training, which we have implemented.

However, Max-Pooling is not implemented in Caffe, and because of this, we couldn't complete this experiment\footnote{The author estimates that implementing the 3D pooling layer in Caffe will take roughly a week}.  The network (\texttt{.prototxt} file), however, is ready.

\bibliography{../references}
\bibliographystyle{plain}

\hrule

\section{Caffe, MatCaffe, PyCaffe Installation on Halmos}
\begin{itemize*}
	\item Used Milan Šulc’s Caffe installation from \texttt{/mnt/datagrid/personal/sulcmila/tools/caffe} and copied it to \texttt{/home.guest/qqranade/caffe}
	\item Set the appropriate Matlab directory in \texttt{Makefile.config}
	\item Added/changed the following variables:
	\begin{verbatim}
		export PATH=/usr/local/cuda/bin:$PATH
		export LD_LIBRARY_PATH=/opt/intel/mkl/lib/intel64:/usr/local/cuda/lib64
		export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6
		export CAFFE_ROOT=/home.guest/qqranade/caffe
		export PYTHONPATH=/home.guest/qqranade/caffe/python:$PYTHONPATH
	\end{verbatim}
	\item Need to set compilation flag \texttt{WITH\_PYTHON\_LAYER := 1} in \texttt{Makefile.config} to use Python Layer.
	\item \texttt{make clean}, then \texttt{make all matcaffe pycaffe -j32}, and then \texttt{make mattest} and \texttt{make pytest} to check that everything is working.
	\item Start MATLAB from \texttt{\$CAFFE\_ROOT} (for first time) and do the following:
	\item \texttt{addpath ./matlab}
	\item \texttt{savepath} -- This will create a \texttt{pathdef.m}, copy this to whichever directory you plan to start MATLAB from.
	\item MatCaffe ready to use
\end{itemize*}
For new users: Copy my Caffe installation, and there won’t be a need to edit \texttt{Makefile.config}. All other steps remain the same.

BVLC CaffeNet is already installed. The ImageNet classification demo works (for new users: make sure that it does indeed work, as testing).

\end{document}