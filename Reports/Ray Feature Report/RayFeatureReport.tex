%---------------------------------------------------------------------------------
% Ray Feature Report
% October 14th, 2015
% Rodrigo Nava
%---------------------------------------------------------------------------------

\documentclass[11pt]{report}

\usepackage{fix-cm}
\usepackage[utf8]{inputenc}
\usepackage[letterpaper,bindingoffset=0in,head=25pt,includeheadfoot,left=3cm,right=3cm,bottom=3cm,top=2cm,headsep=1cm,
showframe]{geometry}
\usepackage{subfigure}
\usepackage{setspace}
\usepackage{lastpage} 
\usepackage{wrapfig} 
\usepackage[labelfont=bf]{caption}
\usepackage[svgnames]{xcolor}
\usepackage{pgfgantt}
\usepackage{color, colortbl}
\usepackage{hyperref}

\usepackage{tikz}
\usetikzlibrary{arrows,shapes.geometric,matrix,positioning,decorations.markings,shadows,calc,patterns,decorations.pathreplacing,angles,quotes}

\usepackage{graphicx}
\DeclareGraphicsExtensions{.png}
\graphicspath{{resources/}}

\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
\setlength{\algomargin}{3em}

\newcounter{parnum}
\newcommand{\patentParagraph}{
\par\noindent
\refstepcounter{parnum}[\textbf{%
\ifnum \value{parnum} < 10 0\else\fi
\ifnum \value{parnum} < 100 0\else\fi
\ifnum \value{parnum} < 1000 0\else\fi
\arabic{parnum}}]
\indent}

%--------------------------------------------------------
%--------------------------------------------------------
\begin{document} 
\begin{center}
\vskip 50pt
{\Large\textit{Supertexton segmentation + Ray Feature \\distance histograms}}
\vskip 10pt
version 0.1\\
\today
\end{center}

Dear Dr. Jan,\\
Please find a brief description of the current pipeline: segmentation with supertextons + ray feature distance histograms.

The methodology is divided into two stages. First, a pre-segmentation procedure is performed with superpixels and textons (already published in \cite{NAVA2015}) and then, over the segmented slices, the extraction of the distance histograms using ray features is calculated. The source code of all the programs and this latex can be found at \url{https://gitlab.fel.cvut.cz/biomedical-imaging-algorithms/drosophila.git}

%--------------------------------------------------------
%--------------------------------------------------------
\section*{Segmentation of 2D localized \textit{Drosophila} egg chambers\label{sec:description}}

\patentParagraph In order to create a supertexton dictionary, isolated egg chambers were labeled manually into four classes: 0-background, 1-follicle cells, 2-nurse cells, and 3-cytoplasm. The number of the class represents the value of the label. Most of the sample images represent eggs that belong to the first stage of the development process, namely they are small and have shapes similar to circles with well-defined boundaries (follicle cells). However, during the first stages, nurse cells are overlapping among them. Therefore, sometimes it is not possible to distinguish individual nurse cells.

\begin{center} 
        \begin{minipage}{0.85\linewidth}
            \begin{algorithm}[H]
                \SetAlgoLined
                \KwResult{Supertexton dictionary}
                \KwIn{LI := labeled images}
                \Begin{
                    \ForEach{$image$}{
                        Segment $image$ with \textbf{SLIC}\;
                        Filter $image$ with $LM_{18}$ filter bank\;
                        \ForEach{valid superpixel $\in$ $image$}{
                            Compute features ($\mu$, $\sigma$, $E$, and $G$)\;
                        }
                    Normalization of the features\;
                    Supertexton$_{image}$ $\leftarrow$ Cluster the normalized features into 4 classes with \textit{k}-means\;    
                    }
                    Dictionary $\leftarrow$ $\forall$ Supertexton$_{image}$
                }
                \caption{Construction of a supertexton dictionary}
                \label{alg:alg_01}
            \end{algorithm}
        \end{minipage}
\end{center}  

\patentParagraph The segmentation of both isolated \textit{Drosophila} egg chambers and full slices is performed with k-NN and the dictionary. After computing the features of the superpixels, the corresponding region is assigned to a label: $\{0,1,2,3\}$ according to the closest supertextons. 

\begin{center} 
        \begin{minipage}{0.85\linewidth}
            \begin{algorithm}[H]
                \SetAlgoLined
                \KwResult{2D segmented image}
                \KwIn{SPD:= Supertexton dictionary}
                \KwIn{I:= Image to be segmented}
                \Begin{
                    Segment I with \textbf{SLIC}\;
                    Filter I with $LM_{18}$ filter bank\;
                    \ForEach{superpixel $\in$ I}{
                        Compute features ($\mu$, $\sigma$, $E$, and $G$)\;
                        Normalization of the features\;
                        Label the $superpixel$ with k-NN, Euclidean distance and SPD\;
                    }
                }
                \caption{2D Segmentation of a \textit{Drosophila} egg chamber}
                \label{alg:alg_02}
            \end{algorithm}
        \end{minipage}
\end{center}  

\patentParagraph Segmentation of localized egg chambers $\approx$ 200$\times$200 px. takes around 2[s] (see Figure \ref{fig:fig_1}). However, the segmentation of a full slice $\approx$ 1200$\times$700 px. takes around 100[s], (see Figure \ref{fig:fig_2a}). The segmentation of a full volume takes around 45 minutes.

\begin{figure}[!htbp]
	\hspace*{\fill}
    \subfigure{\includegraphics[width=0.4\textwidth]{1a}}
    \subfigure{\includegraphics[width=0.4\textwidth]{1b}}
    \hspace*{\fill}\\
    \hspace*{\fill}
    \subfigure{\includegraphics[width=0.4\textwidth]{1c}}
    \subfigure{\includegraphics[width=0.4\textwidth]{1d}}
    \hspace*{\fill}\\
    \hspace*{\fill}
    \subfigure{\includegraphics[width=0.4\textwidth]{1e}}
    \subfigure{\includegraphics[width=0.4\textwidth]{1f}}
    \hspace*{\fill}
    \caption{Examples of segmented Drosophila egg chambers}
    \label{fig:fig_1}
\end{figure}

\begin{figure}[!htbp]
	\hspace*{\fill}
    \subfigure{\includegraphics[width=0.4\textwidth]{2a}}
    \subfigure{\includegraphics[width=0.4\textwidth]{2b}}
    \hspace*{\fill}\\
    \hspace*{\fill}
    \subfigure{\includegraphics[width=0.4\textwidth]{2c}}
    \subfigure{\includegraphics[width=0.4\textwidth]{2d}}
    \hspace*{\fill}\\
    \hspace*{\fill}
    \subfigure{\includegraphics[width=0.4\textwidth]{2e}}
    \subfigure{\includegraphics[width=0.4\textwidth]{2f}}
    \hspace*{\fill}
    \caption{Examples of segmentation of full slices of \textit{Drosophila} germarium.}
    \label{fig:fig_2a}
\end{figure}

\begin{figure}[!htbp]
\ContinuedFloat 
    \hspace*{\fill}
    \subfigure{\includegraphics[width=0.4\textwidth]{2g}}
    \subfigure{\includegraphics[width=0.4\textwidth]{2h}}
    \hspace*{\fill}\\
    \hspace*{\fill}
    \subfigure{\includegraphics[width=0.4\textwidth]{2i}}
    \subfigure{\includegraphics[width=0.4\textwidth]{2j}}
    \hspace*{\fill}
    \caption{(Cont...) Examples of segmentation of full slices of \textit{Drosophila} germarium.}
    \label{fig:fig_2b}
\end{figure}

%--------------------------------------------------------
%--------------------------------------------------------
\section*{Rey feature descriptor \label{sec:ray_feature}}

\patentParagraph The original ray feature proposal \cite{SMITH2009} contains four distinct image features: distance difference, distance feature, orientation, and norm feature. Given an image $I$, a point $p$, and direction $\theta$, the descriptor assesses the four properties of the nearest edge location in the direction of $\theta$, (see Figure \ref{fig:fig_3}).

\begin{figure}[!htbp]
\centering
\input{resources/figure_8.tikz}
\caption{Diagram of the original ray feature descriptor.}
\label{fig:fig_3} 
\end{figure}

\patentParagraph In our experiments, we only compute the distance feature from an interior point to the nearest follicle cell in the direction of $\theta$ as follows:
\begin{equation}
   f(p)_{\theta}\left(I\right) = \left\|c\left(I,p,\theta\right) - p \right\|
\label{eq:eq_1}
\end{equation} 
where $c(I,p,\theta)$ returns the location of the nearest follicle cell in the image $I$ to the point $p$ in the direction of $\theta$.

\patentParagraph First, we collected center points from the isolated egg chambers, then the distances were computed every $\frac{\pi}{64}$ steps to build 128-length vectors with $0\leq\theta\leq2\pi$. Due to some segmentation issues, some rays may pass through the border inducing large distances. 

\begin{figure}[!htbp]
    \hspace*{\fill}
    \subfigure{\includegraphics[width=0.33\textwidth]{3a}}
    \subfigure{\includegraphics[width=0.33\textwidth]{3b}}
    \hspace*{\fill}\\
    \hspace*{\fill}
    \subfigure{\includegraphics[width=0.33\textwidth]{3d}}
    \subfigure{\includegraphics[width=0.33\textwidth]{3e}}
    \hspace*{\fill}\\
    \hspace*{\fill}
    \subfigure{\includegraphics[width=0.33\textwidth]{3g}}
    \subfigure{\includegraphics[width=0.33\textwidth]{3h}}
    \hspace*{\fill}\\
    \setcounter{subfigure}{0}
    \hspace*{\fill}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{3j}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{3k}}
    \hspace*{\fill}
    \caption{Ray feature distances. \textbf{(a)} Ray features depicted on the segmented images and \textbf{(b)} distances from the center to the nearest follicle cell on the direction of $\theta$.}
    \label{fig:fig_4}
\end{figure}

\patentParagraph In order to train the ray feature descriptor, centers of 60 isolated eggs and 60 full slices were extracted. So, the distance histograms were computed in a 2D fashion.

\begin{figure}[!htbp]
    \hspace*{\fill}
    \subfigure{\includegraphics[width=0.33\textwidth]{4a}}
    \subfigure{\includegraphics[width=0.33\textwidth]{4b}}
    \hspace*{\fill}\\
    \hspace*{\fill}
    \subfigure{\includegraphics[width=0.33\textwidth]{4d}}
    \subfigure{\includegraphics[width=0.33\textwidth]{4e}}
    \hspace*{\fill}\\
    \hspace*{\fill}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{4g}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{4h}}
    \hspace*{\fill}\\
    \setcounter{subfigure}{0}
    \hspace*{\fill}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{4j}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{4k}}
    \hspace*{\fill}    
    \caption{Ray feature distances on full slice. \textbf{(a)} ray distances painted on full slices and \textbf{(b)} distances in number of pixels from the center to the nearest follicle cell on the direction of $\theta$.}
    \label{fig:fig_5}
\end{figure}

\patentParagraph

%----------------------------------------------------
%----------------------------------------------------
\clearpage
\bibliographystyle{alpha}
\bibliography{references}

\end{document} 