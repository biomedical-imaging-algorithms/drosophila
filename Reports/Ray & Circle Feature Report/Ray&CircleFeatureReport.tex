%---------------------------------------------------------------------------------
% Ray & Circle Feature Report
% December 1st, 2015
% Rodrigo Nava
%---------------------------------------------------------------------------------

\documentclass[11pt]{article}

\usepackage{fix-cm}
\usepackage[utf8]{inputenc}
\usepackage[letterpaper,bindingoffset=0in,head=25pt,includeheadfoot,left=3cm,right=3cm,bottom=3cm,top=2cm,headsep=1cm,
showframe]{geometry}
\usepackage{subfigure}
\usepackage{setspace}
\usepackage{lastpage} 
\usepackage{wrapfig} 
\usepackage[labelfont=bf]{caption}
\usepackage[svgnames]{xcolor}
\usepackage{pgfgantt}
\usepackage{colortbl}
\usepackage{hyperref}
\usepackage{verbatim}
\usepackage{bm}

\usepackage{hyperref}
\def\UrlBreaks{\do\/\do-}
\hypersetup{
  linktocpage,
  colorlinks,
  citecolor=Purple,    
  %filecolor=LightSalmon,
  linkcolor=DodgerBlue,
  urlcolor=DeepPink  
}

\usepackage{tikz}
\usetikzlibrary{arrows,shapes.geometric,matrix,positioning,decorations.markings,shadows,calc,patterns,decorations.pathreplacing,angles,quotes}

%\usepackage{pgfplots}
%\pgfplotsset{compat=1.8}

\usepackage{3dplot}
\usepackage{paralist}

\usepackage{graphicx}
\DeclareGraphicsExtensions{.png}
\graphicspath{{resources/}}

\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
\setlength{\algomargin}{3em}

\newcounter{parnum}
\newcommand{\patentParagraph}{
\par\noindent
\refstepcounter{parnum}[\textbf{%
\ifnum \value{parnum} < 10 0\else\fi
\ifnum \value{parnum} < 100 0\else\fi
\ifnum \value{parnum} < 1000 0\else\fi
\arabic{parnum}}]
\indent}

\setlength{\parindent}{0em}
\setlength{\parskip}{1.2em}


%--------------------------------------------------------
\title{3D Ray Feature Distance Histograms + \\ Circle Label Histograms + \\
Sphere Label Histograms}
\author{version 0.2}
\date{\today}

%--------------------------------------------------------
%--------------------------------------------------------
\begin{document} 

\maketitle

Dear Dr. Jan,\\
Please find a brief description of the current pipeline. The methodology was divided as follows:
\begin{enumerate}
	\item First, a 2D pre-segmentation procedure was conducted on every slice of each volume using superpixels and textons (already published in \cite{NAVA2015}).
    
    \item Our dataset is composed of 60 volumes; every volume has 25 slices $\approx$ ($600\times100$) px. We marked the centers of all the eggs (\textbf{interior points}). Furthermore, three representative points (\textbf{exterior points}) were marked in every volume according the \verb|slice_index|, which is the slice focused. 
    
    \item Then, on the segmented volumes, the computation of\begin{inparaenum}[\color{Red}(\bfseries a\normalfont)]\item 3D ray feature distances and \item 3D ray feature label histograms \end{inparaenum} using the interior and exterior points was performed in order to build feature vectors for a subsequent classification.
    
    \item Circle label histograms were computed in selected slices, namely on the \verb|slice_index| in each volume.
    
    \item Sphere label histograms were computed for interior and exterior points.
    
    \item This PDF and its latex can be found at \url{https://gitlab.fel.cvut.cz/biomedical-imaging-algorithms/drosophila.git}
    
    \item The source code and data can be found at \url{/datagrid/personal/qqnava/drosophila}
    
\end{enumerate}

%--------------------------------------------------------
\clearpage
\section*{Ray Features.\label{sec:rayfeatures}}

\patentParagraph Our implementation is inspired by \cite{SMITH2009} where the authors presented a 2D descriptor that contains four distinct image features: distance difference, distance feature, orientation, and norm feature. 

\patentParagraph The original algorithm computes the distance, $D$, as the norm $\|\cdot\|$ between the location, $L$, which represents a border of a cell, and the point $P=(x,y)$ in the direction of $\theta$ that is defined on the XY-plane, (see Fig.~\ref{fig:fig_1a}).
\begin{equation}
   D_{\theta}\left(I,P\right) = \left\|L\left(I,P,\theta\right) - P\right\|
\label{eq:eq_1}
\end{equation} 

\patentParagraph Our proposal extends the algorithm of Smith et al. \cite{SMITH2009} to three dimensions by including the angle $\phi$ that is defined along the Z-axis, (see Fig.~\ref{fig:fig_1b}).

\patentParagraph The formal definition is as follows: Given an image $I$, a point $P=(x,y,z)$, and two angles $(\theta,\phi)$, a 3D ray feature is the Euclidean distance $D$ from the point $P$ to the location $L$:
\begin{equation}
   D_{\theta,\phi}\left(I,P\right) = \left\|L\left(I,P,\theta,\phi\right) - P \right\|
\label{eq:eq_2}
\end{equation} 
where $L(I,P,\theta,\phi)$ returns the location of the closest follicle cell to the point $P$ in the direction of the corresponding $\theta$ and $\phi$ angles.
 
\begin{figure}[!tbp]
\centering
\subfigure[]{\label{fig:fig_1a} \input{resources/figure_1.tikz}}
\subfigure[]{\label{fig:fig_1b} \input{resources/figure_2.tikz}}
\caption{Ray feature schemes. $D_1$ represents the distance from the point $P_1$ to $L_1$, which is a follicle cell. \textbf{(a)} 2D ray feature distance. \textbf{(b)} 3D ray feature distance.}
\label{fig:fig_1} 
\end{figure}

%--------------------------------------------------------
\section*{Ray Feature Histograms.\label{sec:rayfeaturehistograms}}

\patentParagraph After computing distances over 64 orientations with $\theta = \{0,\frac{\pi}{32},\ldots,2\pi\}$ based on Eq.~(\ref{eq:eq_1}), it is possible to build a rotationally invariant descriptor using a distance histograms (DH) with non-uniform bins.

\patentParagraph In our experiments, we set the number of bins to 16 and use
the following scale $B_{i}=\{0, 1.6^{W}|W=1,\,\ldots,\,15\}$, so that, the distance histogram $DM$ could be constructed as follows:
\begin{equation}
   DH_{i} = \sum_{x}^{N} \left[\left[B_{i}\leq D_{x}\left(I,P\right) < B_{i+1}\right]\right]\, 
   \label{eq:eq_3}
\end{equation}
where $N$ is the number of orientations in $\theta$, $i=0,\,\ldots,15$, and $[[\cdot]]$ is the Iverson bracket defined as:
\begin{equation}
  [[A]] = \left\{
  \begin{array}{lc}
    1 & \mbox{\textbf{if} A is true}\\
    0 & \mbox{otherwise}
  \end{array} \right.
\label{eq:eq_4}
\end{equation}

\patentParagraph We carried out experiments using full slices. We marked 180 interior points that belong to the center of the egg chambers, (see Fig.~\ref{fig:fig_2}) and 180 points that lay outside the eggs, (see Fig.~\ref{fig:fig_3}). For each marked point, 30 extra points were randomly selected within a circle of radius = 20px. to generate more samples.

\begin{figure}[!tbp]
\centering
\subfigure{\includegraphics[width=0.3\textwidth]{fig_1}}
\subfigure{\includegraphics[width=0.3\textwidth]{fig_2}}\\
\subfigure{\includegraphics[width=0.3\textwidth]{fig_3}}
\subfigure{\includegraphics[width=0.3\textwidth]{fig_4}}
\caption{Examples of positive samples of 2D Ray features on full slices. Yellow lines represent the paths from the selected point to the closest follicle cell.}
\label{fig:fig_2} 
\end{figure}

\begin{figure}[!tbp]
\centering
\subfigure{\includegraphics[width=0.3\textwidth]{fig_8}}
\subfigure{\includegraphics[width=0.3\textwidth]{fig_9}}
\caption{Examples of negative samples of 2D Ray features on full slices. Yellow lines represent the rays from a exterior point to the closes follicle cell in the direction of $\theta$.}
\label{fig:fig_3} 
\end{figure}

\patentParagraph We computed the average feature vectors for both interior and exterior points, (see Fig.~\ref{fig:fig_4}). Although the shape and size of the eggs varies according to their stage, the mean distance of exterior points is always greater than the mean distance from any center egg to its corresponding borders. Here, we propose DH as a way to describe distances with a rotationally invariant representation, because it collects the distances over all possible angles: $\theta$.

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.8\textwidth]{fig_10}
\caption{Mean distance histograms using 2D ray features. DM represents the distances from a point to the closest border cell over 64 orientations. Blue bars represent distances from interior points, whereas yellow ones represent distances from exterior points.}
\label{fig:fig_4} 
\end{figure}

\patentParagraph We also calculated the label histogram (LH), which represents the frequency of pixels over the ray paths that belong to one of the four classes: background (0), follicle cell (1), nurse cell (2), or cytoplasm (3) in every ray feature. First, a local label histogram, $F_{i}(I,P,\theta)$, is built as follows:
\begin{equation}
   F_{i}\left(I,P,\theta\right) =\sum C\left\{\bm{getLables}\left\{L\left(I,P,\theta\right), P\right\}==i\right\} \mbox{ with } i=0,1,2,3\,
\label{eq:eq_5}
\end{equation} 
where $\bm{getLables}\{\cdot\}$ returns the labels of the pixels between the point $P$ and location $L$ in the direction of $\theta$. 

\patentParagraph $LH$ is the summation of all local histograms, Eq.~\ref{eq:eq_5}, over the $\theta$ angle: 
\begin{equation}
   LH_{i} = \sum_{\theta=0}^{2\pi}F_{i}\left(I,P,\theta\right)\,
   \label{eq:eq_6}
\end{equation}

\patentParagraph The average label histograms for interior and exterior points are shown in Fig,\ref{fig:fig_5}.

\begin{figure}[!tbp]
\centering
\includegraphics[width=0.6\textwidth]{fig_11}
\caption{Label histograms using 2D ray features. Blue bars represent labels of interior points, whereas yellow bars represent labels of exterior points.}
\label{fig:fig_5} 
\end{figure}

\patentParagraph Limitations. Since the ray features are based on a pre-segmentation step, two problems arise. First, it is not always possible to find all the follicle cells that surround the egg chamber; and second, the eggs in the germarium are not always separated or are merge. Therefore, the rays are projected beyond the borders, (see Fig.~\ref{fig:fig_6}). 

\begin{figure}[!htb]
\centering
\subfigure{\includegraphics[width=0.3\textwidth]{fig_5}}
\subfigure{\includegraphics[width=0.3\textwidth]{fig_6}}
\subfigure{\includegraphics[width=0.3\textwidth]{fig_7}}
\caption{Examples of 2D Ray Features on a germarium. Note that under some circumstances the rays go beyond the egg chambers.}
\label{fig:fig_6} 
\end{figure}

%--------------------------------------------------------
\section*{Circle Label Histograms.\label{sec:circlelabelhistograms}}

\patentParagraph To better describe the different classes that constitute an egg chamber, we propose using concentric circles to characterize the interior points. For each circle, a label histogram is built in a similar manner to that mentioned previously.

\patentParagraph Since the shape of the eggs varies from one sample to another, we first estimate the shape of the egg using 2D ray features over all possible angles and the mean distance. Very large and short rays are discarded because they are considered outliers. We need five rays to estimate an initial ellipse, (see Fig.~\ref{fig:fig_7}). The goal is to build a reference about orientation and size of the egg.

\begin{figure}[!tbp]
\centering
\subfigure{\includegraphics[width=0.4\textwidth]{fig_12}}
\subfigure{\includegraphics[width=0.4\textwidth]{fig_13}}\\
\subfigure{\includegraphics[width=0.4\textwidth]{fig_14}}
\subfigure{\includegraphics[width=0.4\textwidth]{fig_15}}
\caption{Initial estimation of egg ellipses using ray features. Note that the goal is to estimate an ellipse not to detect the egg chamber shape.}
\label{fig:fig_7} 
\end{figure}

\patentParagraph We define a scale parameter $ES$, so that, $ES=1$ is for the estimated ellipse. Then, we build five circles $R_{k}$ by varying $ES=\{0,\,0.3,\,0.6,\,0.9,\,1.2\}$, (see Fig~\ref{fig:fig_8}). For each circle $R$ a label histogram is built as follows:
\begin{equation}
   CLH_{i} = \sum C\left\{\bm{getLables}\left\{R_{i},R_{i+1}\right\}==i\right\} \mbox{ with } i=0,1,2,3\,
   \label{eq:eq_7}
\end{equation}
where $\bm{getLables}\{\cdot\}$ returns the labels of the pixels between circle $R_{i}$ and $R_{i+1}$. 

\begin{figure}[!htb]
\centering
\subfigure{\includegraphics[width=0.45\textwidth]{fig_16}}
\subfigure{\includegraphics[width=0.45\textwidth]{fig_17}}\\
\subfigure{\includegraphics[width=0.45\textwidth]{fig_18}}
\subfigure{\includegraphics[width=0.45\textwidth]{fig_19}}
\caption{Four concentric circles are built to characterize interior points. For each circle, a label histogram is built according the pixels that belong to the corresponding circle. \textbf{Top Row}: examples of interior points. \textbf{Bottom row}: examples of exterior points.}
\label{fig:fig_8} 
\end{figure}

\patentParagraph The average circle label histograms are shown in Fig.\ref{fig:fig_9}. 

\begin{figure}[!htb]
\centering
\subfigure[]{\includegraphics[width=0.49\textwidth]{fig_20}}
\subfigure[]{\includegraphics[width=0.49\textwidth]{fig_21}}\\
\subfigure[]{\includegraphics[width=0.49\textwidth]{fig_22}}
\subfigure[]{\includegraphics[width=0.49\textwidth]{fig_23}}
\caption{Average circle label histograms. Blue bars represent labels of interior points, while yellow bars represent labels of exterior points. \textbf{(a)} $CLH_{0}$, \textbf{(b)} $CLH_{0}$, \textbf{(c)} $CLH_{0}$, and \textbf{(d)} $CLH_{0}$.}
\label{fig:fig_9} 
\end{figure}

%--------------------------------------------------------
\clearpage
\section*{3D Ray Features.\label{sec:3drayfeatures}}

\patentParagraph The extension of 2D ray feature to 3D is straightforward, Hence, based on Eq. \ref{eq:eq_2}, we build a distance histogram  and consider not only $\theta$ but $\phi$ angle:
\begin{equation}
  DH_{i} = \sum_{x}^{N}\sum_{y}^{M} C\left\{B_{i}\leq D_{x,y}\left(I,P\right) < B_{i+1}\right\}\, 
\label{eq:eq_8}
\end{equation}
where $N$ and $M$ are the number of orientations in $\theta$ and $\phi$ respectively.

\patentParagraph Since the resolution of the stacks along Z-axis is poor, the $\phi$ step is not lineal. 
\begin{equation}
   STEP_{\phi} = \left\{
  \begin{array}{ll}
    \frac{\pi}{8} & \mbox{\textbf{if} } 0\leq\phi<\frac{\pi}{4}\,\&\,\frac{3pi}{4}\leq\phi<\pi \\
    \frac{\pi}{16} & \mbox{\textbf{otherwise}}
  \end{array} \right.
\label{eq:eq_9}
\end{equation}
 

\begin{figure}[!htb]
\centering
\subfigure[]{\includegraphics[width=0.45\textwidth]{fig_24}}
\subfigure[]{\includegraphics[width=0.45\textwidth]{fig_25}}
\caption{Visualization of 3D ray features over $\theta$ and $\phi$.}
\label{fig:fig_10} 
\end{figure}

\begin{figure}[!htb]
\centering
\subfigure[]{\includegraphics[width=0.9\textwidth]{fig_26}}\\
\subfigure[]{\includegraphics[width=0.9\textwidth]{fig_27}}
\caption{3D ray features in a germarium. Examples of \textbf{(a)} interior points and \textbf{(b)} exterior points.}
\label{fig:fig_11} 
\end{figure}


%--------------------------------------------------------
\clearpage
\section*{Sphere Histograms.\label{sec:spherehistograms}}



\begin{figure}[!htb]
\centering
\subfigure[]{\includegraphics[width=0.49\textwidth]{fig_28}}
\subfigure[]{\includegraphics[width=0.49\textwidth]{fig_29}}
\caption{\textbf{(a)}  and \textbf{(b)} .}
\label{fig:fig_12} 
\end{figure}

\begin{figure}[!htb]
\centering
\subfigure[]{\includegraphics[width=0.4\textwidth]{fig_32}}
\subfigure[]{\includegraphics[width=0.4\textwidth]{fig_33}}\\
\subfigure[]{\includegraphics[width=0.4\textwidth]{fig_34}}
\subfigure[]{\includegraphics[width=0.4\textwidth]{fig_35}}\\
\subfigure[]{\includegraphics[width=0.4\textwidth]{fig_35}}
\caption{\textbf{(a)} , \textbf{(b)} , \textbf{(c)} , \textbf{(d)} , and \textbf{(d)}.}
\label{fig:fig_13} 
\end{figure}

\begin{figure}[!htb]
\centering
\subfigure[]{\includegraphics[width=0.49\textwidth]{fig_30}}
\subfigure[]{\includegraphics[width=0.49\textwidth]{fig_31}}
\caption{\textbf{(a)}  and \textbf{(b)} .}
\label{fig:fig_14} 
\end{figure}

\begin{figure}[!htb]
\centering
\subfigure[]{\includegraphics[width=0.4\textwidth]{fig_36}}
\subfigure[]{\includegraphics[width=0.4\textwidth]{fig_37}}\\
\subfigure[]{\includegraphics[width=0.4\textwidth]{fig_38}}
\subfigure[]{\includegraphics[width=0.4\textwidth]{fig_39}}\\
\subfigure[]{\includegraphics[width=0.4\textwidth]{fig_40}}
\caption{\textbf{(a)} , \textbf{(b)} , \textbf{(c)} , \textbf{(d)} , and \textbf{(d)}.}
\label{fig:fig_15} 
\end{figure}




%----------------------------------------------------
%----------------------------------------------------
\clearpage
\bibliographystyle{alpha}
\bibliography{references}

\end{document} 