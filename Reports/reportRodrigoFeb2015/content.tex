%-------------------------------------------------------------------
% Latex template for Research Report
% Title: Automatic Analysis of Spatial Gene Expression Patterns
% Date: February 28th, 2015
%-------------------------------------------------------------------

%-------------------------------------------------------------------
\section{Introduction \label{sec:introduction}}

%-------------------------------------------------------------------
\subsection{Biological perspective \label{sec:biological_perspective}}
The cell is the fundamental structure of all known living organisms. It has the ability to regulate functions, replicate, and transmit information to the next generation. The body of a cell comprises complex macromolecules such as proteins, lipids, and nuclei acids embedded in a mass called cytoplasm. Nuclei acids, which are generally very large molecules, include deoxyribonucleic acid (DNA) that serves as the storage material for genetic information and ribonucleic acid (RNA) that carries genetic information from a cell to ribosomes. The ribosomes act as synthesizers of proteins, namely they produce a specific amino acid for assembling into a new protein \cite{CLANCY2008}. In summary, the information encoded in a gene (string of DNA) is used to control the assembly of a protein. This process is called \textbf{gene expression}.

The expression of genetic information is a very complex process involving hundreds of molecules and represents a means to explore and understand genetic interactions within biological development. Depending on the type of cell and its biological sate, specific proteins will appear at different levels and time. Hence, the importance of understanding this process is that gene expression provides information about the current state of the cell. 

Gene expression analysis is among the most commonly used methods in modern biology because it can provide quantitative information about the behavior of biological systems and may be a powerful tool for the diagnosis of a disease, the discovery of new drugs or tailoring therapies to specific pathologies \cite{LOVEN2012}. 

In the literature, classic methods are focused on measuring the expression of one gene at a time and not in any particular biological context. Nevertheless, two methods have been used successfully to determine gene expression patterns on a large scale: DNA microarrays \cite{KARAKACH2010} and RNA \textit{in situ} hybridization \cite{TOMANCAK2002}.

A DNA microarray is a flat solid support where a collection of microscopic DNA spots are attached. This technique provides a quantitative comparison of thousands of genes at once. On the other hand, RNA \textit{in situ} hybridization is a common approach based on the ability of acids to hybridize with complementary sequences enabling the visualization of broad gene expression patterns \cite{JUG2014}.

An excellent model for studying these phenomena is the fruit fly, \textit{Drosophila melanogaster}. In 1908, the embryologist Thomas Morgan from Columbia University cultured \textit{Drosophila} in large quantities in order to explore the existence of mutations. It was chosen due to its interesting attributes such as rapid generation time, ease culturing, low maintenance cost, and robustness against plagues and pathogens \cite{ARIAS2008}.   

\textit{Drosophila} oogenesis is of particular interest because it presents many aspects of cell and developmental biology. A female \textit{Drosophila} has two ovaries each containing 15–20 ovarioles and each of which can be considered as an egg production line \cite{BASTOCK2008}. The development of a cell into a mature oocyte or egg begins in a structure called germarium that contains somatic and germline stem cells and functions as an assembly line \cite{MONTELL2003}. The stem cells are divided asymmetrically to produce 15 nurse cells and an oocyte, which are surrounded by somatic follicle cells (border cells) to form an egg chamber, (see Figure \ref{fig:figure_1}). 

The development of the egg chamber has been divided into 14 stages grouped as follows: 1 to 6 are called early stages; 7 to 10 mid stages; and stages from 11 to 14 are known as late oogenesis \cite{ROTH2009}. After stage 14, the egg length is approaching a millimeter. 

\begin{figure}[!tbp]
    \centering
\includegraphics[width=0.6\textwidth]{figure_1}
    \caption{The production of \textit{Drosophila} egg chambers is accomplished in assembly lines called ovarioles. Note that the egg chambers become more mature moving to the right where one of the germline cells becomes the oocyte.}
    \label{fig:figure_1}
\end{figure}

%-------------------------------------------------------------------
\subsection{Motivation and related work \label{sec:motivation_related_work}}
The oogenesis provides an outstanding model for genetic traceability; ovarioles of an adult \textit{Drosophila} contain all the stages of development from germline stem cell to mature oocyte. Therefore, it can provide insights applicable to more complex processes such as vertebrate development and disease progression. However, mapping a gene requires analyzing hundred of egg chambers that have been segmented and aligned previously. This task is time-consuming and depends on trained observers. Hence, a reliable automated segmentation method would be useful to reduce time and bias. 

To the best of our knowledge, there is little literature addressing \textit{Drosophila} oogenesis segmentation, most of the state-of-the-art methods are devoted to segment and process sub-cellular embryogenesis data \cite{CASTRO2011}. Xiong et al. \cite{XIONG2006a} proposed a level set method for segmenting nuclei and cytoplasm; this approach relies on the idea that a curve from a given image can evolve to detect objects. Also graph-based methods have been used in \cite{CHEN2006,GHITA2014} to segment mitochondria, whereas Lucchi et al. \cite{LUCCHI2012} take advantage of superpixel segmentation to address the same problem. Object class based image segmentation is a novel approach that produces a pixel-level segmentation and then a conditional random field or another spatial coherency method is applied to refine the results \cite{VAKILI2011, LADICKY2009,SCHROFF2008}. Furthermore, in \cite{SHOTTON2006}, a joint approach incorporates appearance, shape, and context information for semantic segmentation.

Although the aforementioned methods are reasonably successful, sub-cellular imaging is commonly performed using electron microscopy modalities. Therefore, the existing approaches cannot be used on \textit{Drosophila} oogenesis directly. 

%-------------------------------------------------------------------
\subsection{Our tasks and organization \label{sec:task_organization}}
In the first part of this report, we will focus our attention on texture because it is a property of images and can reflect local variations that define shapes or objects. Our proposal is inspired from \cite{VARMA2005} where Varma and Zisserman introduced the concept of \textbf{textons} as the representative responses occurring after convolving an image with a filter bank. The collection of such responses is then clustered using \textsl{k}-means to produce a dictionary of visual words. Finally, the dictionary is used to calculate an average histogram that is employed as a probabilistic model of a specific texture. 

However, the main drawback of this approach is that textons characterize spatial properties of regions based on their patch size. Thus, small patches cannon represent regions due to the lack of information to build a proper histogram, while larger patches lead to a block-like segmentation \cite{JAVED2011}.

In the second part, we are interested on object selection, we want to determine if the segmented object is indeed an object of interest. Hence, we present the ray feature algorithm \cite{SMITH2009} which is able to describe irregular shapes based on distances from a given centroid to the nearest borders. ***

This report is organized as follows. In Section \ref{sec:material}, a description of the data is given. In Section \ref{sec:supertexton_segmentation}, we investigate the use of a pre-processing step to segment early stage \textit{Drosophila} egg chambers into four classes: \begin{inparaenum}[\bfseries i\normalfont)]\item nurse cells; \item follicle cells; \item cytoplasm; and \item background \end{inparaenum}. In Section \ref{sec:object_localization}, we propose a novel method for localization of valid \textit{Drosophila} egg chambers in a 3D fashion. ***

%-------------------------------------------------------------------
\section{Material \label{sec:material}}
We selected 54 early stage egg chambers from 3D-stacks of size 647$\times$1024$\times$24. They were captured using an optical sectioning fluorescence microscope. Contrary to a confocal microscope, which is used to produce 2D images, a sectioning fluorescence microscope takes a whole through-focus series of thin optical sections as the specimen is moved through Z-axis; then, 3D rendering is achieved by processing the sequentially recorded data set. Each stack contains two channels, the green one shows nuclei of cells that constitute egg chambers providing a common reference, while the magenta channel comes from specific RNA probes labeled with a fluorescent dye and was not considered for classification. Images of isolated egg chambers were obtained manually by expert biologists at Max Planck Institute of Molecular Cell Biology and Genetics in Dresden, Germany. It is worth to say that the dimensions of the egg chambers varied from one to another, (see Figure \ref{fig:figure_2}). 

\begin{figure}[!tbp]
    \centering
\includegraphics[width=0.5\textwidth]{figure_2}
    \caption{Examples of localized ovaries during oogenesis. Green signal shows nuclei of cells during an early stage of the occyte development, whereas magenta signal comes from the specific RNA expression.}
    \label{fig:figure_2}
\end{figure}

%-------------------------------------------------------------------
\section{Supertexton segmentation \label{sec:supertexton_segmentation}}
Classic texture segmentation approaches generally extract information from pixels or square regions. Here, we investigated the use of a pre-processing step using the simple linear iterative clustering (SLIC) algorithm \cite{ACHANTA2012}. SLIC groups perceptually-similar pixels and yields adherence to salient edges avoiding block-like shapes. This procedure is a way to utilize visual cues and produce coherent groups of related visual words by dividing an input image into small regions and a simple region-growing algorithm is performed to cluster them for later process.

For each group of superpixels that belongs to a single class, a set of features that corresponds to a filter bank response is computed. Then, a dictionary is constructed using representative features vectors that we call ``supertextons.'' 

Our proposal avoids building a histogram and distinguishes small regions that represent follicle and nurse cells from the background and cytoplasm in \textit{Drosophila} egg chambers successfully. Here, we propose a methodology composed of three stages: \begin{inparaenum}[\bfseries i\normalfont)] \item pre-segmentation with SLIC superpixels; \item construction of a supertexton dictionary; and \item classification. \end{inparaenum}

%-------------------------------------------------------------------
\subsection{Superpixels \label{sec:superpixels}}
Although there are several variants \cite{SHI2000,LEVINSHTEIN2009,VEDALDI2008}, SLIC has shown to outperform existing superpixel methods in performance because it limits the search space and computes clusters using \textit{k}-means algorithm within $2S\times 2S$ regions where $S\times S$ is the size of the superpixel, (see Figure \ref{fig:figure_3}). 

The algorithm uses an initial number of seeds sampled onto a regular grid. For each seed, $\{C_{i}|i=1,\,\ldots,\,K\}$, a region based on intensity values and connectivity is calculated iteratively. The number of seeds is determined by $K = {MN}/{S^{2}}$ where $M$ and $N$ are the width and height of the image, respectively. 

\begin{figure}[!tbp]
\centering
\input{resources/figure_3.tikz}
\caption{For each pixel $i$ in a $2S\times 2S$ region around $C_{i}$, SLIC computes \textit{k}-means clustering using distance $D$.} 
\label{fig:figure_3} 
\end{figure}

The original proposal generates superpixels based on color similarity and proximity. Here, we use a reduce distance $D$ that considers only gray values, $l$, between a cluster center $C_{k}$ and a pixel $i$ at positions $[x,y]$ as follows:
\begin{equation}
    \begin{aligned}
        d_{c} &= \sqrt{\left(l_{k}-l_{i}\right)^{2}} \\
        d_{s} &= \sqrt{\left(x_{k}-x_{i}\right)^{2} + \left(y_{k}-y_{i}\right)^{2}}
    \end{aligned}\,
    \label{eq:eq_1}
\end{equation}

In order to combine both distances a normalized factor $m$ is included:
\begin{equation}
    D = \sqrt{d^2_c + \left(\frac{d_s}{S}\right)^2m^2}
    \label{eq:eq_2}
\end{equation}
$m$ is related to region compactness. If $m \rightarrow 0$ then the adherence to borders is better, otherwise the region tends to a square shape. 

We set the initial superpixel size, $S = 10$, and regularizer, $m = 0.02$, (see Figure \ref{fig:figure_4}). 

\begin{figure}[!tbp]
  \hspace*{\fill}
  \subfloat[]{\includegraphics[width=0.3\textwidth]{figure_a4}}\hfill
  \subfloat[]{\includegraphics[width=0.3\textwidth]{figure_b4}}\hfill
  \subfloat[]{\includegraphics[width=0.3\textwidth]{figure_c4}}
 \hspace*{\fill}
  \caption{\textit{Drosophila} egg chamber segmented using SLIC superpixel of size: \textbf{(a)} $S = 10$, \textbf{(b)} $S = 20$, and \textbf{(c)} $S = 30$.}
  \label{fig:figure_4}
\end{figure}

% --------------------------------------------------------------------------
\subsection{Supertexton dictionary \label{sec:supertextons}}
The original texton approach is computed using patches or blocks. A common practice is to split up an input image into blocks of size $32\times 32$ or $64\times 64$ px. The blocks are convolved with a filter bank; then, for each pixel, a vector is build by storing the corresponding filter responses. The resulting vectors are divided into $k$ clusters called textons \cite{JAVED2011}. However, the size of follicle cells is not larger that 10 px. therefore, patches of such a size do not contain enough information to discriminate among classes.

In order to build a supertexton dictionary, we use a reduce set of the Leung-Malik filter bank ($LM_{18}$) \cite{LEUNG2001} by keeping only the maximum response across six orientations $\{0^{\circ}, 30^{\circ}, 60^{\circ}, 90^{\circ}, 120^{\circ}, 150^{\circ}\}$ on first and second Derivatives of Gaussian (DoG). This reduced representation is rotational invariance along three dyadic scales $\{\sqrt{2}, 2, 2\sqrt{2}\}$. Furthermore, we use eight Laplacian of Gaussian and four Gaussian filters with standard deviation $\sigma = \left\{\sqrt{2}, 2, 2\sqrt{2}, 4\right\}$ resulting in 18 filters, (see Figure \ref{fig:figure_5}). 

Let $T(x,y)$ be a segmented texture with SLIC and given a filter bank, $LM_{18}$, then the responses $F_i$ are computed as follows:
\begin{equation}
   F_i = T(x,y) \otimes LM_i
\label{eq:eq_3}
\end{equation}

\begin{figure}[!tbp]
    \hspace*{\fill}
    \subfloat[]{\label{fig:figure_5a}\includegraphics[width=0.4\textwidth]{figure_5a}}\hfill
    \subfloat[]{\label{fig:figure_5b}\includegraphics[width=0.4\textwidth]{figure_5b}}
    \hspace*{\fill}\\
    \centering
    \subfloat[]{\label{fig:figure_5c}\includegraphics[width=0.4\textwidth]{figure_5c}}
    \caption{\textbf{(a)} First and \textbf{(b)} second derivatives of Gaussian at 6 directions and 3 scales (We kept only the maximal value in every scale to achieve rotational invariance). \textbf{(c)} 8 Laplacian of Gaussian and 4 Gaussian filters}
    \label{fig:figure_5}
\end{figure}

For each superpixel that covers a single class, $\left\{C{i}\middle|i=1\,\ldots\,4\right\}$, the following features are evaluated, (see Figure \ref{fig:figure_6}):
\begin{itemize}
    \setlength{\itemsep}{0pt}
	\item Mean: $\mu_{i} = \displaystyle\frac{1}{N_{I}}\sum I_{F_{i}}$, where $I_{F_{i}}$ are the intensity values $\in F_{i}$  within the corresponding superpixel region and $N_{I}$ the number of intensity values.  
    \item Standard deviation: $\sigma_{i} = \sqrt{\frac{1}{N_{I}}\sum\left(I_{F_{i}}-\mu_{i}\right)^2}$
    \item Energy: $E_{i} = \frac{1}{N_{I}}\displaystyle\sum I_{F_{i}}^{2}$
    \item Average gradient: $G_{i} = \displaystyle\sum\left\|\nabla I_{F_{i}}\right\|$  
\end{itemize}

We included a normalization process to guarantee that features be in the $[-1,\,1]$ range. We assumed that each feature, $x$, is normally distributed. Thus, according to \cite{AKSOY2001} the following transformation was applied:  
\begin{equation}
   \tilde{x} = \frac{x-\mu_{x}}{2\left(3\sigma_{x}+1\right)}
\label{eq:eq_4}
\end{equation}
where $m_{x}$ and $\sigma_{x}$ are the mean and standard deviation of the corresponding feature, respectively.

Hence, a rescaled feature vector is build as follows:
\begin{equation}
   \overline{f} = \big[\tilde{\mu_{1}},\,\tilde{\sigma_{1}},\,\tilde{E_{1}},\,\tilde{G_{1}},\,\ldots,\,\tilde{\mu_{18}},\,\tilde{\sigma_{18}},\,\tilde{E_{18}},\,\tilde{G_{18}}\big]
\label{eq:eq_5}
\end{equation}
where $L$ corresponds to the $L$-filter response. 

Then, the \textit{k}-means clustering is applied to the feature vectors. The clusters of the classes are called supertextons. Therefore, the dictionary will consist of $C \times k$ supertextons. In our experiments, $k$ is the $10\%$ of the total of the feature vectors per class.


\begin{figure}[!tbp]
\centering
\input{resources/figure_6.tikz}
\caption{The input texture $T(x,y)$ is convolved with the filter bank $LM_{18}$. For each superpixel (color dots) a response vector is computed, then these vectors are clustered using the k-means algorithm. The resulting centers are called textons.} 
\label{fig:figure_6} 
\end{figure}


% --------------------------------------------------------------------------
\subsection{Classification \label{sec:classification}}
The test images were pre-segmented and the feature vectors were calculated as we described in the previous subsection. We used \textit{k}-NN classifier with $k=6$ which approximately represents less than 1\% of the vectors in our dataset and assigned the input feature vectors to the class with the highest number of members within the neighborhood using the Euclidean distance as a metric. We experimented with $k=10$ and $k=15$, however, the variable did not affect the results significantly.

% --------------------------------------------------------------------------
\subsection{Experimental Results \label{sec:results}}
We compared our proposal against two methods: first, a pixel-based classification method. In this method, the feature vectors were built using pixel intensities after filtering the ovaries with $LM_{18}$ filter bank. Then, such feature vectors were clustered and classified in the same way as supertextons, (see results in Table \ref{tab:tab_1}). Second, the original Varma and Zisserman's texton approach. We used textons patches of size 10$\times$10 px. (see results in Table \ref{tab:tab_2}). The results of our proposal are summarized in Table \ref{tab:tab_3}.

In order to assess segmentation, leave-one-out cross-validation was used. The precision (PR) and sensitivity (SE) were computed and compared with a manual segmentation in a pixel-wise fashion. We also present $F_{1}$-Score $=2\frac{PR \times SE}{PR+SE}$, which is a measure of accuracy and reaches its best value at $1$ and worst score at $0$.

\begin{table}[!bbp]
\rowcolors{5}{AliceBlue}{}
\renewcommand{\arraystretch}{1.5}
\caption{Classification results of a pixel-based classification. The values represent mean rates after the classification of 54 images. All data are expressed in $(\%)$. The values between parenthesis represent standard deviations.}
\label{tab:tab_1}
\centering
{\footnotesize
\begin{tabular*}{0.72\textwidth}{cccc}
\toprule
& \multicolumn{3}{c}{Pixels} \\
\cmidrule[0.5pt](l{5pt}r{5pt}){2-4}
Class & PR & SE & $F_{1}$-Score \\
\hline
\hline
Background & 96.83 ($\pm$ 3.32) & 80.80 ($\pm$ 14.41) & 87.22 ($\pm$ 9.50) \\
Follicle cells & 72.06 ($\pm$ 6.76) & 64.92 ($\pm$ 8.19) & 67.76 ($\pm$ 4.60) \\
Nurse cells & 64.33 ($\pm$ 10.12) & 71.69 ($\pm$ 7.36) & 67.11 ($\pm$ 5.77) \\
Cytoplasm & 67.11 ($\pm$ 10.03) & 84.26 ($\pm$ 5.95) & 74.01 ($\pm$ 6.04) \\
\bottomrule
\end{tabular*}
}
\end{table}

\begin{table}[!bbp]
\rowcolors{5}{AliceBlue}{}
\renewcommand{\arraystretch}{1.5}
\caption{Classification results of Varma and Zisserman's texton approach. The values represent mean rates after the classification of 54 images. All data are expressed in $(\%)$. The values between parenthesis represent standard deviations.}
\label{tab:tab_2}
\centering
{\footnotesize
\begin{tabular*}{0.72\textwidth}{cccc}
\toprule
& \multicolumn{3}{c}{Textons} \\
\cmidrule[0.5pt](l{5pt}r{5pt}){2-4}
Class & PR & SE & $F_{1}$-Score \\
\hline
\hline
Background & 99.82 ($\pm$ 0.82) & 45.19 ($\pm$ 23.35) & 59.18 ($\pm$ 23.24) \\
Follicle cells & 37.50 ($\pm$ 7.37) & 85.85 ($\pm$ 7.38) & 52.20 ($\pm$ 7.14) \\
Nurse cells & 74.55 ($\pm$ 13.84) & 63.04 ($\pm$ 9.25) & 67.59 ($\pm$ 7.28) \\
Cytoplasm & 72.18 ($\pm$ 20.57) & 47.41 ($\pm$ 20.73) & 56.28 ($\pm$ 18.20) \\
\bottomrule
\end{tabular*}
}
\end{table}

\begin{table}[!bbp]
\rowcolors{5}{AliceBlue}{}
\renewcommand{\arraystretch}{1.5}
\caption{Supertexton classification rates. The values represent mean rates after the classification of 54 images. All data are expressed in $(\%)$. The values between parenthesis represent standard deviations. Bold values represent the best rates.}
\label{tab:tab_3}
\centering
{\footnotesize
\begin{tabular*}{0.72\textwidth}{cccc}
\toprule
& \multicolumn{3}{c}{Supertextons} \\
\cmidrule[0.5pt](l{5pt}r{5pt}){2-4}
Class & PR & SE & $F_{1}$-Score \\
\hline
\hline
Background & 95.98 ($\pm$ 3.96) & 90.99 ($\pm$ 8.25) & \textbf{93.15} ($\pm$ 4.90) \\
Follicle cells & 82.90 ($\pm$ 6.50) & 79.14 ($\pm$ 7.17) & \textbf{80.66} ($\pm$ 4.66) \\
Nurse cells & 79.64 ($\pm$ 9.52) & 85.22 ($\pm$ 5.03) & \textbf{81.87} ($\pm$ 4.99) \\
Cytoplasm & 80.18 ($\pm$ 10.06) & 86.68 ($\pm$ 4.46) & \textbf{82.84} ($\pm$ 5.75) \\
\bottomrule
\end{tabular*}
}
\end{table}

\begin{figure}[!tbp]
    \centering
    \subfloat{\includegraphics[width=0.18\textwidth]{figure_2a}}
    \subfloat{\includegraphics[width=0.18\textwidth]{figure_2b}}
    \subfloat{\includegraphics[width=0.18\textwidth]{figure_2c}}
    \subfloat{\includegraphics[width=0.18\textwidth]{figure_2e}}
    \subfloat{\includegraphics[width=0.18\textwidth]{figure_2d}}\\
    \subfloat{\includegraphics[width=0.18\textwidth]{figure_3a}}
    \subfloat{\includegraphics[width=0.18\textwidth]{figure_3b}}
    \subfloat{\includegraphics[width=0.18\textwidth]{figure_3c}}
    \subfloat{\includegraphics[width=0.18\textwidth]{figure_3e}}
    \subfloat{\includegraphics[width=0.18\textwidth]{figure_3d}}\\

    \subfloat[]{\label{fig:11}{\includegraphics[width=0.18\textwidth]{figure_4a}}}
    \subfloat[]{\label{fig:12}{\includegraphics[width=0.18\textwidth]{figure_4b}}}
    \subfloat[]{\label{fig:13}{\includegraphics[width=0.18\textwidth]{figure_4c}}}
    \subfloat[]{\label{fig:14}{\includegraphics[width=0.18\textwidth]{figure_4e}}}
    \subfloat[]{\label{fig:15}{\includegraphics[width=0.18\textwidth]{figure_4d}}}\\
    \caption{Examples of segmentation in \textit{Drosophila} egg chambers. \textbf{(a)} the original egg chamber; \textbf{(b)} the corresponding ground-truth; \textbf{(c)} supertexton segmentation; \textbf{(d)} pixel-based segmentation and; \textbf{(e)} texton segmentation.}
    \label{fig:figure_7}
\end{figure}


%-------------------------------------------------------------------
\section{Object localization \label{sec:object_localization}}

%-------------------------------------------------------------------
\subsection{Ray features \label{sec:ray_features}}
The original Ray feature proposal contains four distinct image features. Given an image $I$, a location $l$, and direction $\theta$, Ray features test some property of the nearest edge location in direction $\theta$ ***

\begin{figure}[!tbp]
\centering
\input{resources/figure_8.tikz}
\caption{***}
\label{fig:figure_8} 
\end{figure}


%-------------------------------------------------------------------
\section*{Acknowledgments}
This project is supported by the European social fund within the framework ``Support of inter-sectoral mobility and quality enhancement of research teams at Czech Technical University in Prague,'' CZ.1.07/2.3.00/30.0034 and the Czech Science Foundation project P202/12/2032.
