
"""
Copy images from dir2 that has a key contained in dir1 into dir3.

Note that some params are hardcoded.
"""

import os
from shutil import copyfile

# Directories

# my segmented images with labels
# dir1 = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/slice_struct/all'
dir1 = '/home/marek/CMP/data2_segmented'

# source images that will be filtered
#dir2 = '/datagrid/Medical/microscopy/drosophila/RESULTS/segment_ovary_slices_selected_superTextons-Python_visu'
dir2 = '/home/marek/CMP/data6'

# target directory for storing images
#target_dir = '/datagrid/Medical/microscopy/drosophila/TEMPORARY/selected_segment_ovary_slices_selected_superTextons-Python_visu'
target_dir = '/home/marek/CMP/data6_selected'


files1 = os.listdir(dir1)
files2 = os.listdir(dir2)

files1.sort()
files2.sort()

keys1 = map(lambda x: x[6:-4], files1)
keys2 = map(lambda x: x[6:-4], files2)

keys_both = list(set(keys1).intersection(keys2))

prefix = files2[0][0:6]
suffix = files2[0][-4:]

for key in keys_both:
    file_name = prefix + key + suffix
    file_path = dir2 + '/' + file_name
    dest_path = target_dir + '/' + file_name

    copyfile(file_path, dest_path)

    print('Copping file ' + file_name)
