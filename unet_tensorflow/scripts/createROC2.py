"""
This is a script for computing ROC for a given model and data (testing images).
"""

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

from utils import imageUtils
from utils import loggingUtils
from utils.utils import copute_fpr_tpr

log = loggingUtils.get_logger(__name__)

import setting.rootSetting as Setting
from utils.imageUtils import apply_thresholding
from scripts.visualization_of_two_datasets import get_data


def compute_fpr_tpr(orig_images, labels, segm_jirka, segm_unet):

    n_points = 100

    fprs_total = (n_points + 2) * [0]
    tprs_total = (n_points + 2) * [0]

    fprs_total[n_points + 1] = 1
    tprs_total[n_points + 1] = 1

    n_images = len(orig_images)

    for image_index, image, im_jirka, segm_unet, label in zip(range(len(orig_images)), orig_images, segm_jirka, segm_unet,
                                                            labels):

        # compute FP/FN for multiple thresholds
        for i in range(1, n_points + 1):

            # threshold
            size = 1 - i / float(n_points + 2)

            binary_segmentation = imageUtils.crop_image(segm_unet)

            binary_segmentation = apply_thresholding(binary_segmentation, threshold=size)

            # save_image(binary_segmentation, 'tmp/test.png')

            fpr, tpr = copute_fpr_tpr(binary_segmentation, label)

            fprs_total[i] += fpr / float(n_images)
            tprs_total[i] += tpr / float(n_images)

            # assert np.all(binary_segmentation <= 1)
            # assert np.all(0 <= known)
            # assert np.all(0 <= Y)
            # assert np.all(0 <= Y * known)
            # assert 0 <= np.all((1 - Y) * known) <= 1
            # assert np.all(0 <= (1 - binary_segmentation) * Y * known)
            # assert fp >= 0
            # assert fn >= 0

            # print(fpr, fnr)

    return fprs_total, tprs_total


def create_roc(file_path):
    # create and save roc curve
    plt.plot(fprs_total, tprs_total, color='green', label="U-Net")

    # new data (data5): fpr, tpr: 0.206786412835 0.975407803934

    # 0.207 0.990
    plt.plot([0.207], [0.990], marker='o', markersize=3, color="red", label="referential method v1")

    # fpr, tpr: 0.149134541177 0.968356558043
    plt.plot([0.149], [0.968], marker='o', markersize=3, color="purple", label="referential method v3")

    # plt.title('ROC: ')
    plt.xlabel('FPR')
    plt.ylabel('TPR')

    # plt.show()
    plt.savefig(file_path)

    log.info('fig saved')


# ---- Public Interface ----


if __name__ == "__main__":
    # Get the data
    orig_images, labels, segm_jirka, segm_unet = get_data()

    # Compute roc data
    fprs_total, tprs_total = compute_fpr_tpr(orig_images, labels, segm_jirka, segm_unet)

    # --- Plot graph ---

    log.info('Creating ROC graph')

    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])

    create_roc(file_path='tmp/ROC_2.png')

    # --- Zoom ---

    size = 0.05
    center = [.17, .95]

    xmin, xmax = np.max([0.0, center[0] - size]), np.min([1.0, center[0] + size])
    ymin, ymax = np.max([0.0, center[1] - size]), np.min([1.0, center[1] + size])

    print(xmin, xmax, ymin, ymax)

    plt.xlim([xmin, xmax])
    plt.ylim([ymin, ymax])

    # --- Plot graph 2 ---

    create_roc(file_path='tmp/ROC_zoom_2.png')

    # TODO how to test this?
    # TODO need to comapare the same sets (eighter all images or just test images) -> see coec from eval script


    # - are there any random factors?
    # - is resolution of the mast same?
