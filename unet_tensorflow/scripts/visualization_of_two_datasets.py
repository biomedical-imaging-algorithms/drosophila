"""
Compare 1) my segmnetation 2) Jirka`s model and 3) labels.
"""

# TODO BUG: some times it loads segmented images instead of original (seems to occur randomly)

import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

from data import dataLoader, dataProvider, imagePaths
from utils import imageUtils
from utils.imageUtils import *


def get_images():
    """
    Get images from paths specified in imagePaths.
    """
    reload(imagePaths)
    images = dataLoader.read_images_tiles()
    images = dataProvider.get_tiles(images)

    # remove other data we dont need
    images = map(lambda tuple: tuple[0], images)

    return images


def get_labels_and_sources():
    """
    Get labels from paths specified in imagePaths.
    """
    reload(imagePaths)
    images = dataLoader.read_images_tiles()

    sources = map(lambda tuple: tuple[4], images)

    images = dataProvider.get_tiles(images)

    images = map(lambda tuple: tuple[1], images)

    return images, sources


def plot_images1(image_index, image, im_jirka, im_unet, label):
    f, axarr = plt.subplots(1, 4)

    # plot image
    axarr[0].imshow(image, cmap=plt.cm.gray, interpolation='nearest')
    axarr[0].set_axis_off()

    axarr[1].imshow(im_jirka, cmap=plt.cm.gray, interpolation='nearest')
    axarr[1].set_axis_off()

    axarr[2].imshow(im_unet, cmap=plt.cm.gray, interpolation='nearest')
    axarr[2].set_axis_off()

    axarr[3].imshow(imageUtils.uncrop_image(label, True), cmap=plt.cm.gray, interpolation='nearest')
    axarr[3].set_axis_off()

    #     visual.plot_on_grid([image, truth], n_cols = 2)

    # plt.imshow(crop_image(image), cmap=plt.cm.gray, interpolation='nearest')  #
    #     plt.imshow(truth, cmap=plt.cm.gray, interpolation='nearest')
    # plt.contour(truth, linewidths=3, color='green', alpha=0.5, levels=[0.5])
    # plt.show()

    # plt.imshow(truth, cmap=plt.cm.gray, interpolation='nearest')  #
    # plt.set_axis_off()
    path = 'tmp/visualization/segmented_{}.png'.format(image_index)
    plt.savefig(path, bbox_inches='tight', pad_inches=0)
    plt.show()


def plot_images2(image_index, original_image, segm_jirka, segm_unet, label):
    class_threshold = 0.5
    linewidths = 0.5
    alpha = 0.8

    uncroped_label = imageUtils.uncrop_image(label)

    f, axarr = plt.subplots(1, 3)
    countours = []

    # --- Plot original image with contours ---
    ax = axarr[1]
    ax.imshow(original_image, cmap=plt.cm.gray, interpolation='nearest', vmin=0, vmax=1)  #
    ax.set_axis_off()
    # axarr[0].set_title("Original Image")



    ax.contour(segm_jirka, colors='red', label='', linewidths=linewidths, alpha=alpha, levels=[class_threshold], )

    cnt = ax.contour(segm_unet, colors='yellow', linewidths=linewidths, alpha=alpha, levels=[class_threshold])  #
    countours.append(cnt)

    cnt = ax.contour(uncroped_label, colors='green', linewidths=linewidths, alpha=alpha, levels=[class_threshold])  #
    countours.append(cnt)

    ax.clabel(cnt)

    # --- original_image ---
    ax = axarr[0]
    ax.imshow(original_image, cmap=plt.cm.gray, interpolation='nearest')  # , vmin=0, vmax=1
    ax.set_axis_off()

    # --- Label ---
    ax = axarr[2]
    ax.imshow(uncroped_label, cmap=plt.cm.gray, interpolation='nearest', vmin=0, vmax=1)  #
    ax.set_axis_off()

    path = 'tmp/visualization/countours_{}.png'.format(image_index)
    plt.savefig(path, bbox_inches='tight', pad_inches=0)

    # plt.tight_layout()
    plt.show(countours)


def plot_images3(image_index, original_image, segm_jirka, segm_unet, label, src):

    class_threshold = 0.5
    linewidths = 0.5
    alpha = 0.8

    segm_unet = imageUtils.apply_thresholding(segm_unet, threshold=class_threshold)

    uncroped_label = uncrop_image(label, show_unknown_label=True)

    f, axarr = plt.subplots(2, 4)
    countours = []

    # better to have same size as ground truth & output of the network?
    original_image = crop_image(original_image)
    segm_jirka = crop_image(segm_jirka)
    segm_unet = crop_image(segm_unet)
    uncroped_label = crop_image(uncroped_label)

    # --- Plot original ---
    ax = axarr[0, 0]
    ax.imshow(original_image, cmap=plt.cm.gray, interpolation='nearest', vmin=0, vmax=1)  #
    ax.set_axis_off()
    ax.set_title("Original Image")

    # --- Plot original image with contours ---
    ax = axarr[0, 1]
    ax.imshow(original_image, cmap=plt.cm.gray, interpolation='nearest', vmin=0, vmax=1)  #
    ax.set_axis_off()
    ax.set_title("Original Image with Cont.")

    ax.contour(segm_jirka, colors='red', label='Jirka', linewidths=linewidths, alpha=alpha, levels=[class_threshold])

    cnt = ax.contour(segm_unet, colors='yellow', label='U-Net', linewidths=linewidths, alpha=alpha,
                     levels=[class_threshold])  #
    countours.append(cnt)

    cnt = ax.contour(uncroped_label, colors='green', label='Label', linewidths=linewidths, alpha=alpha,
                     levels=[class_threshold])  #
    countours.append(cnt)

    # ax.clabel(cnt, inline=False)


    # --- Label ---
    ax = axarr[1, 3]
    ax.imshow(uncroped_label, cmap=plt.cm.gray, interpolation='nearest', vmin=0, vmax=1)  #
    ax.set_axis_off()
    ax.set_title("Ground Truth")

    # --- Segmentation 1 ---
    ax = axarr[1, 0]
    ax.imshow(segm_jirka, cmap=plt.cm.gray, interpolation='nearest', vmin=0, vmax=1)  #
    ax.set_axis_off()
    ax.set_title("SuperTextons")

    # --- Segmentation 2 ---
    ax = axarr[1, 1]
    ax.imshow(segm_unet, cmap=plt.cm.gray, interpolation='nearest', vmin=0, vmax=1)  #
    ax.set_axis_off()
    ax.set_title("U-Net")


    # --- FP / FN ---
    label = np.copy(uncroped_label)
    label[label == 0.5] = 2
    rgb_unet = create_false_positives_negatives_rgb_image(original_image, segm_unet, label)
    rgb2_jirka = create_false_positives_negatives_rgb_image(original_image, segm_jirka, label)

    ax = axarr[0, 2]
    ax.imshow(rgb2_jirka)
    ax.set_axis_off()
    ax.set_title("SuperTextons FP/FN")

    ax = axarr[0, 3]
    ax.imshow(rgb_unet)
    ax.set_axis_off()
    ax.set_title("U-Net FP/FN")


    # indicator where the segmentations do not agree
    diff = np.abs(segm_jirka - segm_unet)

    # where thay differ, label in not unknown and jirka != lebel
    unet_better = 2 * np.abs(diff * (uncroped_label - 0.5) * (uncroped_label - segm_jirka))
    jirka_better = 2 * np.abs(diff * (uncroped_label - 0.5) * (uncroped_label - segm_unet))

    comparison = 0.5 * unet_better + jirka_better

    ax = axarr[1, 2]
    ax.imshow(comparison, cmap='plasma', interpolation='nearest', vmin=0, vmax=1)  # cmap=plt.cm.gray,
    ax.set_axis_off()
    ax.set_title("Difference")

    # Add title
    # plt.title(src)
    name = src.split('/')[-1]
    fig = plt.gcf()
    fig.suptitle(name, fontsize=14)

    # --- Save the figure ---
    path = 'tmp/visualization/all_{}.png'.format(image_index)
    plt.savefig(path, bbox_inches='tight', pad_inches=0)

    # plt.tight_layout()
    plt.show(countours)


def get_data():
    Setting.augment_images = False

    # original images + ground truth
    orig_images = get_images()

    # remove images that are not present in other datasets
    del orig_images[13]
    del orig_images[20]

    labels, sources = get_labels_and_sources()
    del labels[13]
    del labels[20]

    del sources[13]
    del sources[20]

    # Jirka v3
    Setting.dataset_for_task = 'evaluate segmented'
    Setting.segmented_images_dir = '/home/marek/CMP/data6_selected/binary/'
    segm_jirka = get_images()

    # My segmentation
    Setting.segmented_images_dir = '/home/marek/CMP/data2_segmented/'
    segm_unet = get_images()

    del segm_unet[13]
    del segm_unet[20]

    return orig_images, labels, segm_jirka, segm_unet, sources


# --- Main ---

if __name__ == "__main__":

    # Get the data
    orig_images, labels, segm_jirka, segm_unet, sources = get_data()

    # Create and store visualization
    sns.set_style("white")

    for image_index, image, im_jirka, im_unet, label, src in zip(range(len(orig_images)), orig_images, segm_jirka, segm_unet,
                                                            labels, sources):
        # plot_images1(image_index, image, im_jirka, im_unet, label)
        # plot_images2(image_index, image, im_jirka, im_unet, label)
        plot_images3(image_index, image, im_jirka, im_unet, label, src)
