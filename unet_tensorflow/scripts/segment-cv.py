

# TODO dellete this

"""
For each split in crossvalidatin: segment testing set using model trained on the testing set.
"""



import tensorflow as tf

from data.dataProvider import get_tiles, imageUtils
from data.dataProvider import get_test_images
from utils import loggingUtils
from utils.imageUtils import apply_thresholding

log = loggingUtils.get_logger(__name__)


import setting.rootSetting as Setting
from variables import modelSaver
from variables import variablesManager


def __load_model__(model_name):
    # get model path
    model_path = modelSaver.__get_model_path(exp_name=model_name)

    # get TF variables
    nn, stats = variablesManager.get_tf_variables()

    sess = tf.Session()
    saver = tf.train.Saver()

    # Get values of all TF variables
    saver.restore(sess, model_path)

    return nn, stats, sess


def __create_batch_from_testing_set__():
    """

    :return: X(images), Y (ground truths), W (weights)
    """

    # Get images
    images = get_test_images()

    # Augment them
    images = get_tiles(images)

    X, Y, W = imageUtils.convert_images_to_tensors(images)

    return X, Y, W


# ---- Public -----

models = [
    'test165-cv0-momentum-nobatchnorm-halmos',
    'test169-cv1-momentum-nobatchnorm-halmos',
    'test170-cv2-momentum-nobatchnorm-halmos',
    'test171-cv3-momentum-nobatchnorm-halmos',
    'test172-cv4-momentum-nobatchnorm-halmos'
]


def segment_testing_images(cv_test_set_index):
    Setting.cv_test_set_index = i

    model_name  = models[cv_test_set_index ]

    # TOD do we need restart?
    # Load the model
    log.info('Loading model ...')
    nn, stats, sess = __load_model__(model_name)

    n_images = x.shape[0]

    log.info('Segmenting images ...')

    log.info('n_images {}'.format(n_images))


    for j in range(n_images):

        X = x[j:j + 1, :, :]
        Y = y[j, :, :, 1]

        # get segmentation
        segmented_image = sess.run(nn.output_map,
                                   feed_dict={
                                       nn.x: X,
                                       nn.keep_prob: 1.0,
                                       nn.do_update: False
                                   })
        segmented_image = segmented_image[0, :, :, 1]

        # compute FP/FN for multiple thresholds
        n_points = 100

        binary_segmentation = apply_thresholding(segmented_image, threshold=0.5)

        # TODO need path

        save_image(binary_segmentation)

        log.info('fig saved')


for i in range(Setting.cv_folds):
    segment_testing_images(i)
