"""
Merge classes to get binary segmentation.
"""

import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from itertools import product

from utils import loggingUtils

log = loggingUtils.get_logger(__name__)

# dir1 = '/home/marek/CMP/data4'

dir1 = '/home/marek/CMP/data6_selected'


def convert_image1(im):

    xsize = np.size(im, 0)
    ysize = np.size(im, 1)

    im2 = np.zeros((xsize, ysize))

    # convert to grayscale image
    for x, y in product(range(xsize), range(ysize)):
        # dark blue is background, scripts colors are foreground (i.e. cells)
        im2[x, y] = 255 if im[x, y, 2] > 0 or im[x, y, 1] > 0 else 0  # im[x, y, 0]  # np.average(im[x, y, :])
    return im2


def convert_image2(im):
    xsize = np.size(im, 0)
    ysize = np.size(im, 1)

    log.info('size {}x{}'.format(xsize, ysize))

    im2 = np.zeros((xsize, ysize))

    # convert to grayscale image
    for x, y in product(range(xsize), range(ysize)):
        im2[x, y] = 255 if im[x, y] > 0 else 0
    return im2


image_files = os.listdir(dir1)

log.info('Images found: {}'.format(len(image_files)))

for image_file in image_files:
    source_path = dir1 + '/' + image_file
    target_path = dir1 + '/binary/' + image_file

    log.info('Processing image ' + source_path + ', output image: ' + target_path)

    # im = cv2.imread(source_path, -1).astype(np.float64) #
    im = cv2.imread(source_path, -1)
    # print(im)

    log.info('Image loaded. Dim: {}'.format(np.ndim(im)))

    if np.ndim(im) == 0:
        log.info('Skipping image')
        continue

    im2 = convert_image2(im)

    # save image
    plt.imsave(target_path, im2.astype(np.uint8), vmin=0, vmax=255, cmap='gray')
