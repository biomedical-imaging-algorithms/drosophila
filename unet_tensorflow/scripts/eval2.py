"""
Evaluation of segmented images, i.e. comparing with ground truth and computing error.
"""

import numpy as np

from utils import loggingUtils

log = loggingUtils.get_logger(__name__)

from utils.mathUtils import compute_mse
from utils.utils import copute_fpr_tpr

from setting import rootSetting as Setting
from data import dataProvider
from data.dataProvider import get_tiles, imageUtils
from utils.imageUtils import apply_thresholding

# load segmented images instead of original
Setting.augment_images = False
Setting.dataset_for_task = 'evaluate segmented'

# Setting.segmented_images_dir = '/home/marek/CMP/data4/binary/'
Setting.segmented_images_dir = '/home/marek/CMP/data6_selected/binary/'

# Setting.segmented_images_dir = '/home/marek/CMP/data2/segmented/slice_struct/stage1/'

# Get all images, where image is segmented image
images = dataProvider.get_all_images()

images = get_tiles(images)

# TODO compare with ground truth + compute error, etc.

log.info('Images loaded')

# --- compute l2 loss and pixel-wise loss (for visualization) ---

# sum of mse (mean squared error) for each pixel
mse_sum = 0

fpr_total, tpr_total = 0, 0

for segmented_image, ground_truth, wtmap, image_src in images:

    segmented_image = imageUtils.crop_image(segmented_image)

    # convert to 0/1 values
    segmented_image = apply_thresholding(segmented_image, threshold=0)


    # compute mean squared error
    mse = compute_mse(ground_truth, segmented_image)
    mse_sum += mse

    # compute false positives rate and true positives rate
    fpr, tpr = copute_fpr_tpr(segmented_image, ground_truth)

    fpr_total += fpr / len(images)
    tpr_total += tpr / len(images)

# mean squared error
mse_avg = mse_sum / len(images)

log.info('mse_avg: {}'.format(mse_avg))

log.info('fpr, tpr: {} {}'.format(fpr_total, tpr_total))

# TODO test this
