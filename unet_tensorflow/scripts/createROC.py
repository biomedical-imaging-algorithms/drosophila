"""
This is a script for computing ROC for a given model and data (testing images).
"""

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

from utils import loggingUtils
from utils.utils import copute_fpr_tpr

log = loggingUtils.get_logger(__name__)

import setting.rootSetting as Setting
from utils.imageUtils import apply_thresholding
from variables import modelSaver
from variables import variablesManager
from data.dataLoader import save_image
from data.dataProvider import get_test_images, get_tiles
from utils import imageUtils


def __load_model__(model_name):
    # get model path
    model_path = modelSaver.__get_model_path(exp_name=model_name)

    # get TF variables
    nn, stats = variablesManager.get_tf_variables()

    sess = tf.Session()
    saver = tf.train.Saver()

    # Get values of all TF variables
    saver.restore(sess, model_path)

    return nn, stats, sess


def __create_batch_from_testing_set__():
    """

    :return: X(images), Y (ground truths), W (weights)
    """

    # Get images
    images = get_test_images()

    # Augment them
    images = get_tiles(images)

    X, Y, W = imageUtils.convert_images_to_tensors(images)

    return X, Y, W


def create_roc(file_path):
    # create and save roc curve
    plt.plot(fprs_total, tprs_total, color='green', label="U-Net")

    # new data (data5): fpr, tpr: 0.206786412835 0.975407803934

    # 0.207 0.990
    plt.plot([0.207], [0.990], marker='o', markersize=3, color="red", label="referential method v1")

    # fpr, tpr: 0.149134541177 0.968356558043
    plt.plot([0.149], [0.968], marker='o', markersize=3, color="purple", label="referential method v3")

    # plt.title('ROC: ')
    plt.xlabel('FPR')
    plt.ylabel('TPR')

    # plt.show()
    plt.savefig(file_path)

    log.info('fig saved')


# ---- Public Interface ----


# --- load model ---

# TODO rather load segmented images?


# note: this is still cv0 (just wrong naming)
model_name = 'test168-cv3-momentum-nobatchnorm-halmos'

Setting.cv_test_set_index = 0
Setting.augment_images = False

# Load the model
log.info('Loading model ...')
nn, stats, sess = __load_model__(model_name)

# create batch from test images
log.info('Creating batch ...')
x, y, w = __create_batch_from_testing_set__()

n_images = x.shape[0]

log.info('Segmenting images ...')

log.info('n_images {}'.format(n_images))

n_points = 100

fprs_total = (n_points + 2) * [0]
tprs_total = (n_points + 2) * [0]

fprs_total[n_points + 1] = 1
tprs_total[n_points + 1] = 1

for j in range(n_images):

    X = x[j:j + 1, :, :]
    Y = y[j, :, :, 1]

    # get segmentation
    segmented_image = sess.run(nn.output_map,
                               feed_dict={
                                   nn.x: X,
                                   nn.keep_prob: 1.0,
                                   nn.do_update: False
                               })
    segmented_image = segmented_image[0, :, :, 1]

    # compute FP/FN for multiple thresholds
    for i in range(1, n_points + 1):
        print('c')

        # threshold
        size = 1 - i / float(n_points + 2)

        binary_segmentation = apply_thresholding(segmented_image, threshold=size)
        # save_image(binary_segmentation, 'tmp/test.png')

        fpr, tpr = copute_fpr_tpr(binary_segmentation, Y)

        fprs_total[i] += fpr / float(n_images)
        tprs_total[i] += tpr / float(n_images)

        print('d')

        # assert np.all(binary_segmentation <= 1)
        # assert np.all(0 <= known)
        # assert np.all(0 <= Y)
        # assert np.all(0 <= Y * known)
        # assert 0 <= np.all((1 - Y) * known) <= 1
        # assert np.all(0 <= (1 - binary_segmentation) * Y * known)
        # assert fp >= 0
        # assert fn >= 0

        # print(fpr, fnr)




# --- Plot graph ---

log.info('Creating ROC graph')

plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])

create_roc(file_path='tmp/ROC_2.png')

# --- Zoom ---

size = 0.05
center = [.17, .95]

xmin, xmax = np.max([0.0, center[0] - size]), np.min([1.0, center[0] + size])
ymin, ymax = np.max([0.0, center[1] - size]), np.min([1.0, center[1] + size])

print(xmin, xmax, ymin, ymax)

plt.xlim([xmin, xmax])
plt.ylim([ymin, ymax])

# --- Plot graph 2 ---

create_roc(file_path='tmp/ROC_zoom_2.png')

# TODO how to test this?
# TODO need to comapare the same sets (eighter all images or just test images) -> see coec from eval script


# - are there any random factors?
# - is resolution of the mast same?