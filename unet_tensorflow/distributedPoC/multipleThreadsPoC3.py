
### PoC of data augmentation in separate thread ###

import tensorflow as tf
from data import dataProvider
import setting.rootSetting as Setting

nx = ny = Setting.imSize
n_image = Setting.batch_size

# TODO better performance with 1 element ~ image or 1 element ~ K images?
queue = tf.FIFOQueue(capacity=100,  # max 1000 x (X,Y,W)
                     dtypes=[tf.float32, tf.float32, tf.float32],
                     # This is optional:
                     #shapes=[[nx, ny], [nx - 184, ny - 184, 2], [nx - 184, ny - 184, 2]], # X,Y,W
                     name='InputDataFIFOQueue')


newData = tf.py_func(dataProvider.create_batch, [],[tf.double, tf.double, tf.double])
newData = [tf.cast(newData[0], tf.float32), tf.cast(newData[1], tf.float32), tf.cast(newData[2], tf.float32)]

# newData = tf.py_func(dataProvider.create_batch, [],[tf.float32, tf.float32, tf.float32])

# Add to the queue
# queue_inc = queue.enqueue([newData])
queue_inc = queue.enqueue(newData)

# get training data and do st.
inputData = queue.dequeue()
train_op = tf.reduce_sum(inputData[0])

# TODO dictionary containing the values to enqueue


# with tf.Session() as sess:
#     init.run()
#     queue_inc.run()
#     print(sess.run(queue.dequeue()))


# Create a queue runner that will run 4 threads in parallel to enqueue
# examples.
qr = tf.train.QueueRunner(queue, [queue_inc] * 4)

# Launch the graph.
sess = tf.Session()

# Create a coordinator, launch the queue runner threads.
coord = tf.train.Coordinator()
enqueue_threads = qr.create_threads(sess, coord=coord, start=True)

# Run the training loop, controlling termination with the coordinator.
for step in xrange(1000):
    if coord.should_stop():
        break
    res = sess.run(train_op)
    print(res)

# When done, ask the threads to stop.
coord.request_stop()
# And wait for them to actually do it.
coord.join(enqueue_threads)
