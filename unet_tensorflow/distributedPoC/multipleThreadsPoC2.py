
# Note: Example using custom python function as a TensorFlow operation

import tensorflow as tf
import numpy as np


# Note:
# - queue_inc ... augment data & add them to the queue
# - train_op ... a single training operation


def myPythonFunc():
    return np.random.uniform()

# Custom TF operation
y = tf.py_func(myPythonFunc, [], [tf.double])
y = tf.cast(y, tf.float32)


queue = tf.FIFOQueue(10000, tf.float32)


# Add to the queue
queue_inc = queue.enqueue([y])

# get training data and do st.
train_op = queue.dequeue()



# Create a queue runner that will run 4 threads in parallel to enqueue
# examples.
qr = tf.train.QueueRunner(queue, [queue_inc])

# Launch the graph.
sess = tf.Session()

# Create a coordinator, launch the queue runner threads.
coord = tf.train.Coordinator()
enqueue_threads = qr.create_threads(sess, coord=coord, start=True)

# Run the training loop, controlling termination with the coordinator.
for step in xrange(1000):
    if coord.should_stop():
        break
    res = sess.run(train_op)
    print(res)

# When done, ask the threads to stop.
coord.request_stop()
# And wait for them to actually do it.
coord.join(enqueue_threads)
