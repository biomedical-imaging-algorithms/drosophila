
### PoC of data augmentation in separate thread ###

import tensorflow as tf
from data import dataProvider


def createAndStartInputDataQueueRunner(sess):

    # number of parallel threads for data preprocessing (i.e. data augmentation)
    n_augment_threads = 2

    # max number of triples stored in a queue (X,Y,W)
    queue_capacity = 100

    # TODO better performance with 1 element ~ image or 1 element ~ K images?
    # Create FIFO Queue
    queue = tf.FIFOQueue(capacity=queue_capacity,
                         dtypes=[tf.float32, tf.float32, tf.float32],
                         name='InputDataFIFOQueue')

    # "Create new data" TensorFlow Operation
    newDataOp = tf.py_func(dataProvider.create_batch, [],[tf.double, tf.double, tf.double])
    newDataOp = [tf.cast(newDataOp[0], tf.float32), tf.cast(newDataOp[1], tf.float32), tf.cast(newDataOp[2], tf.float32)]

    # "Add to the queue" TensorFlow Operation
    queue_inc = queue.enqueue(newDataOp)

    # "Training" TensorFlow Operation
    train_op = queue.dequeue()
    # inputData = queue.dequeue()
    # train_op = tf.reduce_sum(inputData[0])

    # Create a queue runner that will run n_augmentaiton_threads threads in parallel to enqueu examples.
    qr = tf.train.QueueRunner(queue, [queue_inc] * n_augment_threads)

    # Create a coordinator
    coord = tf.train.Coordinator()

    # Launch the queue runner threads
    enqueue_threads = qr.create_threads(sess, coord=coord, start=True)

    return train_op, coord, enqueue_threads


# Launch the graph.
sess = tf.Session()

train_op, coord, enqueue_threads = createAndStartInputDataQueueRunner(sess)

# Run the training loop, controlling termination with the coordinator.
for step in xrange(1000):
    if coord.should_stop():
        break
    res = sess.run(train_op)
    print(res)

# When done, ask the threads to stop.
coord.request_stop()
# And wait for them to actually do it.
coord.join(enqueue_threads)
