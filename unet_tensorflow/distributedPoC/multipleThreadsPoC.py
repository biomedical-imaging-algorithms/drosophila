
# Note: Based on code from TensorFlow tutorial:
# https://www.tensorflow.org/versions/r0.10/how_tos/threading_and_queues/index.html

import tensorflow as tf

# Note:
# - queue_inc ... augment data & add them to the queue
# - train_op ... a single training operation

# Create a queue
# queue = tf.RandomShuffleQueue(capacity=20, dtypes=tf.int32, min_after_dequeue=10)
# queue = tf.FIFOQueue(10, 'float')
queue = tf.FIFOQueue(capacity=20, dtypes='float')
init = queue.enqueue_many(([0., 0., 0.], ))

# TF Constant with random value
y = tf.random_uniform([1], dtype=tf.float32)

# Add to the queue
queue_inc = queue.enqueue([y])

# get training data and do st.
train_op = queue.dequeue()


# with tf.Session() as sess:
#     init.run()
#     queue_inc.run()
#     print(sess.run(queue.dequeue()))


# Create a queue runner that will run 4 threads in parallel to enqueue
# examples.
qr = tf.train.QueueRunner(queue, [queue_inc] * 4)

# Launch the graph.
sess = tf.Session()

# Create a coordinator, launch the queue runner threads.
coord = tf.train.Coordinator()
enqueue_threads = qr.create_threads(sess, coord=coord, start=True)

# Run the training loop, controlling termination with the coordinator.
for step in xrange(1000):
    if coord.should_stop():
        break
    res = sess.run(train_op)
    print(res)

# When done, ask the threads to stop.
coord.request_stop()
# And wait for them to actually do it.
coord.join(enqueue_threads)
