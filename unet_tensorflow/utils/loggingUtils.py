import logging

logging_file = 'log.log'

__configured__ = False

default_file_level = logging.DEBUG
default_console_level = logging.DEBUG


def __configure_logging__(file_level=default_file_level, console_level=default_console_level):
    # --- logging to file ---
    logging.basicConfig(level=file_level,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename=logging_file,
                        filemode='w')

    # --- logging to console ---
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(console_level)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)


def get_logger(name):
    global __configured__

    if not __configured__:
        __configure_logging__()
        __configured__ = True

    logger = logging.getLogger(name)

    return logger
