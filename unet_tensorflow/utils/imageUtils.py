import numpy as np
import numpy

import setting.rootSetting as Setting
from setting import rootSetting, rootSetting, rootSetting


class INPUT_DATA:
    def __init__(self, fullImg, cB, mask, wtmap):
        self.fullImg, self.cB, self.mask, self.wtmap = fullImg, cB, mask, wtmap


class BBOX:
    def __init__(self, x1, x2, y1, y2):
        self.x_1, self.x_2, self.y_1, self.y_2 = x1, x2, y1, y2


def uncrop_image(image, show_unknown_label=False):
    """
    Add borders to label or segmented image.
    """

    nx = Setting.nx
    ny = Setting.ny
    b = Setting.border_size

    unknown_class_color = 0.5

    # create empty image of a larger size
    im = np.ones((nx, ny)) * unknown_class_color

    # copy smaller image into larger one
    im[b:nx - b, b:ny - b] = image

    if show_unknown_label:
        im[im < 0] = unknown_class_color
        im[im > 1] = unknown_class_color
    else:
        im[im < 0] = 0
        im[im > 1] = 0

    return im


def remove_mask(image):
    """
    Substitute values outside range with zero.
    """

    im = np.copy(image)
    im[im < 0] = 0
    im[im > 1] = 0

    return im


def crop_image(image, border=Setting.border_size, nx=Setting.nx, ny=Setting.ny):
    return image[border:nx - border, border:ny - border]


def cut_with_context(img_full, cB, context, padding='symmetric', constant_values=0):
    # Extra 'context' pixels on all sides.
    size_mask_x, size_mask_y = cB.x_2 - cB.x_1, cB.y_2 - cB.y_1  # MUST BE int
    size_full_x, size_full_y = size_mask_x + 2 * context, size_mask_y + 2 * context

    origin_mask_x, origin_mask_y = (size_full_x - size_mask_x) / 2, (size_full_y - size_mask_y) / 2
    origin_im_x, origin_im_y = origin_mask_x - cB.x_1, origin_mask_y - cB.y_1

    img_tiled = img_full

    if origin_im_x < 0:
        img_tiled = img_tiled[:, -origin_im_x:]
        origin_im_x = 0
    if origin_im_y < 0:
        img_tiled = img_tiled[-origin_im_y:, :]
        origin_im_y = 0
    if origin_im_x + img_tiled.shape[1] > size_full_x:
        img_tiled = img_tiled[:, :size_full_x - origin_im_x]
    if origin_im_y + img_tiled.shape[0] > size_full_y:
        img_tiled = img_tiled[:size_full_y - origin_im_y, :]

    if padding == 'constant':
        img_tiled = np.lib.pad(img_tiled, ((origin_im_y, size_full_y - img_tiled.shape[0] - origin_im_y),
                                           (origin_im_x, size_full_x - img_tiled.shape[1] - origin_im_x)), padding,
                               constant_values=constant_values)
    else:
        img_tiled = np.lib.pad(img_tiled, ((origin_im_y, size_full_y - img_tiled.shape[0] - origin_im_y),
                                           (origin_im_x, size_full_x - img_tiled.shape[1] - origin_im_x)), padding)
    return img_tiled


def tile_image_and_segment_old(im, segment_fun):
    """
    Older version: apply model only on a single image (inefficient if model requires more images).

    :param im: input image
    :param segment_fun: a function that is applied on image tiles
    :return: segmented image
    """

    # get the image dimmensions
    imSize = Setting.imSize
    maskSize = Setting.maskSize

    context = (imSize - maskSize) / 2

    # resize image
    # im2 = np.zeros((1033,659))
    # im2[:1024, :649] = im
    # im = im2


    # computer number of rows and cells
    R = int(np.ceil(float(im.shape[0]) / maskSize))
    C = int(np.ceil(float(im.shape[1]) / maskSize))

    # print('dsdsd')
    # print('im shape {}'.format(im.shape))
    # print('R,C: {} {}'.format(R,C))

    segmented_image = np.zeros((im.shape[0], im.shape[1]))

    # iterate over tiles and segment them
    for r in range(R):
        for c in range(C):
            # create bounding box
            x1, x2 = c * maskSize, (c + 1) * maskSize
            y1, y2 = r * maskSize, (r + 1) * maskSize
            cBsmall = BBOX(x1, x2, y1, y2)

            # get the tile
            im_cut = cut_with_context(im, cBsmall, context)

            # apply segmentation function
            segmented_cut = segment_fun(im_cut)

            segmented_image[y1:min(y2, im.shape[0]), x1:min(x2, im.shape[1])] = \
                segmented_cut[:min(maskSize, im.shape[0] - y1), :min(maskSize, im.shape[1] - x1)]

            # Some original Siddhants code
            # net.blobs['data'].reshape(1, 1, imSize, imSize)
            # net.blobs['data'].data[0, 0, :, :] = im_cut.astype(np.float32)
            # net.forward()
            # softmax[:, y1:min(y2, im.shape[0]), x1:min(x2, im.shape[1])] \
            # = net.blobs['softmax'].data[0,:,:min(maskSize, im.shape[0] - y1), :min(maskSize, im.shape[1] - x1)]

    return segmented_image


def tile_image_and_segment2(im, segment_multiple_images_fun):
    """
    Segment multiple tiles at once.

    :param im: input image
    :param segment_multiple_images_fun: a function that is applied on image tiles that takes array of images (ie. 3D array,
    where the first coordinate is the image number)
    :return: segmented image
    """

    # get the image dimmensions
    imSize = Setting.imSize
    maskSize = Setting.maskSize

    context = (imSize - maskSize) / 2

    # computer number of rows and cells
    R = int(np.ceil(float(im.shape[0]) / maskSize));
    C = int(np.ceil(float(im.shape[1]) / maskSize));
    n_tiles = R * C

    segmented_image = np.zeros((im.shape[0], im.shape[1]))

    im_cuts = np.zeros((n_tiles, imSize, imSize))

    # iterate over tiles and add thme into an array
    for r in range(R):
        for c in range(C):
            # create bounding box
            x1, x2 = c * maskSize, (c + 1) * maskSize
            y1, y2 = r * maskSize, (r + 1) * maskSize
            cBsmall = BBOX(x1, x2, y1, y2)

            # get the tile
            im_cut = cut_with_context(im, cBsmall, context)

            im_cuts[r * C + c, :, :] = im_cut

    # apply segmentation function
    segmented_tiles = segment_multiple_images_fun(im_cuts)

    # Compose into image
    for r in range(R):
        for c in range(C):
            segmented_tile = segmented_tiles[r * C + c, :, :]

            # create bounding box
            x1, x2 = c * maskSize, (c + 1) * maskSize
            y1, y2 = r * maskSize, (r + 1) * maskSize
            cBsmall = BBOX(x1, x2, y1, y2)

            segmented_image[y1:min(y2, im.shape[0]), x1:min(x2, im.shape[1])] = \
                segmented_tile[:min(maskSize, im.shape[0] - y1), :min(maskSize, im.shape[1] - x1)]

            # Some original Siddhants code
            # net.blobs['data'].reshape(1, 1, imSize, imSize)
            # net.blobs['data'].data[0, 0, :, :] = im_cut.astype(np.float32)
            # net.forward()
            # softmax[:, y1:min(y2, im.shape[0]), x1:min(x2, im.shape[1])] \
            # = net.blobs['softmax'].data[0,:,:min(maskSize, im.shape[0] - y1), :min(maskSize, im.shape[1] - x1)]

    return segmented_image


def convert_images_to_tensors(images):
    """
    Conver list of setting.n_image (batch size) augmented images to tensors.
    :param images:
    :return:
    """

    n_image = len(images)

    nx = Setting.imSize
    ny = Setting.imSize

    X = np.zeros((n_image, nx, ny), dtype=np.float32)
    Y = np.zeros((n_image, nx - 184, ny - 184, 2), dtype=np.float32)
    W = np.zeros((n_image, nx - 184, ny - 184, 2), dtype=np.float32)

    for i in range(n_image):
        image = images[i][0]
        label = images[i][1]
        w = images[i][2]

        if Setting.debug_mode:
            # assert that weights outside the mask are zero
            # assert ((w[label < 0 or label > 1] == 0).all())
            # assert (np.all(w[label < 0 or label > 1] == 0))
            # assert (all(w[label < 0 or label > 1]) == 0)

            b = (0 > np.sign(w) * label).any() or (1 < np.sign(w) * label).any()
            assert not b

        # image
        X[i, :, :] = image

        # label (ground truth)
        Y[i, :, :, 1] = label
        Y[i, :, :, 0] = 1 - label

        # weight map
        W[i, :, :, 0] = w
        W[i, :, :, 1] = w

    # TODO normalize weights to have E[WY] = 0.5?

    if Setting.debug_mode:
        # Y values 0,1,2  (2 <=> outside the mask)
        assert np.min(Y[W != 0]) >= 0
        assert np.max(Y[W != 0]) <= 1

        # non-negative weights
        assert np.min(W) >= 0

        # y not in [0,1] -> w = 0
        # assert np.max(W[Y > 1 or Y < 0]) == 0

        b = (0 > np.sign(W) * Y).any() or (1 < np.sign(W) * Y).any()
        assert not b

        # assert((W[Y > 1 or Y < 0] == 0).all())

        # Assert that both classes should have the same weight in training
        w = W[:, :, :, :]
        y = Y[:, :, :, :]
        # remove region outside the mask
        w1 = w[w != 0]
        y1 = y[w != 0]
        # this should be close to 0.5 (i.e. both classes should have the same weight in training)
        #  - does not hold when we use a higher weights for edges
        # assert 0.4 < np.mean(w1 * y1) < 0.6

        assert np.min(Y * W) >= 0, "Incorrect data in DataProvider"
        assert np.min((1 - Y) * W) >= 0, "Incorrect data in DataProvider"

    return X, Y, W


def normalize_image(image, mean, sd):
    """
    Normalized image using given (e.g. known or estimated) mean and standard deviation.
    """

    normalized_image = (image - mean) / (sd + 1e-5)

    return normalized_image


def __normalize_image_test():
    import numpy as np

    # sample image - not center, not unit variance
    test_image = np.random.randn(50, 50) * 2 + 1

    normalized_image = normalize_image(test_image, 1, 2)

    # should be close to 0 and 1
    np.mean(normalized_image)
    np.var(normalized_image)
    # assert -1e-5 < np.mean(normalized_image) < 1e-5
    # assert 1-1e-3 < np.var(normalized_image) < 1+1e-3


def apply_thresholding(image, threshold=0.5):
    """
    Return image with indicator values: 1[value > threshold]
    """

    binary_image = (np.sign(image - threshold) + 1) // 2
    return binary_image


def create_false_positives_negatives_rgb_image(original_image, segmented_image, ground_truth):
    """
    Create a rgb visualization of true and false negatives.
    """

    # binary segmentation
    thresholded = apply_thresholding(segmented_image)

    # indicator, where we have label
    mask = np.ones_like(ground_truth)
    mask[ground_truth > 1] = 0

    # indicator for incorrect segmentation (i.e. we have ground truth and  is not correct)
    incorrect = mask * abs(thresholded - ground_truth)

    # indicator of false positives (incorrect & predicted 1)
    fp = incorrect * thresholded

    # indicator of false negatives (incorrect & gt is 1)
    fn = incorrect * ground_truth

    # normalize to [0,1]
    a = np.min(original_image)
    b = np.max(original_image)
    original_image_normalized = (original_image - a) / (b - a + 1e-5)

    q = 0.85
    r = q * original_image_normalized + (1 - q) * fp
    g = q * original_image_normalized + (1 - q) * mask * (1 - incorrect)
    b = q * original_image_normalized + (1 - q) * fn

    rgb_image = create_rgb_image(r, g, b)

    return rgb_image


# def __create_false_positives_negatives_rgb_image_test__():
#     from data import dataProvider
#     from modelEvaluator import modelEvaluator
#
#     exp_names = ['test172-cv4-momentum-nobatchnorm-halmos']
#
#     X, Y, W = dataProvider.create_batch()
#     # print('X.shape', X.shape)
#
#     segmented_images = modelEvaluator.segment_images_and_compute_error(exp_names, X, Y, W)
#
#     i = 0
#     original_image = crop_image(X[0, :, :])
#     segmented_image = segmented_images[i][0]
#     pixel_error = segmented_images[i][1]
#     ground_truth = remove_mask(Y[0, :, :, 1])
#
#     # class1_indicator = (1 - abs(ground_truth - 1))
#
#
#     segmented_image[(ground_truth != 1)] = 0
#     segmented_image[ground_truth == 1] = abs(apply_thresholding(segmented_image) - ground_truth)


def create_rgb_image(r, g, b):
    """
    Create a RGB image from channels in range [0,1]
    :return:
    """

    assert np.max(r) <= 1
    assert np.max(g) <= 1
    assert np.max(b) <= 1

    assert np.min(r) >= 0
    assert np.min(g) >= 0
    assert np.min(b) >= 0

    xsize = r[:, 0].size
    ysize = r[0, :].size

    rgbIm = np.zeros((xsize, ysize, 3), 'uint8')

    rgbIm[..., 0] = 255 * r
    rgbIm[..., 1] = 255 * g
    rgbIm[..., 2] = 255 * b

    return rgbIm
