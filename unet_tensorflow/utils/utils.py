"""
General util functions.
"""

import numpy as np
import threading


def print_shape(tf_var):
    print('h_conv[0] shape : {}'.format(tf_var.get_shape()))


def synchronized(func):
    func.__lock__ = threading.Lock()

    def synced_func(*args, **kws):
        with func.__lock__:
            return func(*args, **kws)

    return synced_func


def concat_dictionaries(list_of_dicts):
    """
    Concat two dictionaries with distinct keys.
    :return:
    """

    # d1 = {1:1}
    # d2 = {2:2}
    # list_of_dicts = [d1, d2]

    dict_concat_as_list = reduce(lambda a,b: a+b, map(lambda d: list(d.items()),list_of_dicts))
    dict_concat = dict(dict_concat_as_list)

    return dict_concat


def apply_to_index(fun, tuple, index):

    assert 0 <= index < len(tuple)

    y = fun(tuple[index])
    return tuple[:index] + (y,) + tuple[index+1:]


def apply_to_indexes_test():

    tuple = (1,2,3, 4)

    f = lambda x: x +5

    apply_to_index(f, tuple, 0)
    apply_to_index(f, tuple, 1)
    apply_to_index(f, tuple, 2)
    apply_to_index(f, tuple, 3)

    apply_to_index(f, tuple, -1)
    apply_to_index(f, tuple, 4)


def copute_fpr_tpr(binary_segmentation, Y):
    # indicator for known values
    known = np.sign(2 - Y)

    # compute FP and FN
    fp = np.sum(binary_segmentation * (1 - Y) * known)
    fn = np.sum((1 - binary_segmentation) * Y * known)

    tp = np.sum(binary_segmentation * Y * known)
    tn = np.sum((1 - binary_segmentation) * (1 - Y) * known)
    # print(fp + fn + tp + tn, np.sum(known))

    negatives = np.sum((1 - Y) * known)
    positives = np.sum(Y * known)

    fpr = fp / negatives
    tpr = tp / positives

    return fpr, tpr


