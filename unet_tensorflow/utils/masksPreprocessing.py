
import numpy as np
from scipy import signal
from scipy import misc
from data import dataLoader
from setting import rootSetting as Setting


def __create_smoothing_filter(size=31):
    """
    Create filter for averaging pixel values from a given radius .
    :param size: filter size
    """

    # radius
    r = size // 2

    x, y = np.mgrid[-r:r + 1, -r:r + 1]

    # compute filter
    g = np.sign((r + 1) ** 2 - (x ** 2 + y ** 2))
    g[g == -1] = 0
    g = g / float(g.sum())

    return g


def __shrink_class_boundaries(images):

    mask = images[0]

    # remove areas with value 2 (that indicate not used are in training)
    indicator = [mask == 2]
    mask[indicator] = 0

    # create filter
    filter = __create_smoothing_filter()

    # convolve image with the filter
    new_mask = signal.convolve2d(mask, filter, boundary='symm', mode='same')

    # thresholding
    threshold = 0.9
    new_mask[new_mask < threshold] = 0
    new_mask[new_mask >= threshold] = 1

    # return indicator of not used area
    new_mask[indicator] = 2

    # image = np.random.uniform((100,100)) * 20

    return [new_mask]

# TODO


def shrink_class_boundaries():
    """
    Shrink boundaries of class 1 (i.e. white).

    Changing the problem definition: now we recognize the inner part of the eggs
     - this should help separate particular eggs
     - moreover it also remove some inaccuracies in ground truth (where part of background is labeled as an egg)
    """

    dataLoader.read_and_process_images(__shrink_class_boundaries)


if __name__ == "__main__":
    # set paths of input and output images in setting
    Setting.dataset_for_task = 'adjust labels'

    shrink_class_boundaries()