import numpy as np
import random


def get_sample(x, n):
    """
    Get sample of n elements from list x.
    """

    assert len(x) >= 1
    assert n >= 1

    if n > len(x):
        # if the sample size is larger that the number of images we multiply the list (ie. duplicate the elements)
        x = x * int(np.ceil(n / float(len(x))))

    sample = random.sample(x, n)

    return sample


def normalize_to_unit_interval(image):
    """
    Transform to [0,1].
    """
    a = np.amin(image)
    b = np.amax(image)
    return (image - a) / (b - a + 1e-5)


def sgm2(x):
    """
    Sigmoidal function trainsforming into [-1,1] (hyperbolic tangent).
    """

    return 2 / (1 + np.e ** -x) - 1


def get_sgm_normalization_fun(mu, sigma):
    def normalize(image):
        # replace original image with normalized
        normalized_image = (image - mu) / (sigma + 1e-5)

        # transform using sigmoidal function to the interval [-1,1]
        transformed_image = sgm2(normalized_image)

        return transformed_image

    return normalize


def compute_mse(gt, segmentation):
    """
    Compute E[error | known] as E[error ; known] / P(known), where known means we known the correct segmentation.

    i.e. compute mean squared error only on pixels, where we know the correct labels.
    """

    # indicator of known labels
    known = np.ones_like(gt)
    known[gt == 2] = 0

    # E[error ; known]
    error_known = np.mean(np.square(segmentation - gt) * known)

    # P(known)
    known_prob = np.mean(known)

    # E[error | known]
    mse = error_known / known_prob

    return mse


def compute_mse_test():

    gt, segmentation = np.ones((3,3)), np.ones((3,3))
    assert compute_mse(gt, segmentation) == 0

    gt, segmentation = np.ones((3, 3)), np.zeros((3, 3))
    assert compute_mse(gt, segmentation) == 1

    gt, segmentation = np.ones((3, 3)), np.ones((3, 3))
    gt[0,0] = 2
    assert compute_mse(gt, segmentation) == 0

    gt[0, 1] = 0
    assert compute_mse(gt, segmentation) == 0.125  # 1 incorrect from 8 known pixels
