"""
Getting paths for input and output images for different use cases (e.g. training, segmenation) & directory structures.

Given a row of txt or csv file we extract list of input and output paths.
"""
import os

from setting import rootSetting as Setting

from utils import loggingUtils

log = loggingUtils.get_logger(__name__)


def __get_filename_by_key__(root_dir, image_key):
    """
    Return the filename of the first file that contains image_key in its name.

    If no file is found None is returned.
    """

    files = os.listdir(root_dir)
    files.sort()

    for file in files:
        if file.__contains__(image_key):
            return file

    return None


def __get_root_directory_path__():
    if Setting.dataset_for_task == 'segment images':
        if Setting.task_environment == 'local':
            return '/home/marek/CMP/data3/ovary_selected_slices/'
        return '/mnt/datagrid/Medical/microscopy/drosophila/ovary_selected_slices/'
    else:  # Setting.dataset_for_task in ['train model', 'evaluate model', 'adjust labels']:
        if Setting.task_environment == 'local':
            return '/home/marek/CMP/data2/'
        return '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/'

    raise Exception("Unsupported value of Setting.task: " + Setting.dataset_for_task)


def __get_paths_for_ovary_selected_slices__(row):
    # Get root directory
    root_dir_path = __get_root_directory_path__()

    # Get image name
    image_name = row.itervalues().next()

    # Get paths
    src_path = root_dir_path + 'struct/' + image_name
    target_path = root_dir_path + 'segmented/' + image_name[:len(image_name) - 4] + '.png'

    return [src_path], [target_path]


def __get_paths_for_slices_with_groundtruth__(row):
    # Get root directory
    root_dir_path = __get_root_directory_path__()

    path_fragment = 'slice_struct/stage' + row['stage'] + '/slice_' + row['image_id']

    # image path
    src_path1 = root_dir_path + path_fragment + '.tif'

    # ground truth path
    if Setting.segment_inner_parts_of_cells:
        src_path2 = root_dir_path + 'mask_2d_binary_adjusted/stage' + row['stage'] + "/mask" + row['image_id'] + '.png'
    else:
        src_path2 = root_dir_path + 'mask_2d_binary/stage' + row['stage'] + "/mask" + row['image_id'] + '.png'

    target_path = root_dir_path + 'segmented/' + path_fragment + '.png'

    return [src_path1, src_path2], [target_path]


def __get_paths_for_segmented_images_and_gt__(row):
    """
    This is for getting paths when we have already segmented images (by any method) and we want to load them together
        with corresponding ground truths (e.g. to compute error).
    """

    # Get root directory with ground truths
    root_dir_path = __get_root_directory_path__()

    image_id = row['image_id']

    # ground truth path
    if Setting.segment_inner_parts_of_cells:
        ground_truth_src = root_dir_path + 'mask_2d_binary_adjusted/stage' + row['stage'] + "/mask" + image_id + '.png'
    else:
        ground_truth_src = root_dir_path + 'mask_2d_binary/stage' + row['stage'] + "/mask" + image_id + '.png'

    # find the corresponding segmented image
    segmented_images_dir = Setting.segmented_images_dir
    file_name = __get_filename_by_key__(segmented_images_dir, image_id)

    if file_name is None:
        return None

    segmented_image_src = segmented_images_dir + file_name

    return [segmented_image_src, ground_truth_src], []


def __get_paths_for_labels__(row):
    # Get root directory
    root_dir_path = __get_root_directory_path__()

    # image path
    # src_path1 = path_fragment + '.tif'

    # ground truth path
    src_path2 = root_dir_path + 'mask_2d_binary/stage' + row['stage'] + "/mask" + row['image_id'] + '.png'

    target_path = root_dir_path + 'mask_2d_binary_adjusted/stage' + row['stage'] + "/mask" + row['image_id'] + '.png'

    return [src_path2], [target_path]


# a function for getting images paths
__get_images_paths_fun__ = None


def __get_get_images_paths_fun__():
    """
    Get a function for getting paths to the images that has a single parameter: row.
    """

    global __get_images_paths_fun__

    if __get_images_paths_fun__ is None:

        selector = {
            'segment images': __get_paths_for_ovary_selected_slices__,
            'train model': __get_paths_for_slices_with_groundtruth__,
            'evaluate model': __get_paths_for_slices_with_groundtruth__,
            'adjust labels': __get_paths_for_labels__,
            'evaluate segmented': __get_paths_for_segmented_images_and_gt__
        }

        task = Setting.dataset_for_task

        if not selector.has_key(task):
            log.warn(
                'Missing of invalid value of Setting.dataset_for_task: ' + task + ', using default path for training the model.')
            return

        log.info('Using paths for task: ' + Setting.dataset_for_task)

        __get_images_paths_fun__ = selector[task]

    return __get_images_paths_fun__


# --- Public interface ---


def get_csv_file_path():
    # Get csv filename
    if Setting.dataset_for_task == 'segment images':
        csv_filename = 'list_selected_slices.OLD.txt'  # 'list_selected_slices.txt'
    else:  # Setting.dataset_for_task in ['train model', 'evaluate model', 'adjust labels']:
        csv_filename = 'ovary_image_info_for_prague.csv'
    # else:
    #     raise Exception('Unsupported value of Setting.task: ' + Setting.dataset_for_task)

    # Get root dir path
    root_dir_path = __get_root_directory_path__()

    return root_dir_path + csv_filename


def get_images_paths(row):
    """
    Get paths based on setting.
    """

    get_images_paths_fun = __get_get_images_paths_fun__()

    return get_images_paths_fun(row)
