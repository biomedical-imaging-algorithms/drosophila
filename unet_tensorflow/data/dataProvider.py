"""
High-level input data provider.
"""
import logging

import setting.rootSetting as Setting
from data import dataNormalization
from data.dataAugmentator import cut_tiles
from data.dataLoader import read_images_tiles
from data.testDataGenerator import *
from utils import imageUtils, mathUtils
from utils import loggingUtils
from utils.utils import synchronized

log = loggingUtils.get_logger(__name__)

__imagesCache = None
__trainImagesCache = None
__testImagesCache = None


def __get_test_and_train_set_variant1(images):
    """
    Split data into disjoint sets.
    """
    train_set = []
    test_set = []

    i = 0
    for image in images:
        if i % Setting.cv_folds == Setting.cv_test_set_index:
            test_set.append(image)
        else:
            train_set.append(image)
        i += 1

    return train_set, test_set


def __get_test_and_train_set_variant2(images):
    """
    Using only a single image as both test and train set.
    """
    train_set = images[1:2]
    test_set = images[1:2]

    return train_set, test_set


def __get_test_and_train_set_variant3(images):
    """
    Only selected 'most common' images as both test and train set.
    """

    # selected images given by im_num (that corresponds to the ordering when the images are loaded)
    selected_images = [1, 2, 11, 25, 26, 27, 34, 48, 58, 66]

    train_set = [images[i] for i in selected_images]
    test_set = train_set

    return train_set, test_set


def __get_test_and_train_set(images):
    if Setting.testtrain_type == 1:
        train_set, test_set = __get_test_and_train_set_variant1(images)
    elif Setting.testtrain_type == 2:
        train_set, test_set = __get_test_and_train_set_variant2(images)
    elif Setting.testtrain_type == 3:
        train_set, test_set = __get_test_and_train_set_variant3(images)
    elif Setting.testtrain_type == 4:
        train_set = test_set = images
    else:
        raise Exception('Invalid Setting.testtrain_type: ' + Setting.testtrain_type)

    return train_set, test_set


@synchronized
def __get_images__():
    """
    Get (non-augmented) images based on current setting.

    Can be accessed from multiple threads -> needs to be synchronized.
    :return (all images, trains set, test set)
    """

    global __imagesCache, __trainImagesCache, __testImagesCache

    # Load images if called for the first time
    if __imagesCache is None:
        __imagesCache = read_images_tiles()

        # split data into test set and train set
        __trainImagesCache, __testImagesCache = __get_test_and_train_set(__imagesCache)

        # optionally normalize images
        __trainImagesCache, __imagesCache, __testImagesCache = dataNormalization.normalize_images(
            [__trainImagesCache, __imagesCache, __testImagesCache])

    return __imagesCache, __trainImagesCache, __testImagesCache


# --- Public interface ---


def get_images():
    """
    Get (non-augmented) images based on current setting.
    :return:
    """

    # Load or get cached images
    allImages, trainImages, testImages = __get_images__()

    if Setting.dataset_for_task == 'segment images':
        # we use all images
        return allImages

    if Setting.dataset_for_task == 'train model':
        return trainImages

    if Setting.dataset_for_task == 'evaluate model':
        return testImages

    raise ('Invalid Setting.task ' + Setting.dataset_for_task + ', using all images.')


def get_train_images():
    allImages, trainImages, testImages = __get_images__()
    return trainImages


def get_test_images():
    allImages, trainImages, testImages = __get_images__()
    return testImages


def get_all_images():
    allImages, trainImages, testImages = __get_images__()
    return allImages


def get_sample_images(n_images):
    """
    Randomly sample n_images from all images.
    """

    # Load or get cached images
    images = get_images()

    return mathUtils.get_sample(images, n_images)


def get_tiles(original_images):
    """
    Get cuts of images including label, weights, etc.

    Note: this is wrapper for Siddhants "augment" function.
    """

    # TODO (opt) use cache when augmentation is disabled

    tiles = []
    for original_image in original_images:
        im, truth, wtmap, cB, src, _ = original_image

        # Get (augmented) tile from the image
        im_tile, mask_tile, wtmap_tile = cut_tiles(im, cB, truth, wtmap, augment=Setting.augment_images,
                                                   req_size=setting.imSize, mask_size=setting.maskSize, full_size=1020)

        tiles.append((im_tile, mask_tile, wtmap_tile, src))

    return tiles


@synchronized
def create_batch(test_set_sample=False):
    """

    :return: X(images), Y (ground truths), W (weights)
    """

    log.debug('Creating new batch [from {} set, test is fold {}]'.format('test' if test_set_sample else 'train',
                                                                         Setting.cv_test_set_index))

    # Get data: generate test data or load stored images
    if Setting.useTestData:
        # TODO specify the method in setting? 
        images = createTestData0()
    else:
        # Get images
        if test_set_sample:
            all_images = get_test_images()
        else:
            all_images = get_images()

        images = mathUtils.get_sample(all_images, Setting.batch_size)

        assert (Setting.batch_size == len(images))

        # vals = np.unique(images[0][0], return_counts=False)
        # print(vals)

        # Get images tiles (optionally augment them)
        images = get_tiles(images)

        assert (Setting.batch_size == len(images))

        # vals = np.unique(images[0][0], return_counts=True)
        # print(vals)

    # for im in images:
    #     print('mean: {}'.format(np.mean(im[0])))
    #     print('sd: {}'.format(np.sqrt(np.var(im[0]))))

    X, Y, W = imageUtils.convert_images_to_tensors(images)

    # vals = np.unique(X[0, :, :], return_counts=False)
    # print(vals)

    return X, Y, W
