
from data import dataProvider
from data.dataProvider import __get_test_and_train_set, get_sample_images
from setting import rootSetting as Setting


def create_batch_test():
    """
    Test of create_batch().
    """

    Setting.dataset_for_task = 'train model'

    batch = dataProvider.create_batch()

    X, Y, W = batch


def __split_into_test_and_train_set_test():

    data = range(1,75)

    train_set, test_set = __get_test_and_train_set(data)

    train_set, test_set


def get_sample_images_test():

    get_sample_images(5)