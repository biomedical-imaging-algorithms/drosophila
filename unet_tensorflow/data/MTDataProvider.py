"""
Multi-threaded extension of Data Provider.

Generating augmented data in a separate thread and storing them into FIFO Queue.
"""

import tensorflow as tf
from data import dataProvider
from setting import rootSetting as Setting
import numpy as np


def __create_and_start_inputDataQueueRunner(sess, create_batch_fun):
    # number of parallel threads for data preprocessing (i.e. data augmentation)
    n_augment_threads = Setting.n_augment_threads

    # max number of triples stored in a queue (X,Y,W)
    queue_capacity = Setting.queue_capacity

    n_image = Setting.batch_size
    nx = Setting.imSize
    ny = Setting.imSize

    # TODO better performance with 1 element ~ image or 1 element ~ K images?
    # Create FIFO Queue
    queue = tf.FIFOQueue(capacity=queue_capacity,
                         dtypes=[tf.float32, tf.float32, tf.float32],
                         # optional shapes validation
                         shapes=[[n_image, nx, ny], [n_image, nx - 184, ny - 184, 2], [n_image, nx - 184, ny - 184, 2]],
                         name='InputDataFIFOQueue')

    # "Create new data" TensorFlow Operation
    new_data_op = tf.py_func(create_batch_fun, [], [tf.float32, tf.float32, tf.float32])

    # if Setting.debug_mode:
    #     # check some basic properties of y and w
    #     x, y, w = sess.run(new_data_op)
    #     assert np.min(y * w) >= 0, "MTDataProvider: Incorrect data"
    #     assert np.min((1 - y) * w) >= 0, "MTDataProvider: Incorrect data"


    # "Add to the queue" TensorFlow Operation
    queue_inc = queue.enqueue(new_data_op)

    # "Training" TensorFlow Operation
    dequeue_input_data = queue.dequeue()
    # inputData = queue.dequeue()
    # train_op = tf.reduce_sum(inputData[0])

    # Create a queue runner that will run n_augmentaiton_threads threads in parallel to enqueu examples.
    qr = tf.train.QueueRunner(queue, [queue_inc] * n_augment_threads)

    # Create a coordinator
    coord = tf.train.Coordinator()

    # Launch the queue runner threads
    enqueue_threads = qr.create_threads(sess, coord=coord, start=True)

    return dequeue_input_data, coord, enqueue_threads


# --- Public Interface ---


def create_and_start_inputDataQueueRunner_train(sess):
    return __create_and_start_inputDataQueueRunner(sess, create_batch_fun=dataProvider.create_batch)


def create_and_start_inputDataQueueRunner_test(sess):
    def create_batch_fun():
        return dataProvider.create_batch(test_set_sample=True)

    return __create_and_start_inputDataQueueRunner(sess, create_batch_fun)


# --- Tests ---


def __create_and_start_inputDataQueueRunner_test():
    """
    This test shows an example use of the method above.
    """

    # Launch the graph.
    sess = tf.Session()

    dequeue_imput_data, coord, enqueue_threads = create_and_start_inputDataQueueRunner_train(sess)

    # Run the training loop, controlling termination with the coordinator.
    for step in xrange(1000):
        if coord.should_stop():
            break

        # Mean of X values
        X, Y, W = dequeue_imput_data

        mX = tf.reduce_mean(X)
        mY = tf.reduce_mean(Y)
        mW = tf.reduce_mean(W)

        # Evaluate mean
        res = sess.run([mX, mY, mW])

        # Print mean
        print(res)

    # When done, ask the threads to stop.
    coord.request_stop()

    # And wait for them to actually do it.
    coord.join(enqueue_threads)
