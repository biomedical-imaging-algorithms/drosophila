from data.dataLoader import read_images_tiles, save_image
import numpy as np


def __save_image_test():
    # load sample image
    image = read_images_tiles()[0][0]

    # import visualization.visualUtils as visualUtils

    # visualUtils.plot_histogram(image, False)

    file_name = 'tmp/output_image_test.png'

    # test the method
    save_image(image, file_name)

    # --- RGB Image ---
    xsize = image[:, 0].size
    ysize = image[0, :].size

    rgbArray = np.zeros((xsize, ysize, 3), 'uint8') # , 'uint8'
    rgbArray[..., 0] = image * 255
    rgbArray[..., 1] = image * 255
    rgbArray[..., 2] = image * 255

    file_name = 'tmp/output_image_test_rgb.png'

    save_image(rgbArray, file_name)


def __read_and_process_images_test():
    # Sample processing function: Crop image
    def apply_fun(x):
        return x[0:100, 0:100]

        # Process images
        # read_and_process_images_variant1(apply_fun)
