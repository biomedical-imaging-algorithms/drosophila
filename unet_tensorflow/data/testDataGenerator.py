import numpy as np
from scipy import ndimage
from utils import imageUtils

import data.pixelWeightsConstructor
from setting import rootSetting as setting


def __createCirclesTestData(sigma_inv=10,  # 1 / sigma for the random noise
                            plateau_min=-2, plateau_max=2,  # ??
                            r_min=1, r_max=200  # interval for the radius
                            ):
    """
    Test data based on existing unet tensorflow implementation.
    """

    nx = setting.nx
    ny = setting.ny

    images = []

    for i in range(setting.batch_size):
        # generate a random centroid of the sphere
        x = np.floor(np.random.rand(1)[0] * nx)
        y = np.floor(np.random.rand(1)[0] * ny)

        image = np.ones((nx, ny))
        label = np.ones((nx, ny))

        # ??
        image[x, y] = 0
        image_distance = ndimage.morphology.distance_transform_edt(image)

        r = np.random.rand(1)[0] * (r_max - r_min) + r_min
        plateau = np.random.rand(1)[0] * (plateau_max - plateau_min) + plateau_min

        label[image_distance <= r] = 1
        label[image_distance > r] = 0

        image_distance[image_distance <= r] = 0
        image_distance[image_distance > r] = 1
        image_distance = (1 - image_distance) * plateau

        image = image_distance + np.random.randn(nx, ny) / sigma_inv

        label = label[92:nx - 92, 92:nx - 92]

        weights = data.pixelWeightsConstructor.constructWeightMap(label, higherWeightsOnBoundaries=False)

        images.append((image, label, weights))

    return images


def createTestData0(tileSize = 40):
    """
    Training Identity: almost the simplest possible task.
    """

    # TODO use cache -> compute onely onece
    # TODO tile size as a parameter

    # Create a triple (image,label, weight)
    nx = setting.nx
    ny = setting.ny

    image = np.ones((nx, ny))

    for x in range(nx):
        for y in range(ny):
            xx = int(x % (2 * tileSize) < tileSize)
            yy = int(y % (2 * tileSize) < tileSize)
            # logical xor
            image[x, y] = xx + yy - 2 * xx * yy

    label = imageUtils.crop_image(image)
    weight = np.ones((nx - 184, ny - 184))

    # Add random noise to the image
    noiseIntensity = 0.7
    image = (1-noiseIntensity) * image + noiseIntensity * np.random.uniform(size=(nx,nx))
    # image = image.clip(0,1)

    # Copy them into list
    images = []
    for i in range(setting.batch_size):
        images.append((image,label, weight))

    return images



def createTestData1():
    """
    Identity, i.e. white pixel = class 0, black pixel = class 1 (possibly with a small negligible random noise)
    """
    
    # TODO there might be an error related to generating testing data (label seem to be incorrect sometimes->investigate)

    images = __createCirclesTestData(sigma_inv= 99999)

    return images


def createTestData2():
    """
    Circles with a small random noise.

    Note: These are the test data as in existing unet tensorflow implementation.
    """

    images = __createCirclesTestData()

    return images
