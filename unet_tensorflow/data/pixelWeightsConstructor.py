###
# Computing weights based on the given label (i.e. ground truth)
#
# Note: based on Siddhats code
####


# TODO move to some "mathUtils" package?

import numpy as np
from scipy import ndimage as ndimage

from setting import rootSetting as Setting


def __computeWeights(truth):
    """
    Compute weights for pixels.
    Note: Copied from Siddhats code
    """

    truth_copy = truth.copy()
    truth_copy[truth == 2] = 1
    d1_f = ndimage.distance_transform_edt(truth_copy)
    truth_copy[truth == 0] = 1
    truth_copy[truth == 1] = 0
    d1_b = ndimage.distance_transform_edt(1 - truth)
    d1 = np.maximum(d1_f, d1_b)

    truth_copy = truth.copy()
    truth_copy[truth == 2] = 0

    labeled_array, num_features = ndimage.measurements.label(truth_copy, structure=None, output=None)

    tmp = np.empty_like(labeled_array)
    tmp[:] = labeled_array

    d2 = np.empty(labeled_array.shape)
    w_c = np.empty(labeled_array.shape)
    d2[:] = np.inf;
    w_c[:] = 0

    for ii in range(1, num_features + 1):
        tmp[:] = labeled_array
        tmp[np.logical_or(tmp == ii, tmp == 0)] = -1  # fg
        tmp[tmp != -1] = 0
        tmp[tmp == -1] = 1
        # everything except current class and bg goes to bg, current class and bg goes to fg
        d2_ii = ndimage.distance_transform_edt(tmp)
        d2[labeled_array == ii] = d2_ii[labeled_array == ii]

    tmp[:] = 1
    d2b1 = np.empty(labeled_array.shape)
    d2b2 = np.empty(labeled_array.shape)
    d2b1[:] = np.inf
    d2b2[:] = np.inf
    for ii in range(1, num_features + 1):
        tmp[labeled_array == ii] = 0
        db_ii = ndimage.distance_transform_edt(tmp)
        d2b1[labeled_array == 0], d2b2[labeled_array == 0] = np.minimum(db_ii[labeled_array == 0],
                                                                        d2b1[labeled_array == 0]), \
                                                             np.minimum(np.maximum(db_ii[labeled_array == 0],
                                                                                   d2b1[labeled_array == 0]),
                                                                        d2b2[labeled_array == 0])
        tmp[labeled_array == ii] = 1

    w0, sigma = Setting.pixel_weights_w0, Setting.pixel_weights_sigma

    d2[labeled_array == 0] = d2b2[labeled_array == 0]

    # TODO produce waring or error when the image contain only a single class?

    assert truth.size > 0

    frac0 = np.sum(truth == 0) / float(truth.size)
    frac1 = np.sum(truth == 1) / float(truth.size)

    assert frac0 > 0
    assert frac1 > 0

    w_c[truth == 0] = 0.5 / (frac0)
    w_c[truth == 1] = 0.5 / (frac1)

    return w_c,  w0 * np.exp(-np.square(d1+d2)/(2*sigma**2))


def constructWeightMap(truth,
                       higherWeightsOnBoundaries = True,  # higher weights on boundaries of two classes (e.g. for separating two drosophila eggs)
                       unitWeights = False):  # weights are 1 inside mask and 0 outsize mask
    """
    Expected values in truth:
    0, 1 = class labels,
    2 = outsize the mask (i.e. zero weights)
    """

    if unitWeights:
        wtmap = np.sign(2 - truth)
    else:
        w_c, w_b = __computeWeights(truth)

        if higherWeightsOnBoundaries:
            wtmap = w_c + w_b
        else:
            wtmap = w_c

    # zero weights outside thw mask
    wtmap[truth == 2] = 0

    return wtmap
