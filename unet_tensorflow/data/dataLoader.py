"""
Low-level reading/saving images from/to files.
"""

# TODO consider multi-threaded variant (e.g. using TensorFlow Queues)

import csv
import os
import cv2
import matplotlib.pyplot as plt  # for saving images
import numpy as np
import tifffile as tiff
# from PIL import Image
# import Image

from data import pixelWeightsConstructor as weightsConstructor
from data import imagePaths
from setting import rootSetting as Setting
from utils import imageUtils
from utils import mathUtils, loggingUtils
from utils.imageUtils import BBOX

log = loggingUtils.get_logger(__name__)


def __read_tiff_image(filename):
    im = cv2.imread(filename, -1)
    return im

    # return tiff.imread(filename)
    # return tiff.imread(filename).astype(np.float64)


def __read_png_image(filename):
    im = cv2.imread(filename, -1)
    # im = cv2.imread(filename, 0).astype(np.float64)
    return im


def __tuple2str(t):
    return '(' + ','.join(map(str, t)) + ')'


# --- Public Interface ---


def load_image(filename, normalize=True):
    """
    Low-level reading an image given by the filename.
    """

    # Get file path
    root, ext = os.path.splitext(filename)

    # Read the image
    if ext.lower() == ".tif" or ext.lower() == ".tiff":
        im = __read_tiff_image(filename)
    else:
        im = __read_png_image(filename)

    if im is None:
        log.error("Error reading image from: " + filename)
        return

    # Normalize to [0,1] (different datasets may use different scales)
    # Note: never normalize ground truth!
    if normalize:
        im = mathUtils.normalize_to_unit_interval(im)

    return im


def save_image(image, file_name):
    """
    Save the given image to the png format.
    """

    assert not np.isnan(image).any()
    assert np.amin(image) >= 0

    if image.ndim == 2:
        # We expect image in range [0,1]

        assert np.amax(image) <= 1

        if not Setting.dataset_for_task == 'adjust labels':
            image = image * 255

        # --- gray scale image ---

        # saving in png format
        plt.imsave(file_name, image.astype(np.uint8), vmin=0, vmax=255, cmap='gray')
        # plt.imsave(file_name, image.astype(np.uint8), vmin=0, vmax=255, cmap='gray')

    elif image.ndim == 3:
        # We expect image in range [0,256]
        assert np.amax(image) <= 256

        img = Image.fromarray(image)
        img.save(file_name)


def read_images_tiles():
    """
    Read multiple images tiles for training and crate weight maps.

    All images are loaded at once.

    Expects a specific directory structure: stage<n>/slice_<image_id>

    :returns (non-augmented) image tiles (as list of tuples: (image, truth, wtmap, cB, image_src))
    """

    images = []

    csv_filename = imagePaths.get_csv_file_path()

    with open(csv_filename) as csvfile:

        reader = csv.DictReader(csvfile, delimiter='\t')

        for row in reader:

            # Get bounding box
            cB = BBOX(int(row['bb_begin_x']), int(row['bb_end_x']), int(row['bb_begin_y']), int(row['bb_end_y']))

            stage = row['stage']

            try:
                # Get images paths
                src_paths, _ = imagePaths.get_images_paths(row)

                image_src = src_paths[0]
                ground_truth_src = src_paths[1]

                # Read the image and the grand truth
                image = load_image(image_src)
                truth = load_image(ground_truth_src, normalize=False)

            except Exception as e:
                log.warn(e)
                # print('Errror loading images: ' + image_src + ', ' + ground_truth_src)
                continue

                # try:
                #     pass
                # except Exception:
                #     log.error('Error uncropping image')
                #     raise


                    # log.info('Image loaded: ' + image_src + ', ' +  ground_truth_src)

            # Construct the weight map (based on the grand truth)
            wtmap = weightsConstructor.constructWeightMap(truth)

            # create a class for the image / sample
            images.append((image, truth, wtmap, cB, image_src, stage))

    log.info("Successfully loaded {} images.".format(len(images)))

    # sort images by stage
    images.sort(key=lambda sample: sample[5])

    return images


def read_and_process_images(apply_fun):
    """
    Read images applying the provided function and save to the path given by setting.

    :apply_fun function: list of images , index -> list of images (note: list may be empty)
    """

    csv_filename = imagePaths.get_csv_file_path()

    with open(csv_filename) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')

        for index, row in enumerate(reader):

            # Get source/target images paths
            src_paths, target_paths = imagePaths.get_images_paths(row)

            src_paths_str = reduce(lambda x, y: x + ', ' + y, src_paths)

            # Read the images
            try:
                images = map(lambda src_path: load_image(src_path), src_paths)
            except Exception:
                log.error('Image not found: ' + src_paths_str)
                continue

            log.info('Processing image: ' + src_paths_str)

            # Read the grand truth
            # imSrc = Setting.cropTruthDir + row['stage'] + "/mask" + row['image_id'] + ".png"
            # truth = read_image(imSrc)
            # print('truth size' + tuple2str(np.shape(truth)))

            # Apply the given function (e.g. segmentation of the image)
            processed_images = apply_fun(images, index)

            # --- Save the images ---
            for processed_image_index, processed_image in enumerate(processed_images):

                # Get the corresponding target path
                target_path = target_paths[processed_image_index]

                # Create the target directory if not created yet
                dir = os.path.dirname(target_path)
                if not os.path.exists(dir):
                    os.makedirs(dir)

                # Save the image
                save_image(processed_image, target_path)
                log.info('Image saved to: ' + target_path)
