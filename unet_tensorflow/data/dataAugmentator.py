
"""
Data augmentation based on Siddhants code.
"""

import math

import numpy as np
from scipy.interpolate import interp2d, interpn
import setting.rootSetting as Setting

# np.random.seed(1234)


def rotate2(x, y, theta, ox, oy):
    """
    Rotate arrays of coordinates x and y by theta radians about the
    point (ox, oy).

    """
    s, c = np.sin(theta), np.cos(theta)
    x, y = x - ox, y - oy
    return x * c - y * s + ox, x * s + y * c + oy


def cut_tiles(img_full, cB, truth, wtmap, augment, req_size=572, mask_size=388, full_size=700):
    """

    :param img_full:  full image
    :param cB:  bounding box (?)
    :param truth: ground truth
    :param wtmap: weight map
    :param req_size: required size (?)
    :param mask_size:
    :param full_size:
    :param perturb_var:  param of augmentation
    :return:
    """

    if augment:
        # random rotation: from uniform distribution on [0,2pi)
        theta = 2 * math.pi * np.random.uniform()  # np.random.randn(1)

        # elastic deformation
        perturb_var = 10
    else:
        # TODO can we skip some operations when the augmentaion is disabl<ed?
        theta = 0
        perturb_var = 0


    # TODO move outside this method
    if Setting.dataset_for_task == 'evaluate segmented' and np.ndim(img_full) == 3:
        # convert to grayscale
        img_full = img_full[:,:,0]

        # add borders
        # img_full = imageUtils.uncrop_image(img_full)


    # vals = np.unique(img_full, return_counts=False)
    # print(vals)
    # print(len(vals))


    size_mask_x, size_mask_y = cB.x_2 - cB.x_1, cB.y_2 - cB.y_1  # MUST BE int

    origin_mask_x, origin_mask_y = (full_size - size_mask_x) // 2, (full_size - size_mask_y) // 2
    origin_im_x, origin_im_y = origin_mask_x - cB.x_1, origin_mask_y - cB.y_1

    img_tiled = img_full

    if origin_im_x < 0:
        img_tiled = img_tiled[:, -origin_im_x:]
        origin_im_x = 0
    if origin_im_y < 0:
        img_tiled = img_tiled[-origin_im_y:, :]
        origin_im_y = 0
    if origin_im_x + img_tiled.shape[1] > full_size:
        img_tiled = img_tiled[:, :full_size - origin_im_x]
    if origin_im_y + img_tiled.shape[0] > full_size:
        img_tiled = img_tiled[:full_size - origin_im_y, :]

    img_tiled = np.lib.pad(img_tiled, ((origin_im_y, full_size - img_tiled.shape[0] - origin_im_y),
                                       (origin_im_x, full_size - img_tiled.shape[1] - origin_im_x)), 'symmetric')

    mask_tiled = np.lib.pad(truth, ((origin_mask_y, full_size - truth.shape[0] - origin_mask_y),
                                    (origin_mask_x, full_size - truth.shape[1] - origin_mask_x)), 'constant',
                            constant_values=2)

    wtmap_tiled = np.lib.pad(wtmap, ((origin_mask_y, full_size - truth.shape[0] - origin_mask_y),
                                     (origin_mask_x, full_size - truth.shape[1] - origin_mask_x)), 'constant',
                             constant_values=0)

    Nodes = np.linspace(0, full_size - 1, 4)

    X_small, Y_small = np.meshgrid(Nodes, Nodes)
    X_full, Y_full = np.meshgrid(np.arange(full_size), np.arange(full_size))

    DX_small = perturb_var * np.random.randn(4, 4)
    DY_small = perturb_var * np.random.randn(4, 4)

    # DX_full = griddata((X_small.flatten(), Y_small.flatten()), DX_small.flatten(), (X_full, Y_full), method="cubic", fill_value=0);
    # DY_full = griddata((X_small.flatten(), Y_small.flatten()), DY_small.flatten(), (X_full, Y_full), method="cubic", fill_value=0);

    f1 = interp2d(Nodes, Nodes, DX_small.flatten(), kind="cubic")
    f2 = interp2d(Nodes, Nodes, DY_small.flatten(), kind="cubic")
    DX_full = f1(np.arange(full_size), np.arange(full_size))
    DY_full = f2(np.arange(full_size), np.arange(full_size))

    Xf_full = X_full + DX_full
    Yf_full = Y_full + DY_full

    X_query_norotation, Y_query_norotation = (Xf_full[(full_size - req_size) // 2:(full_size + req_size) // 2,
                                              (full_size - req_size) // 2:(full_size + req_size) // 2],
                                              Yf_full[(full_size - req_size) // 2:(full_size + req_size) // 2,
                                              (full_size - req_size) // 2:(full_size + req_size) // 2])

    X_query, Y_query = rotate2(X_query_norotation, Y_query_norotation, theta, full_size // 2, full_size // 2)
    X_query_mask_norotation, Y_query_mask_norotation = (
        Xf_full[(full_size - mask_size) // 2:(full_size + mask_size) // 2,
        (full_size - mask_size) // 2:(full_size + mask_size) // 2],
        Yf_full[(full_size - mask_size) // 2:(full_size + mask_size) // 2,
        (full_size - mask_size) // 2:(full_size + mask_size) // 2])

    X_query_mask, Y_query_mask = rotate2(X_query_mask_norotation, Y_query_mask_norotation, theta, full_size // 2,
                                         full_size // 2)

    cut_aug = interpn((np.arange(full_size), np.arange(full_size)), img_tiled, (X_query, Y_query), method="nearest",
                      bounds_error=False, fill_value=0)
    truth_aug = interpn((np.arange(full_size), np.arange(full_size)), mask_tiled, (X_query_mask, Y_query_mask),
                        method="nearest", bounds_error=False, fill_value=2)
    wtmap_aug = interpn((np.arange(full_size), np.arange(full_size)), wtmap_tiled, (X_query_mask, Y_query_mask),
                        method="nearest", bounds_error=False, fill_value=0)

    if Setting.debug_mode:
        # assert that weights outside the mask are zero
        b = (0 > np.sign(wtmap_aug) * truth_aug).any() or (1 < np.sign(wtmap_aug) * truth_aug).any()
        assert not b

    return cut_aug, truth_aug, wtmap_aug