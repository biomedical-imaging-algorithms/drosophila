import numpy as np

from setting import rootSetting as Setting
from utils import imageUtils, mathUtils
from utils.utils import apply_to_index

__input_data_mu__ = None
__input_data_sigma__ = None


def __normalize_images1__(images):
    """
    Normalize images to the unit variance and zero mean.

    Note1: We assume that the images are already loaded.
    """

    global __input_data_mu__, __input_data_sigma__

    train_images = images[0]

    # --- compute mean and (approx) sd on TRAIN SET ---
    means = map(lambda x: np.mean(x[0]), train_images)
    mean = sum(means) / len(means)  # mean from means

    vars = map(lambda x: np.var(x[0]), train_images)
    sd = np.sqrt(np.mean(vars))

    __input_data_mu__ = mean
    __input_data_sigma__ = sd

    # --- Normalize ALL ---
    # #TODO normalized more than once
    def normalize(sample):
        # replace original image with normalized
        normalized_image = imageUtils.normalize_image(sample[0], mean, sd)
        return (normalized_image,) + sample[1:]

    normalized = map(lambda set_of_images: map(normalize, set_of_images), images)
    return normalized


def __normalize_images2__(images):
    """
    Centralize images and scale to the interval [-1,1].

    Note1: We assume that the images are already loaded.
    """

    train_images = images[0]

    # --- compute mean and (approx) sd on TRAIN SET ---
    means = map(lambda x: np.mean(x[0]), train_images)
    mean = sum(means) / len(means)  # mean from means

    maximums = map(lambda x: np.max(x[0]), train_images)
    minimums = map(lambda x: np.min(x[0]), train_images)

    total_max = np.max(maximums)
    total_min = np.min(minimums)

    scale = np.max([total_max, -total_min])

    # --- Normalize ALL ---
    def normalize(sample):
        # replace original image with normalized
        normalized_image = imageUtils.normalize_image(sample[0], mean, scale)
        return (normalized_image,) + sample[1:]

    normalized = map(lambda set_of_images: map(normalize, set_of_images), images)
    return normalized


def __normalize_images3__(images):
    """
    Scole to the interval [0,1].

    Note1: We assume that the images are already loaded.
    """

    train_images = images[0]

    maximums = map(lambda x: np.max(x[0]), train_images)
    minimums = map(lambda x: np.min(x[0]), train_images)

    total_max = np.max(maximums)
    total_min = np.min(minimums)

    # --- Scale ALL ---
    def normalize(sample):
        # replace original image with normalized
        normalized_image = (sample[0] - total_min) / (total_max - total_min + 1e-5)
        return (normalized_image,) + sample[1:]

    normalized = map(lambda set_of_images: map(normalize, set_of_images), images)
    return normalized


def __normalize_images4__(images):
    """
    Centralize, scale to a unit variance and the apply sigmoidal transform to the interval [-1,1].

    Note1: We assume that the images are already loaded.
    """

    global __input_data_mu__, __input_data_sigma__

    train_images = images[0]

    # --- compute mean and (approx) sd on TRAIN SET ---
    means = map(lambda x: np.mean(x[0]), train_images)
    mean = sum(means) / len(means)  # mean from means

    vars = map(lambda x: np.var(x[0]), train_images)
    sd = np.sqrt(np.mean(vars))

    __input_data_mu__ = mean
    __input_data_sigma__ = sd

    # --- Normalize ALL ---

    normalize_fun = mathUtils.get_sgm_normalization_fun(mean, sd)
    normalize = lambda sample: apply_to_index(normalize_fun, sample, 0)

    normalized = map(lambda set_of_images: map(normalize, set_of_images), images)

    return normalized


# --- Public Interface ---

def normalize_images(images):
    """
    :param images: sets of images, where the first is train set (on which is computed eg. mean, variance etc)
    """

    variants = {
        1: __normalize_images1__,
        2: __normalize_images2__,
        3: __normalize_images3__,
        4: __normalize_images4__
    }

    # call the selected method
    return variants[Setting.input_images_normalization_type](images)


def get_mu_and_sigma():
    """
    Get stats computed on the training data.
    """
    return __input_data_mu__, __input_data_sigma__
