import os
import sys
import numpy as np
import tensorflow as tf

importPath = os.path.abspath(os.path.join('.'))
sys.path.append(importPath)
# print('\nPath to modules:' + importPath)

# --- Import internal libraries ---

# must be before importing scripts modules
from utils import loggingUtils
log = loggingUtils.get_logger(__name__)

import setting.rootSetting as Setting
from utils import mathUtils, imageUtils, utils
from model import modelOptimizer, trainStep
from data import MTDataProvider, dataProvider
from variables import modelSaver


def __terminate_training(sess, train_coord, test_coord, enqueue_train_threads, enqueue_test_threads):
    """
    Stop threads for generating train and test data and close the sesstion.
    """

    # When done, ask the threads to stop. And wait for them to actually do it.
    train_coord.request_stop()
    train_coord.join(enqueue_train_threads)

    if not test_coord is None:
        train_coord.request_stop()
        train_coord.join(enqueue_test_threads)

    # Close the session
    sess.close()


def __get_train_ops(nn, stats):
    train_ops = [
        {'step': nn.step, 'error': nn.classification_error_, 'loss': nn.loss_, 'train_step': nn.train_step},
        stats['train'].update_ops
    ]

    # optionally add nan check
    train_ops = train_ops + [{'nan_check': nn.nan_check}] if Setting.debug_mode else train_ops

    # convert to a single dictionary
    train_ops = utils.concat_dictionaries(train_ops)

    return train_ops


def __get_test_ops(nn, stats):
    test_ops = [
        {'step': nn.step, 'error': nn.classification_error_, 'loss': nn.loss_},
        stats['test'].update_ops
    ]

    # optionally add nan check
    test_ops = test_ops + [{'nan_check': nn.nan_check}] if Setting.debug_mode else test_ops

    # convert to a single dictionary
    test_ops = utils.concat_dictionaries(test_ops)

    return test_ops


def train_model():
    """
    Train U-Net on generated augmented data.
    """

    # Log used Setting
    for attr in dir(Setting):
        if not attr.startswith('__'):
            v = getattr(Setting, attr, None)
            log.debug((attr + ': ', v))

    # Create TensorFlow variables with initialized variables.
    sess, nn, stats = modelSaver.initiate_variables_for_training()

    # --- Input data queues ---

    use_test_data = Setting.testtrain_type == 1
    test_eval_period = Setting.test_eval_period

    # Start parallel processes for generating & storing input data into queue
    dequeue_train_sample, train_coord, enqueue_train_threads = MTDataProvider.create_and_start_inputDataQueueRunner_train(
        sess)

    if use_test_data:
        dequeue_test_sample, test_coord, enqueue_test_threads = MTDataProvider.create_and_start_inputDataQueueRunner_test(
            sess)
    else:
        dequeue_test_sample, test_coord, enqueue_test_threads = None, None, None

    # --- Train & Test Operations ---

    # operations that are evaluated on train samples
    train_ops = __get_train_ops(nn, stats)

    # operations that are evaluated on test samples
    test_ops = __get_test_ops(nn, stats)

    # --- Training loop ---

    step = sess.run(nn.step)

    while True:

        # input_data_mu, input_data_sigma = sess.run([nn.input_data_mu, nn.input_data_sigma])
        # print('input_data_mu, input_data_sigma ', input_data_mu, input_data_sigma)

        # Controlling termination with the coordinator
        if train_coord.should_stop() or (not test_coord is None and test_coord.should_stop()):
            break

        # Get the training sample
        X, Y, W = sess.run(dequeue_train_sample)

        # Eval the operations
        try:
            train_vals = sess.run(train_ops,
                                  feed_dict={
                                      nn.x: X, nn.y_: Y, nn.weight_map: W,
                                      nn.keep_prob: 1.0 - Setting.dropout_prob,
                                      nn.learning_rate_: trainStep.get_learning_rate(step),
                                      nn.do_update: True  # update step, estimates for batch normalization etc.
                                  })

        except tf.InvalidArgumentError as error:
            log.error('InvalidArgumentError', error)
            __terminate_training(sess, train_coord, test_coord, enqueue_train_threads, enqueue_test_threads)

        # Print some info
        log.info('Step: {} | '.format(train_vals['step']) +
                 'Classification_error: {} | '.format(train_vals['error']) +
                 'Loss: {}'.format(train_vals['loss']))

        # Current step  (it was updated by an update op)
        step = (train_vals['step'] + 1)

        if use_test_data and step % test_eval_period == 0:
            # TODO  test it - loading stats
            # Get the test sample
            X, Y, W = sess.run(dequeue_test_sample)

            try:
                pass
                test_vals = sess.run(test_ops,
                                     feed_dict={
                                         nn.x: X, nn.y_: Y, nn.weight_map: W,
                                         nn.keep_prob: 1.0 - Setting.dropout_prob,
                                         nn.learning_rate_: 0,  # learning rate should not be used for evaluation here
                                         nn.do_update: False
                                     })
            except tf.InvalidArgumentError as error:
                log.error('InvalidArgumentError', error)
                __terminate_training(sess, train_coord, test_coord, enqueue_train_threads, enqueue_test_threads)

            # Print some info
            log.info('Step: {} | '.format(test_vals['step']) +
                     'Test_classification_error: {} | '.format(test_vals['error']) +
                     'Test_loss: {}'.format(test_vals['loss']))

        # Print values of stats
        # if Setting.debug_mode:
        #     stat_names = stats['train'].stats.keys()
        #     stat_vars = stats['train'].stats.values()
        #     stat_values = sess.run(stat_vars)
        #
        #     for name, val in zip(stat_names, stat_values):
        #         log.info(name)
        #         log.info(val)

        # Store model
        modelSaver.maybe_save_model(sess, train_vals['step'])

    __terminate_training(sess, train_coord, test_coord, enqueue_train_threads, enqueue_test_threads)


def train_model_test():
    """
    Like train_model_main(), but we use test/debug setting.
    """

    Setting.dataset_for_task = 'train model'

    # Setting.debug_mode = True
    Setting.chanel_root = 16  # lower number of channels for fast testing
    # Setting.unet_architecture_variant = 2

    # use minimal resolution as in paper -> we can easily check that all shapes are correct
    # Setting.nx = Setting.ny = Setting.imSize = 572
    # Setting.maskSize = 388

    with tf.device(Setting.device):
        train_model()


def train_model_main():
    """
    High-level train function.
    """

    with tf.device(Setting.device):
        train_model()


if __name__ == "__main__":
    Setting.dataset_for_task = 'train model'

    train_model_main()
    # train_model_test()
