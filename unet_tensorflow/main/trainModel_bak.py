
"""
TODO delete this file when the nwe version is complemete and tested
"""

import os
import sys
from time import *

import numpy as np
import tensorflow as tf

importPath = os.path.abspath(os.path.join('.'))
sys.path.append(importPath)
print('\nPath to modules:' + importPath)

# Import internal libraries
import setting.rootSetting as Setting
from utils import mathUtils, imageUtils
from model import modelOptimizer
from data import MTDataProvider, dataProvider
from variables import modelSaver
import modelEvaluator.modelEvaluator


def train_model():

    # Create TensorFlow variables with initialized variables.
    sess, NN, Step, error_over_time = modelSaver.initiate_variables_for_training()

    # --- Train U-Net on generated augmented data ---

    # TODO show proper iteration when a stored NN is loaded
    # TODO remove local iteration "i"? and use "while"?

    # Start parallel process for generating & storing input data into queue
    dequeue_imput_data, coord, enqueue_threads = MTDataProvider.create_and_start_inputDataQueueRunner(sess)

    for local_step in xrange(Setting.max_train_steps):

        # Controlling termination with the coordinator
        if coord.should_stop():
            break

        # TODO rename itration to step and use integer
        # Step considering the previous runs
        step = int(sess.run(Step)[0])

        # Get data from the queue
        x, y, w = sess.run(dequeue_imput_data)

        # A single threaded version - i.e. creating data now instead of using parallel process
        # x, y, w = dataProvider.dataProvider.create_batch()

        if Setting.debug_mode:
            # check some basic properties of y and w
            assert np.min(y * w) >= 0, "trainModel: Incorrect data"
            assert np.min((1 - y) * w) >= 0, "trainModel: Incorrect data"

        # Compute error on X,Y & update graph
        if step % Setting.eval_period == 0:
            # Compute error
            def compute_mean_error_on_sample_images(non_augmented_images, sample_size = 10 - (10 % Setting.batch_size)):

                batch_size = Setting.batch_size
                assert (sample_size % batch_size == 0)

                # get sample augmented images
                # sample = random.sample(dataProvider.get_augmented_images(non_augmented_images), sample_size)
                sample = mathUtils.get_sample(dataProvider.get_tiles(non_augmented_images), sample_size)

                # for i in range(sample_size / batch_size):
                #     images = sample[i*batch_size:(i+1)*batch_size]
                #     compute_error(images)

                # groups of images of size batch_size
                grouped_images = map(lambda i: sample[i*batch_size:(i+1)*batch_size], range(sample_size / batch_size))

                def compute_error(images):
                    X, Y, W = imageUtils.convert_images_to_tensors(images)
                    return modelEvaluator.modelEvaluator.compute_error_for_model(NN, X, Y, W, sess)

                errors = map(compute_error, grouped_images)

                # compute mean
                error = np.mean(errors)
                return error

            # TODO rewrite & minimize number of evaluation i.e. calling sess.run() ->use more tensorflow style
            # TODO they are not augmented?
            # Generate sample from train set and test set and compute error
            train_set_error = compute_mean_error_on_sample_images(dataProvider.get_train_images())
            test_set_error = compute_mean_error_on_sample_images(dataProvider.get_test_images())

            if train_set_error < 0 or test_set_error < 0:
                raise('Negative error: {} (train), {} (test)'.format(train_set_error, test_set_error))

            # (Note on the error computing: X,Y are augmented data, it is unlikely that the same data were
            # have been already used for training)
            # already used for training, however the dame data (just with different transformation) might
            # t = time()
            # error = modelOptimizer.compute_error(NN, x, y, w)
            # print("Compute Error - Duration: {}s".format(round(time() - t, 3)))

            # append the most recet error
            update_error_op = tf.assign(
                error_over_time,
                tf.concat(0, [error_over_time, [[float(step), train_set_error, test_set_error]]]),
                validate_shape=False
            )

            # update_error_op = error_over_time.assign(
            #     tf.concat(0, [error_over_time, [[float(step), train_set_error, test_set_error]]]) #,validate_shape=False
            # )

            sess.run(update_error_op)

            ### Print Iteration & Error ###

            iterationInThisRunText = ''
            if step != local_step:
                # Case when we continue to train previously trained NN
                iterationInThisRunText = " (current run {})".format(local_step)

            print("| " + ctime(time()) + " | Iteration {}".format(step) + iterationInThisRunText + " | Error: {} (train), {} (test)"
                  .format(train_set_error, test_set_error) + " | ")


        # Train on X,Y
        # t = time()

        # for Y values outside [0,1] weights must be 0
        if Setting.debug_mode:
            assert np.min(y * w) >= 0
            assert np.min((1 - y) * w) >= 0

        # Check that cross enropy is non-negative
        # cross_entropy = nn.cross_entropy_.run(feed_dict={
        #     nn.out: x, nn.y_: y, nn.weight_map: w
        # })
        # assert(cross_entropy >= 0)

        modelOptimizer.run_train_iteration(NN, step, x, y, w, sess)
        # print("Train Iteration - Duration: {}s".format(round(time() - t, 3)))

        # Save the model iff step > 0 and step % save_period == 0
        modelSaver.maybe_save_model(sess, step)

        # Update iteration
        sess.run(Step.assign(Step + 1).op)
        # Step.assign(Step + 1).op.run()

    # When done, ask the threads to stop. And wait for them to actually do it.
    coord.request_stop()
    coord.join(enqueue_threads)


def train_model_test():
    """
    Like train_model_main(), but we use test/debug setting.
    """

    Setting.dataset_for_task = 'train model'

    # Setting.debug_mode = True
    Setting.chanel_root = 16  # lower number of channels for fast testing
    # Setting.unet_architecture_variant = 2

    # use minimal resolution as in paper -> we can easily check that all shapes are correct
    # Setting.nx = Setting.ny = Setting.imSize = 572
    # Setting.maskSize = 388

    with tf.device(Setting.device):
        train_model()


def train_model_main():
    """
    High-level train function.
    """

    with tf.device(Setting.device):
        train_model()


if __name__ == "__main__":

    Setting.dataset_for_task = 'train model'

    train_model_main()
    # train_model_test()
