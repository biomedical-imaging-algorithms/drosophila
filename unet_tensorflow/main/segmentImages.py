"""
Images segmentation using a given model.
"""

import os
import sys
import getopt
from time import *
import numpy as np
import tensorflow as tf

importPath = os.path.abspath(os.path.join('.'))
sys.path.append(importPath)
print('\nPath to modules:' + importPath)

# --- Import internal libraries ---

# must be before importing scripts modules
from utils import loggingUtils

log = loggingUtils.get_logger(__name__)

import setting.rootSetting as Setting
from data import dataLoader
from utils import mathUtils, imageUtils
from variables import modelSaver, variablesManager


def __load_model__(model_name):
    # get model path
    model_path = modelSaver.__get_model_path(exp_name=model_name)

    # get TF variables
    nn, stats = variablesManager.get_tf_variables()

    sess = tf.Session()
    saver = tf.train.Saver()

    # Get values of all TF variables
    saver.restore(sess, model_path)

    return nn, stats, sess


def __combine_images__(original_image, segmented_image):
    """
    Visualize an original image and its segmentation in a single image.

    :param original_image: original grayscale image
    :param segmented_image: black-white (or grayscale) image that represent segmentation (or probabilistic segmentation)
    """

    # Scale to [0,1] to have an equal weight  (out original image is in [-1, 1])
    original_image = mathUtils.normalize_to_unit_interval(original_image)

    xsize = original_image[:, 0].size
    ysize = original_image[0, :].size

    rgbIm = np.zeros((xsize, ysize, 3), 'uint8')

    # weight sof the original image
    q = 0.9
    # q = 1

    # TODO use create_rgb_image function

    # note that the values will be rounded to integer values
    rgbIm[..., 0] = 255 * (q * original_image + (1 - q) * segmented_image)
    rgbIm[..., 1] = 255 * (q * original_image + (1 - q) * (1 - segmented_image))
    rgbIm[..., 2] = 255 * (q * original_image + (1 - q) * segmented_image)

    # image = original_image
    # xsize = image[:, 0].size
    # ysize = image[0, :].size
    #
    # rgbArray = np.zeros((xsize, ysize, 3), 'uint8')
    # rgbArray[..., 0] = image
    # rgbArray[..., 1] = 1 - image
    # rgbArray[..., 2] = image
    # rgbIm = rgbArray

    return rgbIm


def __get_segment_fun__(nn, stats, sess, apply_thresholding):
    """
    Create a function for the full images segmentation.
    """

    # Get function for normalization of images
    input_data_mu, input_data_sigma = sess.run([nn.input_data_mu, nn.input_data_sigma])
    normalize_fun = mathUtils.get_sgm_normalization_fun(input_data_mu, input_data_sigma)

    def segment_image_tiles(image_tiles):
        """
        Segment multiple tiles at once.
        :param image_tiles:
        :return:
        """

        n_tiles = image_tiles.shape[0]

        if n_tiles > Setting.batch_size:
            # In this case we split tiles into more batches

            segmented_images = np.zeros((n_tiles, Setting.maskSize, Setting.maskSize))

            segmented_images[:Setting.batch_size] = segment_image_tiles(image_tiles[:Setting.batch_size, :, :])
            segmented_images[Setting.batch_size:] = segment_image_tiles(image_tiles[Setting.batch_size:, :, :])

            return segmented_images
            # print('too many tiles ({}) at once - not supported yet'.format(n_tiles))

        # Copy the image Setting.n_image times
        X = np.zeros((Setting.batch_size, Setting.nx, Setting.ny))
        X[:n_tiles, :, :] = image_tiles

        # Compute NN Output
        output = sess.run(nn.output_map, feed_dict={nn.x: X, nn.keep_prob: 1.0, nn.do_update: False})

        # Extract the segmented images
        segmented_images = output[:n_tiles, :, :, 1]

        return segmented_images

    def segment_image(images, index):

        if Setting.segment_only_test_set and index % Setting.cv_folds != Setting.cv_test_set_index:
            return []

        original_image = images[0]

        # normalize the image
        original_image = normalize_fun(original_image)

        # also store histogram of the normalized imaged for testing
        # save_histogram(original_image, 'tmp/normalized_image.png')

        segmented_image = imageUtils.tile_image_and_segment2(original_image,
                                                             segment_multiple_images_fun=segment_image_tiles)

        # apply thresholding: we expect output in range [0,1]
        if apply_thresholding:
            # thresholding
            # segmented_image = imageUtil.apply_thresholding(segmented_image)

            # soft thresholding  - using sigmoidal function
            r = 50.0
            segmented_image = 1.0 / (1 + np.exp(- r * (segmented_image - 0.5)))

            # optional visualization
            segmented_image = __combine_images__(original_image, segmented_image)

        return [segmented_image]

    return segment_image


def segment_images(model_name, apply_thresholding):
    """
    Main function.
    """

    # Load the model
    nn, stats, sess = __load_model__(model_name)

    # Create a function for images segmentation
    segment_image_fun = __get_segment_fun__(nn, stats, sess, apply_thresholding)

    # Segment the images
    dataLoader.read_and_process_images(apply_fun=segment_image_fun)

    # Close session
    sess.close()


if __name__ == "__main__":
    # This make sense to be True only when segmenting the original data on which the model for trained.
    Setting.segment_only_test_set = True

    models = [
        'test165-cv0-momentum-nobatchnorm-halmos',
        'test169-cv1-momentum-nobatchnorm-halmos',
        'test170-cv2-momentum-nobatchnorm-halmos',
        'test171-cv3-momentum-nobatchnorm-halmos',
        'test172-cv4-momentum-nobatchnorm-halmos'
    ]

    for i in range(Setting.cv_folds):

        Setting.cv_test_set_index = i
        model_name = models[Setting.cv_test_set_index]
        # 'test168-cv3-momentum-nobatchnorm-halmos'  # 'test163-testset-momentum-nobatchnorm-halmos'

        # Setting.dataset_for_task = 'segment images'
        Setting.dataset_for_task = 'evaluate model'  # 'segment images'

        log.info('Segmenting images ...')
        log.debug('Setting.task_environment: ' + Setting.task_environment)

        # model_name = 'debug030'  # 'test160-testset-momentum-nobatchnorm-halmos'  # 'test159-testset-momentum-nobatchnorm-halmos_step120000' #  'debug027' # 'test27'
        # note; this is still cv0 (just wrong naming)
        # apply_thresholding = True
        apply_thresholding = False

        segment_images(model_name, apply_thresholding)
