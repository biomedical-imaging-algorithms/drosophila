"""
Statistics (e.g. error) that are computed at each training or evaluation step.
"""

import tensorflow as tf

from data import dataProvider
from model import unet


# --- Public interface ---


class Statistics:
    """
    Representation of statistics that are computer on the same category of data (e.g. train / test set).
    """

    def __init__(self):
        # dict: statistics_name -> tf variable
        self.stats = {}

        # dict: statistics_name -> update op
        self.update_ops = {}

    def get_stat(self, name):
        raise NotImplementedError()

    def load_stats(self):
        raise NotImplementedError()

    def save_stats(self):
        raise NotImplementedError()

    def add_stat(self, stat_name, new_value, dtype=tf.float32):
        """
        Add a new statistics.

        :param stat_name: name of the statistics
        :param new_value: how the new value is computer (placeholder or function of placeholders)
        :param dtype: optional tensorflow type, default is float32
        :return:
        """

        # create tf variable
        var, update_op = self.__create_stat_variable(stat_name, new_value, dtype)

        # add variable and update operation to corresponding dictionaries
        self.stats[stat_name] = var
        self.update_ops[stat_name] = update_op

        # setattr(self, stat_name, var)
        # setattr(self, stat_name + '_update_op', update_op)

    @staticmethod
    def __create_stat_variable(name, new_value, dtype):
        """
        Create a tf variable for the statistics and the corresponding update operation.

        :param name: name of the variable that represents the statistics
        :param new_value: how the new value is computer
        :param dtype: tensorflow type, default is float32 - TODO maybe this could be derived from the new_value
        :return: variable for the statistics and update operation
        """

        # create an empty variable for storing values of statistics (e.g. at each step)
        statistics = tf.Variable([], name=name, dtype=dtype, trainable=False, validate_shape=False)

        # create tf operation for appending new value
        update_op = tf.assign(
            statistics,
            tf.cond(tf.equal(tf.shape(statistics)[0], 0),
                    # adding first value  (shape is determined by the first value)
                    lambda: tf.pack([new_value]),  # add 1 dimension (that will be zero-th dimension)
                    # adding value to non-empty variable
                    lambda: tf.concat(0, [statistics, [new_value]])  # tf.concat(0, [statistics, tf.pack([new_value])])
                    ),
            validate_shape=False
        )

        return statistics, update_op


def create_statistics(nn):
    """
    Create non-trainable variables for storing statistics (e.g. loss, durations, ...) and intermediate results of layers
    or statistics of them (e.g. min/max/mean weights/activations in the hidden layers).

    :param nn: a model containing placeholders (or functions of placeholder)
    :return dictionary: data_set -> Statistics class (eg. train_set -> stat1, test_set -> stat2)
    """

    # TODO add learning rate (to ensure it is computer correctly)
    # TODO possibiliry to load a model that does not contain all statistics

    # === Create stats and the corresponding update operations ===

    # --- Training statistics ---

    train_stats = Statistics()

    train_stats.add_stat('train_loss', nn.loss_)  # train loss
    train_stats.add_stat('train_error', nn.classification_error_)  # train classification error (e.g. cross entropy)

    # --- Test / validation statistics ---

    test_stats = Statistics()

    test_stats.add_stat('eval_step', nn.step, tf.int32)  # numbers of steps, when the model was evaluated
    test_stats.add_stat('test_loss', nn.loss_)  # train loss
    test_stats.add_stat('test_error', nn.classification_error_)  # train classification error (e.g. cross entropy)

    # stats = [eval_step, train_loss, train_error]
    # update_ops = [eval_step_update_op, train_loss_update_op, train_error_update_op]  #

    stats = {'train': train_stats, 'test': test_stats}

    return stats


# --- Tests ---


def create_variables_for_statistics_test():
    """
    This test demonstrates an example working with statistics.
    """
    # we use this import only for test
    from variables import modelSaver

    nn = unet.create_network()

    # create statistics and update ops
    stats = create_statistics(nn)
    stat_names = stats['train'].stats.keys()
    stat_vars = stats['train'].stats.values()
    update_ops = stats['train'].update_ops.values()

    with tf.Session() as sess:
        # Initialize variables
        sess.run(tf.initialize_all_variables())

        # Get initial values of variables - should be empty arrays
        values = sess.run(stat_vars)
        print(stat_names, values)

        # Update values of variables on sample data
        print('updating values ...')
        for step in range(5):
            X, Y, W = dataProvider.create_batch()

            sess.run(update_ops,
                     feed_dict={nn.step_: step, nn.x: X, nn.y_: Y, nn.weight_map: W, nn.keep_prob: 1.0,
                                nn.do_update: False})

        # Print values of variables - now it should not be empty arrays
        values = sess.run(stat_vars)
        print(stat_names, values)

        # store all tf variables (model + statistics)
        modelSaver.maybe_save_model(sess, 0)


def create_variables_for_statistics_test2():
    """
    Test loading statistics.

    Note that: we should load statistics in a new session.
    """

    # we use this import only for test
    from variables import modelSaver

    #
    sess, nn, stats = modelSaver.initiate_variables_for_training()

    stat_names = stats['train'].stats.keys()
    stat_vars = stats['train'].stats.values()
    update_ops = stats['train'].update_ops.values()

    stat_values = sess.run(stat_vars)

    for name, val in zip(stat_names, stat_values):
        print(name)
        print(val)

    # TODO next step: use statistics in the main script