
from data import dataProvider
from variables.modelSaver import  InitializedSession, get_errors_from_multiple_experiments


# def __multiple_eval_test():
#
#     X, Y, _ = dataProvider.create_batch()
#
#     exp_names = ['test27', 'test29-16channels']
#
#     res = multiple_eval(exp_names, X, Y)
#
#     print(len(res))
#     print(res)


def initialized_session_test():

    import tensorflow as tf

    exp_name = 'debug025'

    with InitializedSession(exp_name) as (sess, nn, stats):
        vars = tf.all_variables()

        print(sess.run(nn.step))

        # values = sess.run(vars)
        # print('val', values[0])


def get_errors_from_multiple_experiments_test():

    exp_names = ['debug026']
    res = get_errors_from_multiple_experiments(exp_names)
    print(res)