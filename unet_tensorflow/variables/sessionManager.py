
"""
Creating Tensor Flow session.
"""

import tensorflow as tf
import setting.rootSetting as Setting


# def get_session_config():
#
#     config = tf.ConfigProto(
#         allow_soft_placement=True,  # TODO investigate what exactly does this mean?
#         log_device_placement=Setting.log_device_placement,
#     )
#
#     config.gpu_options.allow_growth = True
#

__sess = None


def __create_session():
    """
    Create a new tensorFlow session.
    """

    config = tf.ConfigProto(
        allow_soft_placement=True,  # TODO investigate what exactly does this mean?
        log_device_placement=Setting.log_device_placement,
    )

    config.gpu_options.allow_growth = True

    sess = tf.Session(config=config)

    # set as default
    sess.as_default()

    return sess


def get_session():

    # TODO should be synchronized for accessing from multiple threads

    global __sess

    if __sess is None:
        __sess = __create_session()

    return __sess


# def create_session_test():
#     """
#     This test show a common usage of a session.
#     """




