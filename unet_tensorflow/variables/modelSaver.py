"""
Saving and loading models (i.e. values of TF variables).

Note that: Access from multiple threads is not supported. This would require adding synchronization.
"""

import tensorflow as tf
import numpy as np
import os
from shutil import copyfile
import time

import setting.rootSetting as Setting
from data import dataNormalization
from data import dataProvider
from utils import loggingUtils
from variables import sessionManager, variablesManager
from utils.utils import synchronized

log = loggingUtils.get_logger(__name__)

__saver = None


@synchronized
def __get_saver():
    global __saver

    if __saver is None:
        # create a saver with no limitation on number of saved models
        __saver = tf.train.Saver(max_to_keep=0)

    return __saver


def __get_model_path(step=None, exp_name=Setting.experiment_name):
    """
    Get the path for storing / loading trained model.

    :param step: Optional step number that will be added as a suffix.
    :return:
    """

    # Path to the stored model (i.e. variables)
    if step is None:
        model_path = os.path.abspath(os.path.join(Setting.trained_models_dir + exp_name))
    else:
        model_path = os.path.abspath(os.path.join(Setting.trained_models_dir + exp_name + "_step{}".format(step)))

    return model_path


# --- Public Interface ---


class InitializedSession:
    """
    Context Manager for working with loaded model.

    Usage:
    with InitializedSession(exp_name) as (sess, nn, stats):
        ...

    Notes:
        - This removes the tf graph, creates new (from .meta file) and loads variables.
        - Variables can be then accesses using: tf.get ...
        - Session is closed when leaving the 'with' block.

    For more details how to work with Context Manager see:
     http://preshing.com/20110920/the-python-with-statement-by-example/
    """

    def __init__(self, exp_name):
        self.exp_name = exp_name

    def __enter__(self):
        """
        Enter 'with' block: create an initialized session.
        """

        sess, nn, stats = self.__create_initialized_session()
        self.sess = sess

        return sess, nn, stats

    def __exit__(self, type, value, traceback):
        """
        Exit 'with' block: close the session.
        """
        self.sess.close()

    def __create_initialized_session(self):
        """
        Create a session and load the model from relative path given by exp_name.

        Note that: close session using sess.close, or use inside 'with' block (see the unittest)

        :return session with loaded model
        """

        # Clear the tf graph
        tf.reset_default_graph()

        # Create new session
        sess = tf.Session()

        # Create new graph (with uninitialized variables)
        nn, stats = variablesManager.get_tf_variables()

        # sess.run(tf.initialize_all_variables())

        # Get paths
        model_path = os.path.abspath(os.path.join(Setting.trained_models_dir + self.exp_name))
        metafile_path = model_path + '.meta'

        # Note that: this creates a new graph
        # Create saver from metafile (i.e. load tf graph defs)
        # saver = tf.train.Saver() #
        saver = tf.train.import_meta_graph(metafile_path)

        # Restore the model (i.e. values of variables)
        # note that we could also use: tf.train.latest_checkpoint(dir_path) to get the newest
        saver.restore(sess, model_path)

        return sess, nn, stats


def initiate_variables_for_training():
    """
    Start variables and restore (continue with a previous experiment) or initiate variables (new experiment).

    Note: loading an existing model works only when the tf graph is compatible (i.e. no new variables we added or removed)
    """

    # Get or create a new session
    sess = sessionManager.get_session()

    # Get the path for storing / loading trained model
    model_path = __get_model_path()

    # Get or create all TF variables
    nn, stats = variablesManager.get_tf_variables()

    # Restore variables or initiate them
    if os.path.isfile(model_path):
        # Create/get saver
        saver = __get_saver()

        # Continue in the previous experiment
        saver.restore(sess, model_path)

        step = sess.run(nn.step)

        log.info("Saver: Continuing with experiment: " + Setting.experiment_name +
                 ", starting from iteration {}".format(step))

    else:
        # Start a new experiment
        sess.run(tf.initialize_all_variables())

        # Call create_batch() just to initialize stats in dataNormalizer
        dataProvider.create_batch()
        # get input data sd and var
        mu, sigma = dataNormalization.get_mu_and_sigma()

        # assign to the corresponding nn variables
        set_mu_and_sigma_ops = [nn.input_data_mu.assign(mu), nn.input_data_sigma.assign(sigma)]
        sess.run(set_mu_and_sigma_ops)

        log.info("Saver: Starting new experiment: " + Setting.experiment_name)

    return sess, nn, stats


def maybe_save_model(sess, step):
    """
    Save the model (i.e. all tf variables) iff step > 0 and step % save_period == 0.
    """

    saver = __get_saver()

    # Saving & rewriting previous stored model
    # if step > 0 and step % Setting.save_period == 0:
    if step % Setting.save_period == 0:  # we also save model with random weights
        model_path = __get_model_path()
        saver.save(sess, model_path)
        log.info('Model saved to: ' + model_path)

    # Saving model in form "..._step<step>" that will be not rewritten
    if step > 0 and step % Setting.backup_period == 0:
        model_path = __get_model_path(step=step)
        saver.save(sess, model_path)
        log.info('Model saved to: ' + model_path)

    # Setting - Copy setting file along the trained models (when the model is stored for the fist time)
    if step == Setting.save_period:
        src_path = os.path.abspath('setting/experimentSetting.py')
        dest_path = os.path.abspath('tmp/' + Setting.experiment_name + '-experimentSetting.py')
        copyfile(src_path, dest_path)
        # print(path)


def get_errors_from_multiple_experiments(exp_names):
    """
    Getting errors from multiple experiments.
    """

    nn, stats = variablesManager.get_tf_variables()

    errors = []

    for exp_name in exp_names:
        sess = tf.Session()
        saver = __get_saver()

        model_path = __get_model_path(exp_name=exp_name)

        # Get values of all variables
        saver.restore(sess, model_path)

        # error_list = sess.run(stats['train'].stats['train_error'])

        train_error = sess.run(stats['train'].stats['train_error'])

        try:
            test_error = sess.run(stats['test'].stats['test_error'])
            eval_step = sess.run(stats['test'].stats['eval_step'])

            # test error with same length as train error (missing values are None)
            test_error2 = [np.nan] * len(train_error)

            for step, err in zip(eval_step, test_error):
                test_error2[step] = err

        except:
            test_error = None

        errors.append((train_error, test_error2))

        sess.close()

    return errors
