"""
Creating & getting all tensor flow variables.
"""

import tensorflow as tf
from model import unet
from variables import statistics

# TODO we will have more types of variables:
# 1) model params - trainable vars
# 2) model params - not-trainable vars (eg. fixed weights, estimates for batch normalization)
# 3) statistics - not-trainable vars

# Cache for TF variables
__tf_vars = None


# class TFVars:
#     """
#     Class containing all tensor flow variables.
#     """
#     def __init__(self, nn, iteration, error_over_time):
#         # Neural network
#         self.nn = nn
#
#         # Current step
#         self.iteration = iteration
#
#         # Error
#         self.error_over_time = error_over_time
#
#     def as_tuple(self):
#         return self.nn, self.iteration, self.error_over_time


def __create_tf_variables():
    """
    Create all TF variables

    Note: We need to set trainable=False to all tf variables that are not params of the model.
    """

    # Network instance that include all model trainable and non-trainable variables
    nn = unet.create_network()

    # Statistics: non-trainable variables
    stats = statistics.create_statistics(nn)

    return nn, stats


# --- Public Interface ---


def get_tf_variables():
    """
    Get all Tensor Flow variables.

    Note: that the variable may not be initialized.
    """

    global __tf_vars

    if __tf_vars is None:
        # create variables
        nn, stats = __create_tf_variables()

        __tf_vars = (nn, stats)

    return __tf_vars
