# TODO plot the error function
# TODO plot true label and segmentation
# TODO remove unsudem methods
import logging

import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
import matplotlib.pyplot as plt

# from skimage import viewer
# from data import dataProvider
from utils import loggingUtils

log = loggingUtils.get_logger(__name__)
log.setLevel(logging.DEBUG)


def save_histogram(image, dest_path):
    """
    Create and save histogram of given image.
    """
    plt.hist(image)
    plt.savefig(dest_path)


def plot_segmented_image_with_pixel_error(original_image, segmented_image, ground_truth, pixel_error, save_to=None):
    # TODO also write total error
    # TODO transform the errors, use the same measure

    class_threshold = 0.5

    f, axarr = plt.subplots(2, 3)
    countours = []

    # --- Plot original image with contours ---
    ax = axarr[0, 0]
    ax.imshow(original_image, cmap=plt.cm.gray, interpolation='nearest', vmin=0, vmax=1)  #
    ax.set_axis_off()
    # axarr[0].set_title("Original Image")

    ax.contour(segmented_image, colors='red', label='', linewidths=3, alpha=0.5, levels=[class_threshold], )

    cnt = ax.contour(ground_truth, colors='green', linewidths=3, alpha=0.5, levels=[class_threshold])  #
    countours.append(cnt)
    ax.clabel(cnt)

    axarr[1, 0].hist(original_image)

    # --- Plot segmented image ---
    ax = axarr[0, 1]
    ax.imshow(segmented_image, cmap=plt.cm.gray, interpolation='nearest')  # , vmin=0, vmax=1
    ax.set_axis_off()

    axarr[1, 1].hist(segmented_image)

    # --- Plot pixel error ---
    ax = axarr[0, 2]
    ax.imshow(pixel_error, cmap=plt.cm.gray, interpolation='nearest', vmin=0, vmax=1)  #
    ax.set_axis_off()

    axarr[1, 2].hist(pixel_error)

    if save_to is not None:
        plt.savefig(save_to, bbox_inches='tight', pad_inches=0)

    # plt.tight_layout()
    plt.show(countours)


def plot_histogram(im):
    plt.hist(im)

    plt.show()


def plot_on_grid(images, labels=None, n_cols=5, interpolation='nearest'):
    """
    Plot multiple images on a grid of images.

    Note: call plt.show() to actually show the images.
    """

    sns.set_style("white")

    n_images = len(images)

    assert (n_images >= 1)

    if labels is None:
        labels = [''] * n_images

    n_rows = int(np.math.ceil((n_images - 1) / n_cols)) + 1

    f, axarr = plt.subplots(n_rows, n_cols)

    if n_rows == 1:
        for im_index in range(n_images):
            # When number of rows is 1, axaar has only 1 dimension

            axarr[im_index].imshow(images[im_index], cmap=plt.cm.gray, interpolation=interpolation)
            axarr[im_index].set_title(labels[im_index])

        # remove axis in all subplots
        for i in range(n_cols):
            axarr[i].set_axis_off()
    else:
        for im_index in range(n_images):
            col = im_index % n_cols
            row = im_index // n_cols

            axarr[row, col].imshow(images[im_index], cmap=plt.cm.gray, interpolation=interpolation)
            axarr[row, col].set_title(labels[im_index])

        # remove axis in all subplots
        for col in range(n_cols):
            for row in range(n_rows):
                axarr[row, col].set_axis_off()

                # plt.tight_layout()
                # plt.show()


def plotError(error):
    """Plot error over time (given as an np.array or list)"""

    # error = [[0,1], [1,1.2]]

    # convert list to array
    if type(error) == list:
        error = np.array(error)

    # df = pd.DataFrame(error, columns=("iter", "error"))

    # sns.tsplot([error, np.zeros(len(error))])
    sns.tsplot(error)
    sns.plt.ylim(0, )

    plt.title('Training Error')
    plt.xlabel('Step')
    plt.ylabel('Error')
    # plt.annotate('Battle of Two Bridges', xy=(4, 26), xytext=(5, 40),
    #              arrowprops=dict(facecolor='indianred', shrink=0.05, headwidth=20, width=7))


def plotError2(error, smooth_size=30):
    """Plot error over time (given as an np.array or list)"""

    # convert list to array
    if type(error) == list:
        error = np.array(error)

    x = error[:, 0]
    y = error[:, 1]

    # plt.scatter(x, y, alpha=0.3)

    # fit with np.polyfit
    # m, b = np.polyfit(x, y, 1)

    plt.plot(x, y, '.')  # , alpha=0.7
    # plt.plot(x, m * x + b, '-')

    # smooth over k samples
    smooth_frac = min(float(smooth_size) / len(x), 1.0)

    lowess = sm.nonparametric.lowess(y, x, frac=smooth_frac)

    plt.plot(lowess[:, 0], lowess[:, 1])


def plotError3(error, smooth_size=30):
    # TODO add labels
    # TODO optional test set
    # TODO (opt) add more graphs (e.g. loss, learning rate, ...)

    # convert list to array
    if type(error) == list:
        error = np.array(error)

    x = range(len(error))  # error[:, 0]
    y = error

    # plt.scatter(x, y, alpha=0.3)

    # fit with np.polyfit
    # m, b = np.polyfit(x, y, 1)

    plt.plot(x, y, '.')  # , alpha=0.7
    # plt.plot(x, m * x + b, '-')

    # smooth over k samples
    smooth_frac = min(float(smooth_size) / len(x), 1.0)

    lowess = sm.nonparametric.lowess(y, x, frac=smooth_frac)

    plt.plot(lowess[:, 0], lowess[:, 1])


def plot4(data, labels, xlabel='', ylabel='', title='', smooth=True, alpha=None, smooth_size=None, smooth_alpha=0.5):
    """

    Note: call plt.show() to actually show the graph.

    :param data: list of lists of the same lenght
    """

    xsize = len(data[0])

    if xsize < 1000:
        smooth_size = 30 if smooth_size is None else smooth_size
        alpha = 0.5 if alpha is None else alpha
    else:
        smooth_size = 100 if smooth_size is None else smooth_size
        alpha = 0.15 if alpha is None else alpha

    # cmap = plt.get_cmap('rainbow') # hsv  # 'prism' #'gnuplot'
    # colors = [cmap(i) for i in np.linspace(0, 1 - 1.0 / len(data), len(data))]
    colors = ['blue', 'red', 'green', 'black', 'pink']

    for y, label, color in zip(data, labels, colors):

        if y is None:
            continue

        x = range(xsize)

        # remove missing values
        # mask = np.isfinite(y)
        #
        # log.debug('y')
        # log.debug(y)
        # log.debug('mask')
        # log.debug(mask)

        # x = x[mask]
        # y = y[mask]

        if len(x) == 0:
            continue

        # plot points
        plt.plot(x, y, '.', label=label, color=color, alpha=alpha)

        # plt.plot(x, y1, '-b', label='sine')
        # plt.plot(x, y2, '-r', label='cosine')

        # plot smooth curve
        if smooth:
            # smooth over k samples
            smooth_frac = min(float(smooth_size) / len(x), 1.0)
            lowess = sm.nonparametric.lowess(y, x, frac=smooth_frac)
            plt.plot(lowess[:, 0], lowess[:, 1], color=color, alpha=smooth_alpha)

    title_font = {'fontname': 'Arial', 'size': '22', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}

    axis_font = {'fontname': 'Arial', 'size': '14'}

    plt.title(title, **title_font)
    plt.xlabel(xlabel, **axis_font)
    plt.ylabel(ylabel, **axis_font)

    plt.legend(loc='upper right')


def __plotError2_test():
    error = [[1, 2], [2, 3], [3, 4]]

    plotError2(error)

    plt.show(block=True)
    plt.show()


# TODO move to tests
def __plotErrorTest():
    """Test of plotError"""

    error = [2, 3, 1.5, 8]
    plotError(error)

    # Generate sample test data
    # x = np.linspace(0, 15, 31)
    # data = np.sin(x) + np.random.rand(10, 31) + np.random.randn(10, 1) / (x + 1)


    # plotError(np.random.rand(1, 31))
    #
    # errors1 = np.array([4.3, 3.2, 1.3])
    #
    # # plt.plot(errors)
    #
    #
    # d = pd.DataFrame()
    # d['x'] = [1, 2, 3]
    # d['y'] = errors1
    #
    # sns.set(style="darkgrid")
    #
    # # Load the long-form example gammas dataset
    # gammas = sns.load_dataset("gammas")
    #
    # # sns.tsplot(data=d,time='x', value='y', color="husl")
    #
    # # Plot the response with standard error
    # # sns.tsplot(data=gammas, time="timepoint", unit="subject",
    # #            condition="ROI", value="BOLD signal")
    #
    # x = np.linspace(0, 15, 31)
    # data = np.sin(x) + np.random.rand(10, 31) + np.random.randn(10, 1)
    # ax = sns.tsplot(data=np.random.rand(10, 31))
    # ax = sns.tsplot(data=np.random.rand(1, 31))
