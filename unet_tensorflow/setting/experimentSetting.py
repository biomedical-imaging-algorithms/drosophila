
"""
Setting specific for particular experiments (e.g. model params).
"""

# --- Number of channels in the first and last conv. layer ---

# Varint of unet implementation (some of them may be equivalent)
# unet_architecture_variant = 1
unet_architecture_variant = 2

# Number of features in the high-res layers
# chanel_root = 4  # for fast testing
# chanel_root = 16  # for testing
# chanel_root = 32
chanel_root = 64  # used in paper

# random weights of CNN generation - note do not use until it is correctly computed
initial_weights_generation = 'normalize'  # normalize using formula sqrt(2/n_inputs)

# initial_weights_generation = 'constant std'   # constant variance
# Two alternative bias initializations
# initial_bias_sd = 0.1
# initial_bias_sd = 0

# --- Tile size ---

# tile size (including "borders"), the resulting prediction is of size (nx-184)x(nx-184)
# paper -> we should use larger tiles
# nx = ny = imSize = 572
# maskSize = 388
#
nx = ny = imSize = 700
maskSize = 516

border_size = (imSize - maskSize) / 2

# input_images_normalization_type = 1  # unit variance, zero mean - (X-mi) / sigma
# input_images_normalization_type = 2  # centralize and scale to [-1,1]
input_images_normalization_type = 4  # like type 1, but we additionally apply sigmoidal transform

# use alternative ground truths (where class 1 = cell without borders)
segment_inner_parts_of_cells = False

# --- Batch size ---

# Number of images used in a single training step (or error evaluation)
# Note: the training parameter depend on the batch size (ie. learning rate ~ 1/ batchsize)
# Note2: the memory requirements are higher for higher batch sizes
# n_image = 11
# n_image = 10

# Batch size - currently supported only size of 1
batch_size = 1  # lowest memory usage
# batch_size = 3

# --- Optimizer ---

# Optimization method: adaptive learning step or simple gradient descent with momentum
# Notes:
# 1 ... AdamOptimizer
# 2 ... MomentumOptimizer
# 3 ... AdamOptimizer with lower training step
# 4 ... even lower training step
# model_optimizer_num = 3  # adam
model_optimizer_num = 2  # momentum

# AdamOptimizer: a small constant for numerical stability
adam_epsilon = 0.1

# Setting used by Siddhant
momentum_base_learning_rate = 0.001
momentum_decrease_period = 8000   # as used by siddhant
momentum_factor = 0.1  # consider using factor 2 (e.g. mult by 0.5)

# For batch normalization
#momentum_base_learning_rate = 0.001  # with BN we may consider a higher initial learning rate
#momentum_decrease_period = 800 # lower number of steps should be required with BN
#momentum_factor = 0.1

# ---  Regularization & Batch normalization---

# L2 regularization
alpha_l2loss = 0.00005  # used by Siddhant
# alpha_l2loss = 0.00001   # lower for combination with Batch Normalization (that is already a form of regularization)
# alpha_l2loss = 0
# alpha_l2loss = 0.01

# Dropout probability
# TODO ensure that dropout is computed correctly
# TODO consider dropout only on layer with highest number of features (or probability ~ num of features)
# Note: used only for training (0 = disabled)
dropout_prob = 0  # disabled
# dropout_prob = 0.5

# DEPRECATED:
# decreasing absolute value of all weights by factor (1 - weights_decay)
# note that: this corresponds to L2 regularization (for the constant learning rate)
# weights_decay = 0.00005

# it is computed each k step (i.e. we compute w <- w * (1 - weights_decay)^k each k-th step)
# weights_decay_compute_period = 10

batch_normalization = False
# batch_normalization = True
exp_moving_average_decay = 0.995   # TODO this param needs to be optimized

# TODO do not use them for now (not completed)
use_invariant_conv_layer = False

# --- Other ---

# Number of classes
n_class = 2

# size of the convolution
field_of_view = 3
max_pool_size = 2
max_train_steps = 100000  # TODO delete - no longer used

# pixel weights for computing the error  - TODO is this invariant to the scaling of the eggs?
# pixel_weights_w0 = 0
pixel_weights_w0 = 10  # used in paper
# pixel_weights_w0 = 20  # used by Siddhant
pixel_weights_sigma = 5  # used in paper and by Siddhant

# weights of pixels for training
# pixel_weights_w0 = 0
# pixel_weights_sigma = 5

# --- Cross-validation ---

# how to split data into test and train set
testtrain_type = 1  # two disj. sets
#testtrain_type = 2  # using only a single image
# testtrain_type = 3  # only 10 selected images for both train and test set
# testtrain_type = 4 # using all images (no test and train set)

# TODO store the info into another tf variable (statistics)
cv_folds = 5
cv_test_set_index = 0  # number between 0 and crossvalidation_folds-1

test_eval_period = 10

