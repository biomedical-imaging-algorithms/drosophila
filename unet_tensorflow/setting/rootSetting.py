
"""
Global setting
"""

# TODO load setting that include sizes of some tf variables (eg. number of channels)

# Import scripts settings
from experimentSetting import *
from segmentationSetting import *

#--- Defult setting that is set by main scripts ---

# TODO set in each main scitpt
# TODO add parameter to the scripts

# Notes
# 'train model' ... we use only train data
# 'evaluate model' ... we use only test data
# 'segment images' ... whole data

# TODO rename to e.g. dataset_for_task
# TODO use enum or class or dictionary?
# DATA SET FOR TASK
# note: for these selected tasks we have hardcoded paths to images
dataset_for_task = 'train model'
# task = 'evaluate model'
# task = 'segment images'
# task = 'adjust labels'


# ENVIRONMENT ... local or remote
task_environment = 'local'
# task_environment = 'halmos'


# --- Unique name of the experiment that will be loaded ---

# expName = 'exp02-IdentityWithNoise-GradientDescentOptimizer'
# expName = 'test23-MT'

# experiment_name = 'test31'
experiment_name = 'debug030'
# best so far - using 16 root channels
# experiment_name = 'test27'
# experiment_name = 'test30'
# experiment_name = 'test26'
# expName = 'exp03-IdentityWithNoise-AdamOptimizer'


# Period of computing error
eval_period = 1  # (EVAL_PERIOD = 1  ~ most accurate graph, but slower(?))
# Saving model
save_period = 10
# Saving model in from "..._step<step>" that will be not rewritten
backup_period = 50


# TODO split to multiple setting files (?)

### INPUT DATA ###

# useTestData = True
useTestData = False

augment_images = True

# Check range of values, etc - might result in a minor overhead
debug_mode = True

# Root dir containing the data
# Local dir:
# rootDir = '/home/marek/CMP/data2/'
# On Halmos:
# rootDir = '/mnt/datagrid/Medical/microscopy/drosophila/egg_segmentation/'

# csvFilename = rootDir + 'ovary_image_info_for_prague.csv'
# cropImDir = rootDir + 'ovary_2d/stage'
# cropTruthDir = rootDir + 'mask_2d_binary/stage'
# fullImDir = rootDir + 'slice/stage'

# directory for storing trained models
trained_models_dir = 'tmp/'

# --- Device ---
device = '/cpu:0'
# device = '/gpu:0'

log_device_placement = False


# For a square image nxn, the resulting prediction is of size (n-184) x (n-184)


### MLDataProvider ###

# max number of triples stored in a queue (X,Y,W)
queue_capacity = 20
# number of parallel threads for data preprocessing (i.e. data augmentation)
n_augment_threads = 2


### LEARNING ###

## Momentum Optimizer 
momentum = 0.99
#momentum = 0.9

#trainStep = tf.train\
#        .MomentumOptimizer(learning_rate=learning_rate_, momentum=momentum)\
#        .minimize(cross_entropy_)

        
# Adam Optimizer
# train_step = tf.train.AdamOptimizer(learning_rate=1e-3,epsilon=0.1).minimize(cross_entropy_)

