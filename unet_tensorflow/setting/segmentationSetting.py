
"""
Setting for applying loaded models to segments images.

See main/segmentImages.py
"""


# TODO delate this file

# Root directory
# segment_images_dir = '/mnt/datagrid/Medical/microscopy/drosophila/ovary_selected_slices/'
# segment_images_path = '/home/marek/CMP/data3/ovary_selected_slices/'
# segment_root_dir_path = '/home/marek/CMP/data2/'


# File containing relative paths of images that will be segmented
# segment_csv_filename = 'list_ovary_selected_slices.txt'   # ovary_selected_slices
# data with ground truth


# using a specific implementation for different directory structures
# segment_directory_structure_type = 2
#
# if segment_directory_structure_type == 1: # ovary_selected_slices
#
#     segment_csv_filename = 'list_ovary_selected_slices.txt'
# else: # slices with ground truth
#
#     segment_csv_filename = 'ovary_image_info_for_prague.csv'


