"""
Computing an error & visualization of given models.
"""

import tensorflow as tf

from variables import modelSaver, variablesManager
from variables.modelSaver import __get_model_path, __get_saver


def __multiple_eval__(exp_names, eval_fun):
    """
    Compute the given function using possibly multiple models.

    :param exp_names list of experiment/model names
    :param eval_fun function: (model, sess) -> result
    :param data for feeding the placeholders
    :return list of results (type of results depends on the given function)
    """

    # TODO this does not work for model with incompatible tf graph

    nn, _ = variablesManager.get_tf_variables()

    results = []

    for exp_name in exp_names:

        sess = tf.Session()

        # saver = tf.train.Saver(reshape=True)
        saver = __get_saver()

        model_path = __get_model_path(exp_name=exp_name)

        # Get values of all variables
        saver.restore(sess, model_path)

        # Compute something using the loaded model (NN)
        result = eval_fun(nn, sess)

        sess.close()

        results.append(result)
        # print(errorList)

    return results


def __multiple_eval2__(exp_names, eval_fun):
    """
    Compute the given function using possibly multiple models.

    :param exp_names list of experiment/model names
    :param eval_fun function: () -> result  (all variables are accessed by their name)
    :param data for feeding the placeholders
    :return list of results (type of results depends on the given function)
    """

    # -----

    # TODO this does not work for model with incompatible tf graph

    nn, _ = variablesManager.get_tf_variables()

    results = []

    for exp_name in exp_names:

        sess = tf.Session()

        # saver = tf.train.Saver(reshape=True)
        saver = __get_saver()

        model_path = __get_model_path(exp_name=exp_name)

        # Get values of all variables
        saver.restore(sess, model_path)

        # Compute something using the loaded model (NN)
        result = eval_fun(nn, sess)

        sess.close()

        results.append(result)
        # print(errorList)

    return results


# --- Public Iterface ---


def get_hidden_weights(exp_names):
    """
    Get list of all
    """

    def get_weights_fun(NN, sess):
        # get values of weights
        weights = sess.run(NN.weights)

        return weights

    # Compute the segmented images
    weights = __multiple_eval__(exp_names, get_weights_fun)

    return weights


def __get_hidden_weights_test():
    exp_names = ['test119-allimages-newdeconv2fixedweights-lowerlearningrate-halmos']

    weights = get_hidden_weights(exp_names)

    weights[0]


def eval_hidden_layers(exp_names, X):
    """
    :return TODO what do we exactly return?
    """

    def compute_output_fun(NN, sess):
        # concat layers pool and deconv layers
        layers_to_eval = NN.h_pool + NN.h_deconv

        # Eval all layers at once
        res = sess.run(layers_to_eval, feed_dict={NN.x: X, NN.keep_prob: 1.0, NN.do_update: False})

        return res

    # Compute the segmented images
    layers = __multiple_eval__(exp_names, compute_output_fun)

    return layers


def segment_images(exp_names, X):
    """
    Segment images X using the models given by experiments name.

    Note that: the number of images (0-th coordinate) must correspond to the batch size of model.
    """

    def compute_output_fun(nn, sess):
        # Compute NN Output
        result = sess.run(nn.output_map, feed_dict={nn.x: X, nn.keep_prob: 1.0, nn.do_update: False})

        # return only a single image
        return result[0, :, :, 1]

    # Compute the segmented images
    segmented_images = __multiple_eval__(exp_names, compute_output_fun)

    return segmented_images


def segment_images_and_compute_error(exp_names, X, Y, W):
    """
    Segment images X using the models given by experiments name.

    Note that: the number of images (0-th coordinate) must correspond to the batch size of model.
    """

    def compute_output_fun(nn, sess):
        # Compute NN Output
        output_map_res, pixel_error, error = sess.run([nn.output_map, nn.pixel_error_, nn.classification_error_],
                                                      feed_dict={nn.x: X, nn.y_: Y, nn.keep_prob: 1.0,
                                                                 nn.weight_map: W,
                                                                 nn.do_update: False})

        return output_map_res[0, :, :, 1], pixel_error[0, :, :], error

    # Compute the segmented images
    segmented_images = __multiple_eval__(exp_names, compute_output_fun)

    return segmented_images


def compute_pixel_error(exp_names, X, Y, W):
    """
    Similar to segment_images, but computing en error (thus we also need ground truth Y and weights W).
    """

    def pixel_error_fun(nn, sess):
        # Compute NN Output
        result = sess.run(nn.pixel_error_,
                          feed_dict={nn.x: X, nn.y_: Y, nn.keep_prob: 1.0, nn.weight_map: W, nn.do_update: False})

        # return only a single image
        return result[0, :, :]

    # Compute the pixel errors
    pixel_errors = __multiple_eval__(exp_names, pixel_error_fun)

    return pixel_errors


def compute_error_for_model(nn, X, Y, W, sess):
    """
    Computer error given directly a model.

    Can be called only in existing session.
    """

    # train_accuracy = dice_.eval(feed_dict={x:X, y_: Y,keep_prob:1.0})
    train_accuracy = sess.run(nn.cross_entropy_,
                              feed_dict={nn.x: X, nn.y_: Y, nn.weight_map: W, nn.keep_prob: 1.0, nn.do_update: False})

    return train_accuracy


def compute_error(exp_names, X, Y, W):
    def compute_error_fun(nn, sess):
        # compute error
        train_accuracy = sess.run(nn.cross_entropy,
                                  feed_dict={nn.x: X, nn.y_: Y, nn.weight_map: W, nn.keep_prob: 1.0,
                                             nn.do_update: False})
        return train_accuracy

    error = __multiple_eval__(exp_names, compute_error_fun)

    return error
