"""
This script demonstrate using variables reuse to use an existing variable or create a new one if it does not exists
in the convenient manner.

Based on an example from: http://stackoverflow.com/questions/38545362/tensorflow-variable-scope-reuse-if-variable-exists

See also: https://www.tensorflow.org/versions/master/how_tos/variable_scope/index.html
"""

import tensorflow as tf


def variable(scope_name, var, shape=None, dtype=tf.dtypes.float32, initializer=None,
             trainable=True, collections=None,
             caching_device=None, partitioner=None, validate_shape=True,
             custom_getter=None):
    """
    Get or create var.
    """
    with tf.variable_scope(scope_name) as scope:
        try:
            # Try to create a new variable with the given name and scope
            v = tf.get_variable(var, shape, dtype=dtype, initializer=initializer,
                                trainable=trainable, collections=collections,
                                caching_device=caching_device, partitioner=partitioner, validate_shape=validate_shape,
                                custom_getter=custom_getter)
        except ValueError:
            # The variable already exists -> reuse existing variable (that may have different shape)
            scope.reuse_variables()
            v = tf.get_variable(var)
    return v


def test1():
    v1 = variable('foo', 'v', [1])
    v2 = variable('foo', 'v')
    assert v1 == v2


def test2():
    # TODO create a model
    var1 = variable('model', 'var1', [1])
    var2 = variable('model', 'var2', [1])
    var3 = variable('model', 'var3', [2, 3])

    saver = tf.train.Saver()

    with tf.Session() as sess:
        sess.run(tf.initialize_all_variables())

        saver.save()

        # TODO save
        # TODO load the model from .meta
        # TODO reuse variables


def test3():
    pass
    # TODO like test2, but with new added variables and some missing
