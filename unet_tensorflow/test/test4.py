
"""
PoC of conditional  evaluaiton of updationg estimates.

Copied from: https://github.com/tensorflow/tensorflow/issues/1724
"""


import tensorflow as tf
import numpy as np

inpt = tf.Variable(np.array([1.0]))
do_update = tf.placeholder(tf.bool)

prev_ema = tf.Variable(np.array([0.0]))
decay = 0.9

new_ema = (1-decay)*inpt + decay*prev_ema

ema = tf.cond(do_update, lambda: new_ema, lambda: prev_ema)

assign_op = prev_ema.assign(ema)

with tf.control_dependencies([assign_op]):
    cur_ema = tf.identity(prev_ema)



with tf.Session() as sess:
    sess.run(tf.initialize_all_variables())
    sess.run(cur_ema, {inpt:np.array([2.]), do_update: True})
    print prev_ema.eval()
    sess.run(cur_ema, {inpt:np.array([2.]), do_update: True})
    print prev_ema.eval()
    sess.run(cur_ema, {inpt:np.array([2.]), do_update: True})
    print prev_ema.eval()

    sess.run(cur_ema, {inpt:np.array([2.]), do_update: False})
    print prev_ema.eval()
    sess.run(cur_ema, {inpt:np.array([2.]), do_update: False})
    print prev_ema.eval()
    sess.run(cur_ema, {inpt:np.array([2.]), do_update: False})
    print prev_ema.eval()

    sess.run(cur_ema, {inpt:np.array([2.]), do_update: True})
    print prev_ema.eval()
    sess.run(cur_ema, {inpt:np.array([2.]), do_update: True})
    print prev_ema.eval()
