import numpy as np
import tensorflow as tf

from model import layers
from model.layers import batch_normal


def conv2d_test():
    # create test image
    image = np.ones((5, 5))

    # create test conv matrix
    f = np.ones((3, 3))

    # convert shape
    image_reshaped = tf.reshape(image, [1, 5, 5, 1], name="input_image")
    from model.initialWeightsGenerator import weight_variable
    W_conv = weight_variable([3, 3, 1, 1])

    f_reshaped = tf.ones([3, 3, 1, 1])  # tf.reshape(f, [3,3, 1, 1])

    # compute output
    conv = layers.conv2d(image_reshaped, W_conv, 1)

    with tf.Session() as sess:
        res = sess.run(conv)

    print(res)

    # TODO  assert that is it correct
    expected = np.ones((3, 3)) * 3


def rotate_test():

    n = 3
    image = np.zeros((n,n))
    image[0, 0] = 1
    image[0, n - 1] = 2
    image[n - 1, 0] = 3
    image[n - 1, n - 1] = 4
    print(image)

    from model import tensorUtils
     # tf.image.rot90(image)

    ts, phis = tensorUtils.get_invariant_transformations()

    with tf.Session() as sess:

        # print 4 rotations
        # for k in range(4):
        #     rotated = tensorUtils.rotate90(image, k)
        #     res = sess.run(rotated)
        #     print(k)
        #     print(res)

        # print mirroring
        # mirrored = tensorUtils.mirror(image)
        # res = sess.run(mirrored)
        # print('mirrored')
        # print(res)

        # print all transformations
        # for j in range(8):
        #     tr = tensorUtils.transform(image, j)
        #     res = sess.run(tr)
        #     print(j)
        #     print(res)



        # test inverse transformations
        from model.tensorUtils import transform
        for j in range(8):
            tr = transform(transform(image, j), -j)
            res = sess.run(tr)
            print(j)
            print(res)



def batch_normal_test():
    # create input data - numpy
    sample = np.random.randn(1, 30, 30, 16) * 10

    x = tf.placeholder(tf.float32, shape=[1, 30, 30, 16])

    do_update = tf.placeholder(tf.bool)

    bn = batch_normal(x, do_update)

    with tf.Session() as sess:
        sess.run(tf.initialize_all_variables())

        for iter in range(15):
            update = iter > 3 and iter < 10

            res = sess.run(bn, feed_dict={x: sample, do_update: update})
            # print(res)

            # should be close to zero
            print('mean', np.mean(res))

            # should be close to one
            print('sd', np.sqrt(np.var(res)))



            # ...
