
"""
PoC of conditional  evaluaiton of updationg estimates.

Based on: https://github.com/tensorflow/tensorflow/issues/1724
"""

import tensorflow as tf
import numpy as np

# TODO integrate into the batch normalization layer
# TODO output operations for updates? - register them to modelOptimizer?
# TODO test - print estimates for both training and testing

# def get_curr_ema():

# whether to do an update
do_update = tf.placeholder(tf.bool)

# output of the network
inpt = tf.placeholder(tf.float64)  # tf.Variable(np.array([1.0]))


def create_update_estim_operation(inpt, do_update):
    """

    :param input: output of the network
    :param do_update:
    :return: operation for the conditional update of estimates and estimated value
    """

    # TODO create general version that can be used for both mean and variance?

    # estimate
    estimate = tf.Variable(np.array([0.0]), trainable=False)
    decay = 0.9

    new_ema = (1-decay)*inpt + decay * estimate

    ema = tf.cond(do_update, lambda: new_ema, lambda: estimate)

    # assign new or existing value
    assign_op = estimate.assign(ema)

    with tf.control_dependencies([assign_op]):
        maybe_update_etimate = tf.identity(estimate)

    return maybe_update_etimate, estimate


maybe_update_etimate_op, estimate = create_update_estim_operation(inpt, do_update)


with tf.Session() as sess:
    sess.run(tf.initialize_all_variables())
    sess.run(maybe_update_etimate_op, {inpt:np.array([2.]), do_update: True})
    print estimate.eval()
    sess.run(maybe_update_etimate_op, {inpt:np.array([2.]), do_update: True})
    print estimate.eval()
    sess.run(maybe_update_etimate_op, {inpt:np.array([2.]), do_update: True})
    print estimate.eval()

    sess.run(maybe_update_etimate_op, {inpt:np.array([2.]), do_update: False})
    print estimate.eval()
    sess.run(maybe_update_etimate_op, {inpt:np.array([2.]), do_update: False})
    print estimate.eval()
    sess.run(maybe_update_etimate_op, {inpt:np.array([2.]), do_update: False})
    print estimate.eval()

    sess.run(maybe_update_etimate_op, {inpt:np.array([2.]), do_update: True})
    print estimate.eval()
    sess.run(maybe_update_etimate_op, {inpt:np.array([2.]), do_update: True})
    print estimate.eval()
