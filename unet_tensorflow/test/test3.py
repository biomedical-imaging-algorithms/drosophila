import tensorflow as tf



x = tf.Variable(7.407856784234234234234e-35, dtype=tf.float32)

op = tf.assign(x, x * 0.11)


with tf.Session() as sess:
    sess.run(tf.initialize_all_variables())
    sess.run(op)
    res = sess.run(x)

print(res)