
from setting import rootSetting as Setting
from model import unet


def create_network_test():

    Setting.dataset_for_task = 'train model'

    Setting.debug_mode = True
    # Setting.chanel_root = 4  # lower number of channels for fast testing
    Setting.unet_architecture_variant = 2

    # use minimal resolution as in paper -> we can easily check that all shapes are correct
    Setting.nx = Setting.ny = Setting.imSize = 572
    Setting.maskSize = 388

    nn = unet.create_network()

    assert not nn is None


if __name__ == "__main__":

    create_network_test()