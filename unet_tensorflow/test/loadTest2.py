
"""
This script demonstrate using tf.train.import_meta_graph(metafile) to load the models.

Note that: loading meta files allows us e.g. loading model with various numbers of features.
"""

# TODO use this code to reimplement loading of models im model Evaluator.

import os
import tensorflow as tf
from variables import modelSaver

import setting.rootSetting as Setting
from variables.modelSaver import create_initialized_session


def create_initialized_session_test(exp_name):
    """
    This example code demonstrate loading: 1) variables/model def from metafile and 2) the values of variables.
    """

    with create_initialized_session(exp_name) as sess:

        # Get all/some variables
        all_vars = tf.all_variables()[-4:-1]

        # Get their values
        values = sess.run(all_vars)

        # Print variables and their names
        for var, value in zip(all_vars, values):
            print(var.name + ': ', value)


def test():

    print('-------- First model --------------------')

    create_initialized_session_test('debug023')

    print('-------- Second model --------------------')

    create_initialized_session_test('debug024')


test()
