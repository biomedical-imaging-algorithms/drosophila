
import tensorflow as tf
import numpy as np



# test of add_n
res = tf.add_n([1,2,3])
with tf.Session() as sess:
  r = sess.run(res)
  print(r)



def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial, dtype=tf.float32)


def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)


def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')


x_image = np.zeros((1, 100, 100, 1), dtype=np.float32)

W_conv1 = weight_variable([5, 5, 1, 32])
b_conv1 = bias_variable([32])


b_conv1.get_shape()[0]
tf.shape(b_conv1)

conv = conv2d(x_image, W_conv1)
tf.shape(conv)

conv_b = conv + b_conv1
tf.shape(conv_b)

h_conv1 = tf.nn.relu(conv_b)