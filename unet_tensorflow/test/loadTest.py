
"""
This script demonstrate using tf.train.import_meta_graph(metafile) to load the models.

Note that: loading meta files allows us e.g. loading model with various numbers of features.
"""

# TODO use this code to reimplement loading of models im model Evaluator.

import os
import tensorflow as tf
from variables import modelSaver

import setting.rootSetting as Setting


def import_model_into_session(sess, exp_name):

    # Get paths
    metafile_path = os.path.abspath(os.path.join(Setting.trained_models_dir + exp_name)) + '.meta'
    model_path = os.path.abspath(os.path.join(Setting.trained_models_dir + exp_name))

    print('metafile_path', metafile_path)
    print('model_path', model_path)

    # Create saver from metafile (i.e. load tf graph defs)
    saver = tf.train.import_meta_graph(metafile_path)

    # Restore the model (i.e. values of variables)
    # note that we could also use: tf.train.latest_checkpoint(dir_path) to get the newest
    saver.restore(sess, model_path)


def load_model(exp_name):
    """
    This example code demonstrate loading: 1) variables/model def from metafile and 2) the values of variables.
    """

    # Remove all tf variables - this must be called outside an existing session!
    tf.reset_default_graph()

    with tf.Session() as sess:

        # Load the model
        import_model_into_session(sess, exp_name)

        # Print all/some variables
        all_vars = tf.all_variables()[-4:-1]

        values = sess.run(all_vars)

        for var, value in zip(all_vars, values):
            print(var.name + ': ', value)


def test():

    load_model('debug023')

    load_model('debug024')


test()
