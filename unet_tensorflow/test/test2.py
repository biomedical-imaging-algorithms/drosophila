
import tensorflow as tf

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def weight_variable_devonc(shape):
    initial = 1.0/float(shape[0]*shape[1])
    #return tf.Variable(tf.add((np.ones(shape)*initial).astype(float32),tf.truncated_normal(shape, stddev=0.1)))
    return tf.Variable(tf.truncated_normal(shape, stddev=0.1))

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W,keep_prob_):
    conv_2d = tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')
    return tf.nn.dropout(conv_2d, keep_prob_)

def deconv2d(x, W,stride):
    output_shape = tf.pack([tf.shape(x)[0], tf.shape(x)[1]*2, tf.shape(x)[2]*2, tf.shape(x)[3]/2])
    return tf.nn.conv2d_transpose(x, W, output_shape, strides=[1, stride, stride, 1], padding='VALID')

def max_pool(x,n):
    return tf.nn.max_pool(x, ksize=[1, n, n, 1], strides=[1, n, n, 1], padding='VALID')

#
# nx = ny = 10
# field_of_view = 3
# chanel_root = 16
#
# # Placeholder for the input image
# x = tf.placeholder("float", shape=[None, nx, ny])
# x_image = tf.reshape(x, [-1,nx,ny,1])
#
# keep_prob = tf.placeholder("float")
#
# # Convolution 1
# W_conv1 = weight_variable([field_of_view, field_of_view, 1, chanel_root])
# b_conv1 = bias_variable([chanel_root])
# h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1,keep_prob) + b_conv1)
#
#
# tf.shape(h_conv1)