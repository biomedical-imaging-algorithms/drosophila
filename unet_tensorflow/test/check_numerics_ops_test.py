"""
Testing tf.add_check_numerics_ops()
"""

import tensorflow as tf


def test1():
    """
    Simple usage of tf.add_check_numerics_ops()
    """

    x = tf.Variable(tf.constant(1.0), dtype=tf.float32)

    with tf.Session() as sess:
        sess.run(tf.initialize_all_variables())

        for _ in range(10):
            assign_op = x.assign(x + 1)
            run_ops = [assign_op, tf.add_check_numerics_ops()]
            sess.run(run_ops)

        x_res = sess.run(x)

    print(x_res)


def test2():
    """
    Testing catching exceptions produced by tf.add_check_numerics_ops().
    """

    import tensorflow as tf

    x = tf.Variable(tf.constant(1.0), dtype=tf.float32, name='x')

    with tf.Session() as sess:
        sess.run(tf.initialize_all_variables())

        for _ in range(10):
            # this produces no error, the resulting value is 0
            # assign_op = x.assign(x * 0.00001)

            # this produces: InvalidArgumentError: Variable/read:0 : Tensor had Inf values
            assign_op = x.assign(x / 0.00001)

            # assign_op = x.assign(x + 1e-50)

            run_ops = [assign_op, tf.add_check_numerics_ops()]

            try:
                sess.run(run_ops)
            except tf.errors.InvalidArgumentError as error:
                print('InvalidArgumentError', error)
            except tf.errors.OpError:
                print('OpError')

        try:
            x_res = sess.run(x)
        except tf.errors.InvalidArgumentError as error:
            print('InvalidArgumentError', error)
        except tf.errors.OpError:
            print('OpError')

    print(x_res)


def test3():
    """
    Testing usage of tf.add_check_numerics_ops().
    :return:
    """

    import tensorflow as tf
    from variables import modelSaver
    from utils import utils
    from variables import sessionManager
    from variables import variablesManager
    from data import dataProvider

    sess, nn, stats = modelSaver.initiate_variables_for_training()

    # sess = sessionManager.get_session()
    # nn, stats = variablesManager.get_tf_variables()
    # sess.run(tf.initialize_all_variables())

    X, Y, W = dataProvider.create_batch()

    # Eval the operations
    train_vals = sess.run([tf.add_check_numerics_ops(), nn.classification_error_, nn.train_step], feed_dict={
        nn.x: X, nn.y_: Y, nn.weight_map: W,
        nn.keep_prob: 1.0,
        nn.learning_rate_: 0.001,
        nn.do_update: True  # update step, estimates for batch normalization etc.
    })

    print(train_vals)


# test1()
# test2()


# TODO next steps
#  - create a sequence of scenarios with various complexity - between simplest possible case and the code used in code
# -> isolate problem
