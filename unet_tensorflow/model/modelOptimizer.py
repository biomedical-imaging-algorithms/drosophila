############################
### Training the network ###
############################

# TODO add logging
import tensorflow as tf
from setting import rootSetting as Setting
from model import trainStep


# --- Public interface


def run_train_iteration(nn, iteration, X, Y, W, sess):
    """
    A Single Train Iteration
    """

    x = nn.input[0]
    y_ = nn.y_
    train_step = nn.train_step
    keep_prob = nn.keep_prob
    learning_rate_ = nn.learning_rate_
    weight_map = nn.weight_map

    learning_rate = trainStep.get_learning_rate(iteration)

    # --- Single Train Iteration ---

    # Operations to run: train iteration with optional NaN check
    # run_ops = [train_step, tf.add_check_numerics_ops()] if Setting.debug_mode else [train_step]
    run_ops = [train_step, tf.add_check_numerics_ops()]
    # run_ops = [train_step]

    # Train step
    sess.run(run_ops,
             feed_dict={
                 x: X, y_: Y, weight_map: W,
                 keep_prob: 1.0 - Setting.dropout_prob,
                 learning_rate_: learning_rate,
                 nn.do_update: True
             })

    # train_step.run(
    #     feed_dict={
    #         x: X, y_: Y, weight_map: W,
    #         keep_prob: 1.0 - Setting.dropout_prob,
    #         learning_rate_: learning_rate
    #     }, session=sess)

    # --- Weights decay ---
    # Note that this correspond to L2 regularization (i.e. it could be equivalently achieved adding sum of squaded
    # weights into error function - this would be more general and more effciet? )
    # weights_decay = Setting.weights_decay
    # compute_period = Setting.weights_decay_compute_period
    # factor = pow(1 - weights_decay, compute_period)
    #
    # if weights_decay > 0 and iteration % compute_period == 0:
    #     # TODO this might be very inefficient (i.e. calling run() for each variable separately)
    #     for var in nn.variables:
    #         var.assign(var * factor).op.run()

    # --- test ---
    # Comment train_step and print values of sample variable
    # values = nn.variables[0].eval()
    # print('var. value: {}'.format(values[0,0,0,:]))
