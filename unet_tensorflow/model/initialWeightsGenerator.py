
"""
Generation of initial weights of UNet network.
"""
# TODO add also possibility to adapt variance to the data

import setting.rootSetting as Setting
import tensorflow as tf
import numpy as np


def __compute_variance_of_initial_weights(shape):
    """
    Compute standard deviance of initial weights as described in the paper (U-Net)
    (from shape given as a paramater)
    """

    if Setting.initial_weights_generation == 'normalize':
        field_of_view = Setting.field_of_view

        # Number of features it the previous layer
        n_features = shape[2]

        # Number of inputs
        n_inputs = field_of_view * field_of_view * n_features

        # Compute standard deviance
        stddev = np.sqrt(2 / float(n_inputs))
        return stddev

    if Setting.initial_weights_generation == 'constant std':
        # variance does not depend on the number of the inputs
        return 0.03

    raise Exception('Invalid value of Setting.initial_weights_generation:' + Setting.initial_weights_generation)


def __get_deconv_filter(f_shape):
    """
    Based on: https://github.com/MarvinTeichmann/tensorflow-fcn/blob/master/fcn16_vgg.py

    TODO this seems to be not correct - at least not what we need:
     1) it gives not kernel invariant to 90 angle rotations (e.g. K_00 != K_22)
     2) uses only half of input channels

    :param f_shape: ksize, ksize, num_classes, in_features
    :return:
    """
    height = f_shape[1]
    width = f_shape[0]
    f = np.ceil(width / 2.0)
    c = (2 * f - 1 - f % 2) / (2.0 * f)
    bilinear = np.zeros([f_shape[0], f_shape[1]])
    for x in range(width):
        for y in range(height):
            value = (1 - abs(x / f - c)) * (1 - abs(y / f - c))
            bilinear[x, y] = value
    weights = np.zeros(f_shape)
    for i in range(f_shape[2]):
        weights[:, :, i, i] = bilinear

    return tf.constant(weights, dtype=tf.float32)
    # init = tf.constant_initializer(value=weights, dtype=tf.float32)
    # return tf.get_variable(name="up_filter", initializer=init, shape=weights.shape)


def __get_deconv_filter2(f_shape):
    """
    Adhoc solution.

    :param f_shape: ksize, ksize, num_classes, in_features
    :return:
    """

    width = f_shape[0]
    height = f_shape[1]

    # scripts sized not supported yet
    assert width == 3
    assert height == 3

    # --- create interpolation kernel of size 3x3 ---
    interp = np.array([[.25, .5, .25], [.5, 1.0, .5] , [.25, .5, .25]])

    # -- create weights that are weighted sums of interpolation kernel ---
    weights = np.zeros(f_shape)

    # TODO how compute sd correctly here?
    sd = np.sqrt(2.0 / f_shape[3])

    for i in range(f_shape[2]):
        for j in range(f_shape[3]):
            weights[:, :, i, j] = np.random.standard_normal() * sd * interp  # TODO generate random vals in tensorflow

    return tf.constant(weights, dtype=tf.float32)


# --- Public Interface ---


def weight_variable(shape):

    stddev = __compute_variance_of_initial_weights(shape)

    # initial value
    initial = tf.truncated_normal(shape, stddev=stddev)
    return tf.Variable(initial, dtype=tf.float32)


def weight_variable_devonc(shape):
    # ksize, ksize, out_features, in_features
    # TODO! ensure that this is the correct shape
    f_shape = [Setting.field_of_view, Setting.field_of_view, shape[2], shape[3]]

    # initialize with bilinear interpolation
    initial = __get_deconv_filter2(f_shape)
    # return tf.Variable(initial, dtype=tf.float32,trainable=False)
    return tf.Variable(initial, dtype=tf.float32)

    # initial = 1.0 / float(shape[0] * shape[1])
    # return tf.Variable(tf.add(tf.constant(initial, shape=shape), tf.truncated_normal(shape, stddev=0.1) ))

    # return tf.Variable(tf.add((np.ones(shape)*initial).astype(tf.float32), tf.truncated_normal(shape, stddev=0.1)))
    # stddev = __compute_variance_of_initial_weights(shape)
    # return tf.Variable(tf.truncated_normal(shape, stddev=stddev))


def bias_variable(shape):
    initial = tf.zeros(shape)
    return tf.Variable(initial)
