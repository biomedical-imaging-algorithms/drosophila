
"""
Various measures for quantitative evaluation.
"""

import tensorflow as tf


def pixel_wise_softmax(output_map):
    """
    From http://tf-unet.readthedocs.io/en/latest/_modules/tf_unet/layers.html
    """

    # note: alternatative would be using
    # tf.nn.softmax_cross_entropy_with_logits()

    exponential_map = tf.exp(output_map)
    sum_exp = tf.reduce_sum(exponential_map, 3, keep_dims=True)
    tensor_sum_exp = tf.tile(sum_exp, tf.pack([1, 1, 1, tf.shape(output_map)[3]]))
    return tf.div(exponential_map,tensor_sum_exp)


def weighted_pixel_error(y_, weight_map, output_map):
    # compute mean over the classes, ie. transform [None, nx - 184, ny - 184, 2] -> [None, nx - 184, ny - 184]
    pixel_error = -tf.reduce_mean(y_ * weight_map * tf.log(output_map + 1e-10), reduction_indices=[3])
    return pixel_error


def l2_loss(nn_weights):
    """
    Compute: alpha * [l2 loss of all model weights] (i.e half the L2 norm of a tensor without the `sqrt`).

    :param nn_weights: list of tensors
    :return:
    """

    # return tf.add_n([tf.nn.l2_loss(w) for w in nn_weights])
    return sum([tf.nn.l2_loss(w) for w in nn_weights])


def weighted_cross_entropy(y_, weight_map, output_map):
    """
    Compute a cross entropy with pixels weighted be their importance.

    Note that: shapes are [None, nx - 184, ny - 184, 2], where the dimensions are:
    [image, pixel coordinates (2 dim), class]

    :param y_: the ground truth
    :param weight_map: weights of pixels
    :param output_map: output of the network from range [0,1]
    :return:
    """

    # Related problem on stack overflow: http://stackoverflow.com/questions/33712178/tensorflow-nan-bug

    # Compute the mean across dimensions
    # problem of this solution is that for output_map < threshold the gradient is zero
    # res = -tf.reduce_mean(y_ * weight_map * tf.log(tf.clip_by_value(output_map, 1e-7, 1.0)))

    # TODO check that output_map is not nan & it is in interval [0,1]

    res = -tf.reduce_mean(y_ * weight_map * tf.log(output_map + 1e-10))

    return res


def relaxed_dice(y_, output_map, shape):
    epsilon = 1e-10
    slice_y = tf.slice(y_, [0, 0, 0, 1], [shape[0], shape[1], shape[2], 1])
    slice_output_map = tf.slice(output_map, [0, 0, 0, 1], [shape[0], shape[1], shape[2], 1])

    intersection = tf.reduce_sum(tf.mul(slice_y, slice_output_map), [1, 2])
    average = tf.reduce_sum(tf.div(tf.add(slice_y, slice_output_map), tf.constant([2.0])), [1, 2])

    dice = tf.div(intersection, tf.add(average, tf.constant([epsilon])))

    return tf.reduce_mean(dice)