import tensorflow as tf


def rotate90(t, k=1):
    """Rotate the given 2D nxn tensor by k * pi / 2, 0 <= k < 4"""

    if len(t.get_shape()) == 3:
        return tf.image.rot90(t, k)

    # add 1 dim
    image3d = tf.expand_dims(t, -1)

    # rotate
    rotated = tf.image.rot90(image3d, k)

    # remove the dimension again
    rotated2d = rotated[:, :, 0]

    return rotated2d


def mirror(t, do_mirror=1):
    """Mirror the given 2D nxn tensor (or return the given image)"""

    if do_mirror == 0:
        return t

    if len(t.get_shape()) == 3:
        return tf.image.flip_up_down(t)

    # add 1 dim
    image3d = tf.expand_dims(t, -1)

    # mirror
    mirrored = tf.image.flip_up_down(image3d)

    # remove the dimension again
    mirrored2d = mirrored[:, :, 0]

    return mirrored2d


def transform(F, j):
    """
    Apply trainsformation (for j >=) or inverse transformation for j <= 0.
    """

    # find index of inverse transformation
    # note: except rotations, all scripts transformations are inverse to each scripts
    if j < 0:
        j = -j
        if j == 1: j = 3
        elif j == 3: j = 1

    # index of rotation and mirroring
    k = j % 4
    m = j / 4

    # apply transformation
    return rotate90(mirror(F, m), k)



def get_invariant_transformations():
    """
    Create a list of transformations for 4 rotations * 2 mirrorings.

    :return list of transformations and list of inverse transformations
    """

    # transformations
    t = 8 * [None]

    # TODO test that it really is the inverse
    # inverse transformations
    phi = 8 * [None]

    for k in range(4):
        for m in range(2):
            t[k + 4*m] = lambda F: rotate90(mirror(F, m), k)
            phi[k + 4*m] = lambda F: mirror(rotate90(F, (4-k) % 4), m)

    return t, phi