"""
CNN Building Blocks Defenition
"""

# TODO split to more files

import logging

import numpy as np
import tensorflow as tf

import setting.rootSetting as Setting
from model import tensorUtils
from model.initialWeightsGenerator import bias_variable, weight_variable, weight_variable_devonc

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(level=logging.INFO)


def create_update_estim_operation(stat, do_update, initial_value, name):
    """
    Create a (non-trainable) variable for exp. moving average and an update operation.

    :param stat: placeholder for the statistics that is estimated, (e.g. mean, variance of the layer)
    :param do_update: placeholder whether to perform an update
    :param initial_value: an initial value for the statistics
    :return: operation for the conditional update of estimates and estimated value
    """

    # Create (non-trainable) variable for estimate
    estimate = tf.Variable(initial_value, trainable=False, name=name)

    # Exponential moving average
    decay = Setting.exp_moving_average_decay
    new_ema = (1 - decay) * stat + decay * estimate

    # Conditional assign operation
    maybe_update_estim_op = estimate.assign(tf.cond(do_update, lambda: new_ema, lambda: estimate))

    return estimate, maybe_update_estim_op


def create_update_step_operation(do_update):
    """

    :param layer:
    :param do_update:
    :return:
    """

    # Create a variable for step
    step = tf.Variable(0, trainable=False, dtype=tf.int32, name='train_step')

    # Conditional assign operation
    maybe_update_step = step.assign(tf.cond(do_update, lambda: step + 1, lambda: step))

    return step, maybe_update_step


def batch_normal(x, do_update):
    """
    Batch normalization layer.

    For more details see the paper: http://jmlr.org/proceedings/papers/v37/ioffe15.pdf
    :return:
    """

    # TODO how to test this? - simplest: show output of hidden layers

    # same shape as biases
    shape = x.get_shape()[-1]

    # --- Create non-trainable variables: mu, sigma2 (i.e. estimates of mu, sigma2 of the layer outputs) ---

    # Mean and variance for each channel
    mean, var = tf.nn.moments(x, axes=[0, 1, 2])

    # mean = tf.reduce_mean(x, [0,1,2])
    # var = tf.reduce_mean((x - mean)**2)

    mu_initial = tf.constant(0, shape=[shape], dtype=tf.float32)
    mu, maybe_update_mu = create_update_estim_operation(mean, do_update, mu_initial, 'batch_norm_mu')

    sigma2_initial = tf.constant(1, shape=[shape], dtype=tf.float32)
    sigma2, maybe_update_sigma2 = create_update_estim_operation(var, do_update, sigma2_initial, 'batch_norm_sigma2')

    # Create trainable variables: scale
    scale_initial = tf.constant(1, shape=[shape], dtype=tf.float32)
    scale = tf.Variable(scale_initial, name="batch_norm_scale")

    # Centralized and normalized output of the network
    # note that: we added a small constant to avoid dividing be zero (or close to zero)
    with tf.control_dependencies([maybe_update_mu, maybe_update_sigma2]):
        x_normalized = tf.div(x - mu, tf.sqrt(sigma2 + 1e-5))  # mu, sigma2

    # Scale
    # Note that: we already have bias after conv layer
    y = scale * x_normalized  # + shift

    return y


def conv2d(x, W, keep_prob_, do_update, use_batch_normalization, name=None):
    """
    Convolutional layer.

    :param x: tensor of shape [batch, x, y, channels]
    :param w: tensor of shape [field_of_view, field_of_view, in_channels, out_channels]
    """

    conv_2d = tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')

    # Optionally we add batch normalization (between convolutiion and relu )
    if use_batch_normalization:
        conv_2d = batch_normal(conv_2d, do_update)

    return tf.nn.dropout(conv_2d, keep_prob_, name=name)


def deconv2d2(x, W, stride):
    # TODO ensure that the padding is the correct type, see
    # - http://www.johnloomis.org/ece563/notes/filter/conv/convolution.html
    # - https://github.com/vdumoulin/conv_arithmetic

    # twice higher resolution, twice less features
    output_shape = tf.pack([tf.shape(x)[0], tf.shape(x)[1] * 2, tf.shape(x)[2] * 2, tf.div(tf.shape(x)[3], 2)])

    # assert that the the number of input channels matches for x and W
    assert W.get_shape()[3] == x.get_shape()[3]

    static_shape = x.get_shape()

    output_shape = tf.TensorShape([static_shape[0], static_shape[1] * 2, static_shape[2] * 2, static_shape[3] // 2])

    # output_shape = [1] + static_shape[1:]

    return tf.nn.conv2d_transpose(x, W, output_shape, strides=[1, stride, stride, 1], padding='SAME')  # padding='VALID'

    # return tf.nn.conv2d_transpose(4 * x, W, output_shape, strides=[1, stride, stride, 1], padding='SAME')  # TODO temporal compenzation (4 * x)

    # f_shape = [3, 3, tf.shape(x)[3] / 2, tf.shape(x)[3]]  # tf.pack([tf.shape(x)[0], tf.shape(x)[1] * 2, tf.shape(x)[2] * 2, tf.shape(x)[3] / 2])
    # return get_deconv_filter(f_shape)


def deconv2d(x, W, stride):
    # TODO ensure that the padding is the correct type, see
    # - http://www.johnloomis.org/ece563/notes/filter/conv/convolution.html
    # - https://github.com/vdumoulin/conv_arithmetic

    # assert that the the number of input channels matches for x and W
    assert W.get_shape()[3] == x.get_shape()[3]

    # twice higher resolution, twice less features
    x_shape = tf.shape(x)
    # batch_size = tf.shape(x)[0] # here we use placeholder to allow variable batch_size
    output_shape = tf.pack([x_shape[0], x_shape[1] * 2, x_shape[2] * 2, x_shape[3] // 2])
    return tf.nn.conv2d_transpose(x, W, output_shape, strides=[1, stride, stride, 1], padding='SAME')  # padding='VALID'


def max_pool(x, n, name='maxpool'):
    layer = tf.nn.max_pool(x, ksize=[1, n, n, 1], strides=[1, n, n, 1], padding='VALID')

    log.debug('NEW MAX-POOL LAYER - {}'.format(layer))
    return layer


def crop_and_concat(x1, x2, output_shape):
    # x1.set_shape(input_shape)
    x1_crop = tf.image.extract_glimpse(x1, [output_shape[1], output_shape[2]], np.zeros([output_shape[0], 2]),
                                       centered=True)
    # x1_crop = tf.image.extract_glimpse(x1, [tf.shape(x2)[1], tf.shape(x2)[2]],tf.shape(x2)[0])
    return tf.concat(3, [x1_crop, x2])


def crop_and_concat3(x1, x2, output_shape):
    """
    Coppied from http://tf-unet.readthedocs.io/en/latest/_modules/tf_unet/layers.html

    TODO compare with the original verision (i.e. crop_and_concat)
    """

    offsets = tf.zeros(tf.pack([output_shape[0], 2]), dtype=tf.float32)
    x2_shape = tf.shape(x2)
    size = tf.pack((x2_shape[1], x2_shape[2]))
    x1_crop = tf.image.extract_glimpse(x1, size=size, offsets=offsets, centered=True)
    return tf.concat(3, [x1_crop, x2])


def crop_and_conca4(x1, x2, output_shape):
    # x1.set_shape(input_shape)
    x1_crop = tf.image.extract_glimpse(x1, output_shape[1:3], np.zeros([output_shape[0], 2]), centered=True)
    # x1_crop = tf.image.extract_glimpse(x1, [tf.shape(x2)[1], tf.shape(x2)[2]],tf.shape(x2)[0])
    return tf.concat(3, [x1_crop, x2])


def crop_and_concat2(x1, x2, output_shape):
    """
    Here we also crop the second input x2.
    """

    x1_crop = tf.image.extract_glimpse(x1, [output_shape[1], output_shape[2]], np.zeros([output_shape[0], 2]),
                                       centered=True)
    x2_crop = tf.image.extract_glimpse(x2, [output_shape[1], output_shape[2]], np.zeros([output_shape[0], 2]),
                                       centered=True)

    return tf.concat(3, [x1_crop, x2_crop])


# def pixel_wise_softmax(output_map):
#     exponential_map = tf.exp(output_map)
#     evidence = tf.add(exponential_map, tf.reverse(exponential_map, [False, False, False, True]))
#     return tf.div(exponential_map, evidence)


# def cross_entropy(y_, output_map):
#     return -tf.reduce_mean(y_ * tf.log(tf.clip_by_value(output_map, 1e-10, 1.0)))

# TODO add weights?

def conv_layer(x, in_channels, out_channels, keep_prob, do_update, name='conv_layer', field_of_view=3,
               disable_batch_normalization=False):
    if Setting.use_invariant_conv_layer:
        n_variants = 8
        return invariant_conv_layer(x, in_channels, out_channels, n_variants, keep_prob, do_update, name, field_of_view,
                                    disable_batch_normalization)

    return standart_conv_layer(x, in_channels, out_channels, keep_prob, do_update, name, field_of_view,
                               disable_batch_normalization)


def standart_conv_layer(x, in_channels, out_channels, keep_prob, do_update, name='conv_layer', field_of_view=3,
                        disable_batch_normalization=False):
    """
    Create a convolution layer.

    :param x: tf tensor
    :return: layer and list of all tf variables (both trainable and non-trainable)
    """

    use_batch_normalization = Setting.batch_normalization and not disable_batch_normalization

    W_conv = weight_variable([field_of_view, field_of_view, in_channels, out_channels])
    b_conv = bias_variable([out_channels])
    h_conv = tf.nn.relu(conv2d(x, W_conv, keep_prob, do_update, use_batch_normalization) + b_conv, name=name)

    log.debug('NEW CONV LAYER - {}'.format(h_conv))

    return h_conv  # , {'W_' + layer_name: W_conv, 'b_' + layer_name: b_conv}


def invariant_conv_layer(x, in_channels, out_channels, n_variants, keep_prob, do_update, name='invariant_conv_layer',
                         field_of_view=3, disable_batch_normalization=False):
    """
    Create an invariant convolutional layer i.e. that learns invariant filters.

    The layer has a typical conv layer followed by max-pooling over channels in a groups of variants of filters
    followed by ReLu.

    :param n_variants number of variants (e.g. rotations) for each filter, note that the layer after convolution has
    n_variants * out_channels features
    """

    use_batch_normalization = Setting.batch_normalization and not disable_batch_normalization

    # Convolutional layer
    # W_conv = weight_variable([field_of_view, field_of_view, in_channels, out_channels * n_variants])

    # --- Create shared weights ---

    W_conv = weight_variable([field_of_view, field_of_view, in_channels, out_channels])

    W_list = (out_channels * n_variants) * [None]

    # TODO test this
    counter = 0
    for o in range(out_channels):
        for v in range(n_variants):
            W_list[counter] = tensorUtils.transform(W_conv[:, :, :, o], v)
            counter += 1

    W_conv = tf.pack(W_list, axis=3)

    b_conv = bias_variable([out_channels * n_variants])
    x2 = conv2d(x, W_conv, keep_prob, do_update, use_batch_normalization) + b_conv

    # Max-pooling over channels
    x3 = tf.nn.max_pool(x2, ksize=[1, 1, 1, n_variants], strides=[1, 1, 1, n_variants], padding='VALID')

    # ReLu
    x4 = tf.nn.relu(x3)

    # TODO two variants: 1) shared weights + 2) regularization

    return x4


def last_conv_layer(x, in_channels, out_channels, field_of_view=1):
    """
    Create the last convolution layer.

    No Relu, no batch normalization, no dropout, field_of_view = 1.

    :param x: tf tensor
    :return: layer and list of all tf variables (both trainable and non-trainable)
    """

    W_conv = weight_variable([field_of_view, field_of_view, in_channels, out_channels])
    b_conv = bias_variable([out_channels])
    h_conv = conv2d(x, W_conv, tf.constant(1.0), False, False, name='last_conv_layer') + b_conv

    log.debug('NEW CONV LAYER - {}'.format(h_conv))

    return h_conv


def deconv_layer(x, out_channels, do_update, name='deconv_layer', max_pool_size=2):
    # create weights and biases
    W_deconv = weight_variable_devonc([max_pool_size, max_pool_size, out_channels, out_channels * 2])
    b_deconv = bias_variable([out_channels])

    d = deconv2d2(x, W_deconv, max_pool_size)
    # d = deconv2d(x, W_deconv, max_pool_size)

    if Setting.batch_normalization:
        d = batch_normal(d, do_update)

    h_deconv = tf.nn.relu(d + b_deconv, name=name)

    log.debug('NEW DECONV LAYER - {}'.format(h_deconv))

    return h_deconv


def concat_layer(h_conv, h_deconv, out_channels, name='concat_layer'):
    # TODO test thatthis is correct
    # TODO generalize
    # x = (((nx - 180) / 2 + 4) / 2 + 4) / 2 + 4
    # y = (((ny - 180) / 2 + 4) / 2 + 4) / 2 + 4
    # output_shape = [batch_size, x, y, out_channels]

    # we want to have the same shape as the deconv layer
    output_shape = tf.shape(h_deconv)

    # crop the convolutional layer
    h_deconv1_concat = crop_and_concat3(h_conv, h_deconv, output_shape)

    log.debug('NEW CONCAT LAYER - {}'.format(h_deconv1_concat))

    return h_deconv1_concat
