
import tensorflow as tf
from setting import experimentSetting as Setting

# Small constant for numerical stability of AdamOptimizer
# note: usure how this works
# - the default value e-8 may causes nans for architectures with many layers, see https://github.com/tensorflow/tensorflow/issues/2288
# - too high value might cause worser performance?
# - from documentation: The default value of 1e-8 for epsilon might not be a good default in general.
# For example, when training an Inception network on ImageNet a current good choice is 1.0 or 0.1.
adam_epsilon = Setting.adam_epsilon

# TODO use python dictionary for selection of the optimizer and learing rate


def __get_train_step1(cross_entropy_, learning_rate_):
    """
    Adam Optimizer.

    Note that: parameters are same as in the original unet tensorflow implementation (and may not be optimal).
    """

    learning_rate = 1e-3 / Setting.batch_size

    # learning_rate_
    train_step = tf.train \
        .AdamOptimizer(learning_rate=learning_rate, epsilon=adam_epsilon) \
        .minimize(cross_entropy_)

    return train_step


def __get_train_step2(cross_entropy_, learning_rate_):
    """
    Momentum Optimizer.
    """

    momentum = 0.99

    train_step = tf.train \
        .MomentumOptimizer(learning_rate=learning_rate_, momentum=momentum)\
        .minimize(cross_entropy_)

    return train_step


def __get_learning_rate2(training_step):
    """
    Learning rate decreased each 8000 steps by factor 10.
    """

    base_learning_rate = Setting.momentum_base_learning_rate / Setting.batch_size
    decrease_period = Setting.momentum_decrease_period
    factor = Setting.momentum_factor
    min_learning_rate = 0.00001

    learning_rate = base_learning_rate * pow(factor, training_step / decrease_period)

    learning_rate = max(learning_rate, min_learning_rate)

    return learning_rate


def __get_train_step3(cross_entropy_, learning_rate_):
    """
    Adam Optimizer: using lower learning rate (after the batch size has beeen decreased to 1 image)

    """

    learning_rate = 1e-4 / Setting.batch_size

    train_step = tf.train \
        .AdamOptimizer(learning_rate=learning_rate, epsilon=adam_epsilon) \
        .minimize(cross_entropy_)
    return train_step


def __get_train_step4(cross_entropy_, learning_rate_):
    """
    Adam Optimizer: using lower learning rate (after the batch size has beeen decreased to 1 image)

    """

    # learning_rate_
    train_step = tf.train \
        .AdamOptimizer(learning_rate=5e-5 / Setting.batch_size) \
        .minimize(cross_entropy_)

    return train_step


def __get_train_step5(cross_entropy_, learning_rate_):
    """
    Momentum Optimizer.
    """

    momentum = 0.99

    train_step = tf.train \
        .MomentumOptimizer(learning_rate=learning_rate_, momentum=momentum)\
        .minimize(cross_entropy_)

    return train_step


def __get_learning_rate5(training_step):
    """
    Learning rate decreased each 500 steps by factor 2.
    """

    base_learning_rate = 0.002
    decrease_period = 500
    factor = 0.5
    min_learning_rate = 0.00001

    learning_rate = base_learning_rate * pow(factor, training_step / decrease_period)

    learning_rate = max(learning_rate, min_learning_rate)

    return learning_rate


# --- Public interface ---


def get_learning_rate(training_step):
    if Setting.model_optimizer_num == 1:
        return 0

    if Setting.model_optimizer_num == 2:
        return __get_learning_rate2(training_step)

    if Setting.model_optimizer_num == 3:
        return 0

    if Setting.model_optimizer_num == 4:
        return 0

    if Setting.model_optimizer_num == 5:
        return __get_learning_rate5(training_step)


    raise Exception('Invalid Setting.model_optimizer_num {}'.format(Setting.model_optimizer_num))


def get_train_step(cross_entropy_, learning_rate_):

    if Setting.model_optimizer_num == 1:
        return __get_train_step1(cross_entropy_, learning_rate_)

    if Setting.model_optimizer_num == 2:
        return __get_train_step2(cross_entropy_, learning_rate_)

    if Setting.model_optimizer_num == 3:
        return __get_train_step3(cross_entropy_, learning_rate_)

    if Setting.model_optimizer_num == 4:
        return __get_train_step4(cross_entropy_, learning_rate_)

    if Setting.model_optimizer_num == 5:
        return __get_train_step5(cross_entropy_, learning_rate_)

    raise Exception('Invalid Setting.model_optimizer_num {}'.format(Setting.model_optimizer_num))