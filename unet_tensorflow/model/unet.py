"""
UNet Architecture
"""

import logging

from model import trainStep
from model.layers import *
from model.measures import pixel_wise_softmax, weighted_pixel_error, l2_loss, weighted_cross_entropy, relaxed_dice
from setting import rootSetting as Setting

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(level=logging.INFO)


class UNet:
    def __init__(self, x, output_map, h_conv, h_pool, h_deconv, h_deconv_concat, keep_prob, do_update,
                 weights, y_, weight_map, loss_, classification_error_, pixel_error_, dice_, learning_rate_,
                 train_step, step):
        # layers
        self.x = x
        self.output_map = output_map
        self.h_conv = h_conv
        self.h_pool = h_pool
        self.h_deconv = h_deconv
        self.h_deconv_concat = h_deconv_concat

        # variables
        self.weights = weights

        # keep probability (dropout)
        self.keep_prob = keep_prob

        # update batch normalization stats - this can be done only when training
        self.do_update = do_update

        # --- these are not directly part of the network ---

        # TODO add these to different class?
        # TODO spilit clasification errror and total loss (including regularization, etc)

        # placeholder for ground truth
        self.y_ = y_

        # placeholder for weight map
        self.weight_map = weight_map

        # training properties
        self.classification_error_ = classification_error_  # e.g. cross entropy
        self.loss_ = loss_  # including regularization, etc
        self.pixel_error_ = pixel_error_
        self.dice_ = dice_
        self.learning_rate_ = learning_rate_
        self.train_step = train_step

        # current step
        self.step = step

        # nan check
        self.nan_check = tf.add_check_numerics_ops()

        # parameters for data normalization computed on the training set
        self.input_data_mu = tf.Variable(tf.constant(0, dtype=tf.float32), name='input_data_mu', trainable=False)
        self.input_data_sigma = tf.Variable(tf.constant(0, dtype=tf.float32), name='input_data_sigma', trainable=False)

#
# def create_network_variant1():
#     """
#     Deprecated - older version.
#     Create the network with Unet architecture.
#     Original variant with hardcoded layers.
#     """
#
#     chanel_root = Setting.chanel_root
#     ny = nx = Setting.nx
#     field_of_view = Setting.field_of_view
#     max_pool_size = Setting.max_pool_size
#     batch_size = Setting.batch_size
#     n_class = Setting.n_class
#
#     # Placeholder for the input image
#     x = tf.placeholder("float", shape=[None, nx, ny])
#     x_image = tf.reshape(x, [-1, nx, ny, 1])
#
#     # Probability of keeping connections for "dropout"
#     keep_prob = tf.placeholder("float")
#
#     # Convolution 1
#     W_conv1 = weight_variable([field_of_view, field_of_view, 1, chanel_root])
#     b_conv1 = bias_variable([chanel_root])
#     h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1, keep_prob) + b_conv1)
#
#     # Convolution 2
#     W_conv2 = weight_variable([field_of_view, field_of_view, chanel_root, chanel_root])
#     b_conv2 = bias_variable([chanel_root])
#     h_conv2 = tf.nn.relu(conv2d(h_conv1, W_conv2, keep_prob) + b_conv2)
#
#     # Max Pool 1
#     h_pool1 = max_pool(h_conv2, max_pool_size)
#
#     # Convolution 3
#     W_conv3 = weight_variable([field_of_view, field_of_view, chanel_root, 2 * chanel_root])
#     b_conv3 = bias_variable([2 * chanel_root])
#     h_conv3 = tf.nn.relu(conv2d(h_pool1, W_conv3, keep_prob) + b_conv3)
#
#     # Convolution 4
#     W_conv4 = weight_variable([field_of_view, field_of_view, 2 * chanel_root, 2 * chanel_root])
#     b_conv4 = bias_variable([2 * chanel_root])
#     h_conv4 = tf.nn.relu(conv2d(h_conv3, W_conv4, keep_prob) + b_conv4)
#
#     # Max Pool 2
#     h_pool2 = max_pool(h_conv4, max_pool_size)
#
#     # Convolution 5
#     W_conv5 = weight_variable([field_of_view, field_of_view, 2 * chanel_root, 4 * chanel_root])
#     b_conv5 = bias_variable([4 * chanel_root])
#     h_conv5 = tf.nn.relu(conv2d(h_pool2, W_conv5, keep_prob) + b_conv5)
#
#     # Convolution 6
#     W_conv6 = weight_variable([field_of_view, field_of_view, 4 * chanel_root, 4 * chanel_root])
#     b_conv6 = bias_variable([4 * chanel_root])
#     h_conv6 = tf.nn.relu(conv2d(h_conv5, W_conv6, keep_prob) + b_conv6)
#
#     # Max Pool 3
#     h_pool3 = max_pool(h_conv6, max_pool_size)
#
#     # Convolution 7
#     W_conv7 = weight_variable([field_of_view, field_of_view, 4 * chanel_root, 8 * chanel_root])
#     b_conv7 = bias_variable([8 * chanel_root])
#     h_conv7 = tf.nn.relu(conv2d(h_pool3, W_conv7, keep_prob) + b_conv7)
#
#     # Convolution 8
#     W_conv8 = weight_variable([field_of_view, field_of_view, 8 * chanel_root, 8 * chanel_root])
#     b_conv8 = bias_variable([8 * chanel_root])
#     h_conv8 = tf.nn.relu(conv2d(h_conv7, W_conv8, keep_prob) + b_conv8)
#
#     # Max Pool 4
#     h_pool4 = max_pool(h_conv8, max_pool_size)
#
#     # Convolution 9
#     W_conv9 = weight_variable([field_of_view, field_of_view, 8 * chanel_root, 16 * chanel_root])
#     b_conv9 = bias_variable([16 * chanel_root])
#     h_conv9 = tf.nn.relu(conv2d(h_pool4, W_conv9, keep_prob) + b_conv9)
#
#     # Convolution 10
#     W_conv10 = weight_variable([field_of_view, field_of_view, 16 * chanel_root, 16 * chanel_root])
#     b_conv10 = bias_variable([16 * chanel_root])
#     h_conv10 = tf.nn.relu(conv2d(h_conv9, W_conv10, keep_prob) + b_conv10)
#
#     # Deconvolution 1
#     W_deconv_1 = weight_variable_devonc([max_pool_size, max_pool_size, 8 * chanel_root, 16 * chanel_root])
#     b_deconv1 = bias_variable([8 * chanel_root])
#     h_deconv1 = tf.nn.relu(deconv2d(h_conv10, W_deconv_1, max_pool_size) + b_deconv1)
#     h_deconv1_concat = crop_and_concat(h_conv8, h_deconv1, [batch_size, (((nx - 180) / 2 + 4) / 2 + 4) / 2 + 4,
#                                                             (((ny - 180) / 2 + 4) / 2 + 4) / 2 + 4, 8 * chanel_root])
#
#     # Convolution 11
#     W_conv11 = weight_variable([field_of_view, field_of_view, 16 * chanel_root, 8 * chanel_root])
#     b_conv11 = bias_variable([8 * chanel_root])
#     h_conv11 = tf.nn.relu(conv2d(h_deconv1_concat, W_conv11, keep_prob) + b_conv11)
#
#     # Convolution 12
#     W_conv12 = weight_variable([field_of_view, field_of_view, 8 * chanel_root, 8 * chanel_root])
#     b_conv12 = bias_variable([8 * chanel_root])
#     h_conv12 = tf.nn.relu(conv2d(h_conv11, W_conv12, keep_prob) + b_conv12)
#
#     # Deconvolution 2
#     W_deconv_2 = weight_variable_devonc([max_pool_size, max_pool_size, 4 * chanel_root, 8 * chanel_root])
#     b_deconv2 = bias_variable([4 * chanel_root])
#     h_deconv2 = tf.nn.relu(deconv2d(h_conv12, W_deconv_2, max_pool_size) + b_deconv2)
#     h_deconv2_concat = crop_and_concat(h_conv6, h_deconv2,
#                                        [batch_size, ((nx - 180) / 2 + 4) / 2 + 4, ((ny - 180) / 2 + 4) / 2 + 4,
#                                         4 * chanel_root])
#
#     # Convolution 13
#     W_conv13 = weight_variable([field_of_view, field_of_view, 8 * chanel_root, 4 * chanel_root])
#     b_conv13 = bias_variable([4 * chanel_root])
#     h_conv13 = tf.nn.relu(conv2d(h_deconv2_concat, W_conv13, keep_prob) + b_conv13)
#
#     # Convolution 14
#     W_conv14 = weight_variable([field_of_view, field_of_view, 4 * chanel_root, 4 * chanel_root])
#     b_conv14 = bias_variable([4 * chanel_root])
#     h_conv14 = tf.nn.relu(conv2d(h_conv13, W_conv14, keep_prob) + b_conv14)
#
#     # Deconvolution 3
#     W_deconv_3 = weight_variable_devonc([max_pool_size, max_pool_size, 2 * chanel_root, 4 * chanel_root])
#     b_deconv3 = bias_variable([2 * chanel_root])
#     h_deconv3 = tf.nn.relu(deconv2d(h_conv14, W_deconv_3, max_pool_size) + b_deconv3)
#     h_deconv3_concat = crop_and_concat(h_conv4, h_deconv3,
#                                        [batch_size, (nx - 180) / 2 + 4, (ny - 180) / 2 + 4, 2 * chanel_root])
#
#     # Convolution 15
#     W_conv15 = weight_variable([field_of_view, field_of_view, 4 * chanel_root, 2 * chanel_root])
#     b_conv15 = bias_variable([2 * chanel_root])
#     h_conv15 = tf.nn.relu(conv2d(h_deconv3_concat, W_conv15, keep_prob) + b_conv15)
#
#     # Convolution 16
#     W_conv16 = weight_variable([field_of_view, field_of_view, 2 * chanel_root, 2 * chanel_root])
#     b_conv16 = bias_variable([2 * chanel_root])
#     h_conv16 = tf.nn.relu(conv2d(h_conv15, W_conv16, keep_prob) + b_conv16)
#
#     # Deconvolution 4
#     W_deconv_4 = weight_variable_devonc([max_pool_size, max_pool_size, chanel_root, 2 * chanel_root])
#     b_deconv4 = bias_variable([chanel_root])
#     h_deconv4 = tf.nn.relu(deconv2d(h_conv16, W_deconv_4, max_pool_size) + b_deconv4)
#     h_deconv4_concat = crop_and_concat(h_conv2, h_deconv4, [batch_size, nx - 180, ny - 180, chanel_root])
#
#     # Convolution 17
#     W_conv17 = weight_variable([field_of_view, field_of_view, 2 * chanel_root, chanel_root])
#     b_conv17 = bias_variable([chanel_root])
#     h_conv17 = tf.nn.relu(conv2d(h_deconv4_concat, W_conv17, keep_prob) + b_conv17)
#
#     # Convolution 18
#     W_conv18 = weight_variable([field_of_view, field_of_view, chanel_root, chanel_root])
#     b_conv18 = bias_variable([chanel_root])
#     h_conv18 = tf.nn.relu(conv2d(h_conv17, W_conv18, keep_prob) + b_conv18)
#
#     # Output Map
#     W_conv19 = weight_variable([1, 1, chanel_root, n_class])
#     b_conv19 = bias_variable([n_class])
#     h_conv19 = tf.nn.relu(conv2d(h_conv18, W_conv19, tf.constant(1.0)) + b_conv19)
#
#     output_map = pixel_wise_softmax(h_conv19)
#
#     """Define the Cost Function & Training params"""
#
#     # Placeholder for grand true
#     y_ = tf.placeholder("float", shape=[None, nx - 184, ny - 184, 2])
#
#     # Placeholder for weight map
#     weight_map = tf.placeholder("float", shape=[None, nx - 184, ny - 184, 2])
#
#     # Cost function
#     # cross_entropy_ = cross_entropy(y_, output_map)
#
#     pixel_error_ = weighted_pixel_error(y_, weight_map, output_map)
#     loss_ = weighted_cross_entropy(y_, weight_map, output_map)
#     dice_ = relaxed_dice(y_, output_map, [batch_size, nx - 184, ny - 184, 2])
#
#     # TODO also include bias?
#     weights = [W_conv1, W_conv2, W_conv3, W_conv4, W_conv5, W_conv6, W_conv7, W_conv8, W_conv9, W_conv10, W_conv11,
#                W_conv12, W_conv13, W_conv14, W_conv15, W_conv16, W_conv17, W_conv18, W_conv19,
#                W_deconv_1, W_deconv_2, W_deconv_3, W_deconv_4]
#
#     # Optionally add l2 loss for l2 regularization
#     if Setting.alpha_l2loss > 0:
#         l2loss = Setting.alpha_l2loss * l2_loss(weights)
#         loss_ = tf.add(loss_, l2loss)
#
#     # Get training step
#     learning_rate_ = tf.placeholder(tf.float32, shape=[])
#     train_step = trainStep.get_train_step(loss_, learning_rate_)
#
#     nn = UNet(input=[x], output=[output_map],
#               h_conv=[h_conv1, h_conv2, h_conv3, h_conv4, h_conv5, h_conv6, h_conv7, h_conv8, h_conv9, h_conv10,
#                       h_conv11, h_conv12, h_conv13, h_conv14, h_conv15, h_conv16, h_conv17, h_conv18, h_conv19],
#               h_pool=[h_pool1, h_pool2, h_pool3, h_pool4],
#               h_deconv=[h_deconv1, h_deconv2, h_deconv3, h_deconv4],
#               h_deconv_concat=[h_deconv1_concat, h_deconv2_concat, h_deconv3_concat, h_deconv4_concat],
#               weights=weights,
#               keep_prob=keep_prob, do_update=None, y_=y_, weight_map=weight_map,
#               loss_=loss_, pixel_error_=pixel_error_, dice_=dice_, learning_rate_=learning_rate_, train_step=train_step
#               )
#
#     return nn


def create_network_variant2():
    """
    Create the network with Unet architecture.

    More general - adding layers in a for loop.
    """

    chanel_root = Setting.chanel_root
    ny = nx = Setting.nx
    field_of_view = Setting.field_of_view
    max_pool_size = Setting.max_pool_size
    batch_size = 1  # Setting.batch_size
    n_class = Setting.n_class

    # Placeholder for the input image
    x = tf.placeholder('float', shape=[batch_size, nx, ny], name='x')
    x_image = tf.reshape(x, [1, nx, ny, 1], name="x_image")

    # Probability of keeping connections for "dropout"
    keep_prob = tf.placeholder("float", name="dropout_keep_prob")

    # whether to do an update or stats
    do_update = tf.placeholder(tf.bool, name="do_update")

    h_conv = [None] * 19
    h_pool = [None] * 4
    h_deconv = [None] * 4
    h_deconv_concat = [None] * 4

    # --- Down layers ---

    # TODO (opt) version with general number of layers?

    # First layers
    h_conv[0] = conv_layer(x_image, 1, chanel_root, keep_prob, do_update, 'conv0')
    h_conv[1] = conv_layer(h_conv[0], chanel_root, chanel_root, keep_prob, do_update, 'conv1')

    for level in range(1, 5):  # iterate over levels {1,.., 4}
        # Number of the features for this level
        n_features = chanel_root * 2 ** level

        # Max Pooling
        h_pool[level - 1] = max_pool(h_conv[level * 2 - 1], max_pool_size, name='maxpool{}'.format(level - 1))

        # Fist Convolution
        h_conv[level * 2] = conv_layer(h_pool[level - 1], n_features // 2, n_features, keep_prob, do_update,
                                       'conv{}'.format(level * 2))

        # Second Convolution
        h_conv[level * 2 + 1] = conv_layer(h_conv[level * 2], n_features, n_features, keep_prob, do_update,
                                           'conv{}'.format(level * 2 + 1))

    # --- Up layers ---

    for level in range(3, -1, -1):  # iterate over levels {3, ... , 0}

        deconv_index = 3 - level

        # Number of the features for  level
        n_features = chanel_root * 2 ** level

        # add deconvolution layer
        h_deconv[deconv_index] = deconv_layer(h_conv[9 + deconv_index * 2], n_features, do_update,
                                              'deconv{}'.format(9 + deconv_index * 2))

        # concat with the corresponding conv layer
        h_deconv_concat[deconv_index] = concat_layer(h_conv[7 - 2 * deconv_index], h_deconv[deconv_index], n_features)

        # add conv layer  (twice many input channel because it is concatenation of two layers)
        h_conv[10 + deconv_index * 2] = conv_layer(h_deconv_concat[deconv_index], n_features * 2, n_features, keep_prob,
                                                   do_update, 'conv{}'.format(10 + deconv_index * 2))

        # add the second conv layer
        disable_batch_normalization = level == 0
        h_conv[11 + deconv_index * 2] = conv_layer(h_conv[10 + deconv_index * 2], n_features, n_features, keep_prob,
                                                   do_update, 'conv{}'.format(11 + deconv_index * 2),
                                                   disable_batch_normalization=disable_batch_normalization)

    # TODO visualize more layers - in particular the last layer
    # TODO consider doubling nuber of features before max-pooling (as used in the papers )

    # The final 1x1 conv layer: No Relu, no batch normalization, no dropout, field_of_view = 1.
    h_conv[18] = last_conv_layer(h_conv[17], chanel_root, n_class)

    # Create variable for step + update operation
    # Note that: step = number of times that the network was called with do_update: True
    # (can be used for e.g. number training steps)
    step, maybe_update_step = create_update_step_operation(do_update)

    # Output Map
    # Note: maybe_update_step  must be computer prior to the output of the layer
    with tf.control_dependencies([maybe_update_step]):
        output_map = pixel_wise_softmax(h_conv[18])

    log.debug('output_map: {}'.format(output_map))

    # --- Define the Cost Function & Training params ---

    # Placeholder for grand true
    y_ = tf.placeholder("float", shape=[batch_size, nx - 184, ny - 184, 2])

    # Placeholder for weight map
    weight_map = tf.placeholder("float", shape=[batch_size, nx - 184, ny - 184, 2])

    # Cost function
    pixel_error_ = weighted_pixel_error(y_, weight_map, output_map)
    classification_error_ = weighted_cross_entropy(y_, weight_map, output_map)
    loss_ = tf.identity(classification_error_)
    dice_ = relaxed_dice(y_, output_map, [batch_size, nx - 184, ny - 184, 2])

    # Get all trainable variables (weights, biases and scripts)
    # Note that: we assume here that all trainable variables are exactly these that are params of the network s
    model_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)

    # (Optional) L2 regularization
    if Setting.alpha_l2loss > 0:
        l2loss = Setting.alpha_l2loss * l2_loss(model_params)
        loss_ = tf.add(loss_, l2loss)

    # Get training step
    learning_rate_ = tf.placeholder(tf.float32, shape=[], name='learning_rate')
    train_step = trainStep.get_train_step(loss_, learning_rate_)

    nn = UNet(x=x, output_map=output_map,
              h_conv=h_conv, h_pool=h_pool, h_deconv=h_deconv, h_deconv_concat=h_deconv_concat,
              weights=model_params,
              keep_prob=keep_prob, do_update=do_update, y_=y_, weight_map=weight_map,
              classification_error_=classification_error_, loss_=loss_, pixel_error_=pixel_error_, dice_=dice_,
              learning_rate_=learning_rate_, train_step=train_step, step=step
              )

    return nn


def create_network():
    # reset tf graph to free memory - this also cleans scripts stuffs
    # tf.reset_default_graph()

    #  note that: older implementation is not compatible now - it serves only as an 'inspiration'
    # if Setting.unet_architecture_variant == 1:
    #     return create_network_variant1()

    if Setting.unet_architecture_variant == 2:
        return create_network_variant2()

    raise ValueError("Invalid Setting.unet_architecture_variant value: {}".format(Setting.unet_architecture_variant))
