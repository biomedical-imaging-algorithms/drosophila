% --------------------------------------------------------------------------
% Template for ISBI2015
% Title: Automated Segmentation of Early Oogenesis in Drosophila using Supertextons
% Authors: Nava and Kybic
% Deadline: November 24th, 2014
% --------------------------------------------------------------------------

\documentclass{article}

\usepackage{spconf}

\usepackage[english]{babel}
\selectlanguage{english}

\usepackage[table,svgnames]{xcolor}
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{{resources/}}
\DeclareGraphicsExtensions{.png}

\usepackage{subfigure}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{colortbl}
\usepackage{tabularx}
\usepackage{paralist}

% --------------------------------------------------------------------------
\title{Automated Segmentation in Early Drosophila Oogenesis using Supertextons}

\name{Rodrigo Nava and Jan Kybic \thanks{The authors extend their gratitude to Prof. Dr. Pavel Tomanc\'ak from Max Planck Institute of Molecular Cell Biology and Genetics for providing the images. This publication was supported by the European social fund within the project CZ.1.07/2.3.00/30.0034. R. Nava thanks Consejo Nacional de Ciencia y Tecnolog\'ia (CONACYT). The work of J. Kybic was supported by the Czech Science Foundation project 14-21421S}}

\address{Czech Technical University in Prague, Czech Republic}

% --------------------------------------------------------------------------
\begin{document}
\maketitle

\begin{abstract}
Studies concerning gene expression patterns are of paramount importance in basic biological research. However, mapping a gene requires analyzing hundreds of objects that have been segmented and labeled previously. Thus, a reliable segmentation is a crucial step. In this paper, we propose a novel segmentation procedure: first, a pre-segmentation step is performed using superpixels. Each superpixel that belongs to a single class is transform into a 18-length feature vector by taking the mean and the standard deviation of the responses of a filter bank. Then, a dictionary is built by clustering representative feature vectors per class, such clusters are called supertextons. Finally, in the classification stage, a superpixel is assign to a certain class using the \textit{K}-NN classifier and the supertexton dictionary. This proposal has been applied to the segmentation of nurse and follicle cells in \textit{Drosophila} oogenesis where the results have shown the effectiveness of our approach.
\end{abstract}

\begin{keywords}
Drosophila, Gene expression, Oogenesis, Superpixels, Textons
\end{keywords}

% --------------------------------------------------------------------------
\section{Introduction}
\label{sec:intro}
Since gene expression is highly regulated, namely, genes turn on only in specific cell types and at a specific time; it represents a means to explore and understand genetic interactions involved in the process of biological development. The RNA \textit{in situ} hybridization is a common technique to determine such expressions; it allows to demonstrate the presence of a RNA sequence based on the ability of acids to hybridize with complementary sequences and then make visible such a transformation \cite{TOMANCAK2002}. An excellent model for studying these phenomena is the fruit fly \textit{Drosophila melanogaster}, which was introduced as an experimental animal due to its interesting attributes such as rapid generation time, ease culturing, and low maintenance cost \cite{ARIAS2008}. 

This paper is focused on \textit{Drosophila} oogenesis -the process by which germ cells develop into eggs. It begins in a structure called germarium that contains somatic and germline stem cells and functions as an assembly line \cite{MONTELL2003}. The stem cells are divided asymmetrically to produce sixteen nurse cells, which are surrounded by somatic follicle cells (border cells) to form an egg chamber, (see Fig. \ref{fig:fig_1}). The oogenesis provides an outstanding model for genetic traceability. However, mapping one gene expression requires analyzing hundred of egg chambers that have been segmented previously. This task is time-consuming and depends on trained observers. Hence, a reliable automated segmentation method would be useful to reduce time and bias.

\begin{figure}[!tbp]
    \centering
    \includegraphics[width=0.45\textwidth]{fig_1}
    \caption{Egg chamber development during \textit{Drosophila} oogenesis. Note that egg chambers become more mature moving to the right}
    \label{fig:fig_1}
\end{figure}

\begin{figure*}[!tbp]
    \centering
    \includegraphics[width=\textwidth]{fig_3}
    \caption{Pipeline of our proposal. Each superpixel is transform into a 18-length vector by taking the mean and the standard deviation of the responses of the LM filter bank. Then, a dictionary is built by clustering representative feature vectors per class, such clusters are called supertextons.}
    \label{fig:fig_3}
\end{figure*}

To the best of our knowledge, there is little literature addressing \textit{Drosophila} oogenesis segmentation. Most of the state-of-the-art methods are devoted to segment and process sub-cellular embryogenesis data \cite{CASTRO2011}. Xiong et al. \cite{XIONG2006a} proposed a level set method for segmenting nuclei and cytoplasm; this approach relies on the idea that a curve from a given image can evolve to detect objects. Graph-based methods have been used in \cite{CHEN2006,GHITA2014} for segmenting mitochondria, whereas Lucchi et al. \cite{LUCCHI2012} take advantage of superpixel segmentation to address the same problem. Although the aforementioned methods are reasonably successful, sub-cellular imaging is commonly performed using electron microscopy modalities. Therefore, the existing approaches cannot be used directly on \textit{Drosophila} oogenesis.

On the other hand, texture has played an important role in distinguishing regions because intensity variations may reflect different areas, shapes, or objects. In \cite{VARMA2005}, Varma and Zisserman introduced the concept of ``textons'' as the representative responses occurring after convolving an image with a set of filter banks. The collection of responses are then clustered with \textsl{k}-means to produce a dictionary of visual words in order to classify texture patches. Nevertheless, textons characterize spatial properties of regions rather than pixels. This idea leads to our proposal, we investigated the use of a pre-segmentation step that groups perceptually-similar pixels and yields adherence to salient edges. For each group of pixels that belongs to a single class, the mean and the standard deviation of the corresponding filter responses are computed to form a feature vector, then, a dictionary is built using representative vectors called ``supertextons.'' This proposal allows to distinguish follicle and nurse cells from the background and cytoplasm in \textit{Drosophila} egg chambers. 

This paper is organized as follows: in Section \ref{sec:metho} the proposal is presented in detail. In Section \ref{sec:exp} the data and the experiments are presented, while conclusions and future work are shown in Section \ref{sec:concl}.

\begin{table*}[!tbp]
\rowcolors{5}{}{AliceBlue}
\caption{Classification results of supertextons against segmentation without superpixels. To assess the performance we used leave-one-out cross-validation. The values represent mean rates after the classification of 30 images. All data are expressed in $(\%)$. The values between parenthesis represent standard deviations. Bold values represent the best rates.}
\label{tab:tab_1}
\centering
%{\footnotesize
\begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}ccccccc}
\toprule
& \multicolumn{3}{c}{Supertextons}  & \multicolumn{3}{c}{Textons}  \\
\cmidrule[0.5pt](l{5pt}r{5pt}){2-4}
\cmidrule[0.5pt](l{5pt}r{5pt}){5-7}
Class & PR & SE & $F_{1}$-Score & PR & SE & $F_{1}$-Score  \\
\hline
\hline
Background & 92.95 ($\pm$ 7.77) & 85.73 ($\pm$ 6.76) & \textbf{88.82} ($\pm$ 4.65) &89.52 ($\pm$ 10.67) & 88.59 ($\pm$ 5.77) & 88.49 ($\pm$ 5.96) \\
Follicle cells & 81.64 ($\pm$ 5.82) & 78.00 ($\pm$ 9.35) & \textbf{79.38} ($\pm$ 5.81) & 83.57 ($\pm$ 5.76) & 73.00 ($\pm$ 9.07) & 77.52 ($\pm$ 5.77) \\
Nurse cells & 79.1 ($\pm$ 10.91) & 79.85 ($\pm$ 9.67) & \textbf{78.6} ($\pm$ 6.80) & 76.92 ($\pm$ 11.08) & 79.28 ($\pm$ 9.79) & 77.21 ($\pm$ 7.0) \\
Cytoplasm & 73.88 ($\pm$ 11.37) & 85.58 ($\pm$ 7.86) & \textbf{78.61} ($\pm$ 7.16) & 73.24 ($\pm$ 10.85) & 82.23 ($\pm$ 10.76) & 76.74 ($\pm$ 8.46)\\
\bottomrule
\end{tabular*}
%}
\end{table*}
% --------------------------------------------------------------------------
\section{Methodology}
\label{sec:metho}
We propose a methodology composed of four stages: \begin{inparaenum}[\bfseries i\normalfont)] \item pre-processing; \item superpixel extraction; \item supertexton dictionary; and \item classification using \textit{K}-nearest neighbors\end{inparaenum}.

% --------------------------------------------------------------------------
\subsection{Data pre-processing}
Pre-processing is intended as a normalization stage that produces a new version of the image, so that it can be effectively processed by further methods. In this proposal, a bilateral filtering \cite{SRISUK2014} was applied in order to reduce the effects of noise and preserve salient edges. 

% --------------------------------------------------------------------------
\subsection{Superpixel segmentation}
Since textons are computed over regions or patches rather than pixels, a common practice is to split up the input image into blocks and compute a histogram for each one \cite{JAVED2011}. However, this procedure leads to a block-like segmentation. 

Thus, we propose to generate an initial partition of the data using superpixels which are regions with similar textural properties. We applied the SLIC algorithm proposed in \cite{ACHANTA2012} that begins with a number of seeds sampled onto a regular grid and iteratively for each seed computes superpixels based on intensity values and connectivity. We set the initial superpixel size, $s = 4$, and regularizer, $m = 0.01$.

% --------------------------------------------------------------------------
\subsection{Supertexton dictionary}
In order to build a supertexton dictionary, training images are convolved with a filter bank to generate filter responses. We used the Leung-Malik (LM) filter bank \cite{LEUNG2001} that consists of 48 filters of size 49 $\times$ 49 px. organized as follows: first and second Derivatives of Gaussian at 6 orientations $\left\{0^{\circ}, 30^{\circ}, 60^{\circ}, 90^{\circ}, 120^{\circ}, 150^{\circ}\right\}$ and 3 scales $\left\{\sqrt{2}, 2, 2\sqrt{2}\right\}$; 8 Laplacian of Gaussian and 4 Gaussian filters with standard deviation $\sigma = \left\{\sqrt{2},\, 2,\,2\sqrt{2},\,4\right\}$. We keep only the maximum response across orientations on first and second Derivatives of Gaussian to reduce the number of responses from 48 to 18 and achieve rotational invariance.

Each superpixel $SP_{i}$ that belongs to the class $C$ is transform into a 18-length feature vector by taking the mean and the standard deviation of the corresponding filter responses as follows:
\begin{equation}
   \overline{SP_{i}} = \big[\mu_{i,1},\,\sigma_{i,1},\,\ldots,\,\mu_{i,N},\,\sigma_{i,N}\big]
\label{eq:eq_1}
\end{equation}
$\mu_{i,k}$ and $\sigma_{i,k}$ represent the mean and the standard deviation of the $k$-filter response within the region of the $i$-superpixel and $N$ is the maximum number of filters.

For each class, \textit{K}-means clustering is applied to obtain the supertextons that represent the class. Therefore, the dictionary will consist of $C \times K$ supertextons. In our experiments, we set $K = 100$, thus, every input image in the training stage results into $400$ supertextons that belong to nurse cells, follicle cells, background, and cytoplasm, (see Fig. \ref{fig:fig_3})

% --------------------------------------------------------------------------
\subsection{Classification}
For classification, the test image is pre-segmented and the feature vectors are calculated as we described in the previous subsection. We use \textit{K}-NN classifier with $K=40$ and assign the input feature vector to the class that includes the highest number of neighbors using the Euclidean distance as a metric.

% --------------------------------------------------------------------------
\section{Experimental Results}
\label{sec:exp}
For this studio we selected a 30 3D-stacks of \textit{Drosophila} oogenesis of size 647 $\times$ 1024 $\times$ 60. They were captured using an optical sectioning fluorescence microscope. Each stack contains two channels, the first one (magenta) shows nuclei of all cells that constitute egg chambers providing a common reference. Note that in every slice may appear different stages of the occyte development. The second one (green) comes from specific RNA probes labeled with a fluorescent dye. In this studio, the green channel was ruled out. Images of isolated egg chambers were obtained manually by expert biologists at Max Planck Institute of Molecular Cell Biology and Genetics in Dresden, Germany. As a result, the dimensions of the egg chambers varied from one to another. For each image, four classes were annotated: background, follicle cells, nurse cells, and cytoplasm, (see Fig. \ref{fig:fig_4}). 

In order to assess our proposal we used leave-one-out cross-validation. The precision (PR) and sensitivity (SE) were computed and compared with a manual segmentation. We also present $F_{1}$-Score $=2\frac{PR \times SE}{PR+SE}$, which is a measure of accuracy and reaches its best value at $1$ and worst score at $0$. We compared our proposal against segmentation using textons without superpixels. Table \ref{tab:tab_1} summarizes results and comparisons.

\begin{figure*}[!tbp]
    \centering
    \subfigure{\includegraphics[width=0.15\textwidth]{fig_4a}}
    \subfigure{\includegraphics[width=0.15\textwidth]{fig_4b}}
    \subfigure{\includegraphics[width=0.15\textwidth]{fig_4c}}
    \subfigure{\includegraphics[width=0.15\textwidth]{fig_4d}}
    \subfigure{\includegraphics[width=0.15\textwidth]{fig_4e}}
    \subfigure{\includegraphics[width=0.15\textwidth]{fig_4f}}\\
    \setcounter{subfigure}{0}
    \subfigure[]{\includegraphics[width=0.15\textwidth]{fig_5a}}
    \subfigure[]{\includegraphics[width=0.15\textwidth]{fig_5b}}
    \subfigure[]{\includegraphics[width=0.15\textwidth]{fig_5c}}
    \subfigure[]{\includegraphics[width=0.15\textwidth]{fig_5d}}
    \subfigure[]{\includegraphics[width=0.15\textwidth]{fig_5e}}
    \subfigure[]{\includegraphics[width=0.15\textwidth]{fig_5f}}\\
    \caption{Examples of segmentation in \textit{Drosophila} egg chambers. \textbf{(a)} the original egg chamber; \textbf{(b)} the corresponding ground-truth; \textbf{(c)} segmentation with supertextons; \textbf{(d)} supertexton segmentation over the original image; \textbf{(e)} segmentation without superpixels; and \textbf{(f)} segmentation without superpixel over the original image. Note that the use of superpixels minimize the noise effect.}
    \label{fig:fig_4}
\end{figure*}

% --------------------------------------------------------------------------
\section{Conclusions}
\label{sec:concl}
In this paper, we proposed a novel supervised segmentation method based on superpixels and textons. For each superpixel that belongs to a single class, a feature vector was computed using the mean and the standard deviation of the corresponding filter responses. This procedure overcomes the traditional approach based on patches that leads to a block-like segmentation. Another advantage of our proposal is that results in a substantial reduction of time and complexity. 

Nevertheless, texton-based approaches need large dictionaries in order to classify new images. Here, we segmented only egg chambers from the stage 2 of the Drosophila oogenesis. Since egg chambers are highly variables in shape and intensity values, they represent a challenge for automated segmentation methods. Further improves must include global features that discriminate stages.

% --------------------------------------------------------------------------
\bibliographystyle{IEEEbib}
\bibliography{references}

\end{document}
