%% ---------------------------------------------------------------
%
%
%
%
%% ---------------------------------------------------------------

\documentclass[]{spie}

\usepackage[]{graphicx}
\usepackage{subfigure}	
\graphicspath{{images/}}
\DeclareGraphicsExtensions{.png}

\usepackage[]{paralist}
\usepackage[table,svgnames]{xcolor}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{paralist}
\usepackage{url}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{lineno}

\usepackage{tikz,pgfplots,pgfplotstable}
\usetikzlibrary{pgfplots.groupplots}

\pgfplotsset{width=0.3\textwidth, compat=1.8}
\pgfplotstableread[col sep = comma]{images/data.csv} \mydata
\pgfplotstableread[col sep = comma]{images/datb.csv} \mydatb
\pgfplotstableread[col sep = comma]{images/cepstruma.csv} \Cepstruma
\pgfplotstableread[col sep = comma]{images/cepstrumb.csv} \Cepstrumb
\pgfplotstableread[col sep = comma]{images/cepstrumc.csv} \Cepstrumc

\renewcommand{\labelitemi}{$\diamond$}

%% ---------------------------------------------------------------

\title{Texel-based Image Classification with Orthogonal Bases} 
 
\author{Erik Carbajal-Degante\supit{a}, Rodrigo Nava\supit{b}, Jimena Olveres\supit{c}, Boris Escalante-Ramírez\supit{d}, and Jan Kybic\supit{b}
\skiplinehalf
\supit{a}Posgrado en Ingeniería Eléctrica, Facultad de Ingeniería, Universidad Nacional Autónoma de México, Mexico City, Mexico \\
\supit{b}Faculty of Electrical Engineering, Czech Technical University in Prague, Czech Republic \\
\supit{c}Posgrado en Ciencia e Ingeniería de la Computación, Universidad Nacional Autónoma de México, Mexico City, Mexico \\
\supit{d}Departamento de Procesamiento de Señales, Facultad de Ingeniería, Universidad Nacional Autónoma de México, Mexico City, Mexico
}

\authorinfo{Further author information: (Send correspondence to E.C-D.)
\\E.C-D.: E-mail: erikycd@gmail.com}
 
%% ---------------------------------------------------------------

\begin{document} 
\maketitle 

\begin{abstract}
Periodic variations in patterns within a group of pixels provide important information about the surface of interest and can be used to identify objects or regions. Hence, a proper analysis can be applied to extract particular features according to some specific image properties. Recently, texture analysis using orthogonal polynomials has gained attention since polynomials characterize the pseudo-periodic behavior of textures through the projection of the pattern of interest over a group of kernel functions. However, the maximum polynomial order is often linked to the size of the texture, which implies in many cases, a complex calculation and introduces instability in higher orders leading to computational errors. In this paper, we address this issue and explore a pre-processing stage to compute the optimal size of the window of analysis called ``texel.'' We propose Haralick-based metrics to find the main oscillation period, such that, it represents the fundamental texture and captures the minimum information, which is sufficient for classification tasks. This procedure avoids the computation of large polynomials and reduces substantially the feature space with small classification errors. Our proposal is also compared against different fixed-size windows. We also show similarities between full-image representations and the ones based on texels in terms of visual structures and feature vectors using two different orthogonal bases: Tchebichef and Hermite polynomials. Finally, we assess the performance of the proposal using well-known texture databases found in the literature.
\end{abstract}
 
\keywords{Hermite transform, orthogonal bases, Tchebichef moments, texel, texture classification}

%% ---------------------------------------------------------------

\section{Introduction}
\label{sec:introduction}

Traditionally, texture is defined, in a broad sense, as periodic-like behavior patterns within a spatial region in an image\cite{Jain95}. It is also a property related to material, roughness, or the shape of a surface. Furthermore, it provides important information that is suitable for further processing in: synthesis and perception\cite{Balas2015,Wu2015}, remote sensing\cite{Guo05,Gao2015}, medical imaging\cite{Sorensen12,Zhang07}, image retrieval and classification\cite{Shao2014}, among others. The latter is still an open and important problem that deals with the correct identification of a given texture from a set of different classes. According to Petrou et al.\cite{Petrou2006}, approaches to image retrieval and classification can be categorized into four groups:\begin{inparaenum}[\color{OrangeRed}(\bfseries i\normalfont)]~\item \textbf{statistical methods} analyze the spatial distribution of pixels and extracts a set of statistics; \item \textbf{structural methods} represent textures by large primitives and placement rules; \item \textbf{model-based methods} estimate parameters using a model that resembles the texture of interest; and \item \textbf{spatial-frequency methods} use a set of filters to highlight and localize salient features\end{inparaenum}.

Texture may appear in many different ways, so that, classification algorithms should also consider the purpose for which a texture is used. Commonly, texture classification can be divided into two stages. During the first step, extraction of salient characteristics is computed to create a representation that acts as a texture signature. Such a representation must be robust to environmental changes including both geometrical and photometric transformations. In this regard, some methods in the literature are focused on the extraction of texture characteristics; for instance, Haralick\cite{Haralick79} introduced the gray-level co-occurrence matrices (GLCM) and Cross et al.\cite{Cross83} used Markov random fields to create texture models. Other approaches are local binary patterns\cite{Ojala02} and Gabor filters\cite{Bovik90}. On the other hand, the goal during the second step is to assign a texture sample to a class that best describes the sample. 

The continuous development of texture analysis techniques has allowed to increase the information that can be obtained from a region of interest. Specifically, feature extraction with orthogonal bases has gained attention due to the ability of the bases to describe textures by projecting an image over a set of functions that describes the oscillation behavior of all the elements that constitute the image. First, Mukundan et al.\cite{Mukundan01} proposed discrete Tchebichef moments (DTMs) to describe textures and overcome limitations of bases such as Zernike and Legendre\cite{Teh88}. Later, Marcos et al.\cite{Marcos13} analyzed DTMs and classified large textures, whereas Nava et al.\cite{Nava2013} suggested Tchebichef polynomials to describe emphysematous tissue. Furthermore, Estudillo et al.\cite{Estudillo2011} explored the use of Hermite polynomials to characterize textures, while Yang et al.\cite{Bo2013} discussed the advantages of Hermite polynomials on texture characterization. 

However, the aforementioned methods have limitations as they calculate a single feature vector for the whole texture or use a fixed-size window of analysis. These approaches do not consider the size of the texel, which implies the computation of high-order moments and leads to numerical instabilities due to the fast oscillations of polynomials\cite{Mukundan04,Nava2015}. In this paper, first, we propose a novel technique to find the window of analysis or texel that captures on both X- and Y-axes the fundamental pattern, which is able to describe the complete image of interest. This methodology avoids the computation of higher-order polynomials and reduces substantially the feature space. We perform experiments to validate our proposal using a texture dataset composed of 40 images. Second, we conduct texture classification using the resulted texels and compare our results against well-known algorithms using fixed-size windows on the Brodatz\cite{Brodatz66} database using 34 textures.

Next, we review the fundamentals of the orthogonal moments and briefly introduce Tchebichef and Hermite polynomials in the following Section \ref{sec:background}. Our proposal is detailed in Section \ref{sec:methodology}. The validation of the texel size procedure is presented in Section \ref{sec:validation}, while texture classification results are shown in Section \ref{sec:classification}. Finally, conclusions are discussed in Section \ref{sec:conclusion}.

%% ---------------------------------------------------------------

\section{Orthogonal Bases}
\label{sec:background}

Bases play a prominent role in the analysis of vector spaces. They are used in both finite- and infinite-dimensional cases and allow to represent the signal of interest through projections by using a subset of vectors. We can define an orthogonal basis $\{t_{pq}\}$ as a group of vectors $P(x)$ that satisfies a relation of orthogonality or weighted orthogonality as follows:
\begin{equation}
\int P_p(x)P_q(x)dx = h\delta_{pq}\,
\label{eq:Orthogonality}
\end{equation}
where $h = \|P_p(x)\|\|P_q(x)\|$ is the product of the norms of the polynomials $P_p(x)$ and $P_q(x)$ and $\delta_{pq} = 1$ if $p=q$, otherwise $\delta_{pq}=0$.

On the other hand, orthogonal moments are scalar quantities that characterize a function of interest in terms of the basis $\{t_{pq}\}$ and possess the ability to represent texture features with the minimum amount of redundancy\cite{Teh88}. They are computed as projections between the texture $f(x,y)$ and the basis $\{t_{pq}\}$ within the region $\Omega: T_{pq} =\mathop{\int\!\!\!\int}_{\Omega} t_{pq}(x,y)f(x,y)\,dxdy$ where $p$ and $q$ are non-negative integers and $s = p + q$ represents the order of the moment. $T_{pq}$ measures the correlation between $f(x,y)$ and the corresponding polynomials $t_{pq}(x,y)$\cite{Nava2015}. 
 
The discrete version holds most of the useful properties embedded in the continuous analysis. The implementation of the discrete orthogonal moments does not require any numerical approximation or large computational development since the basis is orthogonal over the domain of image coordinate space. Thus, the expansion of a given texture in the discrete domain onto the basis of $\{t_{pq}\}$ is calculated according to the following formula:
\begin{equation}
T_{pq} = \frac{1}{\rho(p,N)\rho(q,N)}\sum_{x=0}^{N-1}\sum_{y=0}^{N-1}f(x,y)t_p(x)t_q(y)\,
\label{eq:sum3}
\end{equation}
where $N$ is the size of the texture and $\rho(n,N)$ is a normalization factor.

%% ---------------------------------------------------------------
 
\subsection{Discrete Tchebichef moments}

The introduction of the discrete Tchebichef moments (DTMs) was made by Mukundan et al.\cite{Mukundan01,Mukundan04}. They are based on the scaled Tchebichef polynomials $t_p$ that can be generated using the following recurrent relation, (see Figure \ref{fig:TchebichefP}):
\begin{equation}
\begin{aligned}
t_{0}\left(x\right) &= \frac{1}{\sqrt{N}},\, \\
t_{1}\left(x\right) &= \left(2x+1-N\right)\sqrt{\frac{3}{N\left(N^2-1\right)}},\, \mbox{ and}\\
t_{p}\left(x\right) & = K_1xt_{p-1}\left(x\right) + K_2t_{p-1}\left(x\right) + K_3t_{p-2}\left(x\right)\\
\end{aligned}\,
\label{eq:equation_1}
\end{equation}
with $x = 0,1,\ldots,N-1$. $K_1 = \frac{2}{p}\sqrt{\frac{4p^2-1}{N^2-p^2}}$, $K_2 = \frac{1-N}{p}\sqrt{\frac{4p^2-1}{N^2-p^2}}$, and $K_3 = \frac{p-1}{p}\sqrt{\frac{2p+1}{2p-3}}\sqrt{\frac{N^2-(p-1)^2}{N^2-p^2}}$ are the coefficients that ensure stability in case of large order polynomials.

Marcos et al.\cite{Marcos13} suggested that the correlation between the basis and the texture can be observed in terms of the magnitude of the moment $M$. The larger the moment, the higher the correlation when repetitive patterns occur at a similar rate to some polynomials. Therefore, it is possible to build a texture signature as follows:  
\begin{equation}
M(s) = \sum_{s=p+q}|T_{pq}|\,
\label{eq:sum4}
\end{equation}
with $s = 1,...,2N-1$. The moment of order $s=0$ is discarded because it does not contain information about the texture. Eq. (\ref{eq:sum4}) evaluates similarities between the texture and the varying patterns implemented by DTMs yielding a 2N-1 vector that describes the texture attributes.

\begin{figure}[tbp!]
\hspace*{\fill}
\subfigure[]{\label{fig:TchebichefP}\includegraphics[width=0.3\textwidth]{Tcheb_Poly}}\hspace{50pt}
\subfigure[]{\label{fig:HermiteP}\includegraphics[width=0.3\textwidth]{HermiteGauss_D_Poly}}
\hspace*{\fill}
\caption{Orthogonal bases. \subref{fig:TchebichefP} Tchebichef polynomials of order $t_p$ from 0 to 4. \subref{fig:HermiteP} Hermite analysis functions of order $n$ from 0 to 4.}
\label{fig:Poly}
\end{figure}

%% ---------------------------------------------------------------

\subsection{Steered Hermite transform}

Hermite polynomials resemble receptive fields in the human visual system\cite{Young01} and have proven to be highly effective in texture analysis\cite{Silvan06,Estudillo2011}. They can be interpreted as local generic operators in the scale-space representation and are constructed as the product of Hermite polynomials and a Gaussian window\cite{Martens90}, (see Figure \ref{fig:HermiteP}).

Let $f_n(x)$ be a function that locally represents a signal $f(x)$ in terms of the windowed function $V(x)$ expanded onto the orthogonal polynomials $G_n(x)$ at $x_i$. If $G_n$ represents the Hermite polynomials $H_n(x)$; then, Eq. (\ref{eq:sum5}) defines the Hermite transform: 
\begin{equation}
f_n(x_i) = \int_{x}f(x)G_n(x_i-x)V^{2}(x_i-x)dx\,
\label{eq:sum5}
\end{equation}

The discrete version can be obtained by convolving the signal with the Hermite analysis functions defined by $d_n(x) = H_n V^2$ where $V$ is a Gaussian function.
\begin{equation}
d_n(x) = \frac{(-1)^n}{\sqrt{2^n n!}}\frac{1}{H_{\sigma} \sqrt{\pi}}H_n \left(\frac{x}{H_{\sigma}}\right)\mathrm{e}^{-x^2/H_{\sigma}^2}\,
\label{eq:sum8}
\end{equation}
where $n$ is the order of the Hermite polynomials $H_n$ and $H_{\sigma}$ is the standard deviation of the Gaussian function.

In this study, we use the steerable Hermite filters\cite{Silvan06,vanDijk97} that are a class of rotated Hermite filters and are used to define the Steered Hermite transform (SHT) as follows:
\begin{equation}
f_{n-m,m}^{\theta}(x_0,y_0,\theta) = \sum_{k=0}^{n}f_{n-k,k}(x_0,y_0)\alpha_{n-k,k}(\theta)\,
\label{eq:sum11}
\end{equation}	
where $\alpha_{n-m,m}(\theta)$ approximates a binomial window rotated $\theta$ degrees, $n-m$ and $m$ represent the corresponding orders in X- and Y-axes, respectively.

The Hermite filters can adapt to the local orientation providing a reduced representation because they concentrate most of the visual information along the coefficients $f_{n,0}^{\theta}(x_0,y_0,\theta)$, (see Figure \ref{fig:DHT}). Following the idea of the scale-space representation\cite{Silvan06}, we used four different moments $n$ at four different scales $H_{\sigma}$ to generate four groups of coefficients, (see Figure \ref{fig:DHT}). 

The feature vector is built\cite{Estudillo2011} by concatenating the mean and the standard deviation of the steered Hermite coefficients of order $n$ at every scale $H_{\sigma}$ as follows:  
\begin{equation}
F = \left\{\mu_{n}^{H_{\sigma}},\sigma_{n}^{H_{\sigma}}\middle|n=0\,,\ldots\,N; H_{\sigma}=n\,\ldots,\,N\right\}\,
\label{eq:Union}
\end{equation}
where $\mu_n^{H_{\sigma}}$ and $\sigma_n^{H_{\sigma}}$ represents the mean and the standard deviation of the coefficient $f_{n,0}^{\theta}(x,y,\theta,H_{\sigma})$, respectively.

\begin{figure}[tpb!]
\hspace*{\fill}
\subfigure[]{\label{fig:D1_DHT_1}\includegraphics[width=0.49\textwidth]{Walden_scale2}}
\subfigure[]{\label{fig:D1_DHT_S4}\includegraphics[width=0.49\textwidth]{Walden_scale60}}
\hspace*{\fill}
\caption{Steered Hermite coefficients at different scales of analysis and two orders of expansion. \subref{fig:D1_DHT_1} with scale $H_{\sigma}=4$ and \subref{fig:D1_DHT_S4} with scale $H_{\sigma}=80$.}
\label{fig:DHT}
\end{figure}

%% ---------------------------------------------------------------
	
\section{Finding the window of analysis}
\label{sec:methodology}

The aim of this study is to propose a methodology to find the smallest window of analysis that captures the basic oscillating pattern of the texture also know as texel. So that, the texel is able to describe the complete texture. For this purpose, we used GLCM\cite{Haralick79}, which is a well-known technique for computing spatial and statistical features. It defines the relative position of pairs of pixels using a distance $d$ and orientation $\theta$. With this information, a matrix $C(d,\theta)$ is built with elements $c_{ij}=\{h_{ij}|i,j=1,\ldots,\,8\}$. $h_{ij}$ represents the probability to find a pair of pixels $i,j$ separated by the distance $d$ on the direction of $\theta$. 

In this way, each matrix emphasizes salient structures according to pixels variations. The size of the matrices depends on the number of gray levels in the texture. Eight levels and four orientations are commonly used. We computed correlation, contrast, energy, and homogeneity in every matrix $C(d,\theta)$ to generate characteristic curves $t^{\theta}$ that describe the texture oscillation. This analysis was performed with $d=[2,\,\ldots,\frac{N}{2}]$ and $\theta =[0,\frac{\pi}{2}]$ that produced four 256-length feature vectors. 

In order to find the texel's size, we proposed to apply the cepstral analysis (CA)\cite{Flanagan65} to the curves $t^{\theta}$. CA is a form of spectral analysis and is used in speech processing and telecommunications\cite{Noll67,Jantti2014} and echo signal detection of earthquakes\cite{Anxu2012}. Although CA is a hardly new technique in signal processing, it can be a useful tool in computer vision\cite{Bandari93}. CA relies on the fact that the Fourier transform of a pitched signal has a number of regularly spaced peaks. Such peaks represent the harmonic spectrum of the signal. When the logarithm of the magnitude spectrum is applied, the peaks are reduced and the amplitude is represented in a more suitable scale. 
After applying the inverse Fourier transform, the maximum value $D$ indicates the main period of signal, (see Figure \ref{fig:Ceps}). 

CA is defined as the inverse Fourier transform of the logarithm of the magnitude spectrum of a signal as follows:
\begin{equation}
CA_{t} = F^{-1} \left\{\log\left(|T|\right)\right\}\,
\label{eq:Cepstrum}
\end{equation}
where $T$ is the Fourier transform of the function $t^{\theta}$, which is built by concatenating the correlation values of all matrices $C(d,\theta)$.

Since the CA is performed on two directions, we obtain two values $D$ that describe the size of the texel on X- and Y-axes. Therefore, the texel is rectangular.   

\begin{figure}[tpb!]
\centering	
\includegraphics[width=0.75\textwidth]{Diagramas1}	
\caption{Texel size estimation process for the three methodologies proposed.}
\label{fig:Texel_diag}
\end{figure}

In order to validate our proposal, we compared CA against two other methods: Four metrics and correlation, (see Figure \ref{fig:Texel_diag}). 
\begin{itemize}
\item Four metrics: In this method, we concatenated correlation, contrast, energy, and homogeneity to produce four curves $t^{\theta}$, separately. The power spectrum is applied to each curve and the value $D$ is chosen by a majority rule.

\item Correlation: This method measures the gray level linear dependence between two pixels using marginal distributions. Here, we built the curve $t^{\theta}$ using correlation values of all matrices $C(d,\theta)$. The power spectrum is applied to $t^{\theta}$ and the maximum value $D$ indicates the main period of signal.
\end{itemize}

\begin{figure}[tbp!]
\centering
\begin{tikzpicture}
\begin{groupplot}[group style={group size=3 by 1}]

\nextgroupplot[xlabel style={align=center, text width=2.5cm}, xlabel={Time [s] \newline (a)}, ylabel=Amplitude]
\addplot [mark=*, mark size=0.1,draw=red, smooth,mark options={color = red}] table [x index={0}, y index = {1}] {\Cepstruma};

\nextgroupplot[xlabel style={align=center, text width=2.5cm}, xlabel={Frequency [f] \newline (b)}]
\addplot [mark=*, mark size=0.1,draw=blue, smooth,mark options={color = blue}] table [x index={0}, y index = {1}] {\Cepstrumb};

\nextgroupplot[xlabel style={align=center, text width=2.5cm}, xlabel={Quefrency [s] \newline (c)}]
\addplot [mark=*, mark size=0.1,draw=green, smooth,mark options={color = green}] table [x index={0}, y index = {1}] {\Cepstrumc};
%\end{axis}
\end{groupplot}

\end{tikzpicture}
\caption{Cepstral analysis (CA) applied to a periodic signal. (a) The original signal. (b) The logarithm of the magnitude spectrum and (c) the salient signal in the quefrency domain.}
\label{fig:Ceps}
\end{figure}

$D$ values indicate the number of pixels per cycle of texture oscillation. Since both Tchebichef and Hermite polynomials use square windows, we only consider the maximum value $D$ of X- and Y- axes because the maximum value captures the slower oscillation period. Hence, we generate square texels. 

%---------------------------------------------------------------- 

\subsection{Texel size validation}
\label{sec:validation}

Next section shows the experiments we have conducted to validate our proposal. First, we constructed a dataset composed of 40 textures from Brodatz\cite{Brodatz66}, Vistex\cite{Vistex02}, and Klette\cite{Klette10} databases. Each original texture of 512$\times$512 px. was cropped to extract a basic pattern that was tiling to produce a new texture. In this way, we know the texel size of the pattern employed in each texture. Every new generated texture has different texel size. 

We evaluated three different degradations:\begin{inparaenum}[\color{OrangeRed}(\bfseries i\normalfont)]~\item \textbf{Blurring}, we simulated this degradation using a Gaussian low-pass filter with $\sigma = 0.5$ and repeated this degradation $L=\{5,\,10,\,15,\,20,\,25,\,30\}$ times; \item \textbf{Gaussian noise}, we considered five different levels of variance $\sigma^2=\{0,\,0.2\,,0.4,\,0.6,\,0.8,\,1\}$ and; \item \textbf{impulsive noise}, it was also evaluated according to noise occupancy. 100\% represents a totally corrupted image.\end{inparaenum}

Finally, the mean error was computed in terms of error percentage for each level of distortion. The results (see Figure \ref{fig:Degradations}) show that the texel estimation based on CA achieved better results and is the most robust model to degradations. 

\begin{figure}[ht!]
\centering
\begin{tikzpicture}
\begin{groupplot}[group style = {group size = 2 by 3, horizontal sep = 50pt, vertical sep = 50pt}]

\nextgroupplot[title={Horizontal},xlabel={Blur [$L$]}, ylabel={Error [\%]},
legend style = {column sep = 10pt, legend columns = -1, legend to name = grouplegend}]
\addplot coordinates{(5,22.99) (10,23.83) (15,23.62) (20,23.41) (25,19.12) (30,30.42)}; \addlegendentry{Correlation}
\addplot coordinates{(5,24.08) (10,23.55) (15,22.35) (20,22.35) (25,22.21) (30,22.21)}; \addlegendentry{Four-metrics}
\addplot coordinates{(5,0.45) (10,1.37) (15,0.38) (20,0.42) (25,0.49) (30,0.49)}; \addlegendentry{Cepstrum}

\nextgroupplot[title={Vertical},xlabel={Blur [$L$]}, ylabel={Error [\%]}],
\addplot coordinates{(5,21.15) (10,20.83) (15,20.18) (20,20.21) (25,19.52) (30,23.15)};
\addplot coordinates{(5,18.44) (10,19.92) (15,19.92) (20,19.92) (25,19.92) (30,19.92)};
\addplot coordinates{(5,1.41) (10,1.48) (15,0.47) (20,0.68) (25,0.57) (30,0.47)}; 

\nextgroupplot[xlabel = {Gaussian noise [$\sigma^2$]}, ylabel={Error [\%]}],
\addplot coordinates{(0,23.13) (0.2,25.45) (0.4,25.49) (0.6,25.45) (0.8,25.38) (1,25.45)};
\addplot coordinates{(0,25.45) (0.2,25.45) (0.4,25.42) (0.6,25.45) (0.8,25.49) (1,25.45)};
\addplot coordinates{(0,1.16) (0.2,8.69) (0.4,13.90) (0.6,14.22) (0.8,13.97) (1,23.45)};

\nextgroupplot[xlabel = {Gaussian noise [$\sigma^2$]}, ylabel={Error [\%]}],
\addplot coordinates{(0,19.89) (0.2,22.78) (0.4,22.5) (0.6,21.05) (0.8,21.88) (1,23.33)};
\addplot coordinates{(0,19.89) (0.2,21.08) (0.4,21.08) (0.6,21.05) (0.8,21.88) (1,21.92)};
\addplot coordinates{(0,0.68) (0.2,2.06) (0.4,4.49) (0.6,3.98) (0.8,11.66) (1,11.44)}; 

\nextgroupplot[xlabel = {Salt\&Pepper [$\%$]}, ylabel={Error [\%]}],
\addplot coordinates{(0,23.13) (20,25.10) (40,25.10) (60,25.14) (80,26.09) (100,99.85)};
\addplot coordinates{(0,25.45) (20,25.45) (40,25.45) (60,24.96) (80,28.55) (100,95.91)};
\addplot coordinates{(0,1.16) (20,6.65) (40,17.60) (60,18.55) (80,36.12) (100,69.29)}; 

\nextgroupplot[xlabel = {Salt\&Pepper [$\%$]}, ylabel={Error [\%]}],
\addplot coordinates{(0,19.89) (20,21.34) (40,19.89) (60,21.23) (80,23.94) (100,85.47)};
\addplot coordinates{(0,19.89) (20,21.01) (40,21.01) (60,23.62) (80,26.55) (100,85.47)};
\addplot coordinates{(0,0.68) (20,4.81) (40,4.42) (60,11.44) (80,53.11) (100,90.83) };

\end{groupplot}
\node at ($(group c2r1) + (-2.7,-12.0cm)$) {\ref{grouplegend}}; 
\end{tikzpicture}
\caption{Results for the three methodologies on textures with degradation. Results on the first column correspond to examination on the Horizontal axis, while second column correspond to examination on the Vertical axis. }
\label{fig:Degradations}
\end{figure}


%---------------------------------------------------------------- 

\section{classification results}
\label{sec:classification}

We are interested in comparing the performance of texture classification using texels in order to reduce the feature space. One motivation is to avoid the computation of high-order moments that introduce instability, increase the number of samples needed to classification, and cause the course of dimensionality.  

We generated different texture sets using 34 non-rotated textures of 512$\times$512 px. from the Brodatz database. We only selected textures with a periodic behavior and created independent subsets for training and testing tasks. Each texture was divided into two non-overlapping patches of 256$\times$512 px. Then, each generated set was further divided into patches of sizes 128$\times$128, 64$\times$64, and 32$\times$32 using a sliding window. In addition, another set was created using our proposal based on texels, so that, every image in this set has different window size of analysis. 

We conducted experiments to estimate the size of the texels on both training and testing sets. In the ideal case, texels must be the same size in both sets, Table \ref{TexelSizes} shows the 34 textures with their respective texel in terms of pixels per side. The average error between training and testing sets is 1.91 pixels.

We found there is a relationship between the feature vectors computed on texels and those calculated on the complete images. Regarding DTMs, although the length of the feature vector is linked to the window of analysis, DTMs applied to texels produce scaled feature vectors compared to the ones obtained on complete images, (see Figure \ref{fig:FeatureVectors}(b)). On the other hand, the length of the  feature vectors generated by SHT is independent of the size of the window of analysis. Nevertheless, the length of the feature vectors is associated to the order of expansion and the number of scales used. Here, we compute four orders with four scales. Thus, 32-length feature vectors are obtained. Figure \ref{fig:FeatureVectors}(c) shows a comparison between feature vector of a complete texture and the corresponding representation using texels.

\begin{figure}[tbp!]
\centering
\subfigure[]{\includegraphics[width=0.19\textwidth]{D11_div_G} \hspace{2pt}}
\subfigure[]{
\begin{tikzpicture}
%\begin{axis}[legend entries = {Texel-based, Complete-based}, legend columns = -1,  legend style={at={(1.03,-0.5)},anchor=south}]
\begin{axis}[]
\addplot [mark=*, mark size=0.1,draw=blue, smooth,mark options={color = blue}] table [x index={0}, y index = {1}] {\mydata};
\addplot [mark=*, mark size=0.1,draw=red, smooth,mark options={color = red}] table [x index={0}, y index = {1}] {\mydatb};
\end{axis}
\end{tikzpicture}}
\subfigure[]{
\begin{tikzpicture} 
\begin{axis}[] 
\addplot [mark=*, mark size=0.7,draw=blue, smooth,mark options={color = blue}] coordinates {(1,0.082)	(2,0.044) (3,0.003) (4,0.040) (5,-0.020) (6,0.025) (7,-0.001) (8,0.021) (9,0.080) (10,0.045) (11,0.003) (12,0.043) (13,-0.026)(14,0.025) (15,-0.002) (16,0.023) (17,0.072) (18,0.045) (19,0.002) (20,0.047) (21,-0.032) (22,0.026) (23,-0.002) (24,0.025) (25,0.060) (26,0.039) (27,0.004) (28,0.046) (29,-0.034) (30,0.031) (31,-0.002) (32,0.029)}; 
\addplot [mark=*, mark size=0.7,draw=red, smooth,mark options={color = red}] coordinates {(1,0.074) (2,0.039) (3,0.002) (4,0.038) (5,-0.019) (6,0.023) (7,-0.001) (8,0.020)(9,0.071) (10,0.038) (11,0.001) (12,0.039) (13,-0.025) (14,0.024) (15,-0.001) (16,0.022) (17,0.066) (18,0.037) (19,0.000) (20,0.042) (21,-0.028) (22,0.024) (23,-0.001) (24,0.023) (25,0.057) (26,0.034)	(27,0.000) (28,0.040) (29,-0.029) (30,0.028) (31,0.000) (32,0.027)};
\end{axis} 
\end{tikzpicture}}
\caption{ (a) Complete image texture D11 from Brodatz, divided by a grid of texels. (b) Tchebichef feature vector corresponding to the complete texture and the first texel  and (c) feature vectors corresponding to steered Hermite coefficients.}
\label{fig:FeatureVectors}
\end{figure}

Finally, we performed texture classification using DTMs (Eq. \ref{eq:sum4}) and SHT (Eq. \ref{eq:Union}) using three fixed-size windows and texels. Since DTMs feature vectors are not the same size, we include a interpolation stage to set all the feature vectors to 126 bins. A basic $k$-NN classifier with $k=1$ and city-block distance were used. A summary of classification results is shown in Figure \ref{fig:ConfusionMatrix}. The correct classification rate (CCR) was also assessed. CCR is a ratio between the number of texture patches correctly classified and the total number of patches, (see Table \ref{CCR}). 

\begin{table*}[bp!]
\rowcolors{3}{AliceBlue}{}
\setlength{\tabcolsep}{0pt}
\renewcommand{\arraystretch}{1.2}
\caption{Texel sizes for training and testing data. Values at each column indicate the number of pixels per side of the texel window computed for the corresponding training and testing subimages. DX value indicates the number of the Brodatz texture.}
\label{TexelSizes}
\centering
{\small
\begin{tabular*}{0.4\textwidth}{@{}ccc@{}}
\toprule
\textbf{$\quad$Texture$\quad|$}  & \textbf{$\quad$Texel train$\quad|$} & \textbf{$\quad$Texel test$\quad$} \\ 
\midrule
 D1 & 59                  & 64                 \\ 
 D10 & 63                  & 64                 \\ 
 D101              & 30                  & 31                 \\ 
 D102              & 30                  & 29                 \\ 
 D105              & 34                  & 38                 \\ 
 D11               & 63                  & 63                 \\
 D110              & 53                  & 58                 \\ 
 D111              & 38                  & 46                 \\ 
 D112              & 50                  & 39                 \\ 
 D15               & 64                  & 58                 \\ 
 D16               & 24                  & 23                 \\ 
 D17               & 53                  & 53                 \\ 
 D18               & 46                  & 46                 \\ 
 D20               & 26                  & 26                 \\
D21               & 22                  & 22                 \\ 
 D28               & 23                  & 22                 \\ 
 D3                & 24                  & 25                 \\ 
\bottomrule
\end{tabular*}
}\hspace{10pt}
{\small\rowcolors{3}{AliceBlue}{}
\begin{tabular*}{0.4\textwidth}{@{}ccc@{}} 
\toprule
\textbf{$\quad$Texture$\quad|$}  & \textbf{$\quad$Texel train$\quad|$} & \textbf{$\quad$Texel test$\quad$} \\ 
\midrule
D34               & 33                  & 33                 \\ 
D4                & 61                  & 61                 \\ 
 D52               & 26                  & 26                 \\ 
 D53               & 31                  & 30                 \\ 
 D55               & 38                  & 40                 \\ 
 D56               & 64                  & 64                 \\ 
D6                & 30                  & 29                 \\ 
 D64               & 64                  & 61                 \\
 D65               & 54                  & 55                 \\ 
 D67               & 64                  & 64                 \\ 
 D74               & 45                  & 57                 \\ 
D76               & 24                  & 24                 \\ 
 D77               & 24                  & 24                 \\ 
 D8                & 64                  & 63                 \\ 
 D81               & 30                  & 30                 \\ 
 D86               & 56                  & 56                 \\ 
 D87               & 50                  & 50\\
\bottomrule
\end{tabular*}
}
\end{table*}





\begin{figure}[tbp!]
\centering	
\subfigure[]{\label{fig:DTM_128}\includegraphics[width=0.24\textwidth]{DTM_128}}
\subfigure[]{\label{fig:DTM_64}\includegraphics[width=0.24\textwidth]{DTM_64}}	
\subfigure[]{\label{fig:DTM_32}\includegraphics[width=0.24\textwidth]{DTM_32}}
\subfigure[]{\label{fig:DTM_Texel}\includegraphics[width=0.24\textwidth]{DTM_Texel}}\\
\subfigure[]{\label{fig:Hermite_128}\includegraphics[width=0.24\textwidth]{Hermite_128}}
\subfigure[]{\label{fig:Hermite_64}\includegraphics[width=0.24\textwidth]{Hermite_64}}
\subfigure[]{\label{fig:Hermite_32}\includegraphics[width=0.24\textwidth]{Hermite_32}}
\subfigure[]{\label{fig:Hermite_Texel}\includegraphics[width=0.24\textwidth]{Hermite_Texel}}
\caption{First row displays confusion matrices of (a) 128x128 pixeles, (b) 64x64, (c) 32x32 and (d) texel sizes for DTMs. Second row display confusion matrices of (e) 128x128 pixeles, (f) 64x64, (g)32x32 and (h)texel sizes for SHT.}
\label{fig:ConfusionMatrix}
\end{figure}

\begin{table*}[bp!]
\rowcolors{3}{AliceBlue}{}
\setlength{\tabcolsep}{1pt}
\renewcommand{\arraystretch}{1.2}
\caption{Correct classification rate for DTM's and SHT. The columns indicates the size of window.}
\label{CCR}
\centering
{\small
\begin{tabular*}{0.43\textwidth}{@{}ccccc@{}}
\toprule
\textbf{$\quad$Basis$\quad|$}  & \textbf{$\quad$Texel$\quad|$} & \textbf{$\quad$128$\quad|$} & \textbf{$\quad$64$\quad|$} & \textbf{$\quad$32$\quad$} \\
\midrule
 DTM & 88.0 & 93.0  & 83.4 & 67.3 \\ 
SHT     & 91.3 & 96.3  & 94.5 & 83.3 \\ 
\bottomrule
\end{tabular*}
}
\end{table*}

%---------------------------------------------------------------- 

\section{Conclusions}
\label{sec:conclusion}

In this paper, we proposed a novel method to compute texels that are later used to characterize a full image texture. This methodology incorporates the use of orthogonal bases such as Tchebichef and Hermite polynomials for classification tasks. We prove that the usage of the correlation metric in GLCM is able to capture the main texture oscillation. Furthermore, a validation process was carried out by comparing three methodologies, and we can show that the CA analysis has shown more tolerance to degradations. We showed there is a close relationship between representation of the complete image and the scheme based on texels, so that, the orthogonal projections of texels are similar to the obtained with full texture images and preserve the sufficient information for classification. 

Texture classification using fixed-size windows has shown good results, however in many cases, it captures redundant information. Also the number of computational operations are related to the size of the window of analysis. Therefore, it is possible to represent textures using a minimum amount of information with actually small error rates. This methodology may be applied in other scenarios.

%---------------------------------------------------------------- 

\section{Acknowledgments}
This publication has been sponsored by the grant UNAM PAPIIT IG100814. Also was partially supported by the European social fund within the project CZ.1.07/2.3.00/30.0034. E. Carbajal-Degante, R. Nava and J. Olveres thank Consejo Nacional de Ciencia y Tecnolog\'ia (CONACYT). J. Kybic was supported by the Czech Science Foundation project 14-21421S.

%---------------------------------------------------------------- 
\bibliography{report}   
\bibliographystyle{spiebib}   

\end{document} 
