\section{Method}

\subsection{Problem definition}

We assume we are given two segmented images, 
$f: \real^d \to {\sL}^f$ and 
$g: \real^d \to {\sL}^g$,
and a~bounded region of interest $\Omega\subset\real^d$,
where the dimension $d$ is usually $2\sim 3$, and $\sL^f$,$\sL^g$ are small
sets of class labels (see Fig.~\ref{fig_images} for an example). 
Any of the multitude of existing image segmentation algorithms can be
used; we provide some suggestions in Section~\ref{sec:segmentation}.

Let us choose a~similarity criterion $J:\bigl(\real^d\to {\sL}^f\bigr) \times \bigl(\real^d\to {\sL}^f\bigr) \to \real$ 
which can be expressed as a~sum of pixel contributions
\begin{equation}
J(f,g)=\int\limits_{\vcx \in \Omega} \varrho\bigl(f(\vcx),g(\vcx)\bigr)\,\rd\vcx
\label{eq:critsim}
\end{equation}
where $\vcx$ is the pixel coordinate (see
Section~\ref{sec:similarity} for the discussion of the pixel similarity term $\varrho$).

The final ingredient is a~family of geometrical transformations $\sT$
(see Section~\ref{sec:geomtrans}),
such that $T:\real^d\to\real^d$ for $T\in\sT$ is a~smooth transformation,
depending continuously on a~parameter vector 
$\vctheta\in \real^{\operatorname{dim} \vctheta}$. We shall write
$T_\vctheta$ when we need to emphasize this dependence. 

%${\mathscr L}$ does not in general make sense as the set is
%often not even ordered, we use nearest neighbor approximation
%g'(\vcx)&=\bigl(g \circ T\bigr) (\vcx) =  g(\vcy) \\
%\text{with}\quad y&=\operatorname{round} T(\vcx)\bigr)


A segmented image $g$ can be warped with a~transformation $T$, yielding
\begin{align}
g'(\vcx)&=\bigl(g \circ T\bigr) (\vcx) =  g(\vcy) \\
\text{with}\quad y&= T(\vcx)
\end{align}

The task of registering the segmented images $f$ and $g$ is then straightforwardly
defined as finding the best transformation $T\in \sT$, such
that the warped image $g' = g \circ T$ of the moving image
$g$ is as close as
possible to the reference image $f$ in the sense of the similarity
criterion. More formally,
\begin{align}
T^* &= \arg\min_{T\in\sT} J(T) \\
\text{with}\quad J(T) &= \int\limits_{\vcx \in \Omega}
\varrho\Bigl(f(\vcx),g \bigl(T(\vcx) \bigr) \Bigr) \rd \vcx \label{eq:critj}
\end{align}

\subsection{Criterion approximation}
\label{sec:critapprox}

Numerical evaluation of the criterion~\eqref{eq:critj} can be costly as it
needs to visit all pixels within the $\Omega$ region, of which there
can be easily $10^7\sim 10^9$ or more in todays high-resolution images.

To reduce the complexity, we
will make a~number of simplifying assumptions, namely that 
\begin{inparaenum}[(i)]
\item the optimal transformation $T^*$ is close to
  a~known transformation $T_0$\label{item:assi};
\item the transformation is locally close to affine;\label{item:affine} and
\item the segmented regions are sufficiently large. 
\end{inparaenum}
The last assumption is only needed to obtain a~good speed-up.
The assumption~(\ref{item:assi}) means that we  only need to evaluate
  $J$ for $T$ close to $T_0$, while the `close' transformations can be
defined by limiting the maximum displacement to $\delta$
\begin{equation}
\norm{T(\vcx)-T_0(\vcx)}<\delta \quad\text{for all $\vcx\in\Omega$}
\label{eq:delta}
\end{equation}
%The set of transformations $T$ satisfying~\eqref{eq:delta} will be denoted $\opB(T_0,\delta)$.

%The first assumption can be made more precise by requiring that
%\begin{equation}
%\max_{\vcx\in\Omega^f} \norm{T_\theta(\vcx)-T_0(\vcx)} \le \delta
%\end{equation}
%for some constant $\delta$. 
Observe that in~\eqref{eq:critj}, it is enough to sum over $\vcx$,
which are close to class boundaries, as elsewhere the classes do not
change and the contribution to the criterion $J$ is therefore constant
and does not change the position of the minimum. More formally, let us
define the $\varepsilon$-neighborhood $\partial_\varepsilon f$ of
class boundaries $\partial f$ in image $f$
\begin{align}
\partial_\varepsilon f &= \bigl\{ \vcx \in \real^d; \exists \vcy \in
\real^d,\\\notag
&\qquad\qquad\norm{\vcx-\vcy}\le\varepsilon \land
f(\vcx)\not=f(\vcy) \bigr\}
\label{eq:eneighb}\\
\partial f &= \bigl\{ \vcx \in \real^d; \forall \varepsilon>0, \exists \vcy \in
\real^d,\\
&\qquad\qquad\norm{\vcx-\vcy}\le\varepsilon \land
f(\vcx)\not=f(\vcy) \bigr\}\notag
\end{align}
We can then modify the criterion \eqref{eq:critj} by integrating only
within a~distance $\delta$ of the class boundaries in $g$ by replacing $\Omega$ with
\begin{equation}
\Omega_1 =  \Omega \cap \bigl(T_0^{-1} \circ \partial_\delta g\bigr) = 
\bigl\{ \vcx\in\Omega; T_0(\vcx)\in \partial_\delta g\bigr\}
\label{eq:omegap}
\end{equation}
It can be shown from \eqref{eq:eneighb} and \eqref{eq:delta} that 
$g\bigl(T(\vcx)\bigr)=g\bigl(T_0(\vcx)\bigr)$ for all $\vcx$ far from
the boundaries, $\vcx\not\in \partial_\delta g$. Therefore, for
transformations $T$ close to $T_0$ in the sense of \eqref{eq:delta}, 
there will be a~constant difference between the new criterion $J_1$
based on \eqref{eq:omegap}
and $J$ in \eqref{eq:critj} and the minimum location is thus unchanged. 

Evaluating \eqref{eq:omegap} is often not practical, as it requires
inverting $T_0$. Hence, we propose to replace $J_1$ by
$J_2$ obtained by replacing $\Omega_1$~\eqref{eq:omegap}  with
a~region based on the boundaries in $f$
\begin{equation}
\Omega_2 = \Omega \cap \partial_\gamma f = \bigl\{ \vcx\in\Omega; \vcx\in \partial_\gamma f\bigr\}
\label{eq:omegatwo}
\end{equation}
where the size of the neighborhood $\gamma$ in $f$ is chosen such that
$(\partial_\gamma f) \circ T_0 \supseteq \partial_\delta g$. Then $J_2=J+\text{const}$.
A~sufficient condition is
to choose $\gamma$ so that 
$\gamma\ge 2\delta K$, where $K$ is the Lipschitz constant of
$T_0^{-1}$, provided
that the class boundaries of $f$ and $g \circ T$
coincide for some $T$
satisfying~\eqref{eq:delta}. %\in\opB(T_0,\delta)$. 
In practice, exact match of
the boundaries is not required and thanks to the
smoothness of $T$, the
minima of $J_2$ and $J$ are sufficiently close 
even for $\gamma\approx \delta K$.
 An appropriate value for $\gamma$ is easy to
determine directly as it can be  interpreted as the maximum
expected displacement in the image $f$. So if the optimal $T$ is
expected to keep scale, $\gamma\approx\delta$ is a~good choice, with
$\delta$ given by~\eqref{eq:delta}.


%regions far from class boundaries the classes stay the
%same and contribution of these regions to the criterion $J$ does not change.
%Consequently, 

\subsection{Normal approximation}

We take advantage of the fact that the criterion is only influenced by
movement in the direction perpendicular to the class boundaries, as in
the direction parallel to the boundaries, the classes are
constant. Let us replace the $d$-dimensional integral~\eqref{eq:critj} over
$\Omega_2=\partial_{\gamma} f \cap \Omega$~\eqref{eq:omegatwo}
by an outer integral over the boundary $\partial^\Omega f =  \partial f \cap \Omega$ and an
inner integral in the normal direction
\begin{align}
J_3(f,g \circ T)&=\int\limits_{\vcz \in\partial^\Omega f} 
\int\nolimits_{-\gamma}^{\gamma}
\varrho\Bigl(f(\vcx),g\bigl(T(\vcx)\bigr) \Bigr) \rd h \rd \vcz\label{eq:critjthree}\\
\text{with}\quad \vcx&=\vcz+\vcn(\vcz) h\notag
\end{align}
where $\vcn(\vcz)$ is an oriented normal at point $\vcz$ of the
boundary $\partial f$, with $\norm{\vcn}=1$. Since the transformation between $\vcx$ and the
new coordinates $(\vcz,h)$ is unitary, the difference between $J_2$ and $J_3$ is only
due to the parts of $\partial_\gamma f$ truncated by $\Omega$ and
by the unsmooth part of the boundary $\partial f$. In practice the
approximation is very good. By definition, $f(\vcx)$
is determined by the side of the boundary where $\vcx$ falls, so we
write
\begin{equation}
f(\vcx)=f(\vcz+\vcn(\vcz) h)=\begin{cases}
l^+ & \text{if $h>0$} \\
l^- & \text{if $h<0$} \\
\end{cases}
\end{equation}
for $l^+,l^- \in \sL^f$, assuming that there are no other boundaries within the distance $\gamma$.

By assumption (\ref{item:affine}) in Section~\ref{sec:critapprox} we
linearize the transformation $T_0$ around point $\vcz$
\begin{align}
T_0(\vcx)&=T_0(\vcz)+\bigl(\nabla T_0(\vcz)\bigr) (\vcx-\vcz)=
\vcu+\vcm h\\
\text{with}\quad \vcm&=\bigl(\nabla T_0(\vcz)\bigr) \vcn(\vcz) 
\quad\text{and}\quad \vcu=T_0(\vcz)
\end{align}
and similarly for $T$. Supposing that $T_0$ makes the images approximately aligned, we expect
$g\circ T_0$ to depend predominantly on the shift $h$ along the normal
and not on the movement in the perpendicular direction. Hence we
hypothesize that locally there exists some scalar function $\tilde{g}(\vcu,h)$ such that
\begin{equation}
g(\vcy)=\tilde{g}\bigl(\vcu,\langle \vcy-\vcu, \tilde\vcm \rangle\bigr)
\end{equation}
where we have used $\tilde\vcm=\vcm/\norm{\vcm}^2$ for
later convenience.

Assuming further that $\nabla T(\vcz) \approx \nabla T_0(\vcz)$, \ie
that our guess $T_0$ is approximately correct with respect to
orientation, we get after some algebraic manipulations
\begin{align}
g\bigl(T(\vcx)\bigr)&=\tilde{g}\bigl(\vcu,\xi(\vcz)+h\bigr)\\
\text{with}\quad \xi(\vcz)&=\bigl\langle T(\vcz)-T_0(\vcz),\tilde\vcm \bigr\rangle
\end{align}
Consequently, the integral in~\eqref{eq:critjthree} can be
approximated as
\begin{align}
J_4(T)&=\int\limits_{\vcz \in \partial^\Omega f} 
D\bigl(\vcz,\xi(\vcz)\bigr) \rd \vcz\label{eq:critjfour}\\
\intertext{where the inner integral}
D\bigl(\vcz,\xi(\vcz)\bigr)&=
\int\nolimits_{-\gamma}^{\gamma}
\varrho\Bigl(f(\vcx),g\bigl(T(\vcx)\bigr) \Bigr) \rd h \label{eq:dinner}\\
&=\
\int\nolimits_{-\gamma}^0
\varrho\Bigl(l^{-},\tilde{g}\bigl(\vcu,\xi(\vcz)+h\bigr)\Bigr) \rd h\notag \\
&+\int\nolimits_0^{\gamma}
\varrho\Bigl(l^{+},\tilde{g}\bigl(\vcu,\xi(\vcz)+h\bigr)\Bigr) \rd h\notag
\end{align}
can be easily precomputed for various normal shifts $\xi(\vcz)$,
to accelerate the subsequent optimization.


\subsection{Discretization} 
\label{sec:discr}

The last approximation of the criterion~$J_4\approx J+\text{const}$
from~\eqref{eq:critjfour} can be evaluated numerically as follows.
We pick a~set of sparse keypoints $P=\{\vcp_1,\vcp_2\dots\vcp_{\abs{P}}\}$ on the
class boundaries, $\vcp_i \in \partial^\Omega f$. For each $\vcp_i$, we
find the normal direction $\vcn_i=\vcn(\vcp_i)$ and sample the class
labels along the normals for $h\in\{-\gamma,-\gamma+1,\dots,\gamma-1,\gamma\}$
\begin{equation}
\tilde{g}_i(h)=\tilde{g}(\vcp_i,h)=g\bigl(T_0(\vcp_i)+\vcm_i h\bigr)
\label{eq:samplenormal}
\end{equation}
with $\vcm_i=\bigl(\nabla T_0(\vcp_i)(\vcn_i)\bigr)$ and $\tilde\vcm_i=\vcm_i/\norm{\vcm_i}^2$.
Nearest neighbor interpolation is used to evaluate $g$ away from
integer coordinates. Locations outside of the image or the region of interest return the last
class encountered. 

The class boundary $\partial^\Omega f$ is partitioned into small and approximately flat pieces
$S_i$ such that $\vcp_i \in S_i \subset \partial^\Omega f$ and 
$\bigcup_i S_i = \partial^\Omega f$. Then the continuous
criterion~\eqref{eq:critjfour} can be approximated as a~sum
\begin{equation}
 J_4(T)\approx J_5(T)=\sum_{i=1}^{\abs{P}} \abs{S_i} \, D_i\bigl(\xi_i\bigr)
\label{eq:critjfive}
\end{equation}
where we have written $\xi_i=\xi(\vcp_i)=\bigl\langle T(\vcp_i) -
T_0(\vcp_i), \tilde\vcm_i \bigr\rangle$ and $D_i(\xi)=D(\vcp_i,\xi)$
to simplify the notation. The keypoint contributions $D_i$ are 
approximated by replacing the integral~\eqref{eq:dinner} with a~sum over $h$ with unit step
size, assuming the units are pixels and the scaling is not too extreme
\begin{align}
D_i(\xi)&=\sum_{h=-\gamma}^0 
\varrho\Bigl(l^{-},\tilde{g}_i\bigl(\vcu,\xi+h\bigr)\Bigr)\notag\\
&\quad+\sum_{h=0}^{\gamma} 
\varrho\Bigl(l^{+},\tilde{g}_i\bigl(\vcu,\xi+h\bigr)\Bigr)
\label{eq:di}
\end{align}
The values of $D_i(\xi)$ are
precalculated for $-\delta \le \xi \le
\delta$.
In the interest of notational simplicity, we now consider $\gamma$ and
$\delta$ to be integers. 
Note that while the naive
implementation has complexity $O(\delta\gamma)$, \ie quadratic if
$\gamma\approx\delta$, it is
possible to calculate all $2\delta+1$ values in linear time $O(\delta+\gamma)$
by precalculating the cumulative sums $Q^+, Q^-$ given by
\begin{equation}
Q^\pm(k)=\sum_{j=-\gamma-\delta}^k  \varrho\Bigl(l^\pm,\tilde{g}\bigl(\vcu,j\bigr)\Bigr)
\end{equation}
for $-\gamma-\delta \le k \le \gamma+\delta$ and combining them 
\begin{equation}
D_i(\xi)=
Q^-(\xi)-Q^-(\xi-\gamma)+Q^+(\xi+\gamma)-Q^+(\xi)
\end{equation}
In our implementation, we precalculate $Q^\pm$ for integer $k$, which
allows us to evaluate $D_i$ for integer $\xi$. For non-integer $\xi$,
the values are linearly interpolated.

The above procedure needs a~preprocessing time proportional to
$O\bigl(\abs{P} (\delta+\gamma)\bigr)$ per image. Then, evaluating $J_5$ is very
fast, the most costly operations being to evaluate $T$ in $\abs{P}$ points.

\subsection{Optimization}

Finding the transformation $T^*=T_{\theta^*}$ that minimizes the criterion $J_5$
\begin{equation}
\theta^* = \arg\min_\theta J_5(T_\theta)
\end{equation}
is solved by standard multidimensional minimization methods such as
Powell's BOBYQUA~\cite{Powell09}, taking advantage of the fast
evaluation of $J(T_\theta)$.
Derivatives can be calculated by the chain rule
\begin{equation}
\frac{\partial J_5}{\partial \theta_j} = \sum_i \abs{S_i} \frac{\partial
  D_i}{\partial \xi_i} \tilde\vcm_i^T\frac{\partial
  T(\vcp_i)}{\partial \theta_j}
\label{eq:optimjfive}
\end{equation}
to allow higher-order optimization methods such as BFGS~\cite{liu-nocedal:89}, which are usually faster. The derivative $\partial
D_i(\xi)/\partial \xi$ is calculated by the first-order difference formula from precalculated
values of $D_i(\xi)$ on integers.


A barrier function is introduced to keep all displacements
$\xi_i$ within the assumed range by replacing $D_i$
in~\eqref{eq:critjfive} with
\begin{equation}
\tilde{D}_i(\xi)=D_i(\xi)+\beta \max\bigl(0,\abs{\xi}-\gamma\bigr)^2
\end{equation}
with $\beta=\max_{i,\xi} D_i(\xi)$. The idea is that if
a~displacement larger than the precalculated range is needed, it will
be found by an iterative process as described below.


\subsection{Iterative improvement and multiresolution}
\label{sec:iterative}

In the absence of a~priori information, we set the initial
transformation $T_0$ to identity. If the true transformation is too
far from $T_0$, the minimum of the $J_5$ criterion might be a~poor
approximation for the minimum of the original criterion $J$ we
actually want to minimize. Our solution is to iterate the process if
we suspect that the approximation is wrong. This is detected if
for the optimal $T^*$ found, a~fraction larger than $\kappa_1$ of the displacements
$T^*(\vcp_i)-T_0(\vcp_i)$ are either too close to
the last sampled point on the normal ($\abs{\xi_i}>\gamma-\kappa_2$), or if it is too far from the
normal ($\norm{T^*(\vcp_i)-T_0(\vcp_i)}>\kappa_3 \abs{\xi_i}$). The
constants are set to $\kappa_1=0.2$, $\kappa_2=0.1\gamma$,
$\kappa_3=3$ based on preliminary testing. The algorithm is as follows:

\begin{enumerate}
\item Given $T_0$, sample classes $\tilde{g}_i(\xi)$ along
  normals~\eqref{eq:samplenormal}.
\item Precalculate $D_i(\xi)$~\eqref{eq:di}
\item Find $T^*$ minimizing $J_5$~\eqref{eq:optimjfive}
\item If the approximation is wrong, $T_0\leftarrow T^*$ and repeat
  from the beginning,
  otherwise return $T^*$.
\end{enumerate}

%\subsection{Multiresolution}

To increase speed, robustness and capture range, multiresolution with
a~dyadic scale is
used, with a few particularities due to our formulation. First, when downsampling
the segmented images, we use majority voting. Second, the number of
keypoints is reduced by taking every second one as long as there is
enough left. The maximum displacement $\gamma$ stays constant when
measured in pixels, but
as the image size is reduced on coarser scales, the capture range 
is actually larger on the coarse scale and is progressively reduced.


\subsection{Similarity criteria} 
\label{sec:similarity}

The similarity criterion is defined by the pixelwise penalty
$\varrho(l_f,l_g)$, where $l_f\in\sL^f$ and $l_g\in\sL^g$ are the classes observed in
corresponding locations of the images $f$ and $g$. If the same classes
correspond, a~suitable strategy is to penalize the differences
\begin{equation}
\varrho(l_f,l_g)=\llbracket l_f \not= l_g \rrbracket
\end{equation}
If the relationship between the classes in the two images is not
known, for example because unsupervised segmentation has been used, we use
negative mutual information~\cite{pluim2003} on labels (MIL)~\cite{Kybic-ISBI2014}.
\begin{equation}
\varrho(l_f,l_g)=-\log  \frac {  P_{l_f l_g} }{P_{l_f} P_{l_g}}
\end{equation}
with $P_{l_f}=\sum_{l_g} P_{l_f l_g}$, $P_{l_g}=\sum_{l_f}
P_{l_f,l_g}$. The probability $P_{l_f,l_g}$
is calculated from the joint histogram of $f$ and $g \circ T_0$.
In the iterative registration procedure (Section~\ref{sec:iterative}),
the values of $\varrho(l_f,l_g)$ for all combinations of $l_f$, $l_g$
are calculated and stored before calculating the contributions
$D_i$~in Step~2, making the evaluation very fast. Not allowing $\varrho$ 
to change in each iteration of Step~3 usually has negligible effect on
the result, since the changes of $\varrho$ are small.



\subsection{Segmentation and geometrical transformation}
\label{sec:segmentation}

The method we are presenting works with segmented images. The
segmentation algorithm should be ideally tuned to the images at hand,
to well identify key structures which are then registered. As
a~fallback, the following unsupervised strategy often works reasonably
well and is used for the examples in Section~\ref{sec:exp} except one:
\begin{enumerate}
\item Find SLIC~\cite{achanta12} superpixels. 
\item For each superpixel, calculate suitable descriptors such as the
  mean color, intensity, or texture features.
\item Cluster the descriptor vectors by the $k$-means algorithm to find $3\sim
  5$ classes.
\end{enumerate}
If needed, the segmentation is regularized spatially using GraphCut~\cite{Boykov2001}.

Given a~segmentation, we need to find the keypoints $\vcp_i$
(Section~\ref{sec:discr}). If the superpixels
are known, for all pairs of neighboring superpixels
which were assigned to different classes, we identify all pixels on
the boundary between those superpixels. If there are too few, they are
discarded. Otherwise, a~keypoint is created at the center of gravity
of the boundary pixels. A~covariance matrix is calculated and the normal is set to the eigenvector 
corresponding to the smallest eigenvalue (Fig.~\ref{fig_kpts}).


If superpixels are not used, we use a~greedy,
non-maxima-suppression-like approach. First, we identify all class
boundary pixels in the image. Second, we randomly pick one of the
boundary pixels and make it a~keypoint. Third, we go through the
neighboring boundary pixels, discarding and marking as already visited
all that are closer than the desired distance between keypoints $q$
(typically around $10\sim 30$ pixels). 
As soon as a~boundary point at a~distance $q$ or larger is seen, it becomes a~new keypoint and the procedure is iterated. The points closer
than $q$ are used to calculate the center and the normal as described above.

\subsection{Geometric transformation}
\label{sec:geomtrans}

In this work, we use a~rigid transformation in 2D and 3D,
parameterized by 1 or 3 angles, respectively, and a~translation, \eg
\begin{equation}
T(\vcx)=\begin{bmatrix}
\cos \phi & \sin \phi\\
-\sin \phi & \cos \phi
\end{bmatrix}
\vcx + 
\begin{bmatrix} t_x \\ t_y 
\end{bmatrix}
\end{equation}
with $\theta=\bigl(\phi,t_x,t_y \bigr)$ and similarly for
3D. Derivatives with respect to  $\theta$ %and $\vcx$ 
needed for the optimization
can be calculated analytically.  Any linear transformation can be used, for example the affine
transformation or B-splines~\cite{Kybic-ieeeTIP2003}.


%%% Local Variables:
%%% TeX-master: "bic2015.tex"
%%% End:
 
