\section{Introduction}

Image registration~\cite{zitova2003} is one of the basic tasks in biomedical image
processing and is part of most biomedical image processing
pipelines. In this work we shall address the task of registering the
images fast. This remains a~challenge as processor speeds are
stagnating while the image resolution keeps increasing. Fast
registration is important for example for interactive and large
throughput applications. Currently used methods typically take tens of
seconds for registering medium size images up to several hours for
larger 3D images. This is prohibitive for many applications. Hence the
motivation to find faster methods. 

In computer vision, registration is mostly approached by detecting
feature points in both images, matching them, and fitting a~motion
model~\cite{lowe04}. This has a~potential of being fast as long as the expected
motion model is simple but can only rarely be applied to medical
imaging problems where reliable feature points are hard to find.

Instead, our approach for image registration acceleration is based on three observations:
First, it is known that image registration is indeed driven
mainly by the edges and the similarity criterion can be well approximated from points of high
gradient~\cite{sabuncu04}.  Second, when matching images from different physical
subjects, even if two regions are supposed to match, because they correspond \eg to the same organ of two different
subjects, the internal structure of this organ might be so variable
that there are no geometrically corresponding structures in the region
interiors of the two subjects. Another example is matching two
neighboring histological slices (Fig.~\ref{fig_images}), each containing physically different
cells, so there is no correspondence between the small scale details.
The third observation comes from the so-called `apperture problem' which implies that
locally, we can only distinguish motion in the direction normal to the
boundary between homogeneous regions.

We therefore propose to simplify the input images as much
as possible by segmenting them into a~set of regions which may
correspond \eg to different tissue types (Fig.~\ref{fig_images}). Furthermore, we ignore the
interior of the regions and concentrate on their boundaries. Finally,
we sample the segmentation labels in the moving image only on short
normal 1D line segments (Fig.~\ref{fig_kpts}). As a result, the amount of data the algorithm
needs to consider in each iteration is reduced from $10^6\sim 10^8$ pixels for the classical
formulation using all pixels to about $10^3\sim 10^4$ pixels in
the proposed method, with the corresponding potential for acceleration.

As an additional benefit of working with segmentations, we get `for
free' the possibility of  registering images with very different
appearance, \eg coming from different modalities~\cite{Kybic-ISBI2014}.



\subsection{Other related work}

We are not the first to address the task of registration of segmented
images. However, most authors have worked on the special case of alignment of binary images, by moment
matching~\cite{Domokos2012} or descriptor
matching~\cite{GoncalvesGC11}, which is fast but possibly
inaccurate. On the other hand, there are also  registration strategies
based on level-sets~\cite{Droske05}  and distance function
matching~\cite{Kozinska97}, which are accurate but slow.

It is possible to register multiple-class segmented images using the
demons algorithm~\cite{Thirion1998}, which, similarly to us, samples points on the class
boundaries. The idea of normal sampling comes from
active contour registration~\cite{Cootes01}.  However, in both cases
it is attempted to estimate the necessary movement directly, without
explicitely approximating and minimizing the criterion function.

There is a~number of methods combining segmentation and registration but also mostly limited to
the two-class problem. They propagate a~reference
segmentation~\cite{Bollenbeck2010}, 
segment objects by thresholding~\cite{Arganda-Carreras2004,Ma2008},
by level sets~\cite{Arganda-Carreras2010,Yezzi2003,wang05}, or
GraphCut~\cite{Mahapatra10}, or alternate the segmentation and
registration~\cite{Kybic-ISBI2014,Pohl06a}. In many cases, the fast
registration method we describe here could be easily plugged into some
of those existing segmentation-registration framework, in order to
increase their speed.


Finally, fast registration methods have been succesfully developed
casting the problem into the discrete optimization
setting~\cite{Glocker08,Heinrich11} and using
GPUs~\cite{ShamoninBLSKS14}. Both sources of acceleration could be
leveraged in our formulation, too.



%%% Local Variables:
%%% TeX-master: "bic2015.tex"
%%% End:
