%-------------------------------------------------------------------------
% Latex para Ya!
% DEADLINE: Jun 30, 2015 
% Autores: ***
% T�tulo: Classification of Tumor Epithelium and Stroma in Colorectal Cancer based on Discrete Tchebichef Moments
%-------------------------------------------------------------------------

\documentclass[]{llncs}

\usepackage[english]{babel}
\selectlanguage{english}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathptmx} 

\usepackage{marvosym}

\usepackage[table,svgnames]{xcolor}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{colortbl}
\usepackage{tabularx}

\usepackage{subfigure}
\usepackage[]{graphicx}
\graphicspath{{resources/}}

\usepackage{tikz}
\usetikzlibrary{arrows,positioning}

\usepackage{standalone}
\usepackage{balance}

\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

%-------------------------------------------------------------------------
\begin{document}

\mainmatter

\title{Classification of Tumor Epithelium and Stroma in Colorectal Cancer based on Discrete Tchebichef Moments}%

\author{Rodrigo Nava \inst{1(\mbox{\Letter})}, Germ\'an Gonz\'alez \inst{2}, Jan Kybic\inst{1}, \and Boris Escalante-Ram\'irez \inst{2}} %

\institute{Faculty of Electrical Engineering, Czech Technical University in Prague, Czech Republic\\ \texttt{uriel.nava@gmail.com}\\ \and Facultad de Ingenier\'ia, Universidad Nacional Aut\'onoma de M\'exico, Mexico City}%

\maketitle

%-------------------------------------------------------------------------
\begin{abstract}
Colorectal cancer is a major cause of mortality. As the disease progresses, adenomas and their surrounding tissue are modified. Therefore, a large number of samples from the epithelial cell layer and stroma must be collected and analyzed manually to estimate the potential evolution and stage of the disease. In this study, we propose a novel method for automatic classification of tumor epithelium and stroma in digitized tissue microarrays. To this end, we use discrete Tchebichef moments (DTMs) to characterize tumors based on their textural information. DTMs are able to capture image features in a non-redundant way providing a unique description. A support vector machine was trained to classify a dataset composed of 1376 tissue microarrays from 643 patients with colorectal cancer. The proposal achieved 97.62\% of sensitivity and 95\% of specificity showing the effectiveness of the methodology.

\keywords{Colorectal cancer, Tchebichef moments, Tissue microarray, Tumor classification, Support vector machine}

\end{abstract}


%-------------------------------------------------------------------------
\section{Introduction}
\label{sec:introduction}

Colorectal cancer (CRC) is the third most common type of cancer worldwide with more than 1.4 million cases registered in 2012 \cite{FERLAY2014}. As population aging continues growing more people are susceptible to CRC: around 70\% of cancer mortality occurs among adults over 65 years \cite{HAYAT2009}. Furthermore, almost half of the population will develop at least one benign intestinal tumor during its lifetime \cite{JEMAL2011}. In most cases, CRC begins as a benign polyp or adenoma, which is characterized by accumulation of cells at the epithelial layer of the gastrointestinal track. A small fraction of polyps evolves through accumulation of genetic alterations yielding carcinomas. Such a sequence is called adenoma-carcinoma sequence (ACS) \cite{NICHOLSON2014}. 

Cancer progression through lymphatic or blood vessels (metastasis) to the liver and lungs is the principal cause of death and occurs in up to 25\% of patients \cite{CONTI2011}. In contrast to ACS, colorectal metastasis is not strongly associated with alterations in any genes but with the healthy cells that surround the tumors. Such cells, called stroma, are usually composed of connective tissue. They are essential for the maintenance of both normal epithelial tissue and their malignant counterpart. Oncogenic changes in the epithelial tissue modify the stromal host compartment, which is responsible for establishing and enabling a supportive environment and eventually promotes growth and metastasis. Hence, stroma plays a fundamental role in allowing development and progression of the disease \cite{CALON2015}, \cite{CONTI2011}, \cite{ISELLA2015}.

Tissue microarrays (TMAs) are the gold standard for determining and monitoring the prevalence of alterations associated with colorectal carcinogenesis \cite{SIMON2003}. This procedure collects small histological sections from unique tissues or tumors and places them in an array to form a single paraffin block, (see Fig. \ref{fig:figure_1}). Typical TMAs may contain up to 1000 spots that are used for simultaneous interpretation. Hence, the large amount of information is the main drawback of the manual assessment and the motivation of this study. In addition, the identification of regions of interest depends on visual evaluation of histology slide images by pathologists, which introduces a bias.   

\begin{figure}[!tp]
    \centering
    \subfigure{\includegraphics[width=0.13\textwidth]{epi_0_2}}
    \subfigure{\includegraphics[width=0.13\textwidth]{epi_1_2}}
    \subfigure{\includegraphics[width=0.13\textwidth]{epi_2_2}}
    \subfigure{\includegraphics[width=0.13\textwidth]{epi_3_2}}
    \subfigure{\includegraphics[width=0.13\textwidth]{epi_4_2}} \\ 
    \subfigure{\includegraphics[width=0.13\textwidth]{stro_0_2}}
    \subfigure{\includegraphics[width=0.13\textwidth]{stro_1_2}}
    \subfigure{\includegraphics[width=0.13\textwidth]{stro_2_2}}
    \subfigure{\includegraphics[width=0.13\textwidth]{stro_3_2}}
    \subfigure{\includegraphics[width=0.13\textwidth]{stro_4_2}}
    \caption{Samples of colorectal cancer in digitized tissue microarrays (only red channel) from the database used in \cite{LINDER2012}. First row shows pure tumor epithelium and second row shows tumor stroma extracted from a paraffin block.}
    \label{fig:figure_1}
\end{figure}

Texture analysis has been used in segmentation of epithelial tissue in digital histology previously. For instance, Wang \cite{WANG2011} proposed a Bayesian estimation method for classification of tumoral cells in tissue microarrays of lung carcinoma. Tumor and stroma from prostate tissue microarrays were classified in \cite{JANOWCZYK2013,KWAK2011,DOYLE2012}. Foran et al. \cite{FORAN2011} developed a software platform to compare expression patterns in tissue microarrays using texton-based descriptors and intensity histograms. To the best of our knowledge, automated analysis of CRC in tissue microarrays is relatively new. Linder et al. \cite{LINDER2012} used a methodology based on local binary patterns (LBPs) \cite{OJALA2002} and contrast information called (LBP/C) to classify tumor epithelium and stroma. Here, we use the same dataset and propose a novel descriptor based on discrete Tchebichef polynomials. 

Next, we present a detailed description of the methodology. A comparison between our proposal and LBPs was also performed using $k$-NN and support vector machine (SVM) as classifiers.

\begin{figure}[!tbp]
\hspace*{\fill}
  \subfigure[]{\label{fig:figure_2a}\includegraphics[width=0.45\textwidth]{figure_1}}\hfill
  \subfigure[]{\label{fig:figure_2b}\includegraphics[width=0.45\textwidth]{figure_2}}
\hspace*{\fill}
\caption{Set of scaled Tchebichef kernels. \textbf{(a)} 1-D discrete Tchebichef polynomials of order $s$ from $0$ to $5$. \textbf{(b)} Ensemble of 2-D discrete Tchebichef polynomials. The magnitude of the moment of order $s$ is calculated by summing of the correlation indexes, $p,q$ so that $s = p+q$. Graphically, the sum is carried out diagonally.}
\label{fig:figure_2} 
\end{figure}

%-------------------------------------------------------------------------
\section{Material and methods}
\label{sec:mat_and_met}

We propose a methodology composed of three stages. First, for each image, feature extraction is performed on overlapped sliding windows using discrete Tchebichef polynomials. Then, all the local Tchebichef vectors from a single image are grouped and characterized by statistic moments in order to build a single vector of 234-bins length that can be viewed as the texture signature. Finally, a SVM is trained using a subset of 656 samples, whereas the performance of the proposal is assessed on an independent set of 720 tissue microarray samples.

%-------------------------------------------------------------------------
\subsection{Dataset}
\label{sec:image_database}

We used the dataset provided and described in detail in \cite{LINDER2012}, which consists of 1376 samples of tissue microarray of tumor epithelium and stroma from 643 patients with CRC annotated by expert pathologists, (see Fig. \ref{fig:figure_1}). The samples were divided into two parts. The training subset is composed of 656 images: 400 samples representing tumor epithelium and 256 representing tumor stroma. A separate subset, consists of 425 images of tumor epithelium and 295 images that represent tumor stroma, was used as validation set. The dataset does not contain private information of patients.

Prior to extract Tchebichef feature vectors, the tissue samples were scaled by a 0.5 factor, the mean was subtracted, and only the red channel was used. Blue and green channels were discarded because they do not have relevant information.    

%-------------------------------------------------------------------------
\subsection{Discrete Tchebichef Moments}
\label{sec:tchebi-moments}

Generally speaking, moments are scalar quantities that characterize a function of interest. They are computed as projections between the function $f(x,y)$ and a polynomial basis $r_{pq}(x,y)$ within the region $\Omega: T_{pq} =\mathop{\int\!\!\!\int}_{\Omega} r_{pq}(x,y)f(x,y)\,dxdy$ where $p$ and $q$ are non-negative integers and $s = p + q$ represents the order of the moment. Therefore, $T_{pq}$ measures the correlation between the function $f(x,y)$ and the corresponding polynomial $r_{pq}(x,y)$ \cite{FLUSSER2009}. 

Discrete Tchebichef moments (DTMs) were originally proposed by Mukundan et al. \cite{MUKUNDAN2001} to overcome limitations of conventional orthogonal moments such as Zernike and Legrendre. DTMs are based on a normalized version of discrete Tchebichef polynomials scaled by a factor that depends on the size of the image $N$, (see Fig. \ref{fig:figure_2a}). 

The scaled discrete Tchebichef polynomials, $\widehat{t}_{p}$, can be generated using the following recurrent relation:
\begin{equation}
    \begin{aligned}
    \widehat{t}_{0}\left(x\right) &= \frac{1}{\sqrt{N}},\, \\
    \widehat{t}_{1}\left(x\right) &= \left(2x+1-N\right)\sqrt{\frac{3}{N\left(N^2-1\right)}},\, \mbox{ and}\\
    \widehat{t}_{p}\left(x\right) & = K_1x\widehat{t}_{p-1}\left(x\right) + K_2\widehat{t}_{p-1}\left(x\right) + K_3\widehat{t}_{p-2}\left(x\right)\\
    \end{aligned}\,
\label{eq:equation_1}
\end{equation}
with $x = 0,1,\ldots,N-1$. 

$K_1 = \frac{2}{p}\sqrt{\frac{4p^2-1}{N^2-p^2}}$, $K_2 = \frac{1-N}{p}\sqrt{\frac{4p^2-1}{N^2-p^2}}$, and $K_3 = \frac{p-1}{p}\sqrt{\frac{2p+1}{2p-3}}\sqrt{\frac{N^2-(p-1)^2}{N^2-p^2}}$ are the coefficients that ensure stability in case of large order polynomials \cite{MUKUNDAN2004}.

DTMs are computed by projecting a given image, $I(x,y)$, onto the basis of $\widehat{t}_{p}$. The moment $T_{pq}$ is calculated according the following formula:
\begin{equation}
	T_{pq} =\sum_{x=0}^{N-1}\sum_{y=0}^{N-1}\widehat{t}_{p}(x)\widehat{t}_{q}(y)I(x,y)\,
	\label{eq:equation_2}
\end{equation}
$T_{pq}$ quantifies the correlation between the image, $I(x,y)$, and the kernel $\widehat{t}_{p}(x)\widehat{t}_{q}(y)$, see Fig. \ref{fig:figure_2b}.  

One way to understand this relationship is that the greatest the magnitude of $T_{pq}$, the greatest the similarity between the given image and the polynomials $\widehat{t}_{p}$ that oscillate at similar rates to the image. Hence, it is possible to build a feature vector, $M(s)$, that captures similarities along X- and Y-axes as follows: 
\begin{equation}
    M\left(s\right) = \sum_{p+q=s}\left|T_{pq}\right|\,
	\label{eq:equation_3}
\end{equation}
with $s=1,\,\ldots,\,2N-2$.

$M(s)$ provides a unique description in the expanded Tchebichef space by capturing oscillating behavior of all textures that constitute the image.


\begin{figure}[!tp]
\hspace*{\fill}
  \subfigure[]{\label{fig:figure_3a}\includegraphics[width=0.33\textwidth]{tchebipromedio2}}\hfill
  \subfigure[]{\label{fig:figure_3b}\includegraphics[width=0.33\textwidth]{tchebivar2}}\hfill
 \subfigure[]{\label{fig:figure_3c}\includegraphics[width=0.33\textwidth]{tchebicon2}}
\hspace*{\fill}
\caption{DTM signatures. \textbf{(a)} Average, \textbf{(b)} standard deviation, and \textbf{(c)} contrast vectors obtained from tumor epithelium (black) and stroma (gray)tissues.}
\label{fig:figure_3} 
\end{figure}

%-------------------------------------------------------------------------
\subsection{Feature Extraction}
\label{sec:feature-extraction}

Feature extraction with DTMs was introduced by Marcos et al. \cite{MARCOS2013} on synthetic textures and used by Nava et al. \cite{NAVA2013} on emphysematous tissues. However, they compute a single vector using the whole image, which implies calculating high-order moments. According to \cite{MUKUNDAN2001}, large Tchebichef vectors may introduce an error due to stability in the oscillations. Here, we present a modification based on sliding windows by implementing the following steps:

The scaled images are processed using a window of $40\times40$ pixels; the accuracy was used as the performance measure to evaluate the optimal window's size. The window is moved from the upper-left corner to the lower-right corner by $20$ pixels per iteration, this means an overlap of $50\%$. 

The corresponding $M(s)$ vectors are calculated on each window position. After this process is conducted over all possible windows, we obtained a set of vectors $M_{i}(s)$ where $i$ indicates the window position. Since the images in the dataset are not the same size, then $i$ varies among images. The feature vector is build as follows:

$\forall i \in$ the given image:
\begin{equation}
    \begin{aligned}
    \bar{t} = [&\mu(M_i(1)),\,\sigma(M_i(1)),\,\beta(M_i(1)),\,\ldots,\, \\
    &\mu(M_i(2N-2)),\,\sigma(M_i(2N-2)),\,\beta(M_i(2N-2))]
    \end{aligned}\,
	\label{eq:equation_4}
\end{equation}
where $\mu$ and $\sigma$ are the mean and the standard deviation respectively. The operator $\beta$ is the defined as: $\beta(x) = \frac{\sigma(x)}{\kappa(x)^{1/2}}$ and $\kappa$ is the kurtosis.

Eq. (\ref{eq:equation_4}) represents a novel way to describe texture characteristics. Note that the moment of order $s=0$ is not used because it represents the mean value of the image. Furthermore, correlated coefficients between tumor epithelium and stroma are discarded by applying the \textit{p}-test. The test reflects statistically significant differences ($p<0.001$) between both groups, the features with a \textit{p}-value greater than the threshold \textit{p} are not included. The average Tchebichef signatures for both classes are shown in Fig. \ref{fig:figure_3}.

%-------------------------------------------------------------------------
\subsection{Classifier}
\label{sec:classifier}

A SVM and a \textit{k}-NN classifier were implemented to validate our proposal. The classifiers were trained using a subset of 656 images and a different set with 720 images was used in the validation stage. Both image datasets were processed in the same manner described previously and the accuracy was the measure to assess the performance of the proposal. 

%-------------------------------------------------------------------------
\section{Experimental results}
\label{sec:results}

Using a standard linear SVM classifier, our proposal labeled incorrectly 25 images out of 725, it means an accuracy of $96.53\%$. 15 images were wrongly classified as epithelium, whereas 10 samples were labeled as tumor stroma incorrectly. We computed the performance using \textit{k}-NN with \textit{k} = 11; the number of neighbors was not relevant in the classification performance. The best results are shown as confusion matrices in Fig. \ref{fig:figure_4}.

\begin{figure}[!bp]
\hspace*{\fill}
\subfigure[]{\includestandalone[width=0.27\textwidth]{resources/figure_4_a}}
\subfigure[]{\includestandalone[width=0.27\textwidth]{resources/figure_4_b}}
\subfigure[]{\includestandalone[width=0.27\textwidth]{resources/figure_4_c}}
\hspace*{\fill}
\caption{Final classification results. Epi. and Strom. stand for tumor epithelium and tumor stroma, respectively. \textbf{(a)} DTMs with SVM. \textbf{(b)} DTMs with \textit{k}-NN; and \textbf{(c)} LBPs with SVM.}
\label{fig:figure_4} 
\end{figure}

For comparison purposes, the LBP descriptor described in \cite{OJALA2002} was implemented. For each image, on every window position a feature vector was built by concatenating LBP$_{8,1}$ and LBP$_{16,2}$ histograms. Then,  all the LBP feature vectors from a single image were grouped and characterized by the first two statistic moments: mean and standard deviation. % Contrary to \cite{LINDER2012}, we do not include contrast information because we were interested only in the ability of LBPs to describe local variations. 
Furthermore, we include results reported in \cite{LINDER2012} where the same database was used. Linder et al. propose a combined LBP/C descriptor to characterized the tumor texture.

We also computed the ROC curve for our proposal, (see Fig. \ref{fig:figure_5}). The area under the ROC curve (AUC) is $0.9847$, such a value is pretty similar to the AUC reported by Linder et al. Finally, we calculated the F$_{1}$-Score $= 2*\frac{\mbox{Precision}*\mbox{Sensitivity}}{\mbox{Precision}+\mbox{Sensitivity}}$ and all the results are summarized in Table \ref{tab:table_1}. 

\begin{table}[!tp]
\caption{Comparison and classification results. All the data are expressed in (\%). Bold values represent the best results.}
\label{tab:table_1}
\centering
\begin{tabular}{@{}ccccc@{}}
\toprule
\textbf{$\quad$Method$\quad|$}  & \textbf{Precision$\quad|$} & \textbf{Sensitivity$\quad|$} & \textbf{Specificity$\quad|$} & \textbf{F$_1$-Score$\quad$} \\ 
\midrule
DTMs/SVM & \textbf{96.47} & 97.62 & \textbf{95} & 96.94 \\
DTMs/KNN & 94.12 & 94.79 & 91.61 & 94.45\\
LBPs/SVM & 90.35 & 89.1 & 85.81 & 89.72\\
LBPs/KNN & 91.53 & 83.48 & 85.83 & 87.32\\
LBP/C \cite{LINDER2012} & 95.53 & \textbf{99.02} & 93.87 & \textbf{97.19}\\
\bottomrule
\end{tabular}
\end{table}

\begin{figure}[!tp]
\begin{center}
\includegraphics[width=0.25\textwidth]{ROC}
\end{center}
\caption{ROC curve for DTMs/SVM proposal. The achieved AUC is $0.9847$.}
\label{fig:figure_5} 
\end{figure}


%-------------------------------------------------------------------------
\section{Conclusions}
\label{sec:conclusions}

We propose a novel method based on discrete Tchebichef Moments to classify tumor epithelium and stroma in a large database of colorectal cancer collected from TMAs. We have shown that Tchebichef moments possess the ability to describe textures by projecting the image of interest onto a polynomial basis where its sinusoidal-like behavior provides a suitable representation of all the textures that constitute the image. The sliding window approach improves the descriptor stability by discarding high-order moments and avoids the curse of dimensionality.

As in \cite{LINDER2012}, our proposal achieved an accuracy rate above $96\%$ (only 2 images below the LBP/C descriptor). Our method classifies better the epithelium tissue than LBP/C. Nevertheless, it is not possible to claim that there is a better performance because the difference between accuracies is only $0.28\%$. DTMs performance is about 6\% better than LBPs, which indicates that our proposal captures texture variations in a better way. Furthermore, our proposal does not use contrast information, therefore, it is not necessary to quantize the images to get the local variance. 

%Contrary to Linder \textit{et. al.}, we do not apply a mask to remove background, so that the algorithm is faster.

%-------------------------------------------------------------------------
\subsection*{Acknowledgments}
The authors extend their gratitude to Prof. Dr. Johan Lundin for providing the images. This publication was supported by the European social fund within the project CZ.1.07/2.3.00/30.0034 and UNAM PAPIIT grant IG100814. R. Nava thanks Consejo Nacional de Ciencia y Tecnolog\'ia (CONACYT). G. Gonz\'alez thanks CONACYT--263921 scholarship. J. Kybic was supported by the Czech Science Foundation project 14-21421S. 

%-------------------------------------------------------------------------
%\bibliographystyle{IEEEtran}
\bibliographystyle{splncs03}
%\balance
\bibliography{bibliography}

\end{document}
