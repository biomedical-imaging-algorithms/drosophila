\section{Method}

\begin{figure}[tp!]
\centering
\includegraphics[width=1.\textwidth,height=0.35\textheight,keepaspectratio]{figs/pipeline_schema.pdf}

\caption{\label{fig:pipeline}
A~flowchart of the complete pipeline for processing images of Drosophila imaginal
discs: (i) a~pixel-wise segmentation into $3$ classes (background, imaginal disk
and gene expression); (ii) registration of binary segmented imaginal discs onto
a~reference shape (disc prototype); (iii) atlas estimation from aligned binary gene expressions.
}
\end{figure}

% The complete pipeline for processing images of drosophila imaginal discs and an atlas extraction of gene expressions is following: (i) independent image segmentation into 3-classes (background, imaginal disk and gene expression); (ii) registration of binary segmented imaginal discs into one common shape (disc prototype); (iii) estimation an atlas over all aligned binary segmneted gene expressions.
\jbnew{We shall now describe the complete pipeline consists of preprocessing (segmentation and registration) and atlas estimation via Binary Pattern Dictionary Learning (BPDL), as illustrated in Fig.~\ref{fig:pipeline}.}



\subsection{Preprocessing}

Given a~set of images of imaginal discs (see Fig.~\ref{fig:imgs-segms}a)
containing both anatomical and gene expression information, preprocessing is
applied to obtain a~set of segmented and aligned gene expression images, which serves as input for
the subsequent binary dictionary learning (see Fig.~\ref{fig:pipeline}).
%The method requires binary images assuming that all discs are aligned together. For the  extraction we used unsupervised segmentation~\cite{Borovec2013a} and then we use elastic registration~\cite{Kybic2015} of segmented discs to align extracted gene expressions. 

\paragraph{\jbnew{Segmentation.}}
We first segment the input images into three clases (background, imaginal disc,
gene activation) by the following steps~\cite{Borovec2013a}: 
\begin{enumerate}[a)]
\item calculate SLIC superpixels~\cite{Achanta2012}; 
\item calculate superpixel \jbnew{colour} features --- mean, median, and variance; %;std and energy; 
\item estimating a~Gaussian Mixture Model (GMM) with one component per class,
%  identifying the classes;
\item calculate \jbnew{superpixel}-wise class probabilities based on the GMM;
\item apply Graph Cut~\cite{Boykov2001} to estimate a~spatially regularized segmentation; 
\item post-process, e.g. suppress very small regions and identifying the
  imaginal disc component.
\end{enumerate}

\paragraph{\jbnew{Registration.}}
For each of the four disc types, a~reference shape is calculated \jbnew{as the mean disc shape over all images}. Then all other images are registered to the reference shape. We use a~fast elastic registration
algorithm~\cite{Kybic2015}, which works directly on the segmented images,
transforming the disc shapes by aligning their contours, ignoring the
activations (see Fig.~\ref{fig:imgs-segms}b). 
The activations (Fig.~\ref{fig:imgs-segms}c) are then aligned using the recovered transformation.  


\subsection{Binary pattern dictionary learning --- problem definition}

Let us define the image pixels as $\Omega\subseteq\mathbb{Z}^{d}$, with $d=2$,
and the input binary image as $\x:\,\Omega\rightarrow\{0,1\}$.
Our task is to find an atlas $\y:\,\Omega\rightarrow\mathbb{L}$, with \jbnew{labels}
$\mathbb{L}=[0,\ldots,K]$, assigning to
each pixel either a~background (\jbnew{label $l=0$}), or one of the \jbnew{labels (patterns, set of equal labels)} $1,\dots,K$.
Each binary weight vector $\w:\,\mathbb{L}\rightarrow\{0,1\}$ yields an
image $\hat{\x}$ as 
a~union of the selected patterns \jbnew{in atlas $\y$}
%This vector $\w$ represents an appearance of label $\mathbb{L}$ from atlas $\y$ in image $\x$ such that 
\begin{equation}
\hat{\x} 
%= \cup{{l\in L,\,\w_{l}=1} \llbracket \y=l \rrbracket
= \sum_{l\in \mathbb{L}} \w_{l} \cdot \llbracket \y=l \rrbracket
\label{eq:reconst-x}
\end{equation}
\jbnew{where $\llbracket \cdot \rrbracket$ denotes the Iverson bracket.}
Note that this is a~special case of~\eqref{eq:lin-approx}, with all variables
binary. Note also, that in this representation, the patterns cannot overlap. 
The approximation error on one image $\x$ \jbnew{and its representation by $\y$ and $\w$} is the Hamming distance
\begin{equation}
F(\x,\y,\w)
 = \sum_{i\in\Omega} \llbracket \x_{i}\neq\hat{\x}_{i} \rrbracket
% = \sum_{i\in\Omega} |\x_{i} - \hat{\x}_{i}|
 = \sum_{i\in\Omega}  \left|\x_{i} - \sum_{l\in \mathbb{L}} \w_{l} \cdot \llbracket \y=l
\rrbracket\right|
% = \sum_{i\in\Omega} \llbracket \x_{i}\neq \sum_{l\in \mathbb{L}} \w_{l} \cdot \llbracket \y=l \rrbracket \rrbracket
\label{eq:unary}
\end{equation}
% where $\hat{\x}$ is the reconstructed image, $\y$ the atlas and $\w$ the weights~\eqref{eq:reconst-x}.


\begin{algorithm}[tb]
\begin{algorithmic}[1]
    \State initialise atlas $\y$
%    \For{$t=1\dots N$  \textbf{and} $ARS(\y^{*}_{t-1},\y^{*}_{t}) > \epsilon$}
    \While{not converged}
      \State update weights $\w\in \W$  % with $\sigma = 1$
      \State reinitialise empty patterns in $\y^{*}$
      \State update atlas $\y^{*}$ via Graph Cut
    \EndWhile
%    \State update weights $\w\in \W$ with $\sigma = 1$
\end{algorithmic}
\caption{\label{alg:APDL}General schema of BPDL algorithm.}
\end{algorithm}


To encourage spatial compactness of the estimated atlas, we shall penalize
differences between neighboring pixels $i,j$ in the atlas
\begin{equation}
H(\y) = \!\! \sum_{\substack{i,j\in\Omega,\, i\neq j,\\ d(i,j)=1}} \llbracket \y_{i}\neq\y_{j} \rrbracket
\label{eq:binary}
\end{equation}
where the Kronecker delta $\llbracket \y_{i}\neq\y_{j} \rrbracket$ \jbnew{for all combinations of $\y_{i, j}\in \mathbb{L}$} can be represented as
a square matrix with zeros on the main diagonal and ones otherwise

The optimal atlas and the associated weights are found by optimizing the mean
approximation error for all \jbnew{$N$} images
\begin{equation}
\y^{*},\w^{*} 
= \arg\min_{\y,\W}\, \frac{1}{N}\!\sum_n F(\x^{n},\y,\w^{n}) 
\,\,\, + \beta\cdot H(\y)
\label{eq:crit}
\end{equation}
where the matrix $\W$ contains all weights $\w^n$ \jbnew{for $n \in [0,\ldots,N]$}, and
$\beta$ is the spatial regularization coefficient.
\jbnew{Sufficiently large $\beta$ force the labelling to have} all patterns to be connected.




\subsection{BPDL --- Alternating minimization}

The criterion~\eqref{eq:crit} is minimized alternately with respect to atlas
$\y$ and weights $\w\in \W$, (see Algorithm~\ref{alg:APDL}).


\begin{figure}[tb]
\centering
%\begin{tabular}{c@{\quad}c@{\quad}c}
\begin{tabular}{ccc}
\includegraphics[width=0.32\columnwidth]{imgs_apdl/APDL_init_random} & 
\includegraphics[width=0.32\columnwidth]{imgs_apdl/APDL_init_mosaic2} & 
\includegraphics[width=0.32\columnwidth]{imgs_apdl/APDL_init_mosaic}\tabularnewline
(a) & (b) & (c)\tabularnewline
\end{tabular}

\caption{\label{fig:init}
Random atlas initialization with patch sizes $1$ pixel (a), $m / 2K$ pixels (b), and $m
/ K$  pixels (c), for $K=6$ and $m=64$ pixels being the image size.}
\end{figure}


\paragraph{\label{par:Init}Initialization.}

We initialize the atlas with randomly labeled patches on a~regular grid, with user-defined
sizes; see Fig.~\ref{fig:init} for examples.
%
% between $1$ and $m / K$ pixels where the $m$ is atlas edge size. 
%Three samples of such inicializations are presented in 
%In all cases we assume that the labeled patches/pixels should be spread uniformly.


\paragraph{\label{par:Update-W}Update weights $\W$.}

With the atlas $\y$ fixed, we estimate the weights $\w^{n}$ for each image
$\x^{n}$ independently. It turns out that $F(\x^{n},\y,\w)$ is minimized with
respect to $w_l$, if the majority of pixels in the pattern $l\in \y$  agree with the image.
We set
 \begin{align}
w_{l}&=\llbracket P(\x,\y,l)\geq\sigma \rrbracket
\qquad\text{where $\sigma = 1$}
\label{eq:overlap-thr}\\
\text{and}\quad
P(\x,\y,l) 
 & = \frac{\sum_{i\in\Omega,\y_i=l} \llbracket \x_{i}=1 \rrbracket}
{\sum_{i\in\Omega, \y_i=l} \llbracket \x_{i}\neq1 \rrbracket}
 = \frac{\bigl\Vert \llbracket \y=l\rrbracket \bigr\Vert}{\sum_{i\in\Omega,\y_i=l}\left(1-\x_{i}\right)} - 1
\label{eq:overlap}
\end{align}
We temporarily reduce $\sigma$ in the initial stage of the algorithm, otherwise
very few patterns might be selected.
%% \jknoteR{vysvetlit proc, souvislost s $F$} 
%% between observation $\x^{n}\in \X$ and particular label $l$ in the estimated atlas $l\in \y$ such that 
%% $\min F(\x,\y,\w) \Leftrightarrow \max P(\x,\y,\w)$.
%%  We threshold this overlap for each $w_{l}$ such that 

%% For the computing weights $\W$ we set the threshold  \jknoteR{Proc? Vysvetlit.} meaning the patterns is more populated by foreground then background. 

%% Although for noisy data of estimating less pattern that really appears in the data we can use softer threshold $\sigma = 1 / K$ (meaning at least some foreground is presented in the pattern) which overestimate all pattern appearance and in the final step used $\sigma = 1$.
% For each weight $\w_{l}$ we set the binary threshold as non-negative $P(\x,\y,\w_{l})\geq0$. 


\paragraph{\label{par:reinit-patterns}Reinitialize empty patterns.}

During the weight calculation step, some pattern may not have been used for any
image. This is wasteful, unless the reconstruction is already perfect, we can
always improve it by adding another pattern. We iterate the following procedure until all $K$ labels are used: 
\begin{enumerate}
\item find an image $\x^{n}$ with the largest unexplained
residual $\llbracket \x^{n} \wedge \neg \hat{\x}^{n}  \rrbracket$
% JK: ponechme |\x^{n} - \hat{\x}^{n}|, pokud je to to, co bylo implementovano.
%approximation error  $F(\x^s,\y,\w)$~\eqref{eq:unary}.
%s^{*}=\arg\max_{n}\left\Vert r(\x^{n},\y,\w^{n})\right\Vert
%$$ which maximally minimize the criterion~\eqref{eq:unary}
\item find the largest connected component $c$ of this residual and assign label $l \notin \y$;

%% image remains between input image an the reconstruction 
%%   $F^{*}(\x^s,\y,\w) =  |\x^{n} - \hat{\x}^{n}|$ 
%%   \jknoteR{nerozumim, co se mysli tim 'compact'? :or just compact region of this component
%%   $c^{*}\subseteq c$ of this component using Watershed}, 
%%   assign it an unused label $l$ and insert $c$ intro the atlas $\y$ such that $i\in c \Leftrightarrow \y_i=l$; %c\leftarrow l\in\mathbb{L}$ s.t. $l\notin\y$
%% %  an alternative is using Watershed on the in the image remains $F^{*}(\x^s,\y,\w)$ and then find largest component $c$ 
%% %  (this helps estimating small patterns in agglomeration but usually require more iterations)
\item calculate weights $w^{n}_{l}$ for the new label $l$ for all images
  $\x^{n}\in\X$ using \eqref{eq:overlap-thr}. 
%with soft threshold $\sigma = 1 / K$ which gives higher usage of this pattern.
\end{enumerate}

\paragraph{\label{par:Update-Y}Update of atlas $\y$.}

With the weight vectors $\W$ fixed, finding the atlas $\y$ is a~discrete
labeling problem. We can rewrite the criterion in \eqref{eq:crit} as
\begin{equation}
\frac{1}{N}\!
\sum_{i\in\Omega} \underbrace{\sum_{n} \left|\x^s_{i} - \sum_{l\in \mathbb{L}} \w^s_{l} \cdot \llbracket \y=l
\rrbracket \right|}_{U_i(y_i)}
+\!\! \sum_{\substack{i,j\in\Omega,\, i\neq j,\\ d(i,j)=1}} \llbracket \y_{i}\neq\y_{j} \rrbracket
\end{equation}
which can be solved for example with Graph Cut~\cite{Boykov2001} and alpha expansion.

