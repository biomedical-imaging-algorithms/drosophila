%-------------------------------------------------------------------------
% Latex para Ya!
% DEADLINE: July 15th, 2016 
% Autores: Nava, González, Kybic, and Escalante
% Título: Orthogonal moments based features for lymphoma characterization

% -------------------------------------------------------------------------

\documentclass[conference,a4paper]{ieeetran}

\usepackage[latin1]{inputenc}
\usepackage[english]{babel}
\selectlanguage{english}

\usepackage{balance}
\usepackage[]{paralist}
\usepackage[table,svgnames]{xcolor}
\usepackage{bm}
\usepackage[]{graphicx}
\usepackage{subfigure}	
\graphicspath{{resources/}}
\DeclareGraphicsExtensions{.png}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{lipsum}

\usepackage{booktabs}

\usepackage{tikz,pgfplots,pgfplotstable}
\usetikzlibrary{pgfplots.groupplots}
\pgfplotsset{compat=newest}
\pgfplotstableread[col sep = comma]{resources/krawtchouk.csv} \mykrawtchouk
\pgfplotstableread[col sep = comma]{resources/dhahn.csv} \mydhahn
\pgfplotstableread[col sep = comma]{resources/tchebichef.csv} \mytchebichef
\pgfplotstableread[col sep = comma]{resources/fisher.csv} \myfisher

\definecolor{RYB1}{RGB}{217,33,32}
\definecolor{RYB2}{RGB}{222,167,58}
\definecolor{RYB3}{RGB}{87,163,173}
\definecolor{RYB4}{RGB}{64,64,150}

\newcommand{\sign}{\mathrm{sign}}
\newcommand{\sat}{\mathrm{sat}}

\parskip 1mm
\arraycolsep 0.5mm
\thinmuskip=2mu %-2
\medmuskip=3mu
\thickmuskip=4mu
% -------------------------------------------------------------------------

\title{Characterization of Hematologic Malignancies based on Discrete Orthogonal Moments}

\author{\authorblockN{Rodrigo Nava\authorrefmark{1}, German Gonz\'alez\authorrefmark{2}, Jan Kybic\authorrefmark{1}, and Boris Escalante-Ram\'irez\authorrefmark{2}}
\authorblockA{\authorrefmark{1} Faculty of Electrical Engineering, Czech Technical University in Prague, Czech Republic\\
e-mail: uriel.nava@gmail.com}
\authorblockA{\authorrefmark{2} Facultad de Ingenier\'ia, Universidad Nacional Aut\'onoma de M\'exico, Mexico\\
e-mail: germangs999@gmail.com}}

\begin{document}
\maketitle

% -------------------------------------------------------------------------

\begin{abstract}
During the last decade leukemia and lymphomas have been a hot topic in the biomedical area. Their diagnosis is a time-consuming task that, in many cases, delays treatments. On the other hand, discrete orthogonal moments (DOMs) are a tool recently introduced in biomedical image analysis. Here, we propose a combination of DOMs to help in the diagnosis of leukemia and lymphomas. We classify the IICBU2008-lymphoma dataset that includes three hematologic malignancies: chronic lymphocytic leukemia, follicular lymphoma, and mantle cell lymphoma. Our methodology analyzes such diseases in the hematoxylin and eosin color space. We also include feature analysis to preserve the most discriminating characteristics of the malignant tissues. Finally, the classification of the samples is performed with kernel Fisher discriminant analysis. The accuracy is $93.85\%$. The results show the proposal could be useful in different biomedical applications.
\end{abstract}

\begin{keywords}
Color deconvolution, Discrete orthogonal moments, Hematologic malignancies, Feature selection.
\end{keywords}

% -------------------------------------------------------------------------

\section{Introduction \label{sec:introduction}}

Malignant neoplasm of hematopoietic and lymphoid tissues or simply hematologic malignancies (HMs) are a collection of diseases that includes leukemia and lymphomas \cite{VARDIMAN2010}. Although HMs are rare malignant disorders, they have been a major topic in biomedical journals in recent years \cite{CHIZUKA2006}.

According to Rodriguez-Abreu et al. \cite{RODRIGUEZ-ABREU2007}, over 250,000 cases are diagnosed with leukemia yearly. This number represents 2.5\% of the newly diagnosed cases of cancer around the world. Leukemia affects bone marrow and is associated with the rapid overproduction of immature white blood cells. As the disease progresses, the abnormal cells crowd out healthy cells and hamper the ability to fight against infections. 

\textbf{Chronic lymphocytic leukemia} (CLL) is the most common type of leukemia in Occident; around 80\% of cases are diagnosed in elderly \cite{CAMPO2011}. CLL is a type of slow growing leukemia that attacks B lymphocytes, which are responsible for the production of immunoglobulins. The damaged B lymphocytes cells or B-CLL cells have a prolonged life and express CD38 protein, so that, if the cells contain high amounts of such a protein, then CLL tends to get worse quickly \cite{MALAVASI2011} (see Fig. \ref{fig:leukemia3}).    
   
Lymphoma damages B lymphocytes similarly to leukemia. However, it begins in the lymphoid tissue such as spleen, thymus, or lymph nodes instead of the bone marrow and can grow as a solid tumor and spreads throughout the body \cite{CHIU2015}.

Non-Hodgkin lymphoma (NHL) is the most common cancer of the lymphatic system with about 90\% of the diagnosed cases \cite{SHANKLAND2012}. \textbf{Follicular lymphoma} (FL) is a relatively common lymphoma that represents up to 25\% of NHL cases \cite{TROTMAN2013}. It is asymptomatic and mainly affects lymph nodes. FL progresses quite slowly from B lymphocytes and eventually may spread to the bone marrow. Its name comes from the fact that abnormal cells are organized in a follicular-like pattern, namely, cells tend to grow in clumps known as follicles \cite{REAGAN2015} (see Fig. \ref{fig:follicle3}). 

\textbf{Mantle cell lymphoma} (MCL) is also an HMs that represents approximately 6\% of NHL cases and usually affects males over 50 \cite{MIRANDA2013}. It develops in the mantle zone, which is the outer section of lymph nodes. MCL spreads to the bone marrow and sometimes to the gastrointestinal tract \cite{THIEBLEMONT2015} (see Fig. \ref{fig:mantle3}). Compared to CLL and FL, MCL is an aggressive disease that overexpresses the Cyclin-D1 protein, which helps to control cell growth. 

Since cytopathological phenotypes of HMs may differ significantly, it is crucial to detect specific patterns to propose tailored therapies based on individual risk profiles. Biopsy is the standard clinical practice for detecting and grading HMs. However, pathologists must handle a large number of cases every day by visual inspection, thus, it is a difficult and time-consuming work. 

In order to overcome such an issue, many automatic methods for classification have been proposed. For instance, Orlov et al. \cite{ORLOV2010} classify lymphoma images using a set of features, which includes polynomial, texture, spectral, and spatial characteristics. Sertel et al. \cite{SERTEL2008} modify the gray level co-occurrence matrices of cytopathological images by using a non-linear color quantization, whereas Tuzel et al. \cite{TUZEL2007} use textons and support vector machines to classify five classes of HMs. In \cite{SIJI2014}, the authors use Wndchrm \cite{SHAMIR2008a} that extracts a large set of features to classify a biological dataset proposed in \cite{SHAMIR2008}.

Recently, orthogonal moments have emerged as a useful tool for characterizing biological tissue because they are able to represent texture in a non-redundant way by capturing the oscillating behavior of all components of the texture of interest \cite{NAVA2016,MARCOS2013}. Nevertheless, the main drawback is related to the numerical instability when a large window of analysis is used since the order of the moments is linked to the window size.  

\begin{figure*}[!tbp]
\centering	
\subfigure{\includegraphics[width=0.25\textwidth]{leukemia_1}}
\subfigure{\includegraphics[width=0.25\textwidth]{follicle_1}}
\subfigure{\includegraphics[width=0.25\textwidth]{mantle_1}}\\
\subfigure{\includegraphics[width=0.25\textwidth]{leukemia_2}}
\subfigure{\includegraphics[width=0.25\textwidth]{follicle_2}}
\subfigure{\includegraphics[width=0.25\textwidth]{mantle_2}}\\
\setcounter{subfigure}{0}
\subfigure[]{\label{fig:leukemia3}\includegraphics[width=0.25\textwidth]{leukemia_3}}
\subfigure[]{\label{fig:follicle3}\includegraphics[width=0.25\textwidth]{follicle_3}}
\subfigure[]{\label{fig:mantle3}\includegraphics[width=0.25\textwidth]{mantle_3}}
\caption{IICBU2008-lymphoma dataset \cite{SHAMIR2008}. The images were transformed into the hematoxylin and eosin (H+E) color space. First row shows the original RGB images, second row shows the hematoxylin channel, and third row presents the eosin channel. \textbf{(a)} CLL, \textbf{(b)} FL, and \textbf{(c)} MCL.}
\label{fig:dataset}
\end{figure*}

In this paper, we propose a novel methodology based on discrete orthogonal moments (DOMs) to characterize hematologic malignancies. Sliding windows are used to scan small regions of tissue; then local DOM features are computed on every position. This technique does not involve high-order moment computation and avoids numerical instability. After this step, the most important features are selected; then, kernel Fisher analysis and K-NN are used to classify three different hematologic malignancies.

In the following Section \ref{sec:discrete_orthogonal_moments} we introduce briefly the mathematical foundations of DOMs. In Section \ref{sec:materials_and_methods} the dataset and the methodology are presented. The experiments and results are shown in Section \ref{sec:experiments_and_results}. Finally, conclusions are drawn in Section \ref{sec:summary_and_conclusions}.

% -------------------------------------------------------------------------

\section{Discrete Orthogonal Moments \label{sec:discrete_orthogonal_moments}}

Orthogonal moments are uncorrelated statistical quantities useful for characterizing images \cite{MARCOS2013}. They are calculated as projections of the image over an orthogonal basis \cite{FLUSSER2009}. One way of interpreting the projections is as a measure of correlation between the image and the polynomial basis \cite{NAVA2016}. However, the implementation of the basis involves a discrete approximation affecting the invariance and orthogonality of the polynomials. In the following section, we introduce three discrete orthogonal bases that eliminate the need for numerical approximations and still satisfy the property of orthogonality.

% -------------------------------------------------------------------------

\subsection{Discrete Tchebichef Moments \label{sec:discrete_tchebichef_moments}}

Discrete Tchebichef polynomials (see Fig. \ref{fig:moments}(a)) were introduced by Mukundan et al. \cite{MUKUNDAN2001} and are defined in terms of hypergeometric functions as follows \cite{FLUSSER2009}:
\begin{equation}
t_n(x) = \left(1-N\right)_n\,_{3}F_{2}\left(-n,-x,1+n;1,1-N;1\right)\,
\label{eq:tchebichef}
\end{equation}
where $(x)_{n}=\Gamma(x+n)/\Gamma(x)=x(x+1)\ldots(x+n+1)$ is the Pochhammer symbol, $x,n=\{0,1,\ldots,N-1\}$ and the hypergeometric function $_{x}F_{y}$  is defined as:
\begin{equation}
_{x}F_{y}(a_1,\ldots,a_x;b_1,\ldots,b_y;z) = \sum_{k=0}^{\infty}\frac{(a_1)_{k}(a_2)_{k}\cdots(a_x)_{k}}{(b_1)_{k}(b_2)_{k}\cdots (b_y)_{k}}\frac{z^k}{k!}\,
\label{eq:hypergeometric}
\end{equation}

The property of orthogonality of the discrete Tchebichef polynomials is satisfied by:
\begin{equation}
\sum_{x=0}^{N-1} = t_n(x)t_m(x) = \rho(n,N) \delta_{nm}\,
\label{eq:tchebi_orthogonality}
\end{equation}
$\rho(n,N)$ is a normalizing factor and $\delta_{nm}$ is the Kronecker delta.

Using Eq. (\ref{eq:tchebichef}) is possible to compute the discrete Tchebichef moments (DTM) of the texture $f(x,y)$:
\begin{equation}
T_{nm} = \sum_{x=0}^{N-1}\sum_{y=0}^{M-1} \widehat{t}_n (x) \hat{t}_m (y) f(x,y)\,
\label{eq:discrete_tchebi_moments}
\end{equation}
where $N$ and $M$ are the image size on $X$ and $Y$-axes respectively. $\widehat{t}_x$ represents the scaled Tchebichef moments \cite{MUKUNDAN2004,NAVA2016}. 

\pgfplotsset{
  every axis plot/.append style={line width=1.5pt}
}

\begin{figure*}[!htp]
\centering
\begin{tikzpicture}[>=latex]
\begin{groupplot}[width=0.41\textwidth, group style = {group size = 3 by 1, horizontal sep = 3pt, vertical sep = 10pt}, clip=false, axis line style = ultra thin, ticks=none]

\nextgroupplot[xlabel={(a)}, legend style = {column sep = 10pt, legend columns = -1, legend to name = grouplegend, draw=none}]
\addplot [draw=RYB1] table [x index={0}, y index = {1}] {\mytchebichef}; 
 \addlegendentry{order 0};
\addplot [draw=RYB4] table [x index={0}, y index = {2}] {\mytchebichef};
\addlegendentry{order 1};
\addplot [draw=RYB3] table [x index={0}, y index = {3}] {\mytchebichef};
\addlegendentry{order 2};
\addplot [draw=RYB2] table [x index={0}, y index = {4}] {\mytchebichef};
\addlegendentry{order 3};

\nextgroupplot[xlabel = {(b)}]
\addplot [draw=RYB1] table [x index={0}, y index = {1}] {\mykrawtchouk};
\addplot [draw=RYB2] table [x index={0}, y index = {2}] {\mykrawtchouk}; 
\addplot [draw=RYB3] table [x index={0}, y index = {3}] {\mykrawtchouk}; 
\addplot [draw=RYB4] table [x index={0}, y index = {4}] {\mykrawtchouk}; 

\nextgroupplot[xlabel = {(c)}]
\addplot [draw=RYB1] table [x index={0}, y index = {1}] {\mydhahn};
\addplot [draw=RYB2] table [x index={0}, y index = {2}] {\mydhahn}; 
\addplot [draw=RYB3] table [x index={0}, y index = {3}] {\mydhahn}; 
\addplot [draw=RYB4] table [x index={0}, y index = {4}] {\mydhahn}; 

\end{groupplot}
\node at ($(group c2r1) + (0,-3.5cm)$) {\ref{grouplegend}}; 
\end{tikzpicture}
\caption{Illustration of 1D discrete orthogonal polynomials from order 0 to order 3. The corresponding moments are calculated by projecting a texture over the basis. \textbf{(a)} Tchebichef, \textbf{(b)} Krawtchouk with $p=0.5$, and \textbf{(c)} Dual Hahn with $a=0$, $b=N$, and $c=0$.}
\label{fig:moments}
\end{figure*}

% -------------------------------------------------------------------------

\subsection{Discrete Krawtchouk Moments \label{sec:discrete_krawtchouk_moments}}

Discrete Krawtchouk polynomials \cite{KRAWTCHOUK1929} constitute an orthogonal basis related to the binomial distribution (see Fig. \ref{fig:moments}(b)). The classic \textit{n}th-order discrete Krawtchouk polynomial is defined as:
\begin{equation}
K_n\left(x;p,N\right) =\, _{2}F_{1}\left(-n,-x;-N;\frac{1}{p}\right)\,
\label{eq:discrete_krawt_moments}
\end{equation}
where $x,n=\{0,1,\ldots,N|N>0\}$ and $p\,\epsilon\,(0,1)$.

In practice, the weighted discrete Krawtchouk polynomials \cite{YAP2003} are used to build the basis as follows:
\begin{equation}
\widehat{K}_n\left(x;p,N\right) = K_n\left(x;p,N\right)\sqrt{\frac{w\left(x;p,N\right)}{\rho\left(n;p,N\right)}}\,
\label{eq:weighted_discrete_krawt_moments}
\end{equation}
where $w(x;p,N) = (_x^{N})p^{x}(1-p)^{N-x}$ is the weight function and $\rho(n;p,N)=(1-p/p)^{n}1/(^{N}_n)$ is the norm. 

The weighted discrete Krawtchouk polynomials satisfy the condition of orthogonality:
\begin{equation}
\sum_{x=0}^N \widehat{K}_n(x;p,N)\widehat{K}_n(x;p,N) = \delta_{nm}\,
\label{eq:orthogonality_krawt}
\end{equation}

From Eq. (\ref{eq:weighted_discrete_krawt_moments}), the discrete Krawtchouk moments (DKM) of the texture $f(x,y)$ are computed as:
\begin{equation}
Q_{nm} = \sum_{x=0}^{N-1}\sum_{y=0}^{M-1} \widehat{K}_n\left(x;p_1,N-1\right)\widehat{K}_m\left(y;p_2,M-1\right)f\left(x,y\right)\,
\label{e:krawt_moments}
\end{equation}
where $N$ and $M$ are the image size on $X$ and $Y$-axes respectively.

Discrete Krawtchouk moments are able to extract local features when the parameter $p$ varies between 0 to 1.

% -------------------------------------------------------------------------

\subsection{Discrete Dual Hahn Moments \label{sec:dhahn}}

Discrete Dual Hahn polynomials (see Fig. \ref{fig:moments}(c)) \cite{NIKIFOROV1988} are defined on a non-uniform lattice $x(s) = s(s+1)$ where $s$ is the order of a sample and $x$ is its distance from the origin. In \cite{ZHU2007}, the lattice is adapted to the coordinate space of the image. The discrete Dual Hahn polynomials are defined by the following expression:
\begin{equation}
\begin{aligned}
w_n&\left(s,a,b,c\right) = \frac{(a-b+1)_{n}(a+c+1)_{n}}{n!}\, \\
& \times\, _{3}F_{2}(-n,a-s,a+s+1;a-b+1,a+c+1;1)\,
\end{aligned}\,
\label{eq:discrete_hahn_polynomials}
\end{equation}
where $n=\{0,1,\ldots,N-1\}$ and $s=\{a,a+1,\ldots,b-1\}$. The parameters $a$, $b$, and $c$ are restricted to the following conditions: $-\frac{1}{2}<a<b,\quad |c|<1+a,\quad\mbox{and} \quad b=a+N$.

As in Krawtchouk polynomials, the discrete dual Hahn polynomials are also scaled and normalized \cite{ZHU2007}:
\begin{equation}
\widehat{w}_{n}\left(s,a,b,c\right) = w_n^{c}(s,a,b,c)\sqrt{\frac{\rho(s)}{d_n^2}\left[\Delta x\left(s-\frac{1}{2}\right)\right]}\,
\label{eq:scaled_discrete_dhahn}
\end{equation}

The weight $\rho$ and norm $d_{n}^{2}$ functions are defined:
\begin{equation}
\rho\left(s\right) = \frac{\Gamma(a+s+1)\Gamma(c+s+1)}{\Gamma(s-a+1)\Gamma(b-s)\Gamma(b+s+1)\Gamma(s-c+1)}\,
\label{eq:weight_dhahn}
\end{equation}
and
\begin{equation}
d_n^2 = \frac{\Gamma(a+c+n+1)}{n! (b-a-n-1)! \Gamma (b-c-n)}\,
\label{eq:norm_dhahn}
\end{equation}

The orthogonality condition of the weighted discrete dual Hahn polynomials is given by: 
\begin{equation}
\sum_{s=a}^{b-1} \widehat{w}_n\left(s,a,b,c\right)\widehat{w}_m\left(s,a,b,c\right) = \delta_{mn}\,
\label{eq:orthogonality_dhahn}
\end{equation}

Finally, the discrete dual Hahn moments (DHM) are computed as:
\begin{equation}
W_{nm} = \sum_{s=a}^{b-1} \sum_{t=a}^{b-1} \widehat{w}_n(s,a,b,c) \widehat{w}_m(t,a,b,c) f(s,t)\,
\label{DHahn_moments}
\end{equation}
where $n,m =\{ 0,1,\ldots,N-1\}$. 

% -------------------------------------------------------------------------

\section{Materials and Methods \label{sec:materials_and_methods}}

% -------------------------------------------------------------------------

\subsection{Dataset and preprocessing \label{sec:dataset_preprocessing}}

In order to validate our proposal, we use the IICBU2008-lymphoma dataset   \cite{SHAMIR2008}. Three types of hematologic malignancies stained with hematoxylin/eosin are presented in the set: 113 CLL samples, 139 FL samples, and 122 MCL samples. The dataset was stored in the RGB space color and does not include any information about patients.

% -------------------------------------------------------------------------

\subsubsection{Color deconvolution \label{sec:color_deconvolution}}

The assessment of colored stains may provide further information for classification of hematologic malignancies \cite{BUENO2013}. Hematoxylin stains cell nuclei blue, whereas eosin stains cytoplasm pink or red. Macenko et al. \cite{MACENKO2009} proposed to convert cytology samples into H+E space to improve the ability to analyze quantitatively nuclei cells.

Here, we transform the dataset from RGB, $I$, into H+E, $S$, using the color deconvolution formula \cite{RUIFROK2001}: $S = V^{-1}I_l$, where $I_l = -log_{10}(I)$ and $V$ is the matrix of the stain vectors H+E defined as:
\begin{equation}
V = \left[%
\begin{array}{ccc}
0.65 & 0.70 & 0.29 \\
0.07 & 0.99 & 0.11 \\
0.27 & 0.57 & 0.78% 
\end{array} \right]\,
\end{equation}

% -------------------------------------------------------------------------

\subsection{Methodology \label{sec:methodology}}

% -------------------------------------------------------------------------

\subsubsection{Feature extraction \label{sec:feature_extraction}}

The DOMs are calculated on each channel, H and E, separately using a sliding window with an overlap of $50\%$ on X and Y-axes. We conducted experiments by varying the window size from $\{10 \times 10,\, 20 \times 20,\, \ldots,\, 80\times 80\}$. The accuracy was the criterion to compute the optimal window size of $50 \times 50$ pixels. For every window position $(\alpha,\beta)$, the orthogonal moments DTM, DKM, and DHM are computed. So that, for each image, the average discrete moments are concatenated one after another to build the image descriptor.

The average discrete Tchebichef moment descriptor is computed as:
\begin{equation}
\overline{T_{nm}} = \frac{1}{\alpha\times\beta}\sum_{g=1}^{\alpha}\sum_{h=1}^{\beta} T_{nm}^{(g,h)}\,
\label{eq:mean_tchebi}
\end{equation} 
the average discrete Krawtchouk moment descriptor is:
\begin{equation}
\overline{Q_{nm}} = \frac{1}{\alpha \times \beta} \sum_{g=1}^{\alpha} \sum_{h=1}^{\beta} Q_{nm}^{(g,h)}\,
\label{eq:mean_krawt}
\end{equation} 
and the average discrete dual Hahn moment descriptor is:
\begin{equation}
\overline{W_{nm}} = \frac{1}{\alpha \times \beta} \sum_{g=1}^{\alpha} \sum_{h=1}^{\beta} W_{nm}^{(g,h)}\,
\label{eq:mean_dhahn}
\end{equation} 
for all the equations, the superscripts $(g,h)$ represent the window position.

The image descriptor could be written as:
\begin{equation}
\begin{aligned}
D = \biggl[\overline{T_{nm}}(H),\,&\overline{T_{nm}}(E),\,\overline{Q_{nm}}(H),\,\\
&\overline{Q_{nm}}(E),\,\overline{W_{nm}}(H),\,\overline{W_{nm}}(E)\biggr]\,
\end{aligned}
\label{eq:descriptor}
\end{equation}
$H$ and $E$ represent the stained channels.

% -------------------------------------------------------------------------
\subsubsection{Feature selection \label{sec:feature_selection}}

Since the proposal generates high-dimensional feature vectors and a limited dataset is available, then we select a subset of relevant features using RELIEFF \cite{KONONENKO1997}, which is the multi-class extension of RELIEF \cite{KIRA1992}. 

RELIEFF estimates the quality of features by searching $k$ of the nearest neighbors from the same class and $k$ nearest neighbors from each of the different classes (\textit{hit and miss}). Then, a weight matrix is updated to rank those features that best describe the data \cite{ROBINSON2003}.
 
% -------------------------------------------------------------------------
\subsubsection{Classification \label{sec:classification}}

During the classification stage, we use kernel Fisher discriminant analysis (KFDA) \cite{MIKA1999} to map the selected features into a new space. In KFDA, the goal is to find $w^{*}\in\zeta$ that maximizes
\begin{equation}
J\left(w\right) =\frac{w^{T}\bm{S_{B}}^{\Phi}w}{w^{T}\bm{S_{W}}^{\Phi}w}
\label{eq:kernel_fisher}
\end{equation}
where $\bm{S_{B}}^{\Phi}$ and $\bm{S_{W}}^{\Phi}$ are the corresponding matrices in $\zeta$.

One way to solve Eq. (\ref{eq:kernel_fisher}) is to use a kernel matrix $\bm{K}(x,y)$; compute the between-class scatter matrix, $\bm{A}$; and the within-class scatter matrix $\bm{B}$. The kernel matrix is defined as: $\bm{K}\left(m,n\right)=k\left(X_{m},X_{n}\right)$ where $\bm{X}=\bigcup_{i=1}^{C}\bm{X}^{i}$ are the original data. Here, we use the Gaussian function, $k\left(x,y\right) = e^{-\frac{1}{2}\frac{\left\|x-y\right\|^{2}}{a^{2}}}$ with $a,b\in\mathbb{R^{+}}$ as the kernel function.

The between-class scatter matrix is defined by:
\begin{equation}
\bm{A} = \sum_{j=1}^{C}l_{j}\left(\mu_{j}-\mu\right)\left(\mu_{j}-\mu\right)^{T}
\label{eq:between_matrix}
\end{equation}
where $\mu_{j}=\frac{1}{l_{j}}\sum_{\forall n\in X^{j}}\bm{K}\left(m,n\right)$ and $\mu=\frac{1}{l}\sum_{\forall n} \bm{K}\left(m,n\right)$.

The within-class scatter matrix is defined by:
\begin{equation}
\bm{B} = \bm{KK}^{T} - \sum_{j=1}^{C}l_{j}\mu_{j}\mu_{j}^{T}
\label{eq:within_matrix}
\end{equation}
%$\bm{B}=\bm{B}+r\bm{I}$ to guarantee that $\bm{B}$ is positive definite. $\bm{I}$ is the identity matrix and $r$ = 0.001.

The projection of the data can be computed as:
\begin{equation}
	 y =\bm{K}\alpha^{*}
	\label{eq:eq_28}
\end{equation}
where $\alpha^{*}$ is built with the $C-1$ largest eigenvalues of $\bm{B}^{-1}\bm{A}$.

Finally, we use $K$-NN with $k=10$ and the Euclidean distance to classify the samples in the KFDA space (see Fig. \ref{fig:results}).

% -------------------------------------------------------------------------

\section{Experiments and results \label{sec:experiments_and_results}}

For this study, we classified 374 samples from three classes: CLL, FL, and MCL using ten-fold cross-validation. We kept 115 features out of 592, which means nearly 20\% of the most relevant features using RELIEFF. We also performed comparisons using the three polynomials separately and keep the same criterion to choose features. Our proposal achieved an accuracy = $93.83\%$ and outperformed the other methods when the bases were used independently.

We also characterize the database with local binary patterns \cite{OJALA2002} with radius $R=1$ and neighbors $N=8$ using the same methodology described above. The results present here are better than the ones reported in \cite{SHAMIR2008}, where the authors used 154 out of 1025 features and achieved an accuracy = $85\%$. In \cite{SERTEL2008}, Sertel et al. report $90.3\%$ of accuracy. We summarized in Table \ref{tab:results} the results of the experiments.

\begin{figure}[!tbp]
\centering
\begin{tikzpicture}  
\begin{axis}[
scatter/classes={
a={mark=o,draw=RYB1}, 
b={mark=o,draw=RYB2}, 
c={mark=o,draw=RYB3}}, clip=false, axis line style = ultra thin, ticks=none, legend style = {column sep = 10pt, legend columns = -1, legend to name = fisherlegend, draw=none}] 
\addplot[scatter, only marks, scatter src=explicit symbolic] table [x index={1}, y index={2}, meta index={3}] {\myfisher};
\addlegendentry{CLL}
\addlegendentry{FL}
\addlegendentry{MCL}
\end{axis} 
\node at ($(group c2r1) + (-5.5,-3cm)$) {\ref{fisherlegend}}; 
\end{tikzpicture}
\caption{Final classification of the IICBU2008-lymphoma dataset\cite{SHAMIR2008}. After projecting the features into the KFDA space, the separation among classes is improved and the relevant features are labeled. Our proposal achieves an accuracy of $93.83\%$.}
\label{fig:results}
\end{figure}

\setlength{\tabcolsep}{2pt}
\renewcommand{\arraystretch}{1.5}
\begin{table}[!tbp]
\centering
\caption{Our proposal combines three DOMs to characterize HMs. We used ten-fold cross-validation, thus, the mean results are reported here along with the standard deviation ($\pm$). All data are expressed in \%. Bold values represent best results.}
\label{tab:results}
\begin{tabular}{c|cccc}
\toprule
\textbf{Method} & \textbf{Accuracy} & \textbf{Precision} & \textbf{Sensitivity} & \textbf{Specificity} \\
\hline
\hline
\textit{Proposal} & $\bm{93.83} \pm \bm{2.56}$ & $\bm{93.78} \pm \bm{2.78}$ & $\bm{94.31} \pm \bm{2.13}$ & $\bm{97.00} \pm \bm{1.21}$\\
\textit{Tchebichef} & $82.06 \pm 6.06$ & $81.74 \pm 6.20$ & $82.90 \pm 6.26$ & $91.38 \pm 2.94$\\
\textit{Krawtchouk} & $75.97 \pm 5.32$ & $75.65 \pm 5.64$ & $76.63 \pm 5.52$ & $88.30 \pm 2.63$\\
\textit{Dual Hahn} & $70.88 \pm 8.86$ & $70.69 \pm 9.09$ & $71.24 \pm 8.52$ & $85.70 \pm 4.21$\\
\textit{LBPs \cite{OJALA2002}} & $76.77 \pm 6.07$ & $78.26 \pm 6.46$ & $76.01 \pm 5.99$ & $88.35 \pm 2.89$\\

\bottomrule
\end{tabular}
\end{table}

% -------------------------------------------------------------------------

\section{Summary and conclusions \label{sec:summary_and_conclusions}}

We present a novel method based on discrete orthogonal moments for the classification of three hematologic malignancies. The proposal combines the characteristics produced by different polynomial bases into a single vector, so that, the resulted features constitute over-complete descriptors that describe in a better way the local variations of tissues. Therefore, we take advantage of the redundant information by preserving the most relevant features to improve tissue characterization. Discrete Tchebichef moments contribute with approximately $50\%$ of the total of the features, whereas, discrete Krawtchouk and dual Hahn moments provide $25\%$ of features each one. We also included color analysis that allows us to discriminate cell nuclei and cytoplasm; this analysis may provide detailed information about the stage of the malignancies. 

The technique presents here achieves over $90\%$ of accuracy and outperforms the results obtained when the discrete orthogonal moments are used separately up to $10\%$. Combination of DOMs may help in a better understanding about the hematologic malignancies. Since the diagnosis of leukemia and lymphomas is performed by visual inspection, our proposal is useful to reduce time and bias. As a summary, the results have shown the effectiveness of our approach and the proposal could be useful in different biomedical problems.

% -------------------------------------------------------------------------

%\begin{appendices} 
%\section{Hypergeometric Functions}
%
%\begin{description}
%\item[Gamma function:] The gamma function $\Gamma(n)$ is defined to be an extension of the factorial to complex and real number arguments. It is related to the factorial by:
%
%\begin{equation}
%\Gamma(n)=(n-1)!
%\label{Gamma}
%\end{equation}
%
%\item[Pochhammer symbol:] The Pochhammer symbol is a notation used in the theory of special functions for the rising factorial.Its definition is:
%
%\begin{equation}
%(n)_k = n(n+1) \cdots (n+k-1) = \frac{\Gamma(n+k)}{\Gamma(n)}.
%\label{Pochhammer}
%\end{equation}
 %
%\item[Hypergeometric function:] The hypergeometric functions are solutions to the hypergeometric differential equation, which has a regular singular point at the origin. These functions are used to define orthogonal polynomial sets. The hypergeometric series ${}_xF_y$ could be written as:
%
%\begin{equation}
%{}_xF_y(a_1, \ldots ,a_x;b_1, \ldots, b_y ;z) = \sum_{k=0}^{\infty} \frac{(a_1)_k (a_2)_k \cdots (a_x)_k}{(b_1)_k (b_2)_k \cdots (b_y)_k} \frac{z^k}{k!}.
%\end{equation}
%
%
%\end{description}
%\label{apendice}
%\end{appendices}


% -------------------------------------------------------------------------

\section*{Acknowledgment}
This publication was supported by the European social fund within the project CZ.1.07/2.3.00/30.0034, UNAM PAPIIT grant IG100814, and SECITI 110/2015. R. Nava thanks Consejo Nacional de Ciencia y Tecnolog\'ia (CONACYT). G. Gonz\'alez thanks CONACYT--263921 scholarship. J. Kybic was supported by the Czech Science Foundation project 14-21421S. 

% -------------------------------------------------------------------------

\bibliographystyle{IEEEtran}
\balance
\bibliography{ipta2016}

\end{document}
