% --------------------------------------------------------------------------
% Template for ICIP 2015
% Title: Automated Segmentation of Early Oogenesis in Drosophila using Supertextons
% Authors: Nava and Kybic
% Deadline: January 30th, 2014
% --------------------------------------------------------------------------

\documentclass{article}

\usepackage{spconf}

\usepackage[english]{babel}
\selectlanguage{english}

\usepackage[table,svgnames]{xcolor}
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{{Figures/}}
\DeclareGraphicsExtensions{.png}

\usepackage{adjustbox}
\usepackage{balance}
\usepackage{subfigure}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{colortbl}
\usepackage{tabularx}
\usepackage{paralist}

\def\x{{\mathbf x}}
\def\L{{\cal L}}

% --------------------------------------------------------------------------
\title{Supertexton-based segmentation in early \textit{Drosophila} oogenesis}

\name{Rodrigo Nava and Jan Kybic \thanks{The authors extend their gratitude to Prof. Dr. Pavel Tomanc\'ak from Max Planck Institute of Molecular Cell Biology and Genetics for providing the images. This publication was supported by the European social fund within the     framework of realizing the project ``Support of inter-sectoral mobility    and quality enhancement of research teams at Czech Technical University    in Prague,'' CZ.1.07/2.3.00/30.0034. R. Nava thanks Consejo Nacional de Ciencia y Tecnolog\'ia (CONACYT). The work of J. Kybic was supported by the Czech Science Foundation project 14-21421S}}

\address{Czech Technical University in Prague, Czech Republic}

\begin{document}

\maketitle

\begin{abstract}
Studies concerning gene expression patterns of \textit{Drosophila} are of paramount importance in basic biological research because many genes are conserved across organisms providing information of fundamental activity. However, mapping a gene requires analyzing hundreds of objects that have been segmented previously. Hence, a reliable segmentation is a crucial step. Here, we introduce the concept of supertextons and propose a novel segmentation procedure for localized \textit{Drosophila} ovaries. First, a pre-segmentation step is performed using superpixels; each superpixel that belongs to a single class is transform into a feature vector. Then, a dictionary is built by clustering representative feature vectors per class, such clusters are called supertextons. Finally, during the classification stage, new superpixels are assigned to certain classes using the \textit{k}-NN classifier and the supertexton dictionary. This proposal has been applied to segmentation of cells in \textit{Drosophila} oogenesis where the results have shown the effectiveness of our approach.
\end{abstract}

\begin{keywords}
Drosophila, Gene expression patterns, Oogenesis, Superpixels, Textons, Visual dictionary.
\end{keywords}

% --------------------------------------------------------------------------
\section{Introduction}
\label{sec:intro}

Gene expression represents a means to explore and understand genetic interactions that are involved in the process of biological development because it is highly regulated, namely, genes turn on in specific cell types and at a specific time. A common technique to determine such expressions is RNA \textit{in situ} hybridization that makes visible RNA sequences based on the ability of acids to hybridize with complementary sequences \cite{TOMANCAK2002}. 

An excellent model for studying this phenomenon is the fruit fly, \textit{Drosophila melanogaster}, which was introduced as an experimental animal almost a century ago due to its interesting attributes such as: rapid generation time, ease culturing, and low maintenance cost \cite{ARIAS2008}. 

\textit{Drosophila} oogenesis provides outstanding conditions for genetic traceability. The process functions as an assembly line. It begins in a structure called germarium that contains somatic and germline stem cells. The latter are divided asymmetrically to produce nurse cells that are surrounded by somatic follicle cells (border cells) to form an ovary or egg chamber \cite{MONTELL2003}. This process constitutes the first stage in the development of the ovary (see Fig. \ref{fig:fig_1}). 

\begin{figure}[!tbp]
    \centering
    \subfigure[]{\label{fig:fig_1a}\includegraphics[width=0.23\textwidth]{figure_1a}}
    \subfigure[]{\label{fig:fig_1b}\includegraphics[width=0.23\textwidth]{figure_1b}}
    \caption{\textit{Drosophila} ovaries. \textbf{(a)} examples of localized ovaries during oogenesis. Green signal shows nuclei of cells during early stages of occyte development, whereas magenta signal comes from the specific RNA expression. \textbf{(b)} annotated ovary showing nurse and follicle cells and cytoplasm.}
    \label{fig:fig_1}
\end{figure}

However, gene expression mapping requires analysis of hundreds of ovaries that have been segmented previously. We identify four classes: \begin{inparaenum}[\bfseries i\normalfont)]\item nurse cells; \item follicle cells; \item cytoplasm; and \item background. \end{inparaenum} This task is time-consuming and depends on trained observers. Hence, a reliable automated segmentation method would be useful to reduce time and bias. 

To the best of our knowledge, there is little literature addressing \textit{Drosophila} oogenesis segmentation. Most of the state-of-the-art methods are devoted to segment and process sub-cellular embryogenesis data \cite{CASTRO2011}. Xiong et al. \cite{XIONG2006a} proposed a level set method for segmenting nuclei and cytoplasm; this approach relies on the idea that a curve from a given image can evolve to detect objects. Graph-based methods have been used in \cite{CHEN2006, GHITA2014} to segment mitochondria, whereas Lucchi et al. \cite{LUCCHI2012} take advantage of superpixel segmentation to address the same problem. 

However, these techniques have been developed for electron and not optical microscopy. We have therefore decided to apply texture analysis, which takes advantage of local intensity variations to distinguish shapes, regions, or objects. 

In \cite{VARMA2005}, Varma and Zisserman introduced the concept of ``textons'' as representative responses of the convolution of an image with a filter bank. The collection of such responses is clustered with \textsl{k}-means to produce a dictionary of visual words. The dictionary is used to calculate an average histogram that is employed as a model of a specific texture. Nevertheless, the main drawback of this approach is that textons characterize spatial properties of regions rather than pixels. A common practice is to split up the input image into blocks of size 32$\times$32 or 64$\times$64 px and build a histogram for each one. This procedure causes that small patches cannot represent regions in our data because the size of follicle cells is not larger that 10 px. Patches of such a size do not contain enough information to discriminate among classes, while larger patches lead to a block-like segmentation \cite{JAVED2011}.

In this study, we investigate the use of a pre-segmentation step using simple linear iterative clustering (SLIC) algorithm \cite{ACHANTA2012}. SLIC groups perceptually-similar pixels and yields adherence to salient edges avoiding block-like shapes. For each segmented region that belongs to a single class, a group of features that corresponds to a filter bank response is computed and a feature vector is built. Then, a dictionary is constructed using representative features vectors that we call ``supertextons.'' Our proposal avoids building histograms and distinguishes small regions that represent follicle and nurse cells from the background and cytoplasm in \textit{Drosophila} egg chambers successfully. 

%This paper is organized as follows: in Section \ref{sec:metho} the proposal is presented in detail. In Section \ref{sec:exp} the data and the experiments are presented, while conclusions and future work are shown in Section \ref{sec:concl}.

% --------------------------------------------------------------------------
\section{Methodology}
\label{sec:metho}

We propose a methodology composed of three stages: \begin{inparaenum}[\bfseries i\normalfont)] \item pre-segmentation with SLIC superpixels; \item construction of the supertexton dictionary; and \item classification. \end{inparaenum}

% --------------------------------------------------------------------------
\subsection{Superpixel segmentation}

In our approach, an initial partition of the data is generated with SLIC superpixels. The algorithm uses seeds sampled on a regular grid and the regions belonging to individual seeds are then iteratively updated. 

The number of seeds is determined as: $S = {MN}/{s^{2}}$ where $M$ and $N$ are the width and height of the image respectively and $s$ is the size of the superpixel. Regularizer, $r$, is related to region compactness. If $r \rightarrow 0$ then the adherence to borders is better, otherwise the region tends to a square shape. Another advantage of SLIC is that the processing time is relatively small. In our experiments we set the initial superpixel size, $s = 10$, and regularizer, $r = 0.02$. 

During the training stage, given an input image, $I(x,y)$, we generate its corresponding segmented image, $T(x,y)$, by labeling superpixels according a ground truth. If all pixels within the superpixel belong to a single class then the superpixel is labeled with that class.

% --------------------------------------------------------------------------
\subsection{Supertexton dictionary}

In order to build a supertexton dictionary, we used a reduce set of the Leung-Malik \cite{LEUNG2001} filter bank by keeping only the maximum responses of first and second Derivatives of Gaussian across six orientations $\{0^{\circ},\, 30^{\circ},\, 60^{\circ},\, 90^{\circ},\, 120^{\circ},\ 150^{\circ}\}$ and at three dyadic scales $\{\sqrt{2},\, 2,\, 2\sqrt{2}\}$. Furthermore, we used eight Laplacian of Gaussian and four Gaussian filters with standard deviation $\sigma = \left\{\sqrt{2}, 2, 2\sqrt{2}, 4\right\}$. The 18 resulted filters constitute a rotation invariant representation, ($LM_{18}$). 

Let $I(x,y)$ be an input image and given a filter bank, $LM_{18}$, then the responses $F_i$ are computed as follows:
\begin{equation}
   F_i = I(x,y) \star LM_i
\label{eq:eq_1}
\end{equation}
where $\star$ is the convolution.

For each labeled superpixel in the corresponding $T(x,y)$ the following features are computed:
\begin{itemize}
    \setlength{\itemsep}{0pt}
	\item Mean: $\mu_{i} = \frac{1}{N_{I}}\sum I_{F_{i}}$, where $I_{F_{i}}$ are the intensity values $\in F_{i}$  within the corresponding superpixel region and $N_{I}$ the number of intensity values. 
    
    \item Standard deviation: $\sigma_{i} = \sqrt{\frac{1}{N_{I}}\sum\left(I_{F_{i}}-\mu_{i}\right)^2}$
    
    \item Energy: $E_{i} = \frac{1}{N_{I}}\sum (I_{F_{i}})^{2}$
    
    \item Average gradient: $G_{i} = \sum\left\|\nabla I_{F_{i}}\right\|$  
\end{itemize}

We included a normalization process to transform the feature vectors $x$ to a random variable normally distributed \cite{AKSOY2001}

\begin{equation}
   \tilde{x} = \frac{x-\mu_{x}}{2\left(3\sigma_{x}+1\right)}
\label{eq:eq_2}
\end{equation}
where $m_{x}$ and $\sigma_{x}$ are the mean and standard deviation of the feature vector, respectively.

Hence, the rescaled feature vectors are built as follows:
\begin{equation}
   \overline{f} = \big[\tilde{\mu_{1}},\,\tilde{\sigma_{1}},\,\tilde{E_{1}},\,\tilde{G_{1}},\,\ldots,\,\tilde{\mu_{L}},\,\tilde{\sigma_{L}},\,\tilde{E_{L}},\,\tilde{G_{L}}\big]
\label{eq:eq_3}
\end{equation}
where $L$ corresponds to the $L$-th filter response. 

Then, the \textit{k}-means clustering is applied on each of the four classes to obtain  $k$ representative cluster centers, called supertextons. The dictionary will consist of $4 \times k$ supertextons. In our experiments, $k$ is the $10\%$ of feature vectors per class.

% --------------------------------------------------------------------------
\subsection{Classification}
During classification stage, an input image is segmented. Then, for each superpixel, a feature vector is calculated as we describe in the previous subsection. 

We used \textit{k}-NN classifier with $k=6$ which represents less than 1\% of the vectors in our training dataset and assigned the input feature vector to the class with the highest number of neighbors using the Euclidean distance between feature vectors $ \overline{f}$ as a metric. We experimented with $k=10$ and $k=15$, however, the variable did not affect the results significantly.

\begin{table*}[!tbp]
\rowcolors{5}{AliceBlue}{}
\renewcommand{\arraystretch}{1.3}
\setlength{\tabcolsep}{2pt} 
\caption{Classification rates. The values represent mean rates after the classification of 54 images. All data are expressed in $(\%)$. The values between parenthesis represent standard deviations. Bold values represent the best rates.}
\label{tab:tab_1}
\centering
\begin{adjustbox}{width=1\textwidth}
{\small
\begin{tabular*}{1.18\textwidth}{cccccccccc}
\toprule
& \multicolumn{3}{c}{Supertextons} & \multicolumn{3}{c}{Pixel-based} & \multicolumn{3}{c}{Textons} \\
\cmidrule[0.5pt](l{5pt}r{5pt}){2-4} \cmidrule[0.5pt](l{5pt}r{5pt}){5-7} \cmidrule[0.5pt](l{5pt}r{5pt}){8-10}
Class & PR & SE & $F_{1}$-Score & PR & SE & $F_{1}$-Score & PR & SE & $F_{1}$-Score \\
\hline
\hline
Background & 95.98 ($\pm$ 3.96) & 90.99 ($\pm$ 8.25) & \textbf{93.15} ($\pm$ 4.90) & 96.83 ($\pm$ 3.32) & 80.80 ($\pm$ 14.41) & 87.22 ($\pm$ 9.50) & 99.82 ($\pm$ 0.82) & 45.19 ($\pm$ 23.35) & 59.18 ($\pm$ 23.24) \\
Follicle cells & 82.90 ($\pm$ 6.50) & 79.14 ($\pm$ 7.17) & \textbf{80.66} ($\pm$ 4.66) & 72.06 ($\pm$ 6.76) & 64.92 ($\pm$ 8.19) & 67.76 ($\pm$ 4.60) & 37.50 ($\pm$ 7.37) & 85.85 ($\pm$ 7.38) & 52.20 ($\pm$ 7.14) \\
Nurse cells & 79.64 ($\pm$ 9.52) & 85.22 ($\pm$ 5.03) & \textbf{81.87} ($\pm$ 4.99) & 64.33 ($\pm$ 10.12) & 71.69 ($\pm$ 7.36) & 67.11 ($\pm$ 5.77) & 74.55 ($\pm$ 13.84) & 63.04 ($\pm$ 9.25) & 67.59 ($\pm$ 7.28) \\
Cytoplasm & 80.18 ($\pm$ 10.06) & 86.68 ($\pm$ 4.46) & \textbf{82.84} ($\pm$ 5.75) & 67.11 ($\pm$ 10.03) & 84.26 ($\pm$ 5.95) & 74.01 ($\pm$ 6.04) & 72.18 ($\pm$ 20.57) & 47.41 ($\pm$ 20.73) & 56.28 ($\pm$ 18.20) \\
\bottomrule
\end{tabular*}
}
\end{adjustbox}
\end{table*}

% --------------------------------------------------------------------------
\section{Experimental Results}
\label{sec:exp}

For this study, we selected 54 images from 3D-stacks of \textit{Drosophila} oogenesis of size 647$\times$1024$\times$24. They were captured using an optical sectioning fluorescence microscope. Each stack contains two channels, the green one shows nuclei of cells that constitute egg chambers providing a common reference. Note that in every slice may appear different stages of the occyte development. The magenta channel comes from specific RNA probes labeled with a fluorescent dye and was not used. 

Images of isolated egg chambers were obtained manually by expert biologists at Max Planck Institute of Molecular Cell Biology and Genetics in Dresden, Germany. As a result, the dimensions of the egg chambers varied from one to another. For each image, four classes were annotated: background, follicle cells, nurse cells, and cytoplasm, (see Fig. \ref{fig:fig_3}). 

We compared our proposal against two methods: first, a pixel-based classification method, where the feature vectors were built using pixel-wise filter bank outputs rather than superpixels and; second, the original Varma and Zisserman's texton approach. We used textons patches of size 10$\times$10 px. (see a summary of the results in Table \ref{tab:tab_1}.

Furthermore, we evaluated the discrimination ability of the feature vectors by using the non-parametric Kruskal-Wallis test. This test computes the occurrence of significant differences between two distributions and provides an idea of how well-separated are. Since this procedure is a binary test, we computed one-class-versus-the-rest p-values. The results reflect that there are about $90\%$ of features having p-values $< 0.001$.

In order to assess the segmentation, leave-one-out cross-validation was used. The precision (PR) and sensitivity (SE) were computed and compared with a manual segmentation in a pixel-wise fashion (see Fig. \ref{fig:fig_3}). We also present $F_{1}$-Score $=2\frac{PR \times SE}{PR+SE}$, which is a measure of accuracy and reaches its best value at $1$ and worst score at $0$.

%\begin{figure}[!tbp]
    %\hspace*{\fill}
    %\subfigure[]{\includegraphics[width=0.21\textwidth]{back}}\hfill
    %\subfigure[]{\includegraphics[width=0.21\textwidth]{follicle}}
    %\hspace*{\fill}\vskip 0.1pt
    %\hspace*{\fill}
    %\subfigure[]{\includegraphics[width=0.21\textwidth]{nurse}}\hfill
    %\subfigure[]{\includegraphics[width=0.21\textwidth]{cytoplasm}}
    %\hspace*{\fill}
    %\caption{Results from Kruskal-Wallis test. The cumulative distribution function (CDF) of p-values is shown for: \textbf{(a)} background-versus-the-rest; \textbf{(b)} follicle cells-versus-the-rest; \textbf{(c)} nurse cells-versus-the-rest; \textbf{(a)} cytoplasm-versus-the-rest.}
    %\label{fig:fig_2}
%\end{figure}

\begin{figure*}[!tbp]
    \centering
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_2a}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_2b}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_2c}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_2e}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_2d}}\\
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_3a}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_3b}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_3c}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_3e}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_3d}}\\
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_4a}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_4b}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_4c}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_4e}}
    \subfigure{\includegraphics[width=0.15\textwidth]{figure_4d}}\\
    \setcounter{subfigure}{0}
    \subfigure[]{\includegraphics[width=0.15\textwidth]{figure_6a}}
    \subfigure[]{\includegraphics[width=0.15\textwidth]{figure_6b}}
    \subfigure[]{\includegraphics[width=0.15\textwidth]{figure_6c}}
    \subfigure[]{\includegraphics[width=0.15\textwidth]{figure_6e}}
    \subfigure[]{\includegraphics[width=0.15\textwidth]{figure_6d}}\\
    \caption{Examples of segmentation in \textit{Drosophila} egg chambers. \textbf{(a)} localized egg chamber; \textbf{(b)} the corresponding ground-truth; \textbf{(c)} supertexton segmentation; \textbf{(d)} pixel-based segmentation, and; \textbf{(e)} texton segmentation.}
    \label{fig:fig_3}
\end{figure*}

% --------------------------------------------------------------------------
\section{Conclusions}
\label{sec:concl}
We proposed a novel supervised segmentation method based on superpixels and textons. For each superpixel that belongs to a single class, a feature vector was computed using mean and standard deviation, energy, and the gradient of the corresponding filter responses. Since nurse and follicle cells are pretty similar in intensities, we used gradient information to characterize superpixel contours. In addition, energy increases differences between background and cytoplasm; thus this feature led to an improvement in the classification rate. 

This procedure outperforms the traditional approach based on patches that leads to a block-like segmentation. Our proposal does not use histograms to describe regions; hence, it is not subject to the size of the patch. Square-like regions including those used in textons cannot describe follicle cells as they are narrow, so, it is necessary to use small patches that may hamper the discriminative task. Furthermore, pixel-based classification is highly sensitive to noise which results in regions with holes. Nevertheless, texton-based approaches need large dictionaries in order to classify new images. Here, we segmented only egg chambers from an early stage of the \textit{Drosophila} oogenesis. Since egg chambers are highly variables in shape and intensity values, they represent a challenge for automated segmentation methods. Further improves must include global features that discriminate further stages and avoids to  merge individual nurse cells.

% --------------------------------------------------------------------------
\bibliographystyle{IEEEbib}
\balance
\bibliography{references}

\end{document}
