\beamer@endinputifotherversion {3.33pt}
\select@language {english}
\beamer@sectionintoc {2}{Introduction}{3}{1}{1}
\beamer@sectionintoc {3}{Overcomplete models}{11}{1}{2}
\beamer@sectionintoc {4}{Local descriptors}{24}{1}{3}
\beamer@sectionintoc {5}{Case study: Chronic Obstructive Pulmonary Disease}{27}{1}{4}
\beamer@sectionintoc {6}{Results}{37}{1}{5}
\beamer@sectionintoc {7}{Conclusions and future work}{41}{1}{6}
