%--------------------------------------------------------------
%   Poster template for Postdoc Seminar 2014
%   By Rodrigo Nava
%   November 28th, 2014
%   v 0.3
%--------------------------------------------------------------

\documentclass[portrait,a0paper,margin=1cm,fontscale=0.295]{baposter}

\usepackage[english]{babel}
\usepackage{blindtext}
\usepackage[]{graphicx}
\graphicspath{{resources/}}

\usepackage[default]{sourcesanspro}
\usepackage[T1]{fontenc}

\newcommand{\compresslist}{%
\setlength{\itemsep}{1pt}%
\setlength{\parskip}{0pt}%
\setlength{\parsep}{0pt}%
}

\newcommand{\localtextbulletone}{\textcolor{Blue}{{\scalebox{1.6}{$\bullet$}}}}
\renewcommand{\labelitemi}{\localtextbulletone}

%-----------------------------------------------------------
\begin{document}

\begin{poster}{
  grid=false,
  columns=2,
  colspacing=0.7em,
  % Color style
  bgColorOne=white,
  bgColorTwo=white,
  borderColor=Blue,
  headerColorOne=white,
  headerColorTwo=white,
  headerFontColor=black,
  boxColorOne=Snow,
  boxColorTwo=Snow,
  textborder=faded,   % Format of textbox
  eyecatcher=true, % left logo
  headerborder=open,
  headerheight=0.1\textheight,
	%  textfont=\sc, An example of changing the text font
  headershape=rounded,
  headershade=shadelr,
  headerfont=\Large\bf\textsc, %Sans Serif
  textfont={\setlength{\parindent}{1.5em}},
  boxshade=plain,
  background=plain,
  linewidth=1pt
}
{} % left logo
{Automatic Analysis of Spatial Gene Expression Patterns\vspace{0.5cm} }
{R. Nava, F. Malinka, J. Kybic, and F. \v{Z}elezn\'y \\
\textbf{{\color{NavyBlue}Faculty of Electrical Engineering, Czech Technical University in Prague}}}
{}

%-----------------------------------------------------------
\headerbox{{\color{SlateGray}Goal}}{name=objective,column=0,row=0,textborder=none,headerborder=none,boxColorOne=white}{
\noindent Develop a set of algorithmic tools for processing large sets of microscopy images for a systematic survey of
spatio-temporal gene expression patterns of \textbf{ovaries} and \textbf{larval imaginal discs}
}

%-----------------------------------------------------------
\headerbox{{\color{SlateGray}Introduction}}{name=introduction,column=0,row=0,below=objective}{
\begin{itemize}\compresslist
	\item Gene functionality is of paramount importance in basic biological research with possible therapeutic applications in medicine
    \item Genes turn on in specific cells and at a specific time-point (gene expression pattern)
    \item An excellent model for studying these phenomena is the fruit fly \textit{Drosophila melanogaster}
\end{itemize}

\begin{center}
\includegraphics[width=0.7\textwidth]{fig_1}\\
{\small\textit{Drosophila} development process}
\end{center}
}

%-----------------------------------------------------------
\headerbox{{\color{SlateGray}Data}}{name=data,column=0,below=introduction}{
\begin{itemize}\compresslist
    \item \textbf{Max Planck Institute of Molecular Cell Biology and Genetics} (MPI) provides data
    \item MPI maps expression of thousand of genes \textbf{in a purely manual manner}
    \item Individual embryos were identified and annotated using a controlled vocabulary
    \item \textbf{Two datasets}:  (C) Ovaries from which embryos develop, have two channels and a specific stain reveals cellular anatomy, whereas (B) larval imaginal discs, from which adult body parts develop, are gray-scale images superimposed with blue staining
\end{itemize}

\begin{center}
\includegraphics[width=\textwidth]{fig_11}\\
{\small Examples of gene expression patterns (Courtesy of Pavel Tomancak). \textbf{(A)} \textit{Drosophila} embryos under bright-field microscopy; \textbf{(B)} larval imaginal discs; \textbf{(C)} egg chambers; and \textbf{(D)} 3D rendering of one pattern}
\end{center}
}

%-----------------------------------------------------------
\headerbox{{\color{SlateGray}Tasks}}{name=conclusions,column=0,below=data}{
\begin{itemize}\compresslist
    \item Candidate object detection: identify possible objects of interest
    \item Object segmentation: segment follicle cells and nurse cells from the background
    \item \textbf{Symbolic data mining} algorithms applied to find patterns in the detected gene expressions
\end{itemize}
}

%-----------------------------------------------------------
\headerbox{{\color{SlateGray}Segmentation}}{name=methods,column=1,row=0}{
\begin{itemize}\compresslist
	\item First, a bilateral filtering step is applied to reduce the effect of noise
    \item SLIC \textbf{superpixels} are computed
\end{itemize}
\begin{center}
\includegraphics[width=0.5\textwidth]{fig_3a}\\
{\small{Superpixel segmentation of size: 10 and 30 px}}
\end{center}

\begin{itemize}\compresslist
	\item A \textbf{texton} dictionary is built using the Leung-Malik (LM) dataset.
    \item It consists of 48 filters. First and second derivatives of Gaussian at 6 directions and 3 scales (We kept only the maximal value in every scale to achieve rotational invariance). 8 Laplacian of Gaussian and 4 Gaussian filters
\end{itemize}
\begin{center}
\includegraphics[width=0.45\textwidth]{fig_4}\hspace{5mm}
\includegraphics[width=0.45\textwidth]{fig_5}\vspace{0.5cm}
\includegraphics[width=0.9\textwidth]{fig_6}\\
\end{center}
\begin{itemize}\compresslist
	\item For each superpixel a 18-length feature vector is computed
    \item The images were classified into four classes using a \textit{K}-NN classifier
\end{itemize}

\begin{center}
{\color{SlateGray} \textbf{RESULTS}}
\end{center}

\begin{center}
\includegraphics[width=0.3\textwidth]{fig_4a}
\includegraphics[width=0.3\textwidth]{fig_4c}
\includegraphics[width=0.3\textwidth]{fig_4d}
\end{center}
}

%-----------------------------------------------------------
\headerbox{{\color{SlateGray}Data mining}}{name=advances,column=1,below=methods}{
\begin{itemize}\compresslist
\item Patterns in the form of \textbf{association rules} indicate that the expression of a gene is predicted by the expression of other genes
\end{itemize}

\noindent ``polychaetoid (cell adhesion molecule binding protein) \textbf{IF} Syntaxin 1A  (protein binding) \& gomdanji (calcium ion binding protein)[18\%, 96\%]'' \vspace{0.5cm}

\noindent According to this rule, the expression of Syntaxin 1A (gene coding a a protein-binding protein) and the Gomdanji gene (coding a calcium ion binding protein) predicts the expression of polychaetoid (coding a cell adhesion molecule binding protein) with 96\% confidence. The triple of genes get expressed in 18\% of all considered organism locations

\begin{itemize}\compresslist
\item \textbf{Relational rules} indicate the properties of genes with increased expression in a chosen location in the organism
\end{itemize}

\noindent ``expressed(A) \textbf{IF} component(A,B), is-a(B,C), part-of(C,'proton-transporting V-type ATPase complex').[10,7]'' \vspace{0.5cm}

\noindent The rule indicates that genes that are part of the  proton-transporting V-type ATPase complex (or subclasses of it) are more frequently (7/17=41\% expressed genes) expressed in the target location than random genes (9\% expressed genes)
}

%-----------------------------------------------------------
\headerbox{{\color{SlateGray}Acknowledgments}}{name=ack,column=1,below=advances,textborder=none,headerborder=none,boxColorOne=white}{
\noindent This project is supported by the European social fund within the framework ``Support of inter-sectoral mobility and quality enhancement of research teams at Czech Technical University in Prague,'' CZ.1.07/2.3.00/30.0034 and the Czech Science Foundation project P202/12/2032. Jan Kybic was supported by the Czech Science Foundation project 14-21421S. 
}


\end{poster}
\end{document}
