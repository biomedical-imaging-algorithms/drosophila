% -------------------------------------------------------------------------
%   Poster template for ICIP 2015
%   By Rodrigo Nava
%   July 10, 2015  
%   ver. 0.4
% -------------------------------------------------------------------------

\documentclass[landscape,paperwidth=60in,paperheight=32in,margin=1in]{baposter}

\usepackage{fix-cm}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcase}
\usepackage{setspace}
\usepackage{tcolorbox}
\usepackage[font=small,labelfont=bf]{caption}
\usepackage{float}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{colortbl}
\usepackage{tabularx} 
\usepackage{vwcol}

\input{fancyitem}

\usepackage[]{graphicx}
\graphicspath{{resources/}}
\DeclareGraphicsExtensions{.png}

\definecolor{color_icip}{RGB}{53,167,225} 
\definecolor{color_icip_vio}{RGB}{228,210,196} 
\definecolor{color_icip_mo}{RGB}{122,54,85} 
\definecolor{color_blue}{RGB}{14,77,146}
\definecolor{color_orange}{RGB}{255,76,0}
%\definecolor{fgsubtxt}{RGB}{150,50,50}

% -------------------------------------------------------------------------
\begin{document}

\pgfdeclareimage[interpolate=true,width=\textwidth,height=\textheight]{fig:background}{resources/background.png}

\background{ %
    \begin{tikzpicture}
        [remember picture,overlay]\node[opacity=0.95] at (current page.center) {\pgfuseimage{fig:background}};
    \end{tikzpicture} %
} %

\begin{poster}{ %
  grid=false,
  columns=6,
  colspacing=0.7em,
  headerheight=0.155\textheight,
  % Color style background
  background=user,
  %bgColorOne=white,
  %bgColorTwo=color_icip,
  eyecatcher=true,
  % Box header 
  borderColor=color_orange,
  headerColorOne=color_orange,
  headerColorTwo=color_orange,
  headerFontColor=white,
  % Box area 
  boxColorOne=white,
  boxColorTwo=white,
  % Format of textbox
  textborder=faded,
  % Format of text header
  headerborder=open,
  % textfont=\sc, An example of changing the text font
  headershape=rounded,
  headershade=shadelr,
  headerfont=\Large\bf\textsc, % Sans Serif
  textfont={\setlength{\parindent}{1.5em}},
  boxshade=plain,
  linewidth=1pt
} %
{\includegraphics[height=0.91in]{cmp}} % Eye catcher
{\textcolor{white}{\MakeTextUppercase{\fontsize{19}{36}\selectfont{\vskip 2pt Supertexton-based segmentation in early \textit{Drosophila} oogenesis}}}} % Title
{\textcolor{white}{\fontsize{14}{14}{\textbf{Rodrigo Nava} \& \textbf{Jan Kybic} \\ \vspace{-0.15cm} %
Dept. of Cybernetics, Faculty of Electrical Engineering, Czech Technical University in Prague}} \\ %
\textcolor{white}{\fontsize{12}{12}{\textit{uriel.nava@gmail.com} \textbf{\&} \textit{kybic@fel.cvut.cz}}}} % Authors
{\includegraphics[height=1in]{cvut_1}} % University logo
\fontsize{9}{10}

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{I. Abstract \& Motivation}{name=motivation,column=0,row=0,span=2} {
\noindent Ovary segmentation is a crucial step to analyze the cell development process in \textit{Drosophila} oogenesis, which provides excellent conditions for genetic traceability.
\begin{myenum}
\setlength\itemsep{-0.6em}
\item There is not a reliable automatic method for segmenting ovaries.
\item Take advantage of texture features.
\item Goal $\Rightarrow$ segment localized ovaries into four classes: follicle cells, nurse cells, cytoplasm, and background.
\end{myenum}

\begin{multicols}{2}
{\includegraphics[width=0.47\textwidth]{fig_1}\captionof*{figure}{\textit{Drosophila} germarium}}
\columnbreak
{\includegraphics[width=0.39\textwidth]{fig_2}\captionof*{figure}{Localized ovary egg chambers}}
\end{multicols}

\noindent\textbf{Methodology:}
\begin{myenum}
\setlength\itemsep{-0.6em}
\item Pre-segmentation with SLIC \textbf{superpixels}
\item Construction of the \textbf{supertexton dictionary}
\item Classification using \textit{k}-NN
\end{myenum}

%\hspace*{\fill}
%\begin{center}
%\hspace*{\fill}\caption{\textit{Drosophila} oogenesis}\includegraphics[width=0.45\textwidth]{fig_1}\hspace{0.8cm}
%Isolated eggs\includegraphics[width=0.4\textwidth]{fig_2}
%\end{center}
} 

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{II. Superpixel segmentation}{name=methodology_1,column=0,span=2,below=motivation}{
\begin{myenum}
\setlength\itemsep{-0.6em}
\item  An initial partition $\{C_{i}|i=1,\,\ldots,\,K\}$ is generated with SLIC superpixels.
\item Superpixels are labeling according a ground truth.
\end{myenum}

{\includegraphics[width=0.96\textwidth]{diag_1}\captionof*{figure}{Examples of superpixel partitions}}
}

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{III. Supertexton dictionary}{name=methodology_2,column=2,row=0,span=2}{
\begin{center}\textbf{Leung-Malik filter bank} ($LM_{18}$)\\
First and second Derivatives of Gaussian \textcolor{color_orange}{+} Laplacian of Gaussian \textcolor{color_orange}{+}  Gaussian
\end{center}
\begin{myenum}
\setlength\itemsep{-0.6em}
\item \textbf{Orientations:} $\{0^{\circ}, 30^{\circ}, 60^{\circ}, 90^{\circ}, 120^{\circ}, 150^{\circ}\}$ and \textbf{Scales:}  $\{\sqrt{2}, 2, 2\sqrt{2}\}$
\item \textbf{Standard deviation:} $\{\sqrt{2}, 2, 2\sqrt{2}, 4\}$
\end{myenum}
{\includegraphics[width=0.98\textwidth]{diag_3}}%\captionof*{figure}{Maximum responses of Derivatives of Gaussian}}
\begin{center}
\vskip -3em
{\includegraphics[width=0.8\textwidth]{diag_4}}%
\end{center}

\begin{center}
For each class, \textit{k}-means is applied to obtain $k$ representative centers called \textbf{supertextons}
%\vskip -1em

{\includegraphics[width=0.7\textwidth]{diag_5}}%
\end{center}
%For each superpixel (color dots) a response vector is computed, then these vectors are clustered using the k-means algorithm. The resulting centers are called textons.
}

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{IV. Classification \& results}{name=methodology_3,column=4,row=0,span=2}{
\begin{myenum}
\setlength\itemsep{-0.6em}
\item \textbf{54} images extracted from 3D stacks of 647 \textcolor{color_orange}{x} 1024 \textcolor{color_orange}{x} 24.
\item Isolated images were obtained by expert biologist at \textbf{Max Planck Institute}.
\item Only the green channel was used (spatial information).
\item Leave-one-out cross-validation \& \textit{K}-NN classifier.
\item Comparisons against: \textbf{(a)} pixel-based method and \textbf{(b)} Varma and Zisserman's texton method. 
\end{myenum}

\setlength{\columnsep}{-1cm}
%\setlength{\columnseprule}{0.2pt}
\begin{multicols}{2}
\begin{center}
\includegraphics[width=0.15\textwidth]{figure_2a}
\includegraphics[width=0.15\textwidth]{figure_2b}\\
\includegraphics[width=0.15\textwidth]{figure_3a}
\includegraphics[width=0.15\textwidth]{figure_3b}\\
\includegraphics[width=0.15\textwidth]{figure_4a}
\includegraphics[width=0.15\textwidth]{figure_4b}\\
\includegraphics[width=0.15\textwidth]{figure_5a}
\includegraphics[width=0.15\textwidth]{figure_5b}\\
{\fontsize{8}{8}\textbf{Original \hspace{1em} Ground-truth}}
\end{center}
\columnbreak
\begin{center}
\includegraphics[width=0.15\textwidth]{figure_2c}
\includegraphics[width=0.15\textwidth]{figure_2e}
\includegraphics[width=0.15\textwidth]{figure_2d}\\
\includegraphics[width=0.15\textwidth]{figure_3c}
\includegraphics[width=0.15\textwidth]{figure_3e}
\includegraphics[width=0.15\textwidth]{figure_3d}\\
\includegraphics[width=0.15\textwidth]{figure_4c}
\includegraphics[width=0.15\textwidth]{figure_4e}
\includegraphics[width=0.15\textwidth]{figure_4d}\\
\includegraphics[width=0.15\textwidth]{figure_5c}
\includegraphics[width=0.15\textwidth]{figure_5e} 
\includegraphics[width=0.15\textwidth]{figure_5d}\\
{\fontsize{8}{8}\textbf{Supertextons \hspace{1em} Pixel-based \hspace{1em} Textons}}
\end{center}
\end{multicols}

\begin{center}
The $F_{1}$-Score $=2\frac{PR \times SE}{PR+SE}$ with precision (PR) and sensitivity (SE) was computed in a pixel-wise fashion.

\includegraphics[width=0.65\textwidth]{diag_6}\\
\vspace{0.1cm}
\end{center}
\begin{flushright}
{\fontsize{6}{6} \colorbox{color_orange}{\textbf{IEEE International Conference on Image Processing, September 27-30, Qu\'ebec, Canada}}}
\end{flushright}
}

\end{poster}

\end{document}
