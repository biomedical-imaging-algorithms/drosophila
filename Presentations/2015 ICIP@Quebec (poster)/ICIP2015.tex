% -------------------------------------------------------------------------
%   Poster template for ICIP 2015
%   By Rodrigo Nava
%   July 10, 2015  
%   ver. 0.3
% -------------------------------------------------------------------------

\documentclass[landscape,paperwidth=60in,paperheight=32in,margin=1in,showframe]{baposter}

\usepackage{fix-cm}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcase}
\usepackage{setspace}
\usepackage{tcolorbox}

\usepackage{multicol}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{colortbl}
\usepackage{tabularx}

\input{fancyitem}

\usepackage[]{graphicx}
\graphicspath{{resources/}}
\DeclareGraphicsExtensions{.png}

\definecolor{color_icip}{RGB}{53,167,225} 
\definecolor{color_icip_vio}{RGB}{228,210,196} 
\definecolor{color_icip_mo}{RGB}{122,54,85} 
\definecolor{color_blue}{RGB}{14,77,146}
%\definecolor{fgsubtxt}{RGB}{150,50,50}

% -------------------------------------------------------------------------
\begin{document}

\pgfdeclareimage[interpolate=true,width=\textwidth,height=\textheight]{fig:background}{resources/background.png}

\background{ %
    \begin{tikzpicture}
        [remember picture,overlay]\node[opacity=0.95] at (current page.center) {\pgfuseimage{fig:background}};
    \end{tikzpicture} %
} %

\begin{poster}{ %
  grid=false,
  columns=6,
  colspacing=0.7em,
  headerheight=0.15\textheight,
  % Color style background
  background=user,
  %bgColorOne=white,
  %bgColorTwo=color_icip,
  eyecatcher=false,
  % Box header 
  borderColor=color_icip_mo,
  headerColorOne=color_icip_mo,
  headerColorTwo=color_icip_mo,
  headerFontColor=white,
  % Box area 
  boxColorOne=white,
  boxColorTwo=white,
  % Format of textbox
  textborder=faded,
  % Format of text header
  headerborder=open,
  % textfont=\sc, An example of changing the text font
  headershape=rounded,
  headershade=shadelr,
  headerfont=\Large\bf\textsc, % Sans Serif
  textfont={\setlength{\parindent}{1.5em}},
  boxshade=plain,
  linewidth=1pt
} %
{} % Eye catcher
{\textcolor{white}{\MakeTextUppercase{\fontsize{20}{30}\selectfont{Supertexton-based segmentation in early \textit{Drosophila} oogenesis}}}\vspace{0.1in}} % Title
{\textcolor{white}{\fontsize{18}{28}{\textbf{Rodrigo Nava} \& \textbf{Jan Kybic}} \\ \textbf{Czech Technical University in Prague}}} % Authors
{\includegraphics[height=1in]{cvut_1}} % University logo

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{}{name=foottext, column=0, span=2, above=bottom,textborder=none,headerborder=none,boxheaderheight=0pt,linewidth=0pt}
{
\begin{flushleft}
\includegraphics[height=1cm]{icip2}\\ {\fontsize{6}{16} \colorbox{color_icip_vio}{\textbf{IEEE International Conference on Image Processing, September 27-30, Qu\'ebec, Canada}}}
\end{flushleft}
}

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{Motivation}{name=motivation,column=0,row=0,span=2} %
{
\noindent
\begin{myenum}
\setlength\itemsep{-0.5em}
\item \textbf{Gene expression} allows to understand biological development process
\item Map a gene requires analyzing hundreds of objects that have been segmented previously
\item An excellent model is the fruit fly: \textit{\textbf{Drosophila melanogaster}}
\item \textit{Drosophila} oogenesis provides conditions for genetic traceability
\end{myenum}
} 

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{\textit{Drosophila} oogenesis}{name=oogenesis,column=0,below=motivation,span=2,textborder=none}
{
\noindent {\fontsize{12}{22}It works as an assembly line. It begins in a germarium that contains somatic and germline stem cells. The latter are divided asymmetrically to produce nurse cells that are surrounded by somatic follicle cells (border cells) to form an ovary}
\begin{center}
\includegraphics[width=0.72\textwidth]{fig_1}
\end{center}
\noindent {\fontsize{9}{19}Isolated egg chambers were obtained manually by expert biologists at \textbf{Max Planck Institute of Molecular Cell Biology and Genetics} in Dresden, Germany}
} 

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{Metholodogy}{name=methodology_1,column=2,row=0,span=2,textborder=none}
{
\noindent We propose a methodology composed of three stages:
\begin{myenum}
\setlength\itemsep{-0.5em}
\item Pre-segmentation with SLIC \textbf{superpixels}
\item Construction of the \textbf{supertexton dictionary}
\item Classification using \textit{k}-NN
\end{myenum}
}

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{Metholodogy}{name=methodology_2,column=2,below=methodology_1,span=2,headerborder=none,boxheaderheight=0pt,textborder=none}
{
{\fontsize{15}{25}\textbf{Segmentation}}\vspace{-0.3cm}
\bedknobstwo{shape backgrounds,shape scaled,shape start size=2mm,shape end size=1mm}

\begin{multicols}{2}
\noindent {\fontsize{8}{18}For each seed, $\{C_{i}|i=1,\,\ldots,\,K\}$, an initial partition is calculated iteratively. $D$ considers gray values between $C_{k}$ and a pixel $i$ at positions $[x,y]$ as follows:}\vspace{0.2cm}
$ d_{c} = \sqrt{\left(l_{k}-l_{i}\right)^{2}}$ \\
$ d_{s} = \sqrt{\left(x_{k}-x_{i}\right)^{2} + \left(y_{k}-y_{i}\right)^{2}}$\\
$D = \sqrt{d^2_c + \left(\frac{d_s}{S}\right)^2m^2}$ 

\columnbreak

\begin{center}
\includegraphics[width=0.35\textwidth]{sp}
\end{center}
\end{multicols}

\begin{center}
\includegraphics[width=0.29\textwidth]{fig_2a}
\includegraphics[width=0.29\textwidth]{fig_2b}
\includegraphics[width=0.29\textwidth]{fig_2c}
\end{center}
}

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{Metholodogy}{name=methodology_3,column=4,row=0,span=2,headerborder=none,boxheaderheight=0pt,textborder=none}
{
{\fontsize{15}{25}\textbf{Supertexton dictionary}}\vspace{-0.3cm}
\bedknobstwo{shape backgrounds, shape scaled, shape start size=2mm, shape end size=1mm}

\noindent {\fontsize{8}{18}The maximum response of the Leung-Malik filter bank ($LM_{18}$) across:
\begin{myenum}
\setlength\itemsep{-0.5em}
\item \textbf{Orientations:}  $\{0^{\circ}, 30^{\circ}, 60^{\circ}, 90^{\circ}, 120^{\circ}, 150^{\circ}\}$
\item \textbf{Scales:}  $\{\sqrt{2}, 2, 2\sqrt{2}\}$
\item \textbf{Features:} Mean: $\mu_{i}$, Standard deviation: $\sigma_{i}$,  Energy: $E_{i}$, and the Average gradient $G_{i}$.
\end{myenum}
}
\begin{center}
\includegraphics[width=0.55\textwidth]{fig_3}
\end{center}

\noindent {\fontsize{15}{16}The k-means clustering is applied on each of the four classes to obtain k representative cluster centers, called} \textbf{supertextons}.

\begin{center}
$\overline{f} = \big[\tilde{\mu_{1}},\,\tilde{\sigma_{1}},\,\tilde{E_{1}},\,\tilde{G_{1}},\,\ldots,\,\tilde{\mu_{L}},\,\tilde{\sigma_{L}},\,\tilde{E_{L}},\,\tilde{G_{L}}\big]$
\end{center}
}

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{Results}{name=results_1,column=2,below=methodology_2,span=2,headerColorOne=color_blue!95,headerColorTwo=color_blue!95,boxColorOne=color_icip_mo!20,borderColor=color_blue,boxshade=plain,textborder=none}%
{
\begin{center}
{\fontsize{8}{18} 
\begin{tabular*}{0.9\textwidth}{cccc}
\toprule
& \multicolumn{3}{c}{\textbf{Pixels}} \\
\cmidrule[0.5pt](l{5pt}r{5pt}){2-4}
\textbf{Class} & \textbf{PR} & \textbf{SE} & $F_{1}$\textbf{-Score} \\
\hline
\hline
Background & 96.83 ($\pm$ 3.32) & 80.80 ($\pm$ 14.41) & 87.22 ($\pm$ 9.50) \\
Follicle cells & 72.06 ($\pm$ 6.76) & 64.92 ($\pm$ 8.19) & 67.76 ($\pm$ 4.60) \\
Nurse cells & 64.33 ($\pm$ 10.12) & 71.69 ($\pm$ 7.36) & 67.11 ($\pm$ 5.77) \\
Cytoplasm & 67.11 ($\pm$ 10.03) & 84.26 ($\pm$ 5.95) & 74.01 ($\pm$ 6.04) \\
\bottomrule
\end{tabular*}
}
\end{center}
} 

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{Results}{name=results_2,column=4,below=methodology_3,span=1,headerborder=none,boxheaderheight=0pt,textborder=none}
{\fontsize{12}{22}\textbf{Results}\vspace{-0.3cm}
\bedknobstwo{shape backgrounds,shape scaled,shape start size=2mm,shape end size=1mm}
\begin{center}
\includegraphics[width=0.19\textwidth]{figure_2a}
\includegraphics[width=0.19\textwidth]{figure_2b}
\includegraphics[width=0.19\textwidth]{figure_2c}\\
\includegraphics[width=0.19\textwidth]{figure_3a}
\includegraphics[width=0.19\textwidth]{figure_3b}
\includegraphics[width=0.19\textwidth]{figure_3c}\\
\includegraphics[width=0.19\textwidth]{figure_4a}
\includegraphics[width=0.19\textwidth]{figure_4b}
\includegraphics[width=0.19\textwidth]{figure_4c}\\
\includegraphics[width=0.19\textwidth]{figure_5a}
\includegraphics[width=0.19\textwidth]{figure_5b}
\includegraphics[width=0.19\textwidth]{figure_5c}
%\includegraphics[width=0.2\textwidth]{figure_6a}
%\includegraphics[width=0.2\textwidth]{figure_6b}
%\includegraphics[width=0.2\textwidth]{figure_6c}
\end{center}
}

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
\headerbox{Acknowledgments}{name=ack,column=5,below=methodology_3,span=1,headerborder=none,boxheaderheight=0pt,textborder=none}
{
{\fontsize{10}{20}\textbf{Acknowledgments}}\vspace{-0.3cm}
\bedknobstwo{shape backgrounds, shape scaled, shape start size=2mm, shape end size=1mm}
\begin{spacing}{0.1}
\noindent {\scriptsize This publication was supported by ``Support of inter-sectoral mobility and quality enhancement of research teams at Czech Technical University in Prague,'' CZ.1.07/2.3.00/30.0034. R. Nava thanks CONACYT. The work of J. Kybic was supported by the Czech Science Foundation project 14-21421S
}

\begin{flushright}
\includegraphics[width=0.87\textwidth]{contact}
\end{flushright}
\end{spacing}
}

\end{poster}

\end{document}
