%-----------------------------------------------------------
%-----------------------------------------------------------
%   Poster template LAPI 2013
%   by R.Nava
%   v 0.2
%-----------------------------------------------------------
%-----------------------------------------------------------

%-----------------------------------------------------------
\documentclass[portrait,a0paper,margin=2cm,fontscale=0.295]{baposter}

\usepackage{calc}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{relsize}
\usepackage{multirow}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{colortbl}
\usepackage{enumitem}
\usepackage{dcolumn}
\usepackage{rotating}
\usepackage{bm}
\usepackage{url}
\usepackage{tikz}
\usetikzlibrary{calc}

\input{fancyitem}

\usepackage[T1]{fontenc}
\usepackage[]{graphicx}
\usepackage{multicol}
\usepackage{palatino}

\definecolor{color_fondo}{RGB}{255,255,255}
\definecolor{spie_gris}{RGB}{110,110,110}
\definecolor{color_fondo_caja}{RGB}{60,120,130}
\definecolor{color_texto_caja}{RGB}{255,255,255}
\definecolor{LapiVerde}{RGB}{39,50,48}

\graphicspath{{resources/}}

\setlength{\columnsep}{1.5em}
\setlength{\columnseprule}{0mm}

\newcommand{\compresslist}{%
\setlength{\itemsep}{1pt}%
\setlength{\parskip}{0pt}%
\setlength{\parsep}{0pt}%
}

\setlength{\tabcolsep}{3pt}
\renewcommand{\arraystretch}{1.1}
\arrayrulecolor{green}
%\parskip = 6pt
%\setlength\parindent{0pt}

%\pgfdeclareimage[interpolate=true,width=0.05\textwidth]{fig:logo}{resources/logounam.png}

%\pgfputat{\pgfxy(0,0)}{\pgfbox[left,base]{\hspace{4.5cm}{\bf 18$^{th}$} Iberoamerican Congress on Pattern Recognition, Havana, Cuba. (20-23 Nov 2013)}}

%-----------------------------------------------------------
\begin{document}

\begin{poster}{
  grid=false,
  columns=2,
  colspacing=1em,
  % Color style
  bgColorOne=color_fondo,%gray!40!white,
  bgColorTwo=gray!100!white,%color_fondo,
  borderColor=color_fondo_caja,
  headerColorOne=color_fondo_caja,
  headerColorTwo=color_fondo_caja!65!white,
  headerFontColor=color_texto_caja,
  boxColorOne=white,
  %boxColorTwo=lightblue,
  % Format of textbox
  textborder=roundedleft,
  % Format of text header
  eyecatcher=true,
  headerborder=open,
  headerheight=0.13\textheight,
	%  textfont=\sc, An example of changing the text font
  headershape=roundedleft,
  headershade=shadelr,
  headerfont=\Large\bf\textsc, %Sans Serif
  textfont={\setlength{\parindent}{1.5em}},
  boxshade=plain,
  background=shadetb,
  linewidth=1pt
}
{\includegraphics[height=8.9em]{lapi}}
{Texel-based Image Classification with Orthogonal Bases}
{\fontsize{14pt}{16pt}\selectfont\vskip 2pt E. Carbajal-Degante $^{\textcolor[rgb]{1,0,0}{1}}$, R. Nava$^{\textcolor[rgb]{1,0,0}{2}}$, J. Olveres$^{\textcolor[rgb]{1,0,0}{3}}$,  B. Escalante-Ram\'irez$^{\textcolor[rgb]{1,0,0}{4}}$, and Jan Kybic$^{\textcolor[rgb]{1,0,0}{2}}$
\fontsize{7pt}{10pt}\selectfont\vskip 2pt
\textcolor[rgb]{1,0,0}{$1$} Posgrado en Ingenier\'ia El\'ectrica, Facultad de Ingenier\'ia, Universidad Nacional Aut\'onoma de M\'exico, Mexico City\\
\textcolor[rgb]{1,0,0}{$2$} Faculty of Electrical Engineering, Czech Technical University in Prague, Czech Republic\\
\textcolor[rgb]{1,0,0}{$3$} Posgrado en Ciencia e Ingenier\'ia de la Computaci\'on, Universidad Nacional Aut\'onoma de M\'exico, Mexico City\\
\textcolor[rgb]{1,0,0}{$4$} Departamento de Procesamiento de Se\~nales, Facultad de Ingenier\'ia, Universidad Nacional Aut\'onoma de M\'exico, Mexico City\\
\fontsize{10pt}{12pt}\selectfont\vskip 3pt \{erikycd,\,uriel.nava\}\textcolor[rgb]{0,0.4,0}{\textbf{@gmail.com}}}
{\includegraphics[height=7.5em]{cvut_1}}

%-----------------------------------------------------------
\headerbox{Introduction}{name=introduction,column=0,row=0}{

\noindent\textit{\textcolor[rgb]{1,0,0}{\textbf{Texture}}} is defined as periodic-like behavior patterns within a spatial region and is also a property related to material, roughness, or shape of a surface.\\
\noindent\textit{\textcolor[rgb]{1,0,0}{\textbf{Texel}}} is the smallest window of analysis that captures the fundamental oscillating pattern of a given texture.
\begin{center}
\includegraphics[width=0.25\textwidth]{D11_div_G}\hspace{10pt}
\includegraphics[width=0.25\textwidth]{Mon}\hspace{10pt}
\includegraphics[width=0.25\textwidth]{Burg}\\
{\color{green} \textbf{Texels computed in different textures}}
\end{center}

\noindent\textit{\textcolor[rgb]{1,0,0}{\textbf{Motivation:}}} Orthogonal bases can characterize textures by projecting an image over a set of functions that describes the behavior of the patterns.\vspace{0.2cm}

\noindent However, they present \textit{\textbf{limitations}:}\vspace{-0.2cm}
\begin{myenum}
\setlength\itemsep{-0.6em}\compresslist
\item Numerical instability in higher-order polynomials
\item High computational cost due to the size of the texture
\end{myenum}

\noindent\textit{\textcolor[rgb]{1,0,0}{\textbf{Proposal:}}}\vspace{-0.2cm}
\begin{myenum}
\setlength\itemsep{-0.6em}\compresslist
\item Novel technique to find the texel that describes the texture
\item Feature extraction based on texels
\item Reduced feature space
\item A suitable model for classification tasks
\end{myenum}
}

%-----------------------------------------------------------
\headerbox{Orthogonal bases}{name=dataset,column=0,below=introduction}{
\noindent An \textit{\textcolor[rgb]{1,0,0}{\textbf{orthogonal basis}}} is a finite set of vectors that satisfies the condition of orthogonality and can be used to describe an object by approximations

\begin{myenum}\compresslist
\item Discrete Tchebichef moments (DTM)
\begin{equation*}
T_{pq} = \frac{1}{\rho(p,N)\rho(q,N)}\sum_{x=0}^{N-1}\sum_{y=0}^{N-1}f(x,y)t_p(x)t_q(y)\,
\end{equation*}
\begin{center}
\hspace*{\fill}
\includegraphics[width=0.28\textwidth]{Tcheb_Poly}\hspace{35pt}
\includegraphics[width=0.28\textwidth]{Tchebipol_3}
\hspace*{\fill}\\\vspace{0.2cm}
DTM feature extraction:
\end{center}
\begin{equation*}
M(s) = \dislaystyle\sum_{s = p+q}\left|T_{pq}\right|
\end{equation*}
\item Steered Hermite transform (SHT)
\begin{equation*}
f_{n-m,m}^{\theta}(x_0,y_0,\theta) = \sum_{k = 0}^{n}f_{n-k,k}(x_0,y_0)\alpha_{n-k,k}(\theta)\,
\end{equation*}
\begin{center}
\hspace*{\fill}
\includegraphics[width=0.28\textwidth]{HermiteGauss_D_Poly}\hspace{15pt}
\includegraphics[width=0.25\textwidth]{Copa4}\hspace{8pt}
\includegraphics[width=0.25\textwidth]{Copa100}
\hspace*{\fill}\\\vspace{0.2cm}
Multi-scale SHT feature extraction:
\end{center}
\begin{equation*}
F = \left\{\mu_{n}^{H_{\sigma}},\sigma_{n}^{H_{\sigma}}\middle|n=0\,,\ldots\,N; H_{\sigma} = n\,\ldots,\,N\right\}\,
\end{equation*}
\end{myenum}
}

%-----------------------------------------------------------
\headerbox{}{name=foottext,column=0,below=dataset,textborder=none,headerborder=none,boxheaderheight=0pt,boxColorOne=spie_gris}{
\vspace{-0.5cm}
\begin{center}
\includegraphics[width=0.9\textwidth,height=0.025\textheight]{epe4}
\end{center}
}

%-----------------------------------------------------------
\headerbox{Methodology}{name=methods,column=1,row=0}{
\begin{myenum}\compresslist
\item \textbf{GLCM}. On X- and Y-axes with $\theta =[0,\frac{\pi}{2}]$ and $d=[2,\,\ldots,\,N/2]$.
\item \textbf{Correlation values (CV)}. Dependence measure among \textbf{GLCMs}.
\item \textbf{Cepstral Analysis (CA)}. Magnitude spectrum of \textbf{CV} \textcolor[rgb]{1,0,0}{$\Rightarrow$} a more suitable scale for \textcolor[rgb]{1,0,0}{periodicity detection}.
\item \textbf{Feature extraction}. Multi-scale SHT to characterize texels.
\end{myenum}

\begin{center}
\includegraphics[width=0.93\textwidth]{Diag2}
\end{center}
}

%-----------------------------------------------------------
\headerbox{Results}{name=results,column=1,below=methods}{
\noindent\textit{\textcolor[rgb]{1,0,0}{\textbf{Texel size validation:}}} A dataset of 40 textures from Brodatz, Klette, and Vistex \textcolor[rgb]{1,0,0}{$\Rightarrow$} synthetic textures \textcolor[rgb]{1,0,0}{$\Rightarrow$}  Computation of texels by \textbf{CA} under three degradations\textcolor[rgb]{1,0,0}{$\Rightarrow$} Error assessment.
\begin{center}
\includegraphics[width=0.29\textwidth]{Blur}\hspace{6pt}
\includegraphics[width=0.29\textwidth]{Gauss}\hspace{6pt}
\includegraphics[width=0.29\textwidth]{Impulsive}
\\{\color{green}}
\end{center}
\noindent\textbf{\textit{\textcolor[rgb]{1,0,0}{Classification results:}}} Two independent datasets from 34 Brodatz textures \textcolor[rgb]{1,0,0}{$\Rightarrow$} Texel calculation \textcolor[rgb]{1,0,0}{$\Rightarrow$} Features extraction with DTM and SHT \textcolor[rgb]{1,0,0}{$\Rightarrow$} \textit{k}-NN classifier with $k = 1$ \textcolor[rgb]{1,0,0}{$\Rightarrow$} Correct classification rate (CCR)
\begin{center}
\includegraphics[width=0.29\textwidth]{DTM_Tex}\hspace{5pt}
\includegraphics[width=0.29\textwidth]{DTM_64}\hspace{5pt}
\includegraphics[width=0.29\textwidth]{DTM_32}
\\{\color{green} {\small\textbf{Evaluation of CCR for DTMs}}}
\\\vspace{0.15cm}
\includegraphics[width=0.29\textwidth]{Hermite_Texel}\hspace{5pt}
\includegraphics[width=0.29\textwidth]{Hermite_64}\hspace{5pt}
\includegraphics[width=0.29\textwidth]{Hermite_32}
\\{\color{green} {\small\textbf{Evaluation of CCR for SHT}}}
\end{center}

\iffalse
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{\footnotesize
\begin{center}
\begin{tabular}{ccccc}
\toprule
{\bf Bases} & {\bf \textit{Texel}(CCR)} & {\bf \textit{128}(CCR)} & {\bf \textit{64}(CCR)} & {\bf \textit{32}(CCR)} \\ 
\hline 
\hline 
DTM  & $88.0$ & $93.0$ & $83.4$ & $67.3$ \\  
SHT  & $91.3$ & $96.3$ & $94.5$ & $83.3$ \\
\bottomrule
\end{tabular}
\end{center}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\fi
}

%-----------------------------------------------------------
\headerbox{Conclusions}{name=conclusions,column=1,below=results}{
{\small 
\begin{myenum}\compresslist
\item Texel size estimation based on \textbf{CA} has proven to be a robustness model against degradations. 
\item Texel-base feature vectors keep a close relationship with full texture-base feature vectors.
\item Texels capture the minimum amount of information for describing a texture and achieve good rates in classification tasks.
\end{myenum}
}
}

\end{poster}
\end{document}
