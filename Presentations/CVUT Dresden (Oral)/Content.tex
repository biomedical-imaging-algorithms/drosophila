%--------------------------------------------------------
%--------------------------------------------------------
\section{Introduction \label{introduction}}

%--------------------------------------------------------
\begin{frame}{Introduction}
\only<1>{
\begin{exampleblock}{Motivation}
\begin{enumerate}
    \item The information encoded in a gene --string of DNA-- is used to control the assembly of a protein (\textbf{gene expression})
	\item It is a very complex process and provides information about the current state of a cell
    \item It ultimately determines the morphology and physiology of an organism
    \item A powerful tool  for the diagnosis of a disease, the discovery of new drugs or tailoring therapies to specific pathologies
\end{enumerate}
\end{exampleblock}
}
\only<2>{
\begin{alertblock}{Goal}
\centering
Develop algorithms for processing a large set of microscopy images for a systematic survey of spatio-temporal gene expression patterns in \textit{Drosophila} \textbf{ovaries} and larval imaginal discs\vspace{0.2cm}
\begin{itemize}
	\item \textbf{Ovary segmentation}. Nurse cells, follicle cells, cytoplasm, and background (2D) 
    \item \textbf{Ovary selection}. Determine if the segmented object is indeed an object of interest (3D)
\end{itemize}
\end{alertblock}
}
\end{frame}

%--------------------------------------------------------
\begin{frame}{Introduction}{Drosophila melanogaster}
\begin{columns}[onlytextwidth]
\begin{column}{0.41\textwidth}
    \begin{tcolorbox}[colback=white,colframe=fgsubtxt,valign=center,center upper,center lower]
    \includegraphics[width=\textwidth,keepaspectratio]{drosophila}
    \tcblower
    {\tiny\textit{Drosophila melanogaster} $\backslash\backslash$ Courtesy of Elizabeth Wilder}
    \end{tcolorbox}    	
\end{column}\hspace{0.1cm}

\begin{column}{0.57\textwidth}
    \begin{exampleblock}{Advantages}
    \begin{itemize}
        \item It has served as a genetic model for more than a century (Thomas Morgan, 1908) 
        \item  Ease culturing, low maintenance cost, and robustness against plagues and pathogens
        \item Rapid development. Under standard laboratory conditions (25$^{\circ}$) the whole life cycle does not take longer than 10 days
    \end{itemize}
    \end{exampleblock}
\end{column}
\end{columns}
\end{frame}

%--------------------------------------------------------
\begin{frame}{Introduction}{Drosophila oogenesis: the development of a cell into a mature oocyte}
\begin{columns}[onlytextwidth]
\begin{column}{0.4\textwidth}
    \begin{tcolorbox}[colback=white,colframe=fgsubtxt,valign=center,center upper,center lower]
    \includegraphics[width=0.85\textwidth,keepaspectratio]{oogenesis}
    \tcblower
    {\tiny Ovariole during \textit{Drosophila} oogenesis}
    \end{tcolorbox}    	
\end{column}\hspace{0.1cm}

\begin{column}{0.58\textwidth}
    \begin{exampleblock}{Highlights}
    \begin{itemize}
        \item A female \textit{Drosophila} has two ovaries each containing 15-20 ovarioles 
        \item  Stem cells are divided asymmetrically to produce 15 nurse cells and an oocyte, which are surrounded by somatic follicle cells
        \item 14 stages: 1 to 6 (early stages); 7 to 10 (mid stages); and stages from 11 to 14 (late oogenesis)
    \end{itemize}
    \end{exampleblock}
\end{column}
\end{columns}
\end{frame}

%--------------------------------------------------------
%--------------------------------------------------------
\section{Supertexton segmentation \label{supertexton_segmentation}}

%--------------------------------------------------------
\begin{frame}{Supertexton segmentation}
\begin{overlayarea}{\textwidth}{0.2\textheight}
\begin{block}{}
We focus our attention on texture because it is
a property of images and may reflect local variations that define shapes or objects
\end{block}
\end{overlayarea}

\begin{overlayarea}{\textwidth}{0.9\textheight}
\only<2->{
\begin{columns}[onlytextwidth]
\begin{column}{0.4\textwidth}
    \begin{tcolorbox}[colback=white,colframe=fgsubtxt,valign=center,center upper,center lower]
    \includegraphics[width=0.75\textwidth,keepaspectratio]{egg_chambers}
    \tcblower
    {\tiny Examples of localized ovaries during oogenesis}
    \end{tcolorbox}    	
\end{column}\hspace{0.1cm}

\begin{column}{0.57\textwidth}
\begin{exampleblock}{}
    \begin{enumerate}
    \item We selected 54 early-stage egg chambers from 3D-stacks of size 647 x 1024 x 24 captured using an optical sectioning fluorescence microscope
    \item Only the green channel was considered 
    \item Isolated egg chambers were obtained manually
    \end{enumerate}
\end{exampleblock}
\end{column}
\end{columns}
}
\end{overlayarea}
\end{frame}

%--------------------------------------------------------
\begin{frame}{Supertexton segmentation}{The proposal: a supervised method$^*$}
\centering
\begin{columns}[onlytextwidth]
\begin{column}{0.55\textwidth}
\begin{algorithm}[H]
    \SetAlgoLined
    %\KwIn{I := A set of input image}
    %\KwOut{SI := Segmented images}
    \Begin{
    Seg := Pre-segmentation with SLIC superpixels\;
    Dic := Construction of a supertexton dictionary\;
    Class := Classification\;
    }
    \caption{Early-stage Drosophila egg chamber segmentation}
\end{algorithm}

\begin{block}{}
{\scriptsize $^*$Nava, R. and Kybic, J. ``Supertexton-based segmentation in early \textit{Drosophila} oogenesis,'' submitted to ICIP 2015}
\end{block}{}
\end{column}\hspace{0.1cm}

\begin{column}{0.4\textwidth}
\begin{tcolorbox}[colback=white,colframe=fgsubtxt,valign=center,center upper,center lower]
\includegraphics[width=0.52\textwidth,keepaspectratio]{proposal_a}\vspace{0.1cm}
\includegraphics[width=0.52\textwidth,keepaspectratio]{proposal_b}
\tcblower
{\tiny Egg chamber segmented with our proposal}
\end{tcolorbox}   
\end{column}
\end{columns}
\end{frame}

%--------------------------------------------------------
%--------------------------------------------------------
\subsection{Superpixels \label{superpixels}}

%--------------------------------------------------------
\begin{frame}[shrink=5]{Supertexton segmentation}{Superpixels \cite{ACHANTA2012}}
\centering
\only<1>{
\begin{columns}[onlytextwidth]
\begin{column}{0.55\textwidth}
\begin{alertblock}{}
\begin{itemize}
	\item For each seed, $\{C_{i}|i=1,\,\ldots,\,K\}$, a region is calculated with \textit{k}-means
    \item $K = \frac{MN}{S^{2}}$ where $M$ and $N$ are the width and height of the image, respectively
    \item Distance $D$ considers only gray values, $l$, between $C_{k}$ and a pixel $i$ at positions $[x,y]$
\end{itemize}
\end{alertblock}
\begin{block}{}
\begin{equation}
\begin{aligned}
    d_{c} &= \sqrt{\left(l_{k}-l_{i}\right)^{2}} \\
    d_{s} &= \sqrt{\left(x_{k}-x_{i}\right)^{2} + \left(y_{k}-y_{i}\right)^{2}}\\
    D &= \sqrt{d^2_c + \left(\frac{d_s}{S}\right)^2m^2}\notag
\end{aligned}
\end{equation}
\end{block}
\end{column}\hspace{0.1cm}

\begin{column}{0.4\textwidth}
\begin{tcolorbox}[colback=white,colframe=fgsubtxt,valign=center,center upper,center lower]
\includegraphics[width=0.85\textwidth,keepaspectratio]{slic}
\tcblower
{\tiny SLIC computes \textit{k}-means clustering using distance $D$ in a $2S\times 2S$ region around $C_{i}$}
\end{tcolorbox}   
\end{column}
\end{columns}
}
\only<2>{
\begin{alertblock}{Egg chambers segmented with SLIC:  $S = 10$, $S = 20$, and $S = 30$}
\hspace*{\fill}
\includegraphics[width=0.3\textwidth,keepaspectratio]{slic10}\hfill
\includegraphics[width=0.3\textwidth,keepaspectratio]{slic20}\hfill
\includegraphics[width=0.3\textwidth,keepaspectratio]{slic30}
\hspace*{\fill}
\end{alertblock}
}
\end{frame}

%--------------------------------------------------------
%--------------------------------------------------------
\subsection{Supertexton dictionary \label{supertexton_dictionary}}

\begin{frame}{Supertexton segmentation}{Supertexton dictionary (I)}
\centering
\begin{columns}[onlytextwidth]
\begin{column}{0.56\textwidth}
\begin{alertblock}{}
\begin{itemize}
	\item Our proposal was inspired from \cite{VARMA2005}
    \item Reduce set of the Leung-Malik filter bank ($LM_{18}$)
    \item Maximum response across $\{0^{\circ}, 30^{\circ}, 60^{\circ}, 90^{\circ}, 120^{\circ}, 150^{\circ}\}$ on first and second Derivatives of Gaussian (DoG)
    \item Rotational invariance along three dyadic scales $\{\sqrt{2}, 2, 2\sqrt{2}\}$
    \item Eight Laplacian of Gaussian and four Gaussian filters with standard deviation $\sigma = \left\{\sqrt{2}, 2, 2\sqrt{2}, 4\right\}$
\end{itemize}
\end{alertblock}
\end{column}\hspace{0.1cm}

\begin{column}{0.4\textwidth}
\begin{block}{}
\begin{figure}
\begin{center}
\subfigure[Firts DoG]{\includegraphics[width=0.48\textwidth,keepaspectratio]{filter_a}}\hspace{0.1cm}
\subfigure[Second DoG]{\includegraphics[width=0.48\textwidth,keepaspectratio]{filter_b}}\\
\subfigure[Gaussians]{\includegraphics[width=0.48\textwidth,keepaspectratio]{filter_c}}
\end{center}
\end{figure}
\end{block}
\end{column}
\end{columns}
\end{frame}

%--------------------------------------------------------
\begin{frame}[shrink=33]{Supertexton segmentation}{Supertexton dictionary (II)}
\centering
\begin{columns}[onlytextwidth]
\begin{column}{0.51\textwidth}
\begin{alertblock}{}
\begin{itemize}
	\item Let $T(x,y)$ be a segmented texture with SLIC and given a filter bank, $LM_{18}$ 
    \item The responses $F_i$ are computed as follows:
\begin{equation}
\begin{aligned}
   \hat{F_{i}} &= T(x,y) \otimes LM_i\\
    LN &= \sqrt{\sum{\left(\hat{F_i}^2\right)}}\\
    F_{i} &= \frac{\log\left(1+LN\right)}{0.03LN}\hat{F_i} \notag
\end{aligned}
\end{equation}
where $\otimes$ means convolution and $LN$ is a contrast normalization
\end{itemize}
\end{alertblock}

\begin{tcolorbox}[colback=white,colframe=fgsubtxt,valign=center,center upper,center lower]
\includegraphics[width=0.6\textwidth,keepaspectratio]{textons}
\tcblower
{\tiny Color dots represent superpixels from the same class}
\end{tcolorbox} 
\end{column}\hspace{0.1cm}

\begin{column}{0.46\textwidth}
\only<2>{
\begin{alertblock}{$\forall$ {\color{black}superpixel} (color dot)  $\in F_{i}$ compute:}
\begin{itemize}
    \setlength{\itemsep}{0pt}
	\item Mean: $\mu_{i} = \displaystyle\frac{1}{N_{I}}\sum I_{F_{i}}$, where $I_{F_{i}}$ are the intensity values $\in F_{i}$  within the corresponding superpixel region and $N_{I}$ the number of intensity values.  
    \item Standard deviation: $\sigma_{i} = \sqrt{\frac{1}{N_{I}}\sum\left(I_{F_{i}}-\mu_{i}\right)^2}$
    \item Energy: $E_{i} = \frac{1}{N_{I}}\displaystyle\sum I_{F_{i}}^{2}$
    \item Average gradient: $G_{i} = \displaystyle\sum\left\|\nabla I_{F_{i}}\right\|$  
\end{itemize}
\end{alertblock}

\begin{block}{The feature vector $\tilde{f}$ is build as follows:}
\begin{equation}
   \overline{f} = \big[\tilde{\mu_{1}},\,\tilde{\sigma_{1}},\,\tilde{E_{1}},\,\tilde{G_{1}},\,\ldots,\,\tilde{\mu_{18}},\,\tilde{\sigma_{18}},\,\tilde{E_{18}},\,\tilde{G_{18}}\big]\notag
\end{equation}

We assume features are normally distributed then
\begin{equation}
   \tilde{f} = \frac{\overline{f}-\mu_{\overline{f}}}{2\left(3\sigma_{\overline{f}}+1\right)}\notag
\end{equation}
\end{block}
}
\end{column}
\end{columns}
\end{frame}

%--------------------------------------------------------
\begin{frame}{Supertexton segmentation}{Supertexton dictionary (III)}
\centering
\begin{columns}[onlytextwidth]
\begin{column}{0.65\textwidth}
\onslide<1->{
\begin{alertblock}{Supertextons}
\begin{itemize}
	\item For each class: \textit{k}-means is applied to obtain representative vectors called \textbf{supertextons}
    \item The dictionary consists of $C \times k$ supertextons. In our experiments, $k$ is the $10\%$ of the total of the feature vectors per class
\end{itemize}
\end{alertblock}
}
\onslide<2->{
\begin{exampleblock}{Classification}
We used \textit{k}-NN classifier with $k=6$ which approximately represents less than $1\%$ of the vectors in our dataset and assigned the input feature vector to the class with the highest number of neighbors
\end{exampleblock}
}
\end{column}\hspace{0.1cm}

\begin{column}{0.3\textwidth}
\begin{tcolorbox}[colback=white,colframe=fgsubtxt,valign=center,center upper,center lower]
\includegraphics[width=\textwidth,keepaspectratio]{supertexton}
\end{tcolorbox} 
\end{column}
\end{columns}
\end{frame}

%--------------------------------------------------------
%--------------------------------------------------------
\subsection{Experiments\label{supertexton_dictionary}}

%--------------------------------------------------------
\begin{frame}{Experiments}
\centering
\only<1>{
\begin{block}{}
\begin{table}
\rowcolors{5}{lightgray}{}
\renewcommand{\arraystretch}{1.3}
\caption{All data are expressed in $(\%)$. The values between parenthesis represent standard deviations}
\centering
\begin{tabular*}{0.72\textwidth}{cccc}
\toprule
& \multicolumn{3}{c}{Supertextons} \\
\cmidrule[0.5pt](l{5pt}r{5pt}){2-4}
Class & PR & SE & $F_{1}$-Score \\
\hline
\hline
Background & 95.98 ($\pm$ 3.96) & 90.99 ($\pm$ 8.25) & \textbf{93.15} ($\pm$ 4.90) \\
Follicle cells & 82.90 ($\pm$ 6.50) & 79.14 ($\pm$ 7.17) & \textbf{80.66} ($\pm$ 4.66) \\
Nurse cells & 79.64 ($\pm$ 9.52) & 85.22 ($\pm$ 5.03) & \textbf{81.87} ($\pm$ 4.99) \\
Cytoplasm & 80.18 ($\pm$ 10.06) & 86.68 ($\pm$ 4.46) & \textbf{82.84} ($\pm$ 5.75) \\
\bottomrule
\end{tabular*}
\end{table}
\end{block}
}
\only<2>{
\begin{block}{\small\textit{Drosophila} egg chamber segmentation. \textbf{(a)} the original egg chamber; \textbf{(b)} the corresponding ground-truth; \textbf{(c)} supertexton segmentation}
\begin{figure}
    \centering
    \subfigure{\includegraphics[width=0.12\textwidth]{ex_1a}}
    \subfigure{\includegraphics[width=0.12\textwidth]{ex_1b}}
    \subfigure{\includegraphics[width=0.12\textwidth]{ex_1c}}\\
    \subfigure{\includegraphics[width=0.12\textwidth]{ex_2a}}
    \subfigure{\includegraphics[width=0.12\textwidth]{ex_2b}}
    \subfigure{\includegraphics[width=0.12\textwidth]{ex_2c}}\\
    \setcounter{subfigure}{0}
    \subfigure[]{\includegraphics[width=0.12\textwidth]{ex_3a}}
    \subfigure[]{\includegraphics[width=0.12\textwidth]{ex_3b}}
    \subfigure[]{\includegraphics[width=0.12\textwidth]{ex_3c}}
\end{figure}
\end{block}
}
\end{frame}

%--------------------------------------------------------
%--------------------------------------------------------
\section{Ray feature localization \label{ray_features_localization}}

%--------------------------------------------------------
\begin{frame}[shrink=30]{Ray feature localization}{An algorithm for describing 2D shapes (I) \cite{SMITH2009}}
\centering
\begin{columns}[onlytextwidth]
\begin{column}{0.6\textwidth}
\only<1>{
\begin{alertblock}{}
\begin{itemize}
    \item Ray features are a set of four features for recognizing irregular shapes
    \item Basic function: given an image $I$, a point $C$, and the direction $\theta$\\
    \begin{equation}
    l = f(I,\,C,\,\theta)\notag
    \end{equation}
    returns the location $l$ of the nearest contour point in $I$ to $C$ in the direction of $\theta$
\end{itemize}
\end{alertblock}
}
\only<2>{
\begin{alertblock}{Features:}
\begin{itemize}
    {\small   
    \item Distance feature:\\
    \begin{equation}
    \begin{aligned}
    f_{dist}\left(C_{1},\,\theta_{1}\right) &= \left\|f(I,C_{1},\theta_{1})-C_{1}\right\|\\
    &= d_1\notag
    \end{aligned}
    \end{equation}
    \item Distance different feature:\\
    \begin{equation}
    \begin{aligned}
     f_{diff}\left(C_{1},\,\theta_1,\theta_2\right) &= \frac{\left\|f(I,C_{1},\theta_1)-C_{1}\right\|-\left\|f(I,C_{1},\theta_2)-C_{1}\right\|}{\left\|f(I,C_{1},\theta_1)-C_{1}\right\|}\\
    &= \frac{d1-d2}{d1}\notag
      \end{aligned}
    \end{equation}
    \item Orientation feature: the orientation of G
    \begin{equation}
     f_{ori}\left(C_{1},\,\theta_1\right) = \frac{\nabla I\left(f(I,C_{1},\theta_1)\right)}{\left\|\nabla I\left(f(I,C_{1},\theta_1)\right)\right\|}\left(\cos\theta_1,\,\sin\theta_{1}\right)\notag
    \end{equation}
    \item Norm feature: the strength of G
    \begin{equation}
     f_{norm}\left(C_{1},\,\theta_1\right) = \left\|\nabla I\left(f(I,C_{1},\theta_1)\right)\right\|\notag
    \end{equation}
}   
\end{itemize}
\end{alertblock}
}
\end{column}\hspace{0.1cm} 

\begin{column}{0.38\textwidth}
\only<2>{
\begin{tcolorbox}[colback=white,colframe=fgsubtxt,valign=center,center upper,center lower]
\includegraphics[width=\textwidth,keepaspectratio]{ray_features}
\tcblower
{\tiny Ray features: we do not use red features}
\end{tcolorbox} 
}
\end{column}
\end{columns}
\end{frame}

%--------------------------------------------------------
\begin{frame}{Ray feature localization}{An algorithm for describing 2D shapes (II)}
\begin{alertblock}{The initial proposal}
A feature vector, $\overline{R}$, is build by concatenating $f_{diff}$ values at steps of $30$ degrees as follows: 
\begin{equation}
    \overline{R} = \left[f^{0^\circ}_{diff},\, f^{30^\circ}_{diff},\,\ldots,\,f^{330^\circ}_{diff}\right]\notag
\end{equation}
\begin{center}
\includegraphics[width=0.4\textwidth,keepaspectratio]{ray_features_drosophila}
\end{center}
\end{alertblock}
\end{frame}

%--------------------------------------------------------
%--------------------------------------------------------
\section{Conclusions \& further work \label{conclusions_further_work}}

%--------------------------------------------------------
\begin{frame}{Conclusions \& further work}
\begin{exampleblock}{Supertexton segmentation}
\begin{itemize}
	\item Although segmentation works well, we need to use a more powerful classifier: SVM, adaboost?
    \item Training sets by developing stage
    \item To publish!!
\end{itemize}
\end{exampleblock}

\begin{exampleblock}{Ray feature localization}
\begin{itemize}
	\item Include histogram information: so far, it produces sparse histograms
	\item HOG or Haar features?
    \item Change 2D version to 3D
    \item Include shape models or joint models
    \item To publish!!
\end{itemize}
\end{exampleblock}
\end{frame}
