function [predicted_stage]=predictor(fname)
%This function predicts the stage of the egg present in the slice whose
%path is entered.
%   'fname' is the path of the slice and 'predicted stage' is the stage
%   predicted.This function used already trained and generated decision
%   forest classfier to do so.WHere forestPath variable leads to the
%   Decision Forest file generated while training.
addpath ..\final_classifier
[area ratio oocyte_size distance]=feature_extraction(fname);
features=[area ratio oocyte_size distance];
close all
addpath ..\DecisionForest_v3.1\code
forestPath='..\forest';
[predicted_stage]=RunDecisionForest(features,forestPath);
end
