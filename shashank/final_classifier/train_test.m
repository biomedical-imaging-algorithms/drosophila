%%this script is used for training and testing purpose 
%%It uses both Decision Forest and tree but decison forest gave better result so 
% the final classifier was generated using the decsion forest.

myFolder = '..\final_classifier';
if ~isdir(myFolder)
  errorMessage = sprintf('Error: The following folder does not exist:\n%s', myFolder);
  uiwait(warndlg(errorMessage));
  return;
end
filePattern = fullfile(myFolder, '*.csv'); 
theFiles = dir(filePattern);
for k = 1 %: length(theFiles)
  baseFileName = theFiles(k).name;
  %trainign data
  fullFileName = fullfile(myFolder, 'train.csv');
  fprintf(1, 'Now reading %s\n', fullFileName);
  M = csvread(fullFileName);
  N=size(M,1);
  %loading training data_x
  x_train = csvread(fullFileName,0,0,[0 0 N-1 3]);
  x_train2=x_train;
  x_train2(:,1)=log(x_train2(:,1));
  y_train = csvread(fullFileName,0,4,[0 4 N-1 4]);
  %give decision forest folder apth
  addpath ..\DecisionForest_v3.1\code
  depth=5; % tree depth
  noc=1000; % number of candidates at each tree node
  treeFile='tree.txt'; % tree file name
  treeFile2='tree2.txt';
  forestSize=500; % how many trees does the forest contain
  forestPath='forest'; % forest directory name
  forestPath2='forest2';
  %train decision forest
  TrainDecisionForest(x_train,y_train,forestPath,forestSize,depth,noc);
  TrainDecisionForest(x_train2,y_train,forestPath2,forestSize,depth,noc);
  %train decision tree
  TrainDecisionTree(x_train,y_train,treeFile,depth,noc);
  TrainDecisionTree(x_train2,y_train,treeFile2,depth,noc)
  %test decision forest
  fullFileName=fullfile(myFolder, 'test.csv');
  fprintf(1, 'Now reading %s\n', fullFileName);
  M = csvread(fullFileName);
  N=size(M,1);
  %loading testing data_x
  x_test = csvread(fullFileName,0,0,[0 0 N-1 3]);
  y_test = csvread(fullFileName,0,4,[0 4 N-1 4]);
  x_test2=x_test;
  x_test2(:,1)=log(x_test2(:,1));
  %testing the data
  addpath ..\DecisionForest_v3.1\code
  [y1_test,P]=RunDecisionForest(x_test,forestPath);
  [yy1_test,P]=RunDecisionForest(x_test2,forestPath2);
  %testing the data for decision tree
  [y11_test,P1]=RunDecisionTree(x_test,treeFile);
  [yy11_test,P1]=RunDecisionTree(x_test2,treeFile2);
  % y1_test=y1_test-1;
  %calculating error
  error=sum(y1_test~=y_test);% for decision forest
  errore=sum(yy1_test~=y_test);%for log of area column in forest
  error1=sum(y11_test~=y_test);%for decision tree
  error1e=sum(yy11_test~=y_test);%log of area column in tree
  accuracy=1-error/length(y_test);
  accuracye=1-errore/length(y_test);
  
  accuracy1=1-error1/length(y_test);
  accuracy1e=1-error1e/length(y_test);
  %save the result
  C = horzcat(y_test,y1_test);
  Ce=horzcat(y_test,yy1_test);
  C1= horzcat(y_test,y11_test);
  C1e= horzcat(y_test,yy11_test);
  dlmwrite('..\decision_forest_result1.csv',C,'-append','delimiter',',','roffset',0);
  dlmwrite('..\decision_tree_result1.csv',C1,'-append','delimiter',',','roffset',0);
end
S = 'Hello World.';