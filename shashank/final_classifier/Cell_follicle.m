function [I1]=Cell_follicle(segout, I0, major, u, t3)
%Calculates follicle cell mask for the particular 2D slice.
%% cut out the inner cells. That may contains partial of the inner cell
addpath InsidePolyFolder
if(~exist('t3','var'))
   t3=10;
end
k_shrink=major./(t3*u);
percent=(3/t3);

[inner(:,2),inner(:,1)]=find(segout==50);
[coeff,score,latent] = princomp(inner);

[boundary(:,2),boundary(:,1)]=find(segout==255);
centerlized=boundary-repmat(mean(inner), size(boundary,1),1);
length=sqrt(sum(centerlized.^2,2));
inner_boundary=(centerlized./repmat(length,1,2)).*repmat(length-min(percent*length, k_shrink),1,2)+repmat(mean(boundary), size(boundary,1),1);
theta=orderpoints(inner_boundary, mean(boundary), coeff);
[trash, index]=sort(theta,'ascend');
xv = [inner_boundary(index,1) ; inner_boundary(index(1),1)]; yv = [inner_boundary(index,2) ; inner_boundary(index(1),2)];
IN = insidepoly(inner(:,1),inner(:,2), xv,yv);

I1=I0;

I1(sub2ind(size(I1),inner(IN,2),inner(IN,1)))=0;

end

