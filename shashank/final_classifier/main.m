tic
%This main function is build to iterate over all the slices related to different stages one by one. In this way this generates
%.csv file full of features used for classification
%% start wth stage 2
myFolder = '..\latest_slices\stage2';
if ~isdir(myFolder)
  errorMessage = sprintf('Error: The following folder does not exist:\n%s', myFolder);
  uiwait(warndlg(errorMessage));
  return;
end
filePattern = fullfile(myFolder, '*.tif'); 
theFiles = dir(filePattern);
for k = 1 : length(theFiles)
  baseFileName = theFiles(k).name;
  fullFileName = fullfile(myFolder, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
  [area ratio oocyte_size distance]=feature_extraction(fullFileName);%oocyte and distance not calcultaed for stages lesser than 6(i.e. 1-5)
  if (size(oocyte_size,2)==2)||isnan(oocyte_size)
      oocyte_size=0;
  end
  if (size(distance,2)==2)||isnan(distance)
      distance=0;
  end
  if (size(area,2)==2)||isnan(area)
      area=0;
  end
  
  stage_actual=2;
  dlmwrite('..\latest_slices\stage2.csv',[area ratio oocyte_size distance stage_actual k],'-append','delimiter',',','roffset',0);
  close all
  
end
S = 'Hello World.';
%% Starting for Stage 3
myFolder = '..\latest_slices\stage3';
if ~isdir(myFolder)
  errorMessage = sprintf('Error: The following folder does not exist:\n%s', myFolder);
  uiwait(warndlg(errorMessage));
  return;
end
filePattern = fullfile(myFolder, '*.tif'); 
theFiles = dir(filePattern);
for k = 1 : length(theFiles)
  baseFileName = theFiles(k).name;
  fullFileName = fullfile(myFolder, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
  [area ratio oocyte_size distance]=feature_extraction(fullFileName);%oocyte and distance not calcultaed for stages lesser than 6(i.e. 1-5)
  if (size(oocyte_size,2)==2)||isnan(oocyte_size)
      oocyte_size=0;
  end
  if (size(distance,2)==2)||isnan(distance)
      distance=0;
  end
  if (size(area,2)==2)||isnan(area)
      area=0;
  end
  
  stage_actual=3;
  dlmwrite('..\latest_slices\stage3.csv',[area ratio oocyte_size distance stage_actual k],'-append','delimiter',',','roffset',0);
  close all
  
end
S = 'Hello World.';
%% For stage 4
myFolder = '..\latest_slices\stage4';
if ~isdir(myFolder)
  errorMessage = sprintf('Error: The following folder does not exist:\n%s', myFolder);
  uiwait(warndlg(errorMessage));
  return;
end
filePattern = fullfile(myFolder, '*.tif'); 
theFiles = dir(filePattern);
for k = 1 : length(theFiles)
  baseFileName = theFiles(k).name;
  fullFileName = fullfile(myFolder, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
  [area ratio oocyte_size distance]=feature_extraction(fullFileName);%oocyte and distance not calcultaed for stages lesser than 6(i.e. 1-5)
  if (size(oocyte_size,2)==2)||isnan(oocyte_size)
      oocyte_size=0;
  end
  if (size(distance,2)==2)||isnan(distance)
      distance=0;
  end
  if (size(area,2)==2)||isnan(area)
      area=0;
  end
  stage_actual=4;
  dlmwrite('..\latest_slices\stage4.csv',[area ratio oocyte_size distance stage_actual k],'-append','delimiter',',','roffset',0);
  close all
  
end
S = 'Hello World.';
%% For stage 5
myFolder = '..\latest_slices\stage5';
if ~isdir(myFolder)
  errorMessage = sprintf('Error: The following folder does not exist:\n%s', myFolder);
  uiwait(warndlg(errorMessage));
  return;
end
filePattern = fullfile(myFolder, '*.tif'); 
theFiles = dir(filePattern);
for k = 1 : length(theFiles)
  baseFileName = theFiles(k).name;
  fullFileName = fullfile(myFolder, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
  [area ratio oocyte_size distance]=feature_extraction(fullFileName);%oocyte and distance not calcultaed for stages lesser than 6(i.e. 1-5)
  if (size(oocyte_size,2)==2)||isnan(oocyte_size)
      oocyte_size=0;
  end
  if (size(distance,2)==2)||isnan(distance)
      distance=0;
  end
  if (size(area,2)==2)||isnan(area)
      area=0;
  end
 
  stage_actual=5;
  dlmwrite('..\latest_slices\stage5.csv',[area ratio oocyte_size distance stage_actual k],'-append','delimiter',',','roffset',0);
  close all
  
end
disp(S);
toc