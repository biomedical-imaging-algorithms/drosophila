Copyright (C) 2013 Quan Wang <wangq10@rpi.edu>, 
Signal Analysis and Machine Perception Laboratory, 
Department of Electrical, Computer, and Systems Engineering, 
Rensselaer Polytechnic Institute, Troy, NY 12180, USA

You are free to use this software for academic purposes if you cite our papers: 
[1] Quan Wang, Yan Ou, A. Agung Julius, Kim L. Boyer and Min Jun Kim, 
    "Tracking Tetrahymena Pyriformis Cells using Decision Trees", 
    2012 21st International Conference on Pattern Recognition (ICPR), 
    Pages 1843-1847, 11-15 Nov. 2012.
[2] Quan Wang, Dijia Wu, Le Lu, Meizhu Liu, Kim L. Boyer, and Shaohua 
    Kevin Zhou, "Semantic Context Forests for Learning-Based Knee 
    Cartilage Segmentation in 3D MR Images", 
    MICCAI 2013: Workshop on Medical Computer Vision. 

For commercial use, please contact the authors. 


