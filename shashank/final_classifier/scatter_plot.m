%This function here works as the scatter plotter for various features
%obtained after iterating over the slcies of various stages.Some one can
%experiment more in this sense.(sense of plotting)
clc
addpath ..\final_code_2d
myFolder = '..\final_code_2d';
if ~isdir(myFolder)
  errorMessage = sprintf('Error: The following folder does not exist:\n%s', myFolder);
  uiwait(warndlg(errorMessage));
  return;
end
filePattern = fullfile(myFolder, '*.csv'); 
theFiles = dir(filePattern);
%% stage 2 data
fullFileName = fullfile(myFolder,'stage2.csv');
fprintf(1, 'Now reading %s\n', fullFileName);
M = csvread(fullFileName);
%loading data to make plots
area2=M(:,1);
ratio2=M(:,2);
oocyte2=M(:,3);
follicle2=M(:,4);
actual_stage2=M(:,5);
k2=M(:,6); %filenumber for the particular stage in the dataset
%% Stage 3 data
fullFileName = fullfile(myFolder,'stage3.csv');
fprintf(1, 'Now reading %s\n', fullFileName);
M = csvread(fullFileName);
%loading data to make scatter plot
area3=M(:,1);
ratio3=M(:,2);
oocyte3=M(:,3);
follicle3=M(:,4);
actual_stage3=M(:,5);
k3=M(:,6);
%% stage 4 data
fullFileName = fullfile(myFolder,'stage4.csv');
fprintf(1, 'Now reading %s\n', fullFileName);
M = csvread(fullFileName);
%loading data to make scatter plot
area4=M(:,1);
ratio4=M(:,2);
oocyte4=M(:,3);
follicle4=M(:,4);
actual_stage4=M(:,5);
k4=M(:,6);

%% stage 5 data
fullFileName = fullfile(myFolder,'stage5.csv');
fprintf(1, 'Now reading %s\n', fullFileName);
M = csvread(fullFileName);
%loading data to make scatter plot
area5=M(:,1);
ratio5=M(:,2);
oocyte5=M(:,3);
follicle5=M(:,4);
actual_stage5=M(:,5);
k5=M(:,6);

%% Concatenation of stage 2 , 3 , 4 and 5 matrices 
area = vertcat(area2,area3,area4,area5);
ratio = vertcat(ratio2,ratio3,ratio4,ratio5);
oocyte = vertcat(oocyte2,oocyte3,oocyte4,oocyte5);
actual_stage = vertcat(actual_stage2,actual_stage3,actual_stage4,actual_stage5);
follicle = vertcat(follicle2,follicle3,follicle4,follicle5);
k=vertcat(k2,k3,k4,k5);

%% boxplots
figure(999),boxplot(area,actual_stage);
xlabel('stage'),ylabel('egg chamber area');
figure(1009),boxplot(oocyte,actual_stage);
xlabel('stage'),ylabel('oocyte_size');
figure(1012),boxplot(ratio,actual_stage);
xlabel('stage'),ylabel('ratio');
figure(1013),boxplot(follicle,actual_stage);
xlabel('follicle'),ylabel('actual stage');
logarea=log(area);
logratio=log(ratio);

%% multiclass scatter plot
figure(234),gplotmatrix(area,ratio,actual_stage);
xlabel('area'),ylabel('ratio');%notice it should be properly classified
figure(235),gplotmatrix(logarea,ratio,actual_stage);
xlabel('logarea'),ylabel('ratio');%notice it should be properly classified
figure(236),gplotmatrix(logarea,logratio,actual_stage);
xlabel('logarea'),ylabel('logratio');%notice it should be properly classified
%similarly other variants can also be plotted
S = 'Hello World.';