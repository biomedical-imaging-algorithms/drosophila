function [area ratio oocyte_size distance]=feature_extraction(fname)
%This function is used to generate features for the current slice.
% It uses various morphological operations and PCA to get these features
% out for the particular slice.

% get the area
[major, minor, area, segout, u, I0, seg, boundary]=Cell_area_convex (fname,K,0);
ratio=major./minor;
oocyte_size='NA';
distance='NA';
% chenvese segmentation on the cleaned image
addpath('..\chanvese')
t3=3e-3;
seg = chenvese(imadjust(I0),'whole',1000,t3,'chan');
if numel(find(seg==1))>numel(find(seg==0))
    seg=~seg;
end
figure;
imshow(seg);

%get oocyte percentage        
       
[ coeff,oocyte_size]=Cell_orientation(segout, seg, major, u);
 % get the follicle cell
[I1]=Cell_follicle(segout, seg, major, u, 11);
figure;
imshow(I1);
title('segmentation mask for follicle cells')
figure;
imshow(~I1&seg);
title('segmentation mask for nurse cells')
[distri]=Cell_follicledistribution(segout, I1,coeff);
figure;
bar(distri)
distri_norm=distri./sum(distri);
%delta distance
distance=sum((distri_norm-ones(size(distri))./sum(ones(size(distri)))).^2./...
    (distri_norm+ones(size(distri))./sum(ones(size(distri)))));
fprintf('This delta distance of follicle cell distribution is %4.2f \n',distance);

end












