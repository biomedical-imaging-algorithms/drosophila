#!/bin/bash
cd ~/drosophilla/experiments
mkdir res
cd res
FILE=$(date +"%m_%d_%Y_%H_%M_%S")
mkdir $FILE
cd ..
cp * res/$FILE
cd res/$FILE
qsub -l walltime=5d -l mem=10000mb -l scratch=400mb -l nodes=1:ppn=1:brno ./run_meta.sh

