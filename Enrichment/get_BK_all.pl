#!/usr/bin/perl
use DBI;
use strict;

open my $FILEF, "<", "drosophilla.f" or die $!;
open my $FILEN, "<", "drosophilla.n" or die $!;


my $dsn = "DBI:mysql:database=go_latest;host=mysql-amigo.ebi.ac.uk;port=4085";
my $dbh = DBI->connect($dsn, "go_select", "amigo") or die "Connection Error: $DBI::errstr\n";

#localhost
#my $dsn = "DBI:mysql:database=drosophilla;host=localhost;port=3306";
#my $dbh = DBI->connect($dsn, "root", "") or die "Connection Error: $DBI::errstr\n";

print "connecting....\n";

my $query_prepare = 
"SELECT DISTINCT
child.name,
child.term_type as ontology,
child.acc AS child_acc,
gene_product.symbol,
gene_product.full_name,
dbxref.xref_key AS FBgn_id,
child.name
FROM
term AS child
INNER JOIN association ON (association.term_id = child.id)
INNER JOIN gene_product ON (association.gene_product_id = gene_product.id)
INNER JOIN dbxref ON (dbxref.id=gene_product.dbxref_id)
INNER JOIN term2term ON (term2term.term2_id = child.id)
WHERE
dbxref.xref_key =  ";


my $query_find_all_go =
"
SELECT *
FROM term
WHERE term.acc LIKE 'GO:%'
ORDER BY term.acc
";

my $query_transitive_closure =
"
SELECT  *  
FROM
(SELECT 
term.term_type AS term_type,
term.acc AS acc_child,
graph_path.relation_distance AS relation_distance,
rel.acc AS rel_acc,
term2.term_type AS term_type_child,
term2.acc AS acc_ancestor
 FROM
 term  
 INNER JOIN graph_path ON (term.id=graph_path.term2_id)
INNER JOIN term AS rel ON (rel.id=relationship_type_id)
INNER JOIN term AS term2 ON (term2.id=graph_path.term1_id)
WHERE
term2.acc LIKE 'GO:%'
AND
graph_path.distance != 0
AND
 term.acc= ?
ORDER BY graph_path.relation_distance
) as tmp
GROUP BY tmp.acc_ancestor
ORDER BY  tmp.rel_acc DESC
";

sub parseSQL
{
	my($ref, $BK) = @_;
	
	#parse TERM_TYPE
	my $term_type = $$ref->{'ontology'};
	$term_type =~ /[a-zA-Z]+_([a-zA-Z]+)/;
	$term_type = $1;
	
	#TERM_GO_ID identificater GO for term information
	my $acc_term = $$ref->{'child_acc'};
	my $gene_product_id = $$ref->{'FBgn_id'};
	
	
	$$BK .= "$term_type('$gene_product_id', '$acc_term').\n";
	 
}

sub parseSQL_rel
{
	my($ref, $BK) = @_;
	
	#parse TERM_TYPE
	my $term_type = $$ref->{'term_type'};
	$term_type =~ /[a-zA-Z]+_([a-zA-Z]+)/;
	$term_type = $1;
	
	#TERM_GO_ID identificater GO for term information
	my $acc_term1 = $$ref->{'acc_child'};
	my $acc_term2 = $$ref->{'acc_ancestor'};
	my $relationship = $$ref->{'rel_acc'};
	
	#$acc_term1 = lc $acc_term1;
	#$acc_term2 = lc $acc_term2;
	
	$$BK .= "$relationship('$acc_term1', '$acc_term2').\n";
	 
}

#***********************************************************************
#get domains (component, process and function) for positive and negative examples

sub get_BK
{
	my ($file) = @_;
	my $background_knowledge;
	while(my $line = <$file>)
	{
		my $i = 1;
		if($line =~ /expressed\('([a-zA-Z0-9]+)'\)/)
		{
			my $gene_product_id = $1;

			my $sth = $dbh->prepare("$query_prepare '$gene_product_id'");
			$sth->execute();
			my $count_row = $sth->rows;
			while (my $ref = $sth->fetchrow_hashref())
			{
				print "[$i\/$count_row] getting $gene_product_id ...\n";
				&parseSQL(\$ref, \$background_knowledge);
				$i++;
			}
			$background_knowledge .= "\n";
			$sth->finish();			
		}
	}
return $background_knowledge;
}

#***********************************************************************
#get all relations (is_a, part_of, has_part, regulates) with transitive closure

sub get_ontology_relations
{
	my $background_knowledge;
	my $i = 0;

	my $sth = $dbh->prepare("$query_find_all_go");
	$sth->execute();
	my $count_row = $sth->rows;
	while (my $ref = $sth->fetchrow_hashref())
	{
		my $sth2 = $dbh->prepare( "
			SELECT 
			term.term_type AS term_type,
			term.acc AS acc_child,
			graph_path.relation_distance AS relation_distance,
			rel.acc AS rel_acc,
			term2.term_type AS term_type_child,
			term2.acc AS acc_ancestor
			 FROM
			 term  
			 INNER JOIN graph_path ON (term.id=graph_path.term2_id)
			INNER JOIN term AS rel ON (rel.id=relationship_type_id)
			INNER JOIN term AS term2 ON (term2.id=graph_path.term1_id)
			WHERE
			term2.acc LIKE 'GO:%'
			AND
			graph_path.distance != 0
			AND
			 term.acc= ?
        " );
		$sth2->bind_param( 1, $ref->{'acc'});
		$sth2->execute();
		while (my $ref2 = $sth2->fetchrow_hashref())
		{
		
			print "[$i\/$count_row] getting ontologic relations $ref2->{'acc_child'} ...\n";
			&parseSQL_rel(\$ref2, \$background_knowledge);
		}
		$sth2->finish();
		$i++;
	}
	$sth->finish();
	
return $background_knowledge;
}

open FILEF_WRITE, ">", "bk_F" or die $!;
my $bk_F = &get_BK($FILEF);

print FILEF_WRITE "$bk_F";
close(FILEF_WRITE);

open FILEN_WRITE, ">", "bk_N" or die $!;
my $bk_N = &get_BK($FILEN);

print FILEN_WRITE "$bk_N";
close(FILEN_WRITE);

open FILEREL_WRITE, ">", "bk_rel" or die $!;
my $bk_rel = &get_ontology_relations();

print FILEREL_WRITE "$bk_rel";
close(FILEREL_WRITE);


print "bk_F was created\n";
print "bk_N was created\n";
print "bk_rel was created\n";
$dbh->disconnect();


close($FILEF);
close($FILEN);
 
