:- set(clauselength,4).
:- set(minpos, 3).
:- set(nodes, 25000).
:- set(i, 4).
:- set(depth, 14).
:- set(noise, 2).

%head
:- mode(2, expressed(+gene_product_id)).
%body

:- mode(2, expressed(#gene_id)).

:- mode(2, process(+gene_product_id, -go_id)).
:- mode(2, component(+gene_product_id, -go_id)).
:- mode(2, function(+gene_product_id, -go_id)).

:- mode(2, process(+gene_product_id, #go_id_const)).
:- mode(2, component(+gene_product_id, #go_id_const)).
:- mode(2, function(+gene_product_id, #go_id_const)).

:- mode(2, is_a(+go_id, -go_id)).
:- mode(2, part_of(+go_id, -go_id)).
:- mode(2, regulates(+go_id, -go_id)).
:- mode(2, positively_regulates(+go_id, -go_id)).
:- mode(2, negatively_regulates(+go_id, -go_id)).

:- mode(2, is_a(+go_id, #go_id_const)).
:- mode(2, part_of(+go_id, #go_id_const)).
:- mode(2, regulates(+go_id, #go_id_const)).
:- mode(2, positively_regulates(+go_id, #go_id_const)).
:- mode(2, negatively_regulates(+go_id, #go_id_const)).


%determination

:- determination(expressed/1, expressed/1).

:- determination(expressed/1, process/2).
:- determination(expressed/1, component/2).
:- determination(expressed/1, function/2).

:- determination(expressed/1, is_a/2).
:- determination(expressed/1, part_of/2).
:- determination(expressed/1, regulates/2).
:- determination(expressed/1, positively_regulates/2).
:- determination(expressed/1, negatively_regulates/2).

%BACKGROUND KNOWLEDGE

%consult
:- consult(bk_F).
:- consult(bk_N).
:- consult(bk_rel).
