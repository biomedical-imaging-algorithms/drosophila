#!/bin/bash
#Skript vytiskne na stdout nastaveni experimentu pro Aleph a zaroven
#vytiskne i vyslednou teorii a presnost

#nazev experimentu
EXPERIMENT=$(echo "drosophilla.b")

#vytiskne na stdout nastaveni experimentu
#jmeno docasneho souboru
FILE=$(date +"%m_%d_%Y_%H_%M_%S")
echo "******************* DROSOPHILLA $FILE *****************"

echo "[settings]"
echo ""
cat $EXPERIMENT

#vytiskne na stdout indukovanou teorii
echo ""
echo "##############################################################################"
echo ""
echo "[theory]"

#spustim Aleph
perl subrun.pl > $FILE
POSTHEORY=$(cat $FILE | egrep -n  'theory' | cut -d ':' -f 1)
LINES=$(wc -l $FILE | cut -d ' ' -f1)
POS=$((LINES-POSTHEORY))

#vypis teorie na stdout
tail -n $POS $FILE
#smazani docasneho souboru
rm -r $FILE
