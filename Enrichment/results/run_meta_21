******************* DROSOPHILLA 11_04_2014_14_20_33 *****************
[settings]

:- set(clauselength,5).
:- set(minpos, 3).
:- set(nodes, 70000).
:- set(i, 15).
:- set(depth, 30).
:- set(noise, 2).

%head
:- mode(1, expressed(+gene_product_id)).
%body

:- mode(4, expressed(#gene_id)).

:- mode(4, process(+gene_product_id, -go_id)).
:- mode(4, component(+gene_product_id, -go_id)).
:- mode(4, function(+gene_product_id, -go_id)).

:- mode(4, process(+gene_product_id, #go_id_const)).
:- mode(4, component(+gene_product_id, #go_id_const)).
:- mode(4, function(+gene_product_id, #go_id_const)).

:- mode(4, is_a(+go_id, -go_id)).
:- mode(4, part_of(+go_id, -go_id)).
:- mode(4, regulates(+go_id, -go_id)).
:- mode(4, positively_regulates(+go_id, -go_id)).
:- mode(4, negatively_regulates(+go_id, -go_id)).

:- mode(4, is_a(+go_id, #go_id_const)).
:- mode(4, part_of(+go_id, #go_id_const)).
:- mode(4, regulates(+go_id, #go_id_const)).
:- mode(4, positively_regulates(+go_id, #go_id_const)).
:- mode(4, negatively_regulates(+go_id, #go_id_const)).


%determination

:- determination(expressed/1, expressed/1).

:- determination(expressed/1, process/2).
:- determination(expressed/1, component/2).
:- determination(expressed/1, function/2).

:- determination(expressed/1, is_a/2).
:- determination(expressed/1, part_of/2).
:- determination(expressed/1, regulates/2).
:- determination(expressed/1, positively_regulates/2).
:- determination(expressed/1, negatively_regulates/2).

%BACKGROUND KNOWLEDGE

%consult
:- consult(bk_F).
:- consult(bk_N).
:- consult(bk_rel).

##############################################################################

[theory]

[Rule 2] [Pos cover = 5 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:1903047'), process(A,C), part_of(C,'GO:0007338').

[Rule 4] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,'GO:0051298'), component(A,B), is_a(B,'GO:0044427').

[Rule 5] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,'GO:0051492').

[Rule 6] [Pos cover = 4 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0007017'), function(A,'GO:0005515').

[Rule 7] [Pos cover = 4 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0016192'), process(A,'GO:0000281'), component(A,'GO:0005886').

[Rule 8] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0008355'), function(A,B), is_a(B,'GO:0032550'), function(A,'GO:0005515').

[Rule 9] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0006260'), component(A,C), part_of(C,'GO:0015630').

[Rule 10] [Pos cover = 5 Neg cover = 1]
expressed(A) :-
   process(A,B), regulates(B,'GO:0008283'), component(A,'GO:0045179').

[Rule 14] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0006974'), component(A,'GO:0005737'), function(A,'GO:0005524').

[Rule 15] [Pos cover = 4 Neg cover = 2]
expressed(A) :-
   process(A,B), negatively_regulates(B,'GO:0007166'), process(A,'GO:0000122'), component(A,'GO:0005634').

[Rule 19] [Pos cover = 5 Neg cover = 2]
expressed(A) :-
   process(A,'GO:0008088').

[Rule 20] [Pos cover = 4 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,'GO:0097485'), process(A,C), positively_regulates(C,'GO:0007416').

[Rule 24] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   function(A,B), is_a(B,'GO:0016247').

[Rule 25] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0051705'), component(A,'GO:0005811'), component(A,'GO:0005783').

[Rule 26] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,'GO:0006464'), part_of(B,C), process(A,'GO:0046331').

[Rule 31] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   component(A,B), is_a(B,'GO:0043229'), function(A,'GO:0035091').

[Rule 34] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   component(A,'GO:0070938').

[Rule 40] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0008104'), component(A,'GO:0032154').

[Rule 45] [Pos cover = 4 Neg cover = 0]
expressed(A) :-
   component(A,B), is_a(B,C), is_a(C,'GO:0044427'), component(A,'GO:0008278').

[Rule 53] [Pos cover = 4 Neg cover = 2]
expressed(A) :-
   process(A,'GO:0015992'), component(A,'GO:0005886').

[Rule 54] [Pos cover = 5 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0016310'), process(A,'GO:0007163').

[Rule 58] [Pos cover = 5 Neg cover = 1]
expressed(A) :-
   process(A,'GO:0007612').

[Rule 59] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,C), part_of(C,'GO:0048731'), function(A,'GO:0000287').

[Rule 61] [Pos cover = 7 Neg cover = 0]
expressed(A) :-
   process(A,B), part_of(B,'GO:0051663'), component(A,C), is_a(C,'GO:0044444').

[Rule 64] [Pos cover = 4 Neg cover = 2]
expressed(A) :-
   process(A,B), is_a(B,'GO:0008283'), process(A,C), is_a(C,'GO:0098722').

[Rule 70] [Pos cover = 4 Neg cover = 0]
expressed(A) :-
   process(A,B), positively_regulates(B,'GO:0044237'), process(A,C), regulates(C,'GO:0016192').

[Rule 78] [Pos cover = 7 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0046959'), component(A,B).

[Rule 81] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,'GO:0007610'), process(A,C), part_of(C,'GO:0055002').

[Rule 92] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0003006'), part_of(B,'GO:0007276'), process(A,'GO:0055085').

[Rule 104] [Pos cover = 4 Neg cover = 1]
expressed(A) :-
   process(A,'GO:0000281'), component(A,B), is_a(B,C), part_of(C,'GO:0015630').

[Rule 114] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), regulates(B,'GO:0019538'), process(A,'GO:0007274').

[Rule 116] [Pos cover = 5 Neg cover = 2]
expressed(A) :-
   process(A,'GO:0007165'), function(A,B), is_a(B,'GO:0019899').

[Rule 135] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0006351'), process(A,'GO:0006357'), function(A,'GO:0005515').

[Rule 136] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0006909'), function(A,B), is_a(B,'GO:0000975'), function(A,'GO:0043565').

[Rule 140] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,'GO:0007417'), function(A,B), is_a(B,'GO:0005085').

[Rule 142] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0007362'), component(A,'GO:0005737').

[Rule 158] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), part_of(B,'GO:0042063'), process(A,'GO:0006357').

[Rule 166] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0003002'), process(A,'GO:0001751'), component(A,'GO:0005634').

[Rule 180] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,'GO:0007167'), component(A,'GO:0005886'), function(A,'GO:0004674').

[Rule 192] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0006325'), process(A,'GO:0045944').

[Rule 203] [Pos cover = 4 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0008298'), process(A,'GO:0007310'), function(A,B).

[Rule 206] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,'GO:1902580'), process(A,C), is_a(C,'GO:0043254').

[Rule 216] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), part_of(B,'GO:0007389'), function(A,'GO:0051015').

[Rule 232] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,'GO:0007005'), function(A,B), is_a(B,'GO:0046872').

[Rule 233] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,'GO:0000122'), function(A,'GO:0003676').

[Rule 240] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), part_of(B,'GO:0034613'), function(A,'GO:0008270').

[Rule 243] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,C), is_a(C,'GO:0034613'), process(A,'GO:0046331').

[Rule 274] [Pos cover = 5 Neg cover = 1]
expressed(A) :-
   process(A,B), part_of(B,'GO:0009653'), process(A,'GO:0006355'), process(A,'GO:0007552').

[Rule 291] [Pos cover = 4 Neg cover = 1]
expressed(A) :-
   component(A,'GO:0005811'), component(A,'GO:0012505'), function(A,'GO:0003674').

[Rule 338] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), part_of(B,'GO:0048513'), process(A,'GO:0006355'), function(A,'GO:0003676').

[Rule 393] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   component(A,'GO:0005789'), component(A,'GO:0045169').

[Rule 423] [Pos cover = 5 Neg cover = 0]
expressed(A) :-
   process(A,B), part_of(B,'GO:0022607'), component(A,'GO:0005634'), function(A,'GO:0003677').

[Training set performance]
            Actual
        +          -  
     + 179         33         212 
Pred 
     - 541        6826        7367 

       720        6859        7579 

Accuracy = 0.924264414830453
[Training set summary] [[179,33,541,6826]]
[time taken] [13182.212]
[total clauses constructed] [7543404]
