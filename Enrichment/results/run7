******************* DROSOPHILLA 11_01_2014_21_34_18 *****************
[settings]

:- set(clauselength,5).
:- set(minpos, 3).
:- set(nodes, 30000).
:- set(i, 5).
:- set(depth, 15).
:- set(noise, 2).

%head
:- mode(1, expressed(+gene_product_id)).
%body
:- mode(2, process(+gene_product_id, -go_id)).
:- mode(2, component(+gene_product_id, -go_id)).
:- mode(2, function(+gene_product_id, -go_id)).

:- mode(2, process(+gene_product_id, #go_id_const)).
:- mode(2, component(+gene_product_id, #go_id_const)).
:- mode(2, function(+gene_product_id, #go_id_const)).

:- mode(2, is_a(+go_id, -go_id)).
:- mode(2, part_of(+go_id, -go_id)).
:- mode(2, positively_regulates(+go_id, -go_id)).
:- mode(2, negatively_regulates(+go_id, -go_id)).

:- mode(2, is_a(+go_id, #go_id_const)).
:- mode(2, part_of(+go_id, #go_id_const)).
:- mode(2, regulates(+go_id, #go_id_const)).
:- mode(2, positively_regulates(+go_id, #go_id_const)).
:- mode(2, negatively_regulates(+go_id, #go_id_const)).


%determination
:- determination(expressed/1, process/2).
:- determination(expressed/1, component/2).
:- determination(expressed/1, function/2).

:- determination(expressed/1, is_a/2).
:- determination(expressed/1, part_of/2).
:- determination(expressed/1, regulates/2).
:- determination(expressed/1, positively_regulates/2).
:- determination(expressed/1, negatively_regulates/2).

%BACKGROUND KNOWLEDGE

%consult
:- consult(bk_F).
:- consult(bk_N).
:- consult(bk_rel).

##############################################################################

[theory]

[Rule 5] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,C), part_of(C,'GO:0009653'), process(A,'GO:0007266').

[Rule 6] [Pos cover = 4 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0007017'), function(A,'GO:0005515').

[Rule 7] [Pos cover = 4 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0008360'), function(A,B), is_a(B,'GO:0005085').

[Rule 8] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0009583'), component(A,'GO:0045202').

[Rule 11] [Pos cover = 5 Neg cover = 1]
expressed(A) :-
   process(A,B), regulates(B,'GO:0008283'), component(A,'GO:0045179').

[Rule 16] [Pos cover = 4 Neg cover = 2]
expressed(A) :-
   process(A,B), negatively_regulates(B,'GO:0007166'), process(A,'GO:0000122'), component(A,'GO:0005634').

[Rule 20] [Pos cover = 5 Neg cover = 2]
expressed(A) :-
   process(A,'GO:0008088').

[Rule 24] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   function(A,B), is_a(B,'GO:0016247').

[Rule 25] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0051705'), component(A,'GO:0005811'), component(A,'GO:0005783').

[Rule 31] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   component(A,B), is_a(B,'GO:0043229'), function(A,'GO:0035091').

[Rule 44] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,'GO:0000280'), component(A,'GO:0005813'), function(A,'GO:0004674').

[Rule 47] [Pos cover = 4 Neg cover = 0]
expressed(A) :-
   component(A,B), is_a(B,C), is_a(C,'GO:0044427'), component(A,'GO:0008278').

[Rule 56] [Pos cover = 4 Neg cover = 2]
expressed(A) :-
   process(A,'GO:0016325'), component(A,B), is_a(B,'GO:0043231').

[Rule 60] [Pos cover = 5 Neg cover = 1]
expressed(A) :-
   process(A,'GO:0007612').

[Rule 65] [Pos cover = 4 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,C), is_a(C,'GO:0022412'), process(A,'GO:0007405').

[Rule 72] [Pos cover = 4 Neg cover = 2]
expressed(A) :-
   component(A,B), part_of(B,'GO:0009986'), component(A,C), is_a(C,'GO:0044421').

[Rule 73] [Pos cover = 4 Neg cover = 2]
expressed(A) :-
   process(A,B), is_a(B,'GO:0009790'), component(A,'GO:0005886').

[Rule 79] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   function(A,'GO:0004690').

[Rule 82] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,C), is_a(C,'GO:0007610'), function(A,'GO:0003779').

[Rule 138] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0006351'), process(A,'GO:0006367'), function(A,'GO:0005515').

[Rule 148] [Pos cover = 4 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0007476'), function(A,B), is_a(B,'GO:0016791').

[Rule 189] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,'GO:0007167'), component(A,'GO:0005886'), function(A,'GO:0004674').

[Rule 209] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0030154'), process(A,'GO:0016360'), function(A,'GO:0005515').

[Rule 215] [Pos cover = 4 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0008298'), process(A,'GO:0007310'), function(A,B).

[Rule 218] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,C), part_of(C,'GO:0006997'), component(A,'GO:0005652').

[Rule 228] [Pos cover = 4 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0007315'), function(A,'GO:0003779').

[Rule 254] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), part_of(B,'GO:0034613'), function(A,'GO:0008270').

[Rule 259] [Pos cover = 5 Neg cover = 2]
expressed(A) :-
   function(A,B), is_a(B,'GO:0005488'), function(A,'GO:0004252').

[Rule 285] [Pos cover = 3 Neg cover = 1]
expressed(A) :-
   process(A,B), is_a(B,'GO:0070887'), function(A,'GO:0046872').

[Rule 315] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,C), is_a(C,'GO:0044419'), function(A,D).

[Rule 316] [Pos cover = 4 Neg cover = 2]
expressed(A) :-
   component(A,'GO:0005811'), function(A,'GO:0003674').

[Rule 429] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,'GO:0046331'), component(A,'GO:0005789').

[Rule 461] [Pos cover = 3 Neg cover = 0]
expressed(A) :-
   process(A,B), is_a(B,'GO:0034728'), component(A,'GO:0005634'), function(A,'GO:0003677').

[Training set performance]
            Actual
        +          -  
     + 115         26         141 
Pred 
     - 605        6833        7438 

       720        6859        7579 

Accuracy = 0.916743633724766
[Training set summary] [[115,26,605,6833]]
[time taken] [981.526]
[total clauses constructed] [1144020]
