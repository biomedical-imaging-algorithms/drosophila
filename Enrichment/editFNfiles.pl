#!/usr/bin/perl
use strict;

open my $FILEF, "<", "drosophilla_unedited.f" or die $!;
open my $FILEN, "<", "drosophilla_unedited.n" or die $!;

sub getData
{
	my($file) = @_;
	my $data;
	while(my $line = <$file>)
	{
		if($line =~ /expressed\((FB[a-zA-Z0-9]+)\)/)
		{	
			$data .= "expressed('$1').\n";
		}
	}
	return $data;
}

open FILEWF, ">", "drosophilla.f" or die $!;
open FILEWN, ">", "drosophilla.n" or die $!;


my $data = &getData($FILEF);
print FILEWF "$data";
print "drosophilla.f was created!\n";


my $data = &getData($FILEN);
print FILEWN "$data";
print "drosophilla.n was created!\n";

close(FILEF);
close(FILEN);
close(FILEWF);
close(FILEWN);


