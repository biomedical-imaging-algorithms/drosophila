#!/bin/bash
cd res
FILE=$(date +"%m_%d_%Y_%H_%M_%S")
mkdir $FILE
cd ..
cp * res/$FILE
bash res/$FILE/run.sh
